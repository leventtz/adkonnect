package sandbox

import java.util.UUID
import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.routing.DefaultResizer
import akka.routing.RoundRobinRouter
import com.sponsoracall.db.PostgresDBEngine




class InsertActor extends Actor {
  
  def receive = {
    case i:Int=> insertId(i)
  }
    def insertId(i:Int) {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val id=UUID.randomUUID().toString().replaceAll("-", "")
          println(id+" "+id.length())
          val ps = session.prepareStatement("INSERT INTO test(id) VALUES (?)")
          ps.setString(1, id)
          ps.execute()
        } catch {
          case t: Throwable => {
            t.printStackTrace();
            System.exit(-1)
          }
        }
    }
  }
    
  def addAccount(i:Int) {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("insert into Account(id,accounttypeid,companyName,firstName,middleName,lastname,address1,address2,city,postalcode,country,email,phone1,phone2,password) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
          ps.setString(1, UUID.randomUUID().toString())
          ps.setInt(2, 1)
          ps.setString(3, "Dimwire "+i)
          ps.setString(4, "levent")
          ps.setString(5, "")
          ps.setString(6, "Tinaz")
          ps.setString(7, "3888 S.Lake Dr.")
          ps.setString(8, "")
          ps.setString(9, "Milwaukee")
          ps.setString(10, "53235")
          ps.setInt(11, 1)
          ps.setString(12, "levent@msn.com "+i)
          ps.setString(13, i+"2012806878")
          ps.setString(14, "")
          ps.setString(15, "12121212")
          ps.execute()
        } catch {
          case t: Throwable => {
        	  t.printStackTrace()
          }
        }
    }
  } 

}

object TestMain extends App {

  
  
 val roundRobinRouter = ActorSystem.create().actorOf(Props[InsertActor].withRouter(RoundRobinRouter(resizer = Some(DefaultResizer(lowerBound = 4, upperBound = 64)))), "router")
 var i=1000
 while(true) {
   Thread.sleep(100)
   roundRobinRouter ! i
   i +=1
 }

 
 
 
 
}