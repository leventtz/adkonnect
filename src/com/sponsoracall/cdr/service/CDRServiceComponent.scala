package com.sponsoracall.cdr.service

import com.sponsoracall.cdr.integration.CDRIntegration


trait CDRServiceComponent {
	val integration:CDRIntegration
	val service:CDRService
}