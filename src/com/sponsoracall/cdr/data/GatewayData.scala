package com.sponsoracall.cdr.data

case class GatewayData(id:Int,ip:String,did:String,providerId:String,name:String,hitcount:Int,active:Boolean)  {
    require(name!=null && ip !=null, "Missing required fields")
  
    import play.api.libs.json._
  	val jsonWriter=Json.writes[GatewayData]  
    override def toString={
    jsonWriter.writes(this).toString
  }  
}