package com.sponsoracall.cdr.data

case class CountryData(id:Int,name:String,countryCode:String,countryExitCode:String) {
  require(id>0,"Missing Id")
  require(name!=null && name.length>0,"Missing Name");

  import play.api.libs.json._
  val jsonWriter=Json.writes[CountryData]
  override def toString={
    jsonWriter.writes(this).toString
  }  
 
}