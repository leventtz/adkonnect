package com.sponsoracall.cdr.contract

object CreateDialingCodeContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[CreateDialingCodeContract]
  def parse(value: JsValue): Option[CreateDialingCodeContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class CreateDialingCodeContract(da:String,destinationId:Int) {
  require(da!=null && da.length>0,"da Required")
}
