package com.sponsoracall.cdr.contract

object UpdateCountryContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[UpdateCountryContract]
  def parse(value: JsValue): Option[UpdateCountryContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class UpdateCountryContract(id:Int,name:String) {
  require(id>0,"Id Required")
  require(name!=null && name.length>0,"Country Name Required")
}