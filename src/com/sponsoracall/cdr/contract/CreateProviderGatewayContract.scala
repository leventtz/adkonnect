package com.sponsoracall.cdr.contract


object CreateProviderGatewayContract {
  
  import play.api.libs.json._
  
  val jsonReader = Json.reads[CreateProviderGatewayContract]
  def parse(value: JsValue): Option[CreateProviderGatewayContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}


case class CreateProviderGatewayContract(endpointip:String,providerid:String,prefix:String,username:String,password:String,active:Boolean) {
  require(endpointip!=null && endpointip.length()>0,"endpointip required")
  require(providerid!=null && providerid.length()>0,"providerid required")
}