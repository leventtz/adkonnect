package com.sponsoracall.cdr.contract

object CreateCountryContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[CreateCountryContract]
  def parse(value: JsValue): Option[CreateCountryContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class CreateCountryContract(name:String) {
  require(name!=null && name.length>0,"Country Name Required")
}