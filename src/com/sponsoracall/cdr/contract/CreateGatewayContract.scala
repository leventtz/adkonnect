package com.sponsoracall.cdr.contract


object CreateGatewayContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[CreateGatewayContract]
  def parse(value: JsValue): Option[CreateGatewayContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}


case class CreateGatewayContract(ip:String,did:String,providerId:String,name:String) {
  require(ip!=null && ip.length()>0,"IP required")
  require(did!=null && did.length()>0,"DID required")
  require(name!=null && name.length()>0,"name required")
  require(providerId!=null && providerId.length()>0,"provider required")
}