package com.sponsoracall.cdr.integration

import scala.util.Try
import com.sponsoracall.ad.data.PickAdData
import com.sponsoracall.cdr.contract.CDRSummaryContract
import com.sponsoracall.cdr.data.CDRSummaryByHourData
import com.sponsoracall.cdr.contract.DeleteDialingCodeContract
import com.sponsoracall.cdr.data.DestinationData
import com.sponsoracall.cdr.contract.CreateGatewayContract
import com.sponsoracall.cdr.contract.CreateCountryContract
import com.sponsoracall.cdr.contract.CreateProviderGatewayContract
import com.sponsoracall.cdr.contract.UpdateDestinationContract
import com.sponsoracall.cdr.contract.CreateCDRContract
import com.sponsoracall.cdr.contract.UpdateCDRContract
import com.sponsoracall.cdr.data.DIDData
import com.sponsoracall.cdr.contract.RetrieveDialingCodeByDestinationContract
import com.sponsoracall.cdr.contract.CreateDestinationContract
import com.sponsoracall.cdr.contract.UpdateGatewayContract
import com.sponsoracall.cdr.contract.UpdateProviderGatewayContract
import com.sponsoracall.cdr.data.ProviderGatewayData
import com.sponsoracall.cdr.contract.AddDIDContract
import com.sponsoracall.cdr.data.CDRSummaryData
import com.sponsoracall.cdr.data.GatewayData
import com.sponsoracall.cdr.contract.UpdateCountryContract
import com.sponsoracall.cdr.contract.CreateDialingCodeContract
import com.sponsoracall.cdr.data.BestMatchTemplateData
import com.sponsoracall.cdr.data.CountryData
import com.sponsoracall.cdr.data.CDRAccountData
import com.sponsoracall.cdr.data.DialingCodeData
import com.sponsoracall.cdr.contract.RetrieveDestinationsByCountryContract
import com.sponsoracall.cdr.data.RouteData
import com.sponsoracall.cdr.contract.RouteContract
import com.sponsoracall.cdr.contract.UpdateCDRSummaryContractTable
import com.sponsoracall.cdr.data.LocationData


trait CDRIntegration {
    def createCountry(contract:CreateCountryContract):Try[Option[CountryData]]
    def updateCountry(contract:UpdateCountryContract):Try[Unit]
    def retrieveCountries:Try[List[CountryData]]
    
    
    def createDestination(contract:CreateDestinationContract):Try[Option[DestinationData]] 
    def updateDestination(contract:UpdateDestinationContract):Try[Unit] 
	def retriveDestinationsByCountry(contract:RetrieveDestinationsByCountryContract):Try[List[DestinationData]] 
    def retriveDestinations:Try[List[DestinationData]]
    def retriveIncomingDestinations:Try[List[DestinationData]]
    def retrieveBestMatchTemplate(number: String): Try[Option[BestMatchTemplateData]]
    
    
    def createDialingCode(contract:CreateDialingCodeContract):Try[Unit] 
	def retrieveDialingCodeByDestination(contract:RetrieveDialingCodeByDestinationContract):Try[List[DialingCodeData]]     
    def deleteDialingCode(contract:DeleteDialingCodeContract):Try[Unit]
    
    def createGateway(contract:CreateGatewayContract):Try[Unit]
    def updateGateway(contract:UpdateGatewayContract):Try[Unit]
    def deleteGateway(id:Int):Try[Unit]
    def toggleGateway(id:Int):Try[Unit]
    def listGateways():Try[List[GatewayData]]
    
    def incrementDIDHitCount(did:String)
    def decrementDIDHitCount(did:String)
    
    def resetGatewayHitCount(gatewayId:Int)

    def addDID(contract:AddDIDContract):Try[Unit]
    def deleteDID(DID:String):Try[Unit]       
    def listDIDs(): Try[List[DIDData]]
    def listDIDs(gatewayId:Int): Try[List[DIDData]] 
    def listDIDsByProvider(providerId: String): Try[List[DIDData]]

    def updateCDRSummaryTable(contract: UpdateCDRSummaryContractTable): Try[Unit]
    
    
    def retrieveCDRSummary(contract:CDRSummaryContract):Try[CDRSummaryData]
    def retrieveCDRSummaryByHour(contract:CDRSummaryContract):Try[List[CDRSummaryByHourData]]
    
    def retrieveBestMatchDestination(number:String):Try[Option[(Int,String)]]
    def retrieveBestMatchDestinationCountryCode(number:String):Try[Option[String]]
    def retrieveBestMatchDestinationAll(number:String):Try[List[Int]]
    
    def getDID() : Try[Option[String]]
    
    def listProviderGateways(): Try[List[ProviderGatewayData]]
    def deleteProviderGateway(id:Int): Try[Unit]
    def updateProviderGateway(contract: UpdateProviderGatewayContract): Try[Unit]
    def createProviderGateway(contract: CreateProviderGatewayContract): Try[Unit]
    def toggleProviderGateway(id: Int): Try[Unit]
    
    def retrieveRoute(destinationId:Int):Try[Option[ProviderGatewayData]]
    

    
    def updateCDR(contract:UpdateCDRContract)
    def createCDR(contract:CreateCDRContract)
    
    def getAccountCalls(accountId:String):Try[List[Option[CDRAccountData]]]
    
    def listRoutes(destinationId:Int):Try[List[RouteData]]    
    def addRoute(contract: RouteContract): Try[Unit]
    def deleteRoute(contract: RouteContract): Try[Unit]
    def toggleRoute(contract: RouteContract): Try[Unit]
    
    def retrieveLocations(searchString:String): Try[List[LocationData]]
    def retrieveLocationsFromZip(zipCode:String): Option[List[Int]]
    
}