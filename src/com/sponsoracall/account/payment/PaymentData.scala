package com.sponsoracall.account.payment


case class PaymentData(responseCode:String,statusCodeString:String,authCode:String) {
  import play.api.libs.json._
  implicit val jsonLocationWriter=Json.writes[PaymentData]
  val jsonWriter=Json.writes[PaymentData]
  override def toString={
    jsonWriter.writes(this).toString
  }
  
}