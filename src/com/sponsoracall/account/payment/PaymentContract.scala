package com.sponsoracall.account.payment
object PaymentContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[PaymentContract]
  def parse(value: JsValue): Option[PaymentContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => {
        t.printStackTrace()
        None
      }
    }
  }
}

case class PaymentContract(cvv:String,cardNum:String,expDate:String,amount:BigDecimal,description:String,firstName:String,lastName:String,address:String,state:String,zip:String,phone:String,country:String) {
  require(cardNum!=null && !cardNum.isEmpty(),"Invalid cardNum")
  require(cvv!=null && !cvv.isEmpty(),"Invalid CVV")
  require(expDate!=null && !expDate.isEmpty(),"Invalid expDate")
  require(amount!=null && amount>0,"Invalid Amount")  
  require(description!=null && !description.isEmpty(),"Invalid description")  
  require(firstName!=null && !firstName.isEmpty(),"Invalid firstName")  
  require(lastName!=null && !lastName.isEmpty(),"Invalid lastName")  
  require(address!=null && !address.isEmpty(),"Invalid address")  
  require(state!=null && !state.isEmpty(),"Invalid state")  
  require(zip!=null && !zip.isEmpty(),"Invalid zip")  
  require(phone!=null && !phone.isEmpty(),"Invalid zip")  
}