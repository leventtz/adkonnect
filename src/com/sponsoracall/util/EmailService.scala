package com.sponsoracall.util

import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient
import com.amazonaws.services.simpleemail.model.Destination
import com.amazonaws.services.simpleemail.model.SendEmailRequest
import com.amazonaws.services.simpleemail.model.Message
import com.amazonaws.services.simpleemail.model.Content
import com.amazonaws.services.simpleemail.model.Body
import com.amazonaws.regions._
import scala.util.Try
import scala.util.Failure
import scala.util.Success


object EmailService {
	//val cr = new BasicAWSCredentials("AKIAJA4CGCWS72ZGPM2A", "AkZzHLumjtRMLi9Bc8nC6C4fz3KFsxRu3DmVt5gddljQ");
	
	private val cr = new BasicAWSCredentials(GlobalSettings.awsAccessKey, GlobalSettings.awsSecretKey);
	
	def sendEmail(from:String,to:String,emailSubject:String,emailBody:String):Try[Unit] = {
	  try {
	    val destination = new Destination().withToAddresses(to)
        val subject = new Content().withData(emailSubject)

        val body = new Body().withHtml(new Content().withData(emailBody))
        val message = new Message().withSubject(subject).withBody(body)
        
        
        val request = new SendEmailRequest().withSource(from).withDestination(destination).withMessage(message).withReplyToAddresses(GlobalSettings.noReplyAddress)  
	    
        val client = new AmazonSimpleEmailServiceClient(cr)
        val REGION = Region.getRegion(Regions.US_EAST_1)
        client.setRegion(REGION)
        client.sendEmail(request)
	    Success(())
	  }catch {
	    case t:Throwable => Failure(t)
	  }
	}
}