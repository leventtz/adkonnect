package com.sponsoracall.gatewayaccount.contract

import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.JsValue
import java.util.UUID

object AccountContract {
  val validEmailRegex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
  val validPasswordRegex = "^.*(?=.{6,})(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).*$"
                           
}

object AddAccountContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[AddAccountContract]
  def parse(value: JsValue): Option[AddAccountContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => t.printStackTrace();None
    }
  }
}


case class AddAccountContract(
  id:String,  
  accountType: String,
  companyName: String,
  firstName: String,
  middleName: String,
  lastName: String,
  address1: String,
  address2: String,
  city: String,
  state: String,
  postalCode: String,
  country: String,
  email: String,
  phone1: String,
  phone2: String,
  password: String,
  birthday:String,
  gender:String,
  active: Boolean = false) {
  require(accountType != null && accountType.length() > 0, "Missing accountType")
  require(firstName != null && firstName.length() > 0, "Missing FirstName")
  require(lastName != null && lastName.length() > 0, "Missing Last Name")
  require(address1 != null && address1.length() > 0, "Missing Address1")
  require(city != null && city.length() > 0, "Missing City")
  require(country != null && country.length() > 0, "Missing country")
  require(phone1 != null && phone1.length() > 0, "Invalid Phone Number")
  require(email != null && email.length() > 0, "Missing email") 
  require(email.matches(AccountContract.validEmailRegex), "Invalid Email")
  
  import play.api.libs.json._
  val jsonWriter = Json.writes[AddAccountContract]
  override def toString = {
    jsonWriter.writes(this).toString
  }
  
}




