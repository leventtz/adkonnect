package com.sponsoracall.gatewayaccount.contract

import java.util.UUID
import java.sql.Date




case class AddUserContract(
  email: String,
  password: String,
  firstName: String="",
  lastName: String="",
  active: Boolean = true) {
  val id: String = UUID.randomUUID().toString().replaceAll("-", "")
  require(email != null && email.length() > 0, "Missing email") 
  require(email.matches(AccountContract.validEmailRegex), "Invalid Email")
}