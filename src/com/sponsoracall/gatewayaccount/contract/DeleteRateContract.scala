package com.sponsoracall.gatewayaccount.contract

object DeleteRateContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[DeleteRateContract]
  def parse(value: JsValue): Option[DeleteRateContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}
case class DeleteRateContract(id:Int) {
  require(id>0,"Missing Id")
}