package com.sponsoracall.gatewayaccount.contract

object UpdateAccountMediaContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[UpdateAccountMediaContract]
  def parse(value: JsValue): Option[UpdateAccountMediaContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class UpdateAccountMediaContract(id:Int,description:String,mediaTypeId:String,approved:Boolean=false) {
	require(id>0,"id required")
	require(description!=null && !description.isEmpty(),"Invalid Description")
}