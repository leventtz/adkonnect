package com.sponsoracall.gatewayaccount.contract
object ResetPasswordContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[ResetPasswordContract]
  def parse(value: JsValue): ResetPasswordContract = {
    jsonReader.reads(value).get
  }
}
case class ResetPasswordContract(emailphone:String,newPassword:String) {
  require(emailphone!=null && newPassword.length>0,"Invalid emailphone")

}