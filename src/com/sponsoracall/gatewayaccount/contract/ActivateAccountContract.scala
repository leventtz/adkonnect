package com.sponsoracall.gatewayaccount.contract
object ActivateAccountContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[ActivateAccountContract]
  def parse(jsonString: String): ActivateAccountContract = {
    jsonReader.reads(Json.parse(jsonString)).get
  }
}
case class ActivateAccountContract(accountId:String) {
	  require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
}