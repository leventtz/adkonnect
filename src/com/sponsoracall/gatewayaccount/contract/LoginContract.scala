package com.sponsoracall.gatewayaccount.contract
object LoginContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[LoginContract]
  def parse(value: JsValue): Option[LoginContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => {
        None
      }
    }
  }
}

case class LoginContract(emailPhone:String,password:String) {
  require(emailPhone != null && emailPhone.length() > 0, "Missing emailPhone")
  require(password != null && password.length() > 0, "Missing Password")
}