package com.sponsoracall.gatewayaccount.contract

object CreateTemplateContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[CreateTemplateContract]
  def parse(value: JsValue): Option[CreateTemplateContract] = {
    try {
      Some(jsonReader.reads(value).get) 
    } catch {
      case t: Throwable => None
    }
  }
}

case class CreateTemplateContract(name:String,description:String,mediaId:Int,maxDuration:Int,templateDefaultQty:Int,templateUnitPrice:BigDecimal,templatePromptCost:BigDecimal,byPassSwitch:Boolean) {
  require(name!=null && !name.isEmpty(),"name required")
  require(description!=null && !description.isEmpty(),"description required")
}