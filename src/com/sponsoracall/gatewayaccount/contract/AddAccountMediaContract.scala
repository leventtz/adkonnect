package com.sponsoracall.gatewayaccount.contract

object AddAccountMediaContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[AddAccountMediaContract]
  def parse(value: JsValue): Option[AddAccountMediaContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}


case class AddAccountMediaContract(accountId:String,description:String,mediaTypeId:String,approved:Boolean=false) {
    require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
	require(description!=null && !description.isEmpty(),"Invalid Description")
}