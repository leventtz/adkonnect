package com.sponsoracall.gatewayaccount.service

import scala.util.Try
import scala.util.Success
import java.util.UUID
import redis.clients.jedis.Jedis
import com.sponsoracall.util.EmailService
import com.sponsoracall.util.GlobalSettings
import scala.util.Failure
import com.sponsoracall.gatewayaccount.contract._
import com.sponsoracall.gatewayaccount.data._
import com.sponsoracall.gatewayaccount.integration._

object GatewayAccountServiceRegistry extends GatewayAccountServiceComponent with GatewayAccountIntegrationComponent {
  val integration = new GatewayAccountIntegrationImpl
  val service: GatewayAccountService = new GatewayAccountService() {
    def deleteAccount(contract: DeleteAccountContract): Try[Unit] = integration.deleteAccount(contract)

    def addAccount(account: AddAccountContract): Try[Option[String]] = integration.addAccount(account)
    def addMobileUserAccount(contract: AddAccountContract): Try[Option[String]] = integration.addMobileUserAccount(contract)
    def updateAccount(account: UpdateAccountContract): Try[Unit] = integration.updateAccount(account)

    def activateAccount(contract: ActivateAccountContract): Try[Unit] = integration.activateAccount(contract)
    def login(contract: LoginContract): Try[Option[AccountData]] = integration.login(contract)
    def loginMobileUser(contract: LoginContract): Try[Option[AccountData]] = integration.loginMobileUser(contract)

    def addUser(contract: AddUserContract): Try[Option[String]] = integration.addUser(contract)
    def updateUser(contract: UpdateUserContract): Try[Unit] = integration.updateUser(contract)
    def toggleUser(id: String): Try[Unit] = integration.toggleUser(id)
    def deleteUser(id: String): Try[Unit] = integration.deleteUser(id)
    def loginUser(contract: LoginContract): Try[Option[String]] = integration.loginUser(contract)
    def listTemplates(searchString: Option[String]): Try[List[AdTemplateData]] = integration.listTemplates(searchString: Option[String])
    def addAccountMedia(contract: AddAccountMediaContract): Try[Option[AccountMediaData]] = integration.addAccountMedia(contract)
    def updateAccountMedia(contract: UpdateAccountMediaContract): Try[Unit] = integration.updateAccountMedia(contract)
    def deleteAccountMedia(contract: DeleteAccountMediaContract): Try[Unit] = integration.deleteAccountMedia(contract)
    def listAccountMedia(contract: ListAccountMediaContract): Try[List[AccountMediaData]] = integration.listAccountMedia(contract)
    def listUnApprovedAccountMedia(): Try[List[AccountMediaData]] = integration.listUnApprovedAccountMedia()
    def retrieveAccountMedia(contract: RetrieveAccountMediaContract): Try[Option[AccountMediaData]] = integration.retrieveAccountMedia(contract)
    def updateAccountMediaLocation(contract: UpdateMediaLocationContract) = integration.updateAccountMediaLocation(contract)
    def listAllTemplates(searchString: Option[String]): Try[List[AdTemplateData]] = integration.listAllTemplates(searchString)
    def toggleTemplate(id: Int): Try[Unit] = integration.toggleTemplate(id)
    def toggleBypassSwitch(id: Int): Try[Unit] = integration.toggleBypassSwitch(id)
    def deleteTemplate(id: Int): Try[Unit] = integration.deleteTemplate(id)

    def addRate(contract: RateContract): Try[Option[RateData]] = integration.addRate(contract)
    def updateRate(contract: UpdateRateContract): Try[Unit] = integration.updateRate(contract)
    def deleteRate(contract: DeleteRateContract): Try[Unit] = integration.deleteRate(contract)
    def retrieveRate(id: Int): Try[Option[RateData]] = integration.retrieveRate(id)
    def listRateByDestination(destinationId: Int): Try[List[RateData]] = integration.listRateByDestination(destinationId)
    def listRateByAccount(contract: ListRateByAccountContract): Try[List[RateData]] = integration.listRateByAccount(contract)
    def retriveBuyRateByDestinationProvider(destinationId: Int, providerId: String): Try[Option[RateData]] = integration.retriveBuyRateByDestinationProvider(destinationId, providerId): Try[Option[RateData]]

    def listActiveProviders(): Try[List[AccountData]] = integration.listActiveProviders();
    def listAllProviders(): Try[List[AccountData]] = integration.listAllProviders();
    def toogleAccount(id: String): Try[Unit] = integration.toogleAccount(id)

    def listTemplateDA(adId: Int): Try[List[TemplateDAData]] = integration.listTemplateDA(adId)
    def listTemplateDAWithDestination(adId: Int): Try[List[ListTemplateDAData]] = integration.listTemplateDAWithDestination(adId)
    def addTemplateDa(contract: TemplateDAData): Try[Unit] = integration.addTemplateDa(contract)
    def removeTemplateDa(adId: Int, destinationId: Int): Try[Unit] = integration.removeTemplateDa(adId, destinationId)
    def updateTemplate(contract: UpdateTemplateContract): Try[Unit] = integration.updateTemplate(contract)
    def createTemplate(contract: CreateTemplateContract): Try[Option[Int]] = integration.createTemplate(contract)

    def retrieveSellRateByDestination(destinationId: Int): Try[Option[RateData]] = integration.retrieveSellRateByDestination(destinationId)

    def register(registerContract: RegisterContract): Try[Option[String]] = {
      val contract = AddAccountContract(UUID.randomUUID().toString().replaceAll("-", ""), "U", "", registerContract.firstName, "", registerContract.lastName, "na", "", "na", "", "", registerContract.country, registerContract.email, registerContract.phoneNumber, "", registerContract.password, registerContract.birthDay, registerContract.gender, false)
      addAccount(contract)
    }

    def registerMobileUser(registerContract: RegisterContract): Try[Option[String]] = {
      val contract = AddAccountContract(registerContract.deviceId + "-" + registerContract.phoneNumber, "U", "", registerContract.firstName, "", registerContract.lastName, "na", "", "na", "", "", registerContract.country, registerContract.email, registerContract.phoneNumber, "", registerContract.password, registerContract.birthDay, registerContract.gender, false)
      this.addMobileUserAccount(contract)
    }

  }
}