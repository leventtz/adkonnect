package com.sponsoracall.gatewayaccount.data

import play.api.libs.functional.syntax.toFunctionalBuilderOps

case class AdTemplateData(id:Int,name:String,description:String,mediaLocation:String,maxDuration:Int,templateUnitPrice:BigDecimal,templateDefaultQty:Int,templatePromptCost:BigDecimal,bypassSwitch:Boolean,active:Boolean) {
  require(name!=null,"Null Template Name")
  require(description!=null,"Null Template Description")
  require(mediaLocation!=null,"Null Template mediaLocation")
    import play.api.libs.json._
  val jsonWriter=Json.writes[AdTemplateData]
  override def toString={
    jsonWriter.writes(this).toString
  }     
}