package com.sponsoracall.gatewayaccount.data

object AccountData {
  import play.api.libs.json._
  val jsonReader = Json.reads[AccountData]
  def parse(value: String): Option[AccountData] = {
    try {
      Some(jsonReader.reads(Json.parse(value)).get)
    } catch {
      case t: Throwable => {
        t.printStackTrace()
        None
      }
    }
  }
}

case class AccountData(
  id: String,
  accountType: String,
  companyName: String,
  firstName: String,
  middleName: String,
  lastName: String,
  address1: String,
  address2: String,
  city: String,
  state:String,
  postalCode: String,
  country: String,
  email: String,
  phone1: String,
  phone2: String,
  birthday:String,
  gender:String,
  active:Boolean) {
  require(id!=null && id.length()!=0,"Missing AccountId")
  require(accountType != null && accountType.length() > 0, "Missing accountType")
  require(firstName != null && firstName.length() > 0, "Missing FirstName")
  require(lastName != null && lastName.length() > 0, "Missing Last Name")
  require(address1 != null && address1.length() > 0, "Missing Address1")
  require(city != null && city.length() > 0, "Missing City")
  require(country != null && country.length() > 0, "Missing country")
  require(phone1 != null && phone1.length() > 0, "Missing Phone Number")
  require(email != null && email.length() > 0, "Missing email") 

  import play.api.libs.json._
  val jsonWriter=Json.writes[AccountData]
  override def toString={
    jsonWriter.writes(this).toString
  }
  
}