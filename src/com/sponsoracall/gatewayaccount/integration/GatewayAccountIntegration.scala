package com.sponsoracall.gatewayaccount.integration

import scala.util.Try
import java.io.InputStream
import com.sponsoracall.gatewayaccount.contract._
import com.sponsoracall.gatewayaccount.data._

trait GatewayAccountIntegration {
  def deleteAccount(contract: DeleteAccountContract): Try[Unit]
  def addAccount(account: AddAccountContract): Try[Option[String]]
  def addMobileUserAccount(contract: AddAccountContract): Try[Option[String]]
  def updateAccount(account: UpdateAccountContract): Try[Unit]
  def activateAccount(contract: ActivateAccountContract): Try[Unit]
  def login(contract: LoginContract): Try[Option[AccountData]]
  def loginMobileUser(contract: LoginContract): Try[Option[AccountData]]

  def listTemplates(searchString: Option[String]): Try[List[AdTemplateData]]
      
  def addAccountMedia(contract: AddAccountMediaContract): Try[Option[AccountMediaData]]
  def updateAccountMedia(contract: UpdateAccountMediaContract): Try[Unit]
  def deleteAccountMedia(contract: DeleteAccountMediaContract): Try[Unit]
  def listAccountMedia(contract: ListAccountMediaContract): Try[List[AccountMediaData]]
  def listUnApprovedAccountMedia(): Try[List[AccountMediaData]]
  def retrieveAccountMedia(contract: RetrieveAccountMediaContract): Try[Option[AccountMediaData]]
  def updateAccountMediaLocation(contract: UpdateMediaLocationContract)

  def listAllTemplates(searchString: Option[String]): Try[List[AdTemplateData]]

  def addRate(contract: RateContract): Try[Option[RateData]]
  def updateRate(contract: UpdateRateContract): Try[Unit]
  def deleteRate(contract: DeleteRateContract): Try[Unit]
  def retrieveRate(id: Int): Try[Option[RateData]]
  def listRateByDestination(destinationId: Int): Try[List[RateData]]
  def listRateByAccount(contract: ListRateByAccountContract): Try[List[RateData]]
  def retriveBuyRateByDestinationProvider(destinationId: Int, providerId: String): Try[Option[RateData]]

  def listActiveProviders(): Try[List[AccountData]]

  def addUser(contract: AddUserContract): Try[Option[String]]
  def updateUser(contract: UpdateUserContract): Try[Unit]
  def toggleUser(id: String): Try[Unit]
  def deleteUser(id: String): Try[Unit]
  def loginUser(contract: LoginContract): Try[Option[String]]
  def listAllProviders(): Try[List[AccountData]]
  def toogleAccount(id: String): Try[Unit]

  def toggleTemplate(id: Int): Try[Unit]
  def toggleBypassSwitch(id: Int): Try[Unit]
  def deleteTemplate(id: Int): Try[Unit]
  def listTemplateDA(adId: Int): Try[List[TemplateDAData]]
  def listTemplateDAWithDestination(adId: Int): Try[List[ListTemplateDAData]]
  def addTemplateDa(contract: TemplateDAData): Try[Unit]
  def removeTemplateDa(adId: Int, destinationId: Int): Try[Unit]

  def updateTemplate(contract: UpdateTemplateContract): Try[Unit]
  def createTemplate(contract: CreateTemplateContract): Try[Option[Int]]
  
  def retrieveSellRateByDestination(destinationId: Int): Try[Option[RateData]]  
  
}



