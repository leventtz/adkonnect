package com.sponsoracall.ad.service

import java.util.ArrayList
import java.util.HashMap
import scala.collection.mutable.ListBuffer
import scala.math.BigDecimal.double2bigDecimal
import scala.math.BigDecimal.int2bigDecimal
import scala.util.Failure
import scala.util.Success
import scala.util.Try
import com.amazonaws.services.dynamodbv2.model.AttributeAction
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate
import com.amazonaws.services.dynamodbv2.model.BatchWriteItemRequest
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator
import com.amazonaws.services.dynamodbv2.model.Condition
import com.amazonaws.services.dynamodbv2.model.DeleteRequest
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue
import com.amazonaws.services.dynamodbv2.model.GetItemRequest
import com.amazonaws.services.dynamodbv2.model.PutItemRequest
import com.amazonaws.services.dynamodbv2.model.PutRequest
import com.amazonaws.services.dynamodbv2.model.QueryRequest
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest
import com.amazonaws.services.dynamodbv2.model.WriteRequest
import com.sponsoracall.ad.contract.ActivateAdContractV2
import com.sponsoracall.ad.contract.ApproveAdContract
import com.sponsoracall.ad.contract.CreateAdContractV2
import com.sponsoracall.ad.contract.PickAdContractV2
import com.sponsoracall.ad.contract.UpdateAdActiveTableContractV2
import com.sponsoracall.ad.contract.UpdateDesignContractV2
import com.sponsoracall.ad.contract.UpdateDesignDescriptionContractV2
import com.sponsoracall.ad.contract.UpdateIncomingContractV2
import com.sponsoracall.ad.data.AdDataV2
import com.sponsoracall.ad.data.InvoiceDataV2
import com.sponsoracall.ad.data.InvoiceItemDataV2
import com.sponsoracall.ad.data.OrderTotalDataV2
import com.sponsoracall.ad.data.OrderTotalFeesDataV2
import com.sponsoracall.ad.data.PickAdData
import com.sponsoracall.ad.data.TransactionDataV2
import com.sponsoracall.cdr.service.CDRServiceRegistry
import com.sponsoracall.db.DynamoDBEngine
import com.sponsoracall.exception.NotEnoughFund
import com.sponsoracall.exception.UnknownAccount
import com.sponsoracall.util.GlobalSettings
import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.Json
import redis.clients.jedis.Jedis
import java.util.Hashtable
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest
import com.sponsoracall.ad.integration.AdIntegrationComponent
import com.sponsoracall.ad.integration.AdIntegrationImpl
import com.sponsoracall.ad.data.ImpressionViewData
import com.sponsoracall.ad.contract.PickAdFromContractV2
import com.sponsoracall.ad.contract.RefundContractV2
import com.sponsoracall.ad.contract.UpdateUserTallyContractV2
import com.sponsoracall.ad.contract.AddCCTransactionContract
import com.sponsoracall.ad.contract.CompleteOrderContract
import com.sponsoracall.account.payment.PaymentData
import com.sponsoracall.ad.contract.AddFundContract
object AdServiceRegistry extends AdServiceComponent with AdIntegrationComponent {
  val integration = new AdIntegrationImpl
  val service = new AdService() {

    def toogleAd(contract: ActivateAdContractV2) = integration.toogleAd(contract)

    def calculateOrderTotal(lineItems: List[CreateAdContractV2]): List[OrderTotalDataV2] = integration.calculateOrderTotal(lineItems)
    def listOrderTotalFees(): Try[List[OrderTotalFeesDataV2]] = integration.listOrderTotalFees()
    def retrieveAccountAd(accountId: String, adId: String): Option[AdDataV2] = integration.retrieveAccountAd(accountId, adId)
    def retriveAd(adId: String): Option[AdDataV2] = integration.retriveAd(adId)

    def retrieveAccounAds(accountId: String, adId: String): List[AdDataV2] = integration.retrieveAccounAds(accountId, adId)

    def saveOrder(contract:CompleteOrderContract): Try[PaymentData] = integration.saveOrder(contract)

    def updateIncoming(contract: UpdateIncomingContractV2) = integration.updateIncoming(contract)

    def updateDesign(contract: UpdateDesignContractV2) = integration.updateDesign(contract)

    def updateDesign(contract: UpdateDesignDescriptionContractV2) = integration.updateDesign(contract)

    def addItemstoAdActiveTable(contract: UpdateAdActiveTableContractV2): Try[Unit] = integration.addItemstoAdActiveTable(contract)

    def removeItemsFromAdActiveTable(contract: UpdateAdActiveTableContractV2): Try[Unit] = integration.removeItemsFromAdActiveTable(contract)

    def retrieveInvoice(accountId: String, orderId: String) = integration.retrieveInvoice(accountId, orderId)

    def updateBalance(accountId: String, adId: String): Try[Unit] = integration.updateBalance(accountId, adId)

    def addTransaction(contract: TransactionDataV2): Try[Unit] = integration.addTransaction(contract)

    def retrieveTransactions(accountId: String, orderId: String): List[TransactionDataV2] = integration.retrieveTransactions(accountId, orderId)

    def retrieveCachedCallRedis(number: String): Try[PickAdData] = integration.retrieveCachedCallRedis(number)
    def cacheCallRedis(number: String, adPickDataJson: String): Try[Unit] = integration.cacheCallRedis(number, adPickDataJson)

    def cacheCall(number: String, adPickDataJson: String): Try[Unit] = integration.cacheCall(number, adPickDataJson)

    def retrieveCachedCall(number: String): Try[PickAdData] = integration.retrieveCachedCall(number)

    def retrieveCachedCallForCDR(number: String): Try[PickAdData] = integration.retrieveCachedCallForCDR(number)

    def listUnApprovedAd(): Try[List[AdDataV2]] = integration.listUnApprovedAd()

    def togggleApprove(contract: ApproveAdContract): Try[Unit] = integration.togggleApprove(contract)
    
    def retrieveImpressions(from: String, to: String, postalCode: String): List[ImpressionViewData] = integration.retrieveImpressions(from, to, postalCode)

    def pickAdsFrom(contract:PickAdFromContractV2): List[PickAdData] = integration.pickAdsFrom(contract)   
    
    def returnFunds(contract:RefundContractV2) = integration.returnFunds(contract)    

    def updateAdName(accountId:String,adId:String,name:String) = integration.updateAdName(accountId, adId, name)
  
    def updateAdImpression(accountId:String,adId:String,impression:BigDecimal):Try[Unit] = integration.updateAdImpression(accountId, adId, impression)

    def updateUserTally(contract:UpdateUserTallyContractV2) = integration.updateUserTally(contract)    
    
    def addFund(contract:AddFundContract)= integration.addFund(contract)
    
    def addCCTransaction(contract:AddCCTransactionContract): Try[Unit] = integration.addCCTransaction(contract)
    
    def updateBalance(accountId: String, adId: String,amount:BigDecimal) = integration.updateBalance(accountId, adId, amount)
    
  }
}
