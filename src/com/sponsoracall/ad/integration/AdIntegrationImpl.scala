package com.sponsoracall.ad.integration

import java.util.ArrayList
import java.util.Date
import java.util.HashMap
import java.util.Hashtable

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer
import scala.math.BigDecimal.double2bigDecimal
import scala.util.Failure
import scala.util.Success
import scala.util.Try

import org.apache.commons.pool.impl.GenericObjectPool.Config

import com.amazonaws.services.dynamodbv2.model.AttributeAction
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate
import com.amazonaws.services.dynamodbv2.model.BatchWriteItemRequest
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator
import com.amazonaws.services.dynamodbv2.model.Condition
import com.amazonaws.services.dynamodbv2.model.DeleteRequest
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue
import com.amazonaws.services.dynamodbv2.model.GetItemRequest
import com.amazonaws.services.dynamodbv2.model.PutItemRequest
import com.amazonaws.services.dynamodbv2.model.PutRequest
import com.amazonaws.services.dynamodbv2.model.QueryRequest
import com.amazonaws.services.dynamodbv2.model.ReturnValue
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest
import com.amazonaws.services.dynamodbv2.model.WriteRequest
import com.sponsoracall.account.payment.Payment
import com.sponsoracall.account.payment.PaymentData
import com.sponsoracall.account.service.AccountServiceRegistry
import com.sponsoracall.ad.contract.ActivateAdContractV2
import com.sponsoracall.ad.contract.AddCCTransactionContract
import com.sponsoracall.ad.contract.AddFundContract
import com.sponsoracall.ad.contract.ApproveAdContract
import com.sponsoracall.ad.contract.CompleteOrderContract
import com.sponsoracall.ad.contract.CreateAdContractV2
import com.sponsoracall.ad.contract.PickAdContractV2
import com.sponsoracall.ad.contract.PickAdFromContractV2
import com.sponsoracall.ad.contract.RefundContractV2
import com.sponsoracall.ad.contract.UpdateAdActiveTableContractV2
import com.sponsoracall.ad.contract.UpdateDesignContractV2
import com.sponsoracall.ad.contract.UpdateDesignDescriptionContractV2
import com.sponsoracall.ad.contract.UpdateIncomingContractV2
import com.sponsoracall.ad.contract.UpdateUserTallyContractV2
import com.sponsoracall.ad.data.AdDataV2
import com.sponsoracall.ad.data.AllocatedAdFund
import com.sponsoracall.ad.data.ImpressionData
import com.sponsoracall.ad.data.ImpressionViewData
import com.sponsoracall.ad.data.InvoiceDataV2
import com.sponsoracall.ad.data.InvoiceItemDataV2
import com.sponsoracall.ad.data.OrderTotalDataV2
import com.sponsoracall.ad.data.OrderTotalFeesDataV2
import com.sponsoracall.ad.data.PickAdData
import com.sponsoracall.ad.data.RecentAds
import com.sponsoracall.ad.data.RecentCalls
import com.sponsoracall.ad.data.TransactionDataV2
import com.sponsoracall.ad.data.UserTallyData
import com.sponsoracall.cdr.data.LocationData
import com.sponsoracall.cdr.service.CDRServiceRegistry
import com.sponsoracall.db.DynamoDBEngine
import com.sponsoracall.exception.NotEnoughFund
import com.sponsoracall.exception.UnknownAccount
import com.sponsoracall.gatewayaccount.service.GatewayAccountServiceRegistry
import com.sponsoracall.util.GlobalSettings

import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.Json
import redis.clients.jedis.JedisPool

class AdIntegrationImpl extends AdIntegration {

  def isLimit(userTallyData:Option[UserTallyData]):Boolean = {
      userTallyData match {
        case Some(data) if(data.dailyTotalMinutes+GlobalSettings.averageCallDuration<=GlobalSettings.totalDailyMinutesAllowed && data.monthlyTotalMinutes<=GlobalSettings.totalMonthlyMinutesAllowed+GlobalSettings.averageCallDuration) => true
        case None => true
        case _ => false
      }
    } 

  def retrieveImpressions(from: String, to: String, postalCode: String): List[ImpressionViewData] = {
    def bestMatchImpressions(locationId: Int, templateId: Int, userTallyData: Option[UserTallyData]): List[ImpressionData] = {
      val keyConditions = new HashMap[String, Condition]
      keyConditions.put("inTemplate", new Condition().withComparisonOperator(ComparisonOperator.EQ).withAttributeValueList(new AttributeValue(locationId + "-" + templateId)))
      val queryRequest = new QueryRequest().withTableName("AdActiveTable").withScanIndexForward(false).withLimit(GlobalSettings.numberOfItemsLimit).withKeyConditions(keyConditions)

      val result = DynamoDBEngine.client.query(queryRequest)
      if (result.getCount() == 0) {
        Nil
      } else {
        val items = result.getItems()
        val list = new ArrayBuffer[ImpressionData]()
        for (i <- 0 until items.size) {
          val adPlusAccountId = items.get(i).get("adPlusAccountId").getS().split("\\+")
          val adId = adPlusAccountId(1)
          val accountId = adPlusAccountId(2)
          val impression = adPlusAccountId(0).toDouble
          val impressionView = items.get(i).get("impressionView").getS()
          val adPrice = items.get(i).get("adPrice").getN().toDouble
          val description = items.get(i).get("description").getS()
          val bypassSwitch: Boolean = if (items.get(i).get("bypassSwitch").getS == "false") false else true
          
          val recentAds = if (userTallyData == None) Nil else userTallyData.get.recentAds
          val recentCalls = if (userTallyData == None) Nil else userTallyData.get.recentCalls
          
          if(bypassSwitch) {
        	  list.append(new ImpressionData(accountId, adId, impression, impressionView, adPrice))
          } else {
        	  if (recentAds.filter(item => item.adId == adId).isEmpty && recentCalls.filter(item => item.to == to).isEmpty) {
        		  list.append(new ImpressionData(accountId, adId, impression, impressionView, adPrice))
        	  }
          }
        }
        list.toList
      }
    }

    def retrieveImpressionsInternal(): List[ImpressionViewData] = {
      CDRServiceRegistry.service.retrieveLocationsFromZip(postalCode) match {
        case Some(postalCodes) => {
          val impressionList = CDRServiceRegistry.service.retrieveBestMatchTemplate(to) match {
            case Success(Some(data)) => {
              val userTallyData=retrieveAndCleanUserTally(from)
              if(isLimit(userTallyData)) {
            	  postalCodes.foldLeft(List[ImpressionData]())((list, id) => (bestMatchImpressions(id, data.templateId,userTallyData) ::: list)).toSet.toList
              } else {
                Nil
              }
            }
            case _ => Nil
          }

          if (impressionList != Nil) {
            val finalList = deductImpressionsAndAllocateFunds(from, impressionList).sortBy(s => -1 * s.impression)
            finalList.map(data => ImpressionViewData(data.accountId, data.adId, data.impressionView))
            //Nil
          } else
            Nil

        }
        case None => Nil
      }
    }

    returnFunds(RefundContractV2(from, None))
    CDRServiceRegistry.service.retrieveBestMatchDestination(to) match {
      case Success(Some(data)) => retrieveImpressionsInternal()
      case _ => Nil
    }

  }

  def retrieveUserTally(number: String): Option[UserTallyData] = {
    try {
      val key = new HashMap[String, AttributeValue]
      key.put("id", new AttributeValue(number))
      val getItemRequest = new GetItemRequest().withTableName("SwitchCacheTable").withKey(key)
      val result = DynamoDBEngine.client.getItem(getItemRequest)
      if (result != null) {
        val item = result.getItem()
        val dailyTotalMinutes = if (item.get("dailyTotalMinutes") != null) item.get("dailyTotalMinutes").getN().toInt else 0
        val dailyTotalMinutesTimeStamp = if (item.get("dailyTotalMinutesTimeStamp") != null) item.get("dailyTotalMinutesTimeStamp").getN.toLong else System.currentTimeMillis()
        val monthlyTotalMinutes = if (item.get("monthlyTotalMinutes") != null) item.get("monthlyTotalMinutes").getN.toInt else 0
        val monthlyTotalMinutesTimeStamp = if (item.get("monthlyTotalMinutesTimeStamp") != null) item.get("monthlyTotalMinutesTimeStamp").getN.toLong else System.currentTimeMillis()
        val recentCalls = {
          if (item.get("recentCalls") != null) {
            val temp = item.get("recentCalls").getSS()
            val temp1 = new ArrayBuffer[RecentCalls]()
            for (i <- 0 until temp.size()) {
              RecentCalls.parse(temp.get(i)) match {
                case Some(data) => temp1.append(data)
                case None => //that's fine
              }
            }
            temp1.toList
          } else {
            List()
          }
        }

        val recentAds = {
          if (item.get("recentAds") != null) {
            val temp = item.get("recentAds").getSS()
            val temp1 = new ArrayBuffer[RecentAds]()
            for (i <- 0 until temp.size()) {
              RecentAds.parse(temp.get(i)) match {
                case Some(data) => temp1.append(data)
                case None => //that's fine
              }
            }
            temp1.toList
          } else {
            List()
          }
        }
        Some(UserTallyData(dailyTotalMinutes, dailyTotalMinutesTimeStamp, monthlyTotalMinutes, monthlyTotalMinutesTimeStamp, recentAds, recentCalls))
      } else {
        None
      }
    } catch {
      case t: Throwable => None
    }
  }

  def retrieveAndCleanUserTally(number: String): Option[UserTallyData] = {
    try {
      retrieveUserTally(number) match {
        case Some(data) => {
          var dailyTotalMinutesTimeStamp: Long = 0
          var dailyTotalMinutes = 0
          val dailyExpire: Long = System.currentTimeMillis() - data.dailyTotalMinutesTimeStamp
          if (dailyExpire > 24 * 60 * 1000) {
            dailyTotalMinutesTimeStamp = System.currentTimeMillis()
          } else {
            dailyTotalMinutes = data.dailyTotalMinutes
            dailyTotalMinutesTimeStamp = data.dailyTotalMinutesTimeStamp
          }

          var monthlyTotalMinutesTimeStamp: Long = 0
          var monthlyTotalMinutes = 0

          val monthlyExpire = System.currentTimeMillis() - data.monthlyTotalMinutesTimeStamp
          if (monthlyExpire > 30 * 24 * 60 * 1000) {
            monthlyTotalMinutesTimeStamp = System.currentTimeMillis()
          } else {
            monthlyTotalMinutes = data.monthlyTotalMinutes
            monthlyTotalMinutesTimeStamp = data.monthlyTotalMinutesTimeStamp
          }
          val recentAdsList = data.recentAds.filter(item => System.currentTimeMillis() - item.timeStamp < GlobalSettings.minutesBetweenSameAd * 60 * 1000);
          val recentCallsList = data.recentCalls.filter(item => System.currentTimeMillis() - item.timeStamp < GlobalSettings.minutesBetweenSameCalls * 60 * 1000);
          val userTallyData = UserTallyData(dailyTotalMinutes, dailyTotalMinutesTimeStamp, monthlyTotalMinutes, monthlyTotalMinutesTimeStamp, recentAdsList, recentCallsList)
          updateUserTally(number, userTallyData)
          Some(userTallyData)
        }
        case _ => None
      }
    } catch {
      case t: Throwable => None
    }
  }

  def updateUserTally(number: String, userTallyData: UserTallyData) {
    val key = new HashMap[String, AttributeValue]
    key.put("id", new AttributeValue(number))
    val attributeUpdates = new HashMap[String, AttributeValueUpdate]
    attributeUpdates.put("dailyTotalMinutes", new AttributeValueUpdate().withAction(AttributeAction.PUT).withValue(new AttributeValue().withN(userTallyData.dailyTotalMinutes.toString)))
    attributeUpdates.put("dailyTotalMinutesTimeStamp", new AttributeValueUpdate().withAction(AttributeAction.PUT).withValue(new AttributeValue().withN(userTallyData.dailyTotalMinutesTimeStamp.toString)))
    attributeUpdates.put("monthlyTotalMinutes", new AttributeValueUpdate().withAction(AttributeAction.PUT).withValue(new AttributeValue().withN(userTallyData.monthlyTotalMinutes.toString)))
    attributeUpdates.put("monthlyTotalMinutesTimeStamp", new AttributeValueUpdate().withAction(AttributeAction.PUT).withValue(new AttributeValue().withN(userTallyData.monthlyTotalMinutesTimeStamp.toString)))
    val recentCalls = new ArrayList[String]();
    userTallyData.recentCalls.foreach(f => recentCalls.add(f.toString))
    if (recentCalls.size() > 0) {
      attributeUpdates.put("recentCalls", new AttributeValueUpdate().withAction(AttributeAction.PUT).withValue(new AttributeValue().withSS(recentCalls)))
    }
    val recentAdsCalls = new ArrayList[String]();
    userTallyData.recentAds.foreach(f => recentAdsCalls.add(f.toString))
    if (recentAdsCalls.size() > 0) {
      attributeUpdates.put("recentAdsCalls", new AttributeValueUpdate().withAction(AttributeAction.PUT).withValue(new AttributeValue().withSS(recentAdsCalls)))
    }
    val updateItemRequest = new UpdateItemRequest().withKey(key).withTableName("SwitchCacheTable").withAttributeUpdates(attributeUpdates)
    DynamoDBEngine.client.updateItem(updateItemRequest)
  }

  def updateUserTally(contract:UpdateUserTallyContractV2) {
    val key = new HashMap[String, AttributeValue]
    key.put("id", new AttributeValue(contract.from))
    val attributeUpdates = new HashMap[String, AttributeValueUpdate]
    val normalizedDuration=if(contract.durationInSec.toInt<=60) 1 else Math.ceil(contract.durationInSec.toDouble/60.toString().toDouble)
    attributeUpdates.put("dailyTotalMinutes", new AttributeValueUpdate().withAction(AttributeAction.ADD).withValue(new AttributeValue().withN(normalizedDuration.toString)))
    attributeUpdates.put("monthlyTotalMinutes", new AttributeValueUpdate().withAction(AttributeAction.ADD).withValue(new AttributeValue().withN(normalizedDuration.toString)))

    val recentCalls = new ArrayList[String]();
    recentCalls.add(RecentCalls(contract.to, System.currentTimeMillis()).toString)
    attributeUpdates.put("recentCalls", new AttributeValueUpdate().withAction(AttributeAction.ADD).withValue(new AttributeValue().withSS(recentCalls)))

    val recentAds = new ArrayList[String]();
    recentAds.add(RecentAds(contract.accountId, contract.adId, contract.to, System.currentTimeMillis()).toString)
    attributeUpdates.put("recentAds", new AttributeValueUpdate().withAction(AttributeAction.ADD).withValue(new AttributeValue().withSS(recentAds)))

    val updateItemRequest = new UpdateItemRequest().withKey(key).withTableName("SwitchCacheTable").withAttributeUpdates(attributeUpdates)
    DynamoDBEngine.client.updateItem(updateItemRequest)
  }

  def deductImpressionsAndAllocateFunds(number: String, list: List[ImpressionData]): List[ImpressionData] = {
    val finalList = new ArrayBuffer[ImpressionData]()
    list.foreach(item => {
      allocateFund(item.accountId, item.adId) match {
        case Success(()) => finalList.append(item)
        case Failure(t) => t.printStackTrace() //do Nothing
      }
    })

    def allocateFund(accountId: String, adId: String): Try[Unit] = {
      try {
        retrieveAccountAd(accountId, adId) match {
          case Some(adDataV2) => {
            val key = new HashMap[String, AttributeValue]
            key.put("accountId", new AttributeValue(adDataV2.accountId))
            key.put("adId", new AttributeValue(adDataV2.adId))
            val items = new HashMap[String, AttributeValueUpdate]

            val value = (adDataV2.adPrice + adDataV2.impression)

            items.put("balance", new AttributeValueUpdate().withAction(AttributeAction.ADD).withValue(new AttributeValue().withN(value.toString)))

            val expected = new HashMap[String, ExpectedAttributeValue];
            val expectedValue = new ExpectedAttributeValue().withComparisonOperator(ComparisonOperator.LE).withValue(new AttributeValue().withN((adDataV2.budget - value).toString))
            expected.put("balance", expectedValue)

            val updateItemRequest = new UpdateItemRequest().withTableName("AccountAdTable").withKey(key).withAttributeUpdates(items).withExpected(expected)
            val result = DynamoDBEngine.client.updateItem(updateItemRequest);
            Success(())
          }
          case None => Failure(new UnknownAccount)
        }
      } catch {
        case t: Throwable => Failure(t)
      }
    }

    val listToAppend = new java.util.ArrayList[String];
    finalList.foreach(item => {
      val allocatedFund = AllocatedAdFund(item.accountId, item.adId, item.adPrice, System.currentTimeMillis());
      listToAppend.add(allocatedFund.toString)
    })

    if (!listToAppend.isEmpty()) {
      val key = new HashMap[String, AttributeValue]
      key.put("id", new AttributeValue().withS(number))
      val attributeUpdates = new HashMap[String, AttributeValueUpdate]
      attributeUpdates.put("allocatedAds", new AttributeValueUpdate().withAction(AttributeAction.PUT).withValue(new AttributeValue().withSS(listToAppend)))
      val updateItemRequest = new UpdateItemRequest().withKey(key).withAttributeUpdates(attributeUpdates).withTableName("SwitchCacheTable")
      DynamoDBEngine.client.updateItem(updateItemRequest)
    }
    finalList.toList
  }

  def returnFunds(contract: RefundContractV2) {
    val key = new HashMap[String, AttributeValue]
    key.put("id", new AttributeValue(contract.number))
    val attributeUpdates = new HashMap[String, AttributeValueUpdate]
    attributeUpdates.put("allocatedAds", new AttributeValueUpdate().withAction(AttributeAction.DELETE))
    val updateItemRequest = new UpdateItemRequest().withTableName("SwitchCacheTable").withKey(key).withAttributeUpdates(attributeUpdates).withReturnValues(ReturnValue.ALL_OLD);
    val result = DynamoDBEngine.client.updateItem(updateItemRequest)
    if (result.getAttributes() != null) {
      if (result.getAttributes().containsKey("allocatedAds")) {
        val funds = result.getAttributes().get("allocatedAds").getSS()
        for (i <- 0 until funds.size) {
          AllocatedAdFund.parse(funds.get(i)) match {
            case Some(fund) => {
              contract.excludeAccount match {
                case Some(excludeAccount) if (fund.accountId == excludeAccount.accountId && fund.adId == excludeAccount.adId) => //Do Nothing
                case _ => returnFund(fund.accountId, fund.adId, fund.allocatedAmount)
              }
            }
            case None => //done nothing
          }
        }
      }
    }
    def returnFund(accountId: String, adId: String, value: BigDecimal): Try[Unit] = {
      try {
        retrieveAccountAd(accountId, adId) match {
          case Some(adDataV2) => {
            val key = new HashMap[String, AttributeValue]
            key.put("accountId", new AttributeValue(adDataV2.accountId))
            key.put("adId", new AttributeValue(adDataV2.adId))
            val items = new HashMap[String, AttributeValueUpdate]
            items.put("balance", new AttributeValueUpdate().withAction(AttributeAction.ADD).withValue(new AttributeValue().withN((-value).toString)))
            val updateItemRequest = new UpdateItemRequest().withTableName("AccountAdTable").withKey(key).withAttributeUpdates(items)
            val result = DynamoDBEngine.client.updateItem(updateItemRequest);
            val balance = adDataV2.balance + adDataV2.impression + adDataV2.adPrice - value
            if (balance > adDataV2.budget) {
              toogleAd(ActivateAdContractV2(accountId, adId, false))
            }
            Success(())
          }
          case None => Failure(new UnknownAccount)
        }
      } catch {
        case t: Throwable => Failure(t)
      }
    }

  }

  /////////////////////////////////////////////////////////////////////////Impression Stuff End///////////////////////////////////////////////////////////////////////// 

  def updateAdName(accountId: String, adId: String, name: String) {
    val key = new HashMap[String, AttributeValue];
    key.put("accountId", new AttributeValue(accountId))
    key.put("adId", new AttributeValue(adId))
    val updateItems = new HashMap[String, AttributeValueUpdate]();
    updateItems.put("name", new AttributeValueUpdate().withValue(new AttributeValue(name)))
    val updateItemRequest = new UpdateItemRequest().withTableName("AccountAdTable").withKey(key).withAttributeUpdates(updateItems)
    DynamoDBEngine.client.updateItem(updateItemRequest)

    DynamoDBEngine.client.updateItem(updateItemRequest)
  }

  def updateAdImpression(accountId: String, adId: String, impression: BigDecimal): Try[Unit] = {
    try {
      retrieveAccountAd(accountId, adId) match {
        case Some(data) => {
          removeItemsFromAdActiveTable(UpdateAdActiveTableContractV2(data.accountId, data.adId, data.impression, data.impressionView, data.adPrice, data.description: String,data.bypassSwitch))
          val key = new HashMap[String, AttributeValue]
          key.put("accountId", new AttributeValue(accountId))
          key.put("adId", new AttributeValue(adId))
          val updateValues = new HashMap[String, AttributeValueUpdate]
          updateValues.put("impression", new AttributeValueUpdate().withValue(new AttributeValue().withN(impression.toString)))
          val updateItemRequest = new UpdateItemRequest().withTableName("AccountAdTable").withKey(key).withAttributeUpdates(updateValues)
          DynamoDBEngine.client.updateItem(updateItemRequest)
          if (data.active) {
            addItemstoAdActiveTable(UpdateAdActiveTableContractV2(data.accountId, data.adId, impression, data.impressionView, data.adPrice, data.description,data.bypassSwitch))
          }
          Success();
        }
        case _ => Failure(new Error("UnknownAd"))
      }
    } catch {
      case t: Throwable => Failure(t)
    }

  }

  def toogleAd(contract: ActivateAdContractV2) = {
    retrieveAccountAd(contract.accountId, contract.adId).map(f => {
      if (f.balance + f.adPrice + f.impression > f.budget) {
        toogleAd(f, false)
      } else {
        toogleAd(f, contract.active)
      }
    })

    def toogleAd(adData: AdDataV2, active: Boolean) {
      val key = new HashMap[String, AttributeValue];
      key.put("accountId", new AttributeValue(adData.accountId))
      key.put("adId", new AttributeValue(adData.adId))
      val updateItems = new HashMap[String, AttributeValueUpdate]();
      updateItems.put("active", new AttributeValueUpdate().withValue(new AttributeValue(if (active) "1" else "0")))
      val updateItemRequest = new UpdateItemRequest().withTableName("AccountAdTable").withKey(key).withAttributeUpdates(updateItems)
      DynamoDBEngine.client.updateItem(updateItemRequest)
      val contract = UpdateAdActiveTableContractV2(adData.accountId, adData.adId, adData.impression, adData.impressionView, adData.adPrice, adData.description,adData.bypassSwitch);
      removeItemsFromAdActiveTable(contract)
      if (active) {
        addItemstoAdActiveTable(contract)
      }
    }
  }

  def calculateOrderTotal(lineItems: List[CreateAdContractV2]): List[OrderTotalDataV2] = {

    require(lineItems != Nil, "Empty LineItems");

    //calculate wi tax only hardcoded!!
    var tax: BigDecimal = BigDecimal(0);
    val accountData = AccountServiceRegistry.service.retrieveAccount(lineItems(0).accountId) match {
      case Success(Some(data)) if data.state == "WI" => tax = 0.056;
      case _ => Nil

    }

    val lineTotal = lineItems.foldRight(BigDecimal(0))((lineItem, b) => {
      b + lineItem.budget
    })

    val lineTotalWrap = new OrderTotalDataV2("line_total", lineTotal)

    val fees = listOrderTotalFees();
    val otherChargesWrap: List[OrderTotalDataV2] = {
      if (fees.isSuccess) {
        val otherCharges: List[OrderTotalDataV2] = fees.get.map(f => {
          if (f.isPercent == "0") {
            OrderTotalDataV2(f.feeItemMessageId, f.applyValue)
          } else {
            OrderTotalDataV2(f.feeItemMessageId, (f.applyValue * f.applyValue / 100f))
          }
        })
        otherCharges
      } else {
        Nil
      }
    }

    if (tax != 0) {

      val tempTotal = lineTotalWrap :: otherChargesWrap;
      val orderTotal = tempTotal.foldRight(BigDecimal(0))((item, t) => t + item.total)
      val taxTotal = (orderTotal * tax).setScale(2, scala.math.BigDecimal.RoundingMode.HALF_UP);

      val taxTotalWrap = OrderTotalDataV2("wi_tax", taxTotal)
      val orderTotalWrap = OrderTotalDataV2("order_total", taxTotal + orderTotal)
      lineTotalWrap :: otherChargesWrap ::: List(taxTotalWrap, orderTotalWrap)

    } else {
      val tempTotal = lineTotalWrap :: otherChargesWrap;

      val orderTotalWrap = OrderTotalDataV2("order_total", tempTotal.foldRight(BigDecimal(0))((item, t) => t + item.total))

      lineTotalWrap :: otherChargesWrap ::: List(orderTotalWrap)

    }

  }

  def listOrderTotalFees(): Try[List[OrderTotalFeesDataV2]] = {
    val key = new HashMap[String, AttributeValue];
    key.put("Key", new AttributeValue("OrderTotalFees"))
    val getItemRequest = new GetItemRequest().withTableName("UtilTable").withKey(key)
    val values = Json.parse(DynamoDBEngine.client.getItem(getItemRequest).getItem().get("value").getS());
    implicit val reader = Json.reads[OrderTotalFeesDataV2]
    val fees = values.as[List[OrderTotalFeesDataV2]]
    Success(fees)
  }

  def retrieveAccountAd(accountId: String, adId: String): Option[AdDataV2] = {
    val key = new HashMap[String, AttributeValue]
    key.put("accountId", new AttributeValue(accountId));
    key.put("adId", new AttributeValue(adId));
    val getItemRequest = new GetItemRequest().withTableName("AccountAdTable").withKey(key);
    val result = DynamoDBEngine.client.getItem(getItemRequest);
    val item = result.getItem();
    if (item != null) {
      CreateAdDatafromItem(item) match {
        case Success(data) => Some(data)
        case Failure(t) => None
      }
    } else {
      None
    }

  }

  private def CreateAdDatafromItem(item: java.util.Map[String, AttributeValue]): Try[AdDataV2] = {
    try {
      val accountId: String = item.get("accountId").getS
      val name: String = item.get("name").getS;
      val adId: String = item.get("adId").getS
      val templateId: Int = item.get("templateId").getN.toInt
      val mediaIVRId: String = item.get("mediaIVRId").getS;
      val mediaId: String = item.get("mediaId").getS;
      val impression: BigDecimal = if (item.containsKey("impression")) item.get("impression").getN().toDouble else 0;
      val description: String = item.get("description").getS;
      val designData: String = item.get("designData").getS;
      val impressionView: String = if (item.containsKey("impressionView")) item.get("impressionView").getS else "";
      val bypassSwitch: Boolean = if (item.get("bypassSwitch").getS == "false") false else true
      val budget: BigDecimal = item.get("budget").getN.toDouble
      val adPrice: BigDecimal = item.get("adPrice").getN.toDouble
      val balance: BigDecimal = item.get("balance").getN.toDouble
      val adDateTime: Long = item.get("adDateTime").getN().toLong
      val maxDuration: Int = item.get("maxDuration").getN().toInt

      val listIncomingNameBuffer = new ListBuffer[LocationData]()
      if (item.get("incomingName") != null) {
        val listIncoming = item.get("incomingName").getSS()
        for (j <- 0 until listIncoming.size) {
          listIncomingNameBuffer.append(LocationData.parse(listIncoming.get(j).toString))
        }
      }
      val incoming: List[LocationData] = listIncomingNameBuffer.toList

      val approved: Boolean = if (item.get("approved").getS == "0") false else true
      val active: Boolean = if (item.get("active").getS == "0") false else true
      val templateName: String = item.get("templateName").getS

      Success(new AdDataV2(accountId, name, adId, adDateTime, templateId, mediaIVRId, mediaId, description, designData, impressionView, maxDuration, bypassSwitch, impression, budget, balance, adPrice, incoming, templateName, approved, active))
    } catch {
      case t: Throwable => Failure(t)
    }

  }

  def retriveAd(adId: String): Option[AdDataV2] = {
    val keyCondition = new HashMap[String, Condition]
    keyCondition.put("adId", new Condition().withComparisonOperator(ComparisonOperator.EQ).withAttributeValueList(new AttributeValue(adId)))
    val queryRequest = new QueryRequest().withTableName("AccountAdTable").withIndexName("adId-index").withKeyConditions(keyCondition).withLimit(1)
    val result = DynamoDBEngine.client.query(queryRequest)
    if (result.getCount() != 0) {
      val accountId = result.getItems().get(0).get("accountId").getS()
      retrieveAccountAd(accountId, adId)
    } else {
      None
    }
  }

  def retrieveAccounAds(accountId: String, adId: String): List[AdDataV2] = {
    val queryRequest = new QueryRequest()
    val keyConditions = new HashMap[String, Condition]
    keyConditions.put("accountId", new Condition().withComparisonOperator(ComparisonOperator.EQ).withAttributeValueList(new AttributeValue(accountId)));
    keyConditions.put("adId", new Condition().withComparisonOperator(ComparisonOperator.LT).withAttributeValueList(new AttributeValue(adId)));

    queryRequest.withTableName("AccountAdTable").withKeyConditions(keyConditions);
    queryRequest.setScanIndexForward(false);
    queryRequest.setLimit(20);
    val result = DynamoDBEngine.client.query(queryRequest)
    val items = result.getItems()

    val listBuffer = new ListBuffer[AdDataV2]()
    for (i <- 0 until items.size()) {
      val item = items.get(i)
      val adData = CreateAdDatafromItem(item)
      adData match {
        case Success(data) => listBuffer.append(data)
        case Failure(t) => // Do nothing we can't parse
      }
    }
    listBuffer.toList
  }

 
  def saveOrder(contract:CompleteOrderContract): Try[PaymentData] = {
    try {
      val orderId = System.currentTimeMillis() / 1000
      var i = 0
      var accountId = ""
      val invoceItems = new ListBuffer[InvoiceItemDataV2]()
      var putItemRequest:PutItemRequest=null;
      contract.lineItems.foreach(f => {
        val items = new HashMap[String, AttributeValue]();
        accountId = f.accountId;
        items.put("accountId", new AttributeValue(accountId));
        items.put("name", new AttributeValue(f.name))
        items.put("orderId", new AttributeValue(orderId.toString))
        i = i + 1
        items.put("adId", new AttributeValue(orderId + i.toString));
        items.put("templateId", new AttributeValue().withN(f.templateId.toString));
        items.put("templateName", new AttributeValue().withS(f.templateName));
        items.put("designData", new AttributeValue(f.designData.toString))
        items.put("description", new AttributeValue(f.designData.description))
        items.put("mediaId", new AttributeValue(f.designData.mediaId))
        items.put("impressionView", new AttributeValue(f.designData.impressionView))
        items.put("mediaIVRId", new AttributeValue(if (f.mediaIVRId.isEmpty()) "0" else f.mediaIVRId))
        items.put("maxDuration", new AttributeValue().withN(f.maxDuration.toString))
        items.put("description", new AttributeValue(f.designData.description))
        items.put("bypassSwitch", new AttributeValue(f.bypassSwitch.toString))
        items.put("impression", new AttributeValue().withN(f.impression.toString))
        items.put("adPrice", new AttributeValue().withN((f.unitPrice + f.promptCost).toString))
        items.put("unitPrice", new AttributeValue().withN(f.unitPrice.toString))
        items.put("promptCost", new AttributeValue().withN(f.promptCost.toString))
        items.put("budget", new AttributeValue().withN(f.budget.toString))
        items.put("balance", new AttributeValue().withN("0.00"))
        items.put("numberOfCalls", new AttributeValue().withN("0"))
        items.put("numberOfMinutes", new AttributeValue().withN("0.0"))
        items.put("active", new AttributeValue("0"))
        items.put("approved", new AttributeValue("0"))
        items.put("adDateTime", new AttributeValue().withN((System.currentTimeMillis()).toString));
        items.put("dateFormatted", new AttributeValue().withS(new Date().toString))

        val javaList = new ArrayList[String]();
        val javaListName = new ArrayList[String]();
        f.incoming.foreach(f => javaList.add(f.id.toString));
        f.incoming.foreach(f => javaListName.add(f.toString));

        items.put("incoming", new AttributeValue().withNS(javaList))
        items.put("incomingName", new AttributeValue().withSS(javaListName))

        putItemRequest = new PutItemRequest().withTableName("AccountAdTable").withItem(items);
        invoceItems.append(InvoiceItemDataV2(name = f.templateName, impression = f.impression, budget = f.budget))

      })
      
      
      val orderTotalItems = calculateOrderTotal(contract.lineItems)
      
      val paymentData=Payment.pay(contract.paymentContract)
      if(paymentData.responseCode=="1") {
    	  addTransaction(TransactionDataV2(accountId, orderId.toString, System.currentTimeMillis(), orderTotalItems(orderTotalItems.length - 1).total, "O", InvoiceDataV2(invoceItems.toList, orderTotalItems).toString, AccountServiceRegistry.service.retrieveAccount(accountId).get.get.toString()))
    	  addCCTransaction(AddCCTransactionContract(accountId,orderId.toString,contract.paymentContract.amount,paymentData.authCode))
    	  DynamoDBEngine.client.putItem(putItemRequest);
    	  Success(paymentData)
      } else {
        Failure(new Error(paymentData.statusCodeString))
      }

      
      
    } catch {
      case t: Throwable => Failure(t)
    }
  }

  
  def addFund(contract:AddFundContract):Try[PaymentData] = {
      val paymentData=Payment.pay(contract.paymentContract)
      if(paymentData.responseCode=="1") {
    	  addCCTransaction(AddCCTransactionContract(contract.accountId,contract.adId.toString,contract.paymentContract.amount,paymentData.authCode))
    	  val key = new HashMap[String, AttributeValue];
    	  key.put("accountId", new AttributeValue(contract.accountId))
    	  key.put("adId", new AttributeValue(contract.adId))
    	  val updateItems = new HashMap[String, AttributeValueUpdate]();
    	  updateItems.put("budget", new AttributeValueUpdate().withAction(AttributeAction.ADD).withValue(new AttributeValue().withN(contract.paymentContract.amount.toString)))
    	  val updateItemRequest = new UpdateItemRequest().withTableName("AccountAdTable").withKey(key).withAttributeUpdates(updateItems)
    	  DynamoDBEngine.client.updateItem(updateItemRequest)
    	  Success(paymentData)
      } else {
        Failure(new Error(paymentData.statusCodeString))
      }
  }
  
  
  
  
  
  def updateIncoming(contract: UpdateIncomingContractV2) {
    retrieveAccountAd(contract.accountId, contract.adId) match {
      case Some(data) => {
        val key = new HashMap[String, AttributeValue];
        key.put("accountId", new AttributeValue(contract.accountId))
        key.put("adId", new AttributeValue(contract.adId))
        val updateItems = new HashMap[String, AttributeValueUpdate]();

        val javaList = new ArrayList[String]();
        if (!contract.incoming.isEmpty) {
          contract.incoming.foreach(f => javaList.add(f.id.toString));
        } else {
          javaList.add("0");
        }

        val javaListName = new ArrayList[String]();
        if (!contract.incoming.isEmpty) {
          contract.incoming.foreach(f => javaListName.add(f.toString));
        } else {
          javaListName.add("0");
        }

        updateItems.put("incoming", new AttributeValueUpdate().withValue(new AttributeValue().withNS(javaList)))
        updateItems.put("incomingName", new AttributeValueUpdate().withValue(new AttributeValue().withSS(javaListName)))

        val updateItemRequest = new UpdateItemRequest().withTableName("AccountAdTable").withKey(key).withAttributeUpdates(updateItems)
        DynamoDBEngine.client.updateItem(updateItemRequest)

        toogleAd(new ActivateAdContractV2(contract.accountId, contract.adId, data.active));
        println("here");
      }
      case _ => // do nothing
    }
  }

  def updateDesign(contract: UpdateDesignContractV2) {
    val key = new HashMap[String, AttributeValue];
    key.put("accountId", new AttributeValue(contract.accountId))
    key.put("adId", new AttributeValue(contract.adId))
    val updateItems = new HashMap[String, AttributeValueUpdate]();
    updateItems.put("description", new AttributeValueUpdate().withValue(new AttributeValue(contract.designData.description)))
    updateItems.put("designData", new AttributeValueUpdate().withValue(new AttributeValue(contract.designData.toString)))
    updateItems.put("impressionView", new AttributeValueUpdate().withValue(new AttributeValue(contract.designData.impressionView)))
    updateItems.put("mediaId", new AttributeValueUpdate().withValue(new AttributeValue(contract.designData.mediaId)))
    updateItems.put("approved", new AttributeValueUpdate().withValue(new AttributeValue("0")));
    updateItems.put("active", new AttributeValueUpdate().withValue(new AttributeValue("0")));
    val updateItemRequest = new UpdateItemRequest().withTableName("AccountAdTable").withKey(key).withAttributeUpdates(updateItems)
    DynamoDBEngine.client.updateItem(updateItemRequest)

    toogleAd(new ActivateAdContractV2(contract.accountId, contract.adId, false));

  }

  def updateDesign(contract: UpdateDesignDescriptionContractV2) {
    val key = new HashMap[String, AttributeValue];
    key.put("accountId", new AttributeValue(contract.accountId))
    key.put("adId", new AttributeValue(contract.adId))
    val updateItems = new HashMap[String, AttributeValueUpdate]();
    updateItems.put("description", new AttributeValueUpdate().withValue(new AttributeValue(contract.description)))
    updateItems.put("approved", new AttributeValueUpdate().withValue(new AttributeValue("0")));
    updateItems.put("active", new AttributeValueUpdate().withValue(new AttributeValue("0")));
    val updateItemRequest = new UpdateItemRequest().withTableName("AccountAdTable").withKey(key).withAttributeUpdates(updateItems)
    DynamoDBEngine.client.updateItem(updateItemRequest)
    toogleAd(new ActivateAdContractV2(contract.accountId, contract.adId, false));
  }

  def addItemstoAdActiveTable(contract: UpdateAdActiveTableContractV2): Try[Unit] = {
    val getKey = new HashMap[String, AttributeValue];
    getKey.put("accountId", new AttributeValue(contract.accountId));
    getKey.put("adId", new AttributeValue(contract.adId));
    val getItemRequest = new GetItemRequest().withTableName("AccountAdTable").withKey(getKey)
    val result = DynamoDBEngine.client.getItem(getItemRequest);
    if (result.getItem() == null) {
      Failure(new Error("Unknown account or ad"))
    } else {
      val templateId = result.getItem().get("templateId").getN()
      val putList = new ArrayList[WriteRequest]
      val incomings: java.util.List[String] = result.getItem().get("incoming").getNS()
      for (i <- 0 until incomings.size()) {
        val items = new HashMap[String, AttributeValue]
        items.put("inTemplate", new AttributeValue(incomings.get(i) + "-" + templateId))
        items.put("adPlusAccountId", new AttributeValue(contract.getImpressionAdAccountValue))
        items.put("impressionView", new AttributeValue(contract.impressionView));
        items.put("adPrice", new AttributeValue().withN(contract.adPrice.toString));
        items.put("description", new AttributeValue().withS(contract.description.toString));
        items.put("bypassSwitch", new AttributeValue().withS(contract.bypassSwitch.toString));
        putList.add(new WriteRequest().withPutRequest(new PutRequest().withItem(items)))
      }
      val requestItems = new HashMap[String, java.util.List[WriteRequest]]();
      requestItems.put("AdActiveTable", putList)
      val batchWriteItemRequest = new BatchWriteItemRequest().withRequestItems(requestItems)
      DynamoDBEngine.client.batchWriteItem(batchWriteItemRequest);
    }
    Success()
  }

  def removeItemsFromAdActiveTable(contract: UpdateAdActiveTableContractV2): Try[Unit] = {
    val keyConditions = new HashMap[String, Condition]
    keyConditions.put("adPlusAccountId", new Condition().withComparisonOperator(ComparisonOperator.EQ).withAttributeValueList(new AttributeValue(contract.getImpressionAdAccountValue)))
    val queryRequest = new QueryRequest().withTableName("AdActiveTable").withIndexName("adPlusAccountId-index").withKeyConditions(keyConditions)
    val items = DynamoDBEngine.client.query(queryRequest).getItems()

    if (items.size == 0) {
      Failure(new Error("Unknown account or ad"))
    } else {
      val deleteList = new ArrayList[WriteRequest]
      for (i <- 0 until items.size()) {
        val key = new HashMap[String, AttributeValue]
        key.put("inTemplate", items.get(i).get("inTemplate"))
        key.put("adPlusAccountId", new AttributeValue(contract.getImpressionAdAccountValue))
        deleteList.add(new WriteRequest().withDeleteRequest(new DeleteRequest().withKey(key)))
      }
      val requestItems = new HashMap[String, java.util.List[WriteRequest]]();
      requestItems.put("AdActiveTable", deleteList)
      val batchWriteItemRequest = new BatchWriteItemRequest().withRequestItems(requestItems)
      DynamoDBEngine.client.batchWriteItem(batchWriteItemRequest);
    }
    Success()
  }

  def retrieveInvoice(accountId: String, orderId: String) {
    val key = new HashMap[String, AttributeValue]
    key.put("accountId", new AttributeValue(accountId))
    key.put("orderId", new AttributeValue(orderId))
    val getItemRequest = new GetItemRequest().withTableName("AccountTransactionTable").withKey(key)
    val item = DynamoDBEngine.client.getItem(getItemRequest).getItem()
    item.get("detail");

  }

  def pickAdsFrom(contract: PickAdFromContractV2): List[PickAdData] = {
    AccountServiceRegistry.service.retrieveAccount(contract.adAccountId) match {
      case Success(Some(accountData)) => {
        retrieveAccountAd(contract.adAccountId, contract.adId) match {
          case Some(adDataV2) => {
            CDRServiceRegistry.service.retrieveBestMatchDestination(contract.phone) match {
              case Success(Some(tuple)) => {
                CDRServiceRegistry.service.retrieveBestMatchTemplate(contract.dial) match {
                  case Success(Some(bestMatchTemplateData)) => {
                    CDRServiceRegistry.service.retrieveRoute(bestMatchTemplateData.destinationId) match {
                      case Success(Some(provider)) => {
                        CDRServiceRegistry.service.getDID match {
                          case Success(Some(didData)) => {
                            var buyRate: BigDecimal = {
                              GatewayAccountServiceRegistry.service.retriveBuyRateByDestinationProvider(bestMatchTemplateData.destinationId, provider.providerid) match {
                                case Success(Some(value)) => value.rate.toFloat
                                case _ => 0
                              }
                            }
                            
                            
                            val id = System.currentTimeMillis().toString
                            val accountId = adDataV2.accountId
                            val adId = adDataV2.adId
                            val name = adDataV2.name
                            val ivrmedialocation = adDataV2.mediaIVRId

                            val inDestinationId = tuple._1
                            val outDestinationId = bestMatchTemplateData.destinationId
                            val description = adDataV2.description

                            val bypassSwitch = adDataV2.bypassSwitch
                            val did = didData
                            val providerId = provider.providerid
                            val providerGatewayAddress = provider.endpointip
                            val providerPrefix = provider.prefix
                            
                            
                            var maxDuration:Int = adDataV2.maxDuration;
                            
                            var rate=GatewayAccountServiceRegistry.service.retrieveSellRateByDestination(bestMatchTemplateData.destinationId) match {
                              case Success(Some(rateData)) => rateData.rate*maxDuration
                              case _ => buyRate*1.5*maxDuration;
                            }
                            
                            rate=rate.setScale(2, BigDecimal.RoundingMode.CEILING)
                            
                            if(rate==0)
                            	rate=adDataV2.adPrice
                            
                            buyRate=buyRate.setScale(4, BigDecimal.RoundingMode.CEILING)	
                            
                            
                            val inNumber = contract.phone
                            val outNumber = contract.dial
                            val inDestination = tuple._2
                            val outDestination = bestMatchTemplateData.destinationName
                            if (bypassSwitch) {
                              List(PickAdData(id = id, accountId = accountId, name = name, adId = adId, medialocation = "", ivrmedialocation = ivrmedialocation, "0", maxduration = 0, inDestinationId = inDestinationId, outDestinationId = outDestinationId, did = "bypass", description = description, providerId = "bypass", providerGatewayAddress = "bypass", providerPrefix = providerPrefix, rate = 0, rateBuy = 0, inNumber = inNumber, outNumber = outNumber, inDestination = inDestination, outDestination = outDestination))
                            } else {
                              List(PickAdData(id = id, accountId = accountId, name = name, adId = adId, medialocation = "", ivrmedialocation = ivrmedialocation, "0", maxduration = maxDuration, inDestinationId = inDestinationId, outDestinationId = outDestinationId, did = did, description = description, providerId = providerId.toString, providerGatewayAddress = providerGatewayAddress, providerPrefix = providerPrefix, rate = rate, rateBuy = buyRate, inNumber = inNumber, outNumber = outNumber, inDestination = inDestination, outDestination = outDestination))
                            }
                          }
                          case Success(None) => Nil
                          case Failure(t) => Nil
                        }
                      }
                      case Success(None) => Nil
                      case Failure(t) => Nil
                    }
                  }
                  case Success(None) => Nil
                  case Failure(t) => Nil
                }
              }
              case Success(None) => Nil
              case Failure(t) => Nil

            }
          }
          case None => Nil
        }
      }
      case Success(None) => Nil
      case Failure(t) => Nil
    }
  }

  def updateBalance(accountId: String, adId: String): Try[Unit] = {
    try {
      retrieveAccountAd(accountId, adId) match {
        case Some(adDataV2) => {
          val key = new HashMap[String, AttributeValue]
          key.put("accountId", new AttributeValue(adDataV2.accountId))
          key.put("adId", new AttributeValue(adDataV2.adId))
          val items = new HashMap[String, AttributeValueUpdate]
          items.put("balance", new AttributeValueUpdate().withAction(AttributeAction.ADD).withValue(new AttributeValue().withN(adDataV2.adPrice.toString)))
          items.put("numberOfCalls", new AttributeValueUpdate().withAction(AttributeAction.ADD).withValue(new AttributeValue().withN("1")))

          val expected = new HashMap[String, ExpectedAttributeValue];
          val expectedValue = new ExpectedAttributeValue().withComparisonOperator(ComparisonOperator.LT).withValue(new AttributeValue().withN(adDataV2.budget.toString))
          expected.put("balance", expectedValue)
          val updateItemRequest = new UpdateItemRequest().withTableName("AccountAdTable").withKey(key).withAttributeUpdates(items).withExpected(expected)
          val result = DynamoDBEngine.client.updateItem(updateItemRequest);

          if (adDataV2.balance + adDataV2.adPrice + adDataV2.impression > adDataV2.budget) {
            toogleAd(ActivateAdContractV2(accountId, adId, false))
          }
          Success(())
        }
        case None => Failure(new UnknownAccount)
      }
    } catch {
      case t: Throwable => {
        toogleAd(ActivateAdContractV2(accountId, adId, false))
        Failure(new NotEnoughFund)
      }
    }
  }
  
 def updateBalance(accountId: String, adId: String,amount:BigDecimal) {
    try {
      retrieveAccountAd(accountId, adId) match {
        case Some(adDataV2) => {
          val key = new HashMap[String, AttributeValue]
          key.put("accountId", new AttributeValue(adDataV2.accountId))
          key.put("adId", new AttributeValue(adDataV2.adId))
          val items = new HashMap[String, AttributeValueUpdate]
          items.put("balance", new AttributeValueUpdate().withAction(AttributeAction.ADD).withValue(new AttributeValue().withN(amount.toString)))
          items.put("numberOfCalls", new AttributeValueUpdate().withAction(AttributeAction.ADD).withValue(new AttributeValue().withN("1")))

          val expected = new HashMap[String, ExpectedAttributeValue];
          val expectedValue = new ExpectedAttributeValue().withComparisonOperator(ComparisonOperator.LT).withValue(new AttributeValue().withN(adDataV2.budget.toString))
          expected.put("balance", expectedValue)
          val updateItemRequest = new UpdateItemRequest().withTableName("AccountAdTable").withKey(key).withAttributeUpdates(items).withExpected(expected)
          val result = DynamoDBEngine.client.updateItem(updateItemRequest);

          if (adDataV2.balance + adDataV2.adPrice + adDataV2.impression > adDataV2.budget) {
            toogleAd(ActivateAdContractV2(accountId, adId, false))
          }
          Success(())
        }
        case None => Failure(new UnknownAccount)
      }
    } catch {
      case t: Throwable => {
        toogleAd(ActivateAdContractV2(accountId, adId, false))
        Failure(new NotEnoughFund)
      }
    }
  }
  
  
  

  def addTransaction(contract: TransactionDataV2): Try[Unit] = {
    try {
      val putItems = new HashMap[String, AttributeValue]
      putItems.put("accountId", new AttributeValue(contract.accountId))
      putItems.put("orderId", new AttributeValue(contract.orderId))
      putItems.put("date", new AttributeValue().withN(contract.date.toString))
      putItems.put("amount", new AttributeValue().withN(contract.amount.toString))
      putItems.put("transactionType", new AttributeValue(contract.transactionType))
      putItems.put("detail", new AttributeValue(contract.detail))
      putItems.put("accountDetail", new AttributeValue(contract.accountDetail))
      putItems.put("dateFormat", new AttributeValue().withS(new Date().toString))
      val putItemRequest = new PutItemRequest().withTableName("AccountTransactionTable").withItem(putItems);
      DynamoDBEngine.client.putItem(putItemRequest)
      Success(())
    } catch {
      case t: Throwable => Failure(t)
    }
  }
  
  def addCCTransaction(contract:AddCCTransactionContract): Try[Unit] = {
    try {
      val putItems = new HashMap[String, AttributeValue]
      val timeStamp=System.currentTimeMillis().toString
      putItems.put("accountId", new AttributeValue(contract.accountId))
      putItems.put("orderId", new AttributeValue("P"+contract.orderId+" "+contract.authCode))
      putItems.put("amount", new AttributeValue().withN(contract.amount.toString))
      putItems.put("date", new AttributeValue().withN(timeStamp))      
      putItems.put("authCode", new AttributeValue(contract.authCode))      
      putItems.put("transactionType", new AttributeValue("CC"))
      putItems.put("dateFormat", new AttributeValue().withS(new Date().toString))
      val putItemRequest = new PutItemRequest().withTableName("AccountTransactionTable").withItem(putItems);
      DynamoDBEngine.client.putItem(putItemRequest)
      Success(())
    } catch {
      case t: Throwable => Failure(t)
    }
  }
  

  def retrieveTransactions(accountId: String, orderId: String): List[TransactionDataV2] = {
    val queryRequest = new QueryRequest()
    val keyConditions = new HashMap[String, Condition]
    keyConditions.put("accountId", new Condition().withComparisonOperator(ComparisonOperator.EQ).withAttributeValueList(new AttributeValue(accountId)));
    keyConditions.put("orderId", new Condition().withComparisonOperator(ComparisonOperator.LT).withAttributeValueList(new AttributeValue(orderId)));

    queryRequest.withTableName("AccountTransactionTable").withKeyConditions(keyConditions);
    queryRequest.setScanIndexForward(false);
    queryRequest.setLimit(20);
    val result = DynamoDBEngine.client.query(queryRequest)
    val items = result.getItems()

    val listBuffer = new ListBuffer[TransactionDataV2]()
    for (i <- 0 until items.size()) {
      val item = items.get(i)
      val accountId: String = item.get("accountId").getS
      val orderId: String = item.get("orderId").getS;
      val date: Long = item.get("date").getN.toLong;
      val amount: BigDecimal = item.get("amount").getN.toDouble
      var transactionType: String = item.get("transactionType").getS;
      var detail: String = ""
      var accountDetail: String = ""      
      if(transactionType=="O") {
    	  detail = item.get("detail").getS;
      	  accountDetail = item.get("accountDetail").getS()
      } else if(transactionType=="CC") {
        transactionType=transactionType+" Auth. "+item.get("authCode").getS;
      }
      

      listBuffer.append(new TransactionDataV2(accountId, orderId, date, amount, transactionType, detail, accountDetail))

    }
    listBuffer.toList
  }

  val pool = new JedisPool(new Config(), GlobalSettings.redisEndPoint, 6379, 2000);
  def retrieveCachedCallRedis(number: String): Try[PickAdData] = {
    try {
      val jedis = pool.getResource()
      val adValues = jedis.get(number)
      jedis.del(number)
      pool.returnResourceObject(jedis)
      if (adValues != null) {
        PickAdData.parse(Json.parse(adValues)) match {
          case Some(pickData) => {
            if (number == pickData.inNumber) {
              Success(pickData)
            } else {
              Failure(new IllegalArgumentException("Inconsistent numbers"))
            }
          }
          case _ => Failure(new IllegalArgumentException("Parse Errors"))
        }
      } else {
        Failure(new IllegalArgumentException("Unknown Call"))
      }
    } catch {
      case t: Throwable => Failure(t)
    }
  }

  def cacheCallRedis(number: String, adPickDataJson: String): Try[Unit] = {
    try {
      PickAdData.parse(Json.parse(adPickDataJson)) match {
        case Some(adPickData) => {
          cacheCall(number, adPickDataJson)
          val jedis = pool.getResource()
          jedis.set(adPickData.inNumber, adPickData.toString)
          jedis.expire(adPickData.inNumber, GlobalSettings.callExpire)
          pool.returnResourceObject(jedis)
        }
        case r @ _ => r
      }
      Success(())
    } catch {
      case t: Throwable => Failure(t)
    }
  }

  def cacheCall(number: String, adPickDataJson: String): Try[Unit] = {
    def cacheNumber(adPickData: PickAdData) {
      val key = new Hashtable[String, AttributeValue]
      key.put("id", new AttributeValue(adPickData.inNumber))
      val updateItems = new HashMap[String, AttributeValueUpdate]
      updateItems.put("adPickData", new AttributeValueUpdate().withValue(new AttributeValue(adPickData.toString)))
      updateItems.put("timeExpire", new AttributeValueUpdate().withValue(new AttributeValue().withN(System.currentTimeMillis().toString)))
      updateItems.put("lastcalltime", new AttributeValueUpdate().withValue(new AttributeValue(new Date().toString)))
      val updateItemRequest = new UpdateItemRequest().withTableName("SwitchCacheTable").withAttributeUpdates(updateItems).withKey(key)
      DynamoDBEngine.client.updateItem(updateItemRequest);
    }
    try {
      PickAdData.parse(Json.parse(adPickDataJson)) match {
        case Some(adPickData) => {
          cacheNumber(adPickData)
          Success(())
        }
        case r @ _ => Failure(new IllegalArgumentException("Inconsistent numbers"))
      }
    } catch {
      case t: Throwable => Failure(t)
    }
  }

  def retrieveCachedCall(number: String): Try[PickAdData] = {
    try {
      val key = new Hashtable[String, AttributeValue]
      key.put("id", new AttributeValue(number))
      val getItemRequest = new GetItemRequest().withTableName("SwitchCacheTable").withKey(key)
      val result = DynamoDBEngine.client.getItem(getItemRequest)
      if (result.getItem() != null) {
        val adValues = result.getItem().get("adPickData").getS()
        val timeExpire = result.getItem().get("timeExpire").getN().toLong
        if (System.currentTimeMillis() - timeExpire < GlobalSettings.callExpire * 1000) {
          PickAdData.parse(Json.parse(adValues)) match {
            case Some(pickData) => {
              if (number == pickData.inNumber) {
                Success(pickData)
              } else {
                Failure(new IllegalArgumentException("Inconsistent numbers"))
              }
            }
            case _ => Failure(new IllegalArgumentException("Parse Errors"))
          }
        } else {
          Failure(new IllegalArgumentException("Unknown Call"))
        }
      } else {
        Failure(new IllegalArgumentException("Unknown Call"))
      }
    } catch {
      case t: Throwable => Failure(t)
    }
  }

  def retrieveCachedCallForCDR(number: String): Try[PickAdData] = {
    try {
      val key = new Hashtable[String, AttributeValue]
      key.put("id", new AttributeValue(number))
      val getItemRequest = new GetItemRequest().withTableName("SwitchCacheTable").withKey(key)
      val result = DynamoDBEngine.client.getItem(getItemRequest)
      if (result.getItem() != null) {
        val adValues = result.getItem().get("adPickData").getS()
        val timeExpire = result.getItem().get("timeExpire").getN().toLong
        PickAdData.parse(Json.parse(adValues)) match {
          case Some(pickData) => {
            if (number == pickData.inNumber) {
              Success(pickData)
            } else {
              Failure(new IllegalArgumentException("Inconsistent numbers"))
            }
          }
          case _ => Failure(new IllegalArgumentException("Parse Errors"))
        }
      } else {
        Failure(new IllegalArgumentException("Unknown Call"))
      }
    } catch {
      case t: Throwable => Failure(t)
    }
  }

  def listUnApprovedAd(): Try[List[AdDataV2]] = {
    try {
      val queryRequest = new QueryRequest()
      val keyConditions = new HashMap[String, Condition]
      keyConditions.put("approved", new Condition().withComparisonOperator(ComparisonOperator.EQ).withAttributeValueList(new AttributeValue("0")));
      queryRequest.withTableName("AccountAdTable").withKeyConditions(keyConditions);
      queryRequest.withIndexName("approved-index")
      val result = DynamoDBEngine.client.query(queryRequest)
      val items = result.getItems()
      val listBuffer = new ListBuffer[AdDataV2]()
      for (i <- 0 until items.size()) {
        val item = items.get(i)
        val accountId: String = item.get("accountId").getS
        val adId: String = item.get("adId").getS
        this.retrieveAccountAd(accountId, adId) match {
          case Some(data) => listBuffer.append(data)
          case _ =>
        }
      }
      Success(listBuffer.toList)
    } catch {
      case t: Throwable => Failure(t)
    }
  }

  def togggleApprove(contract: ApproveAdContract): Try[Unit] = {
    try {
      val key = new HashMap[String, AttributeValue]
      key.put("accountId", new AttributeValue(contract.accountId));
      key.put("adId", new AttributeValue(contract.adId));
      val attributeUpdates = new HashMap[String, AttributeValueUpdate]
      attributeUpdates.put("approved", new AttributeValueUpdate().withValue(new AttributeValue(if (contract.approve) "1" else "0")))
      val updateItemRequest = new UpdateItemRequest().withTableName("AccountAdTable").withKey(key).withAttributeUpdates(attributeUpdates)
      DynamoDBEngine.client.updateItem(updateItemRequest);
      if (contract.approve) {
        toogleAd(ActivateAdContractV2(accountId = contract.accountId, adId = contract.adId, false))
      }
      Success(())
    } catch {
      case t: Throwable => Failure(t)
    }
  }

  
  def pickAds(contract: PickAdContractV2): List[PickAdData] = {
    def retrieveAdIds(templateId: Int, incomingId: Int): Option[String] = {
      try {
        val inTemplate = incomingId + "-" + templateId;
        val inTemplateGeneric = "0-" + templateId;
        val keyConditions = new HashMap[String, Condition];
        keyConditions.put("inTemplate", new Condition().withComparisonOperator(ComparisonOperator.EQ).withAttributeValueList(new AttributeValue(inTemplate)));
        val queryRequest = new QueryRequest().withTableName("AdActiveTable").withKeyConditions(keyConditions)
        queryRequest.setLimit(1)
        val result = DynamoDBEngine.client.query(queryRequest);
        if (result.getCount() == 0) {
          val keyConditions = new HashMap[String, Condition];
          keyConditions.put("inTemplate", new Condition().withComparisonOperator(ComparisonOperator.EQ).withAttributeValueList(new AttributeValue(inTemplateGeneric)));
          val queryRequest = new QueryRequest().withTableName("AdActiveTable").withKeyConditions(keyConditions)
          queryRequest.setLimit(1)
          val result = DynamoDBEngine.client.query(queryRequest);
          if (result.getCount() == 0) {
            None
          } else {
            Some(result.getItems().get(0).get("adPlusAccountId").getS())
          }
        } else {
          Some(result.getItems().get(0).get("adPlusAccountId").getS())
        }
      } catch {
        case t: Throwable => t.printStackTrace(); None
      }
    }

    CDRServiceRegistry.service.retrieveBestMatchDestination(contract.phone) match {
      case Success(Some(tuple)) => {
        CDRServiceRegistry.service.retrieveBestMatchTemplate(contract.dial) match {
          case Success(Some(bestMatchTemplateData)) => {
            retrieveAdIds(bestMatchTemplateData.templateId, tuple._1) match {
              case Some(id) => {
                id.split("\\+") match {
                  case Array(adId, accountId) => {
                    AccountServiceRegistry.service.retrieveAccount(accountId) match {
                      case Success(Some(accountData)) => {
                        CDRServiceRegistry.service.getDID match {
                          case Success(Some(didData)) => {
                            CDRServiceRegistry.service.retrieveRoute(bestMatchTemplateData.destinationId) match {
                              case Success(Some(provider)) => {
                                retrieveAccountAd(accountId, adId) match {
                                  case Some(adDataV2) => {
                                    val buyRate: Float = {
                                      GatewayAccountServiceRegistry.service.retriveBuyRateByDestinationProvider(bestMatchTemplateData.destinationId, provider.providerid) match {
                                        case Success(Some(value)) => value.rate.toFloat
                                        case _ => 0
                                      }
                                    }
                                    val id = System.currentTimeMillis().toString
                                    val accountId = adDataV2.accountId
                                    val adId = adDataV2.adId
                                    val companyName = accountData.companyName
                                    val ivrmedialocation = adDataV2.mediaIVRId
                                    val maxDuration = adDataV2.maxDuration
                                    val bypassSwitch = adDataV2.bypassSwitch
                                    val inDestinationId = tuple._1
                                    val outDestinationId = bestMatchTemplateData.destinationId
                                    val did = didData
                                    val description = adDataV2.description
                                    val providerId = provider.providerid
                                    val providerGatewayAddress = provider.endpointip
                                    val providerPrefix = provider.prefix
                                    val rate = adDataV2.adPrice
                                    val rateBuy: BigDecimal = buyRate
                                    val inNumber = contract.phone
                                    val outNumber = contract.dial
                                    val inDestination = tuple._2
                                    val outDestination = bestMatchTemplateData.destinationName
                                    List(PickAdData(id = id, accountId = accountId, name = companyName, adId = adId, medialocation = "", ivrmedialocation = ivrmedialocation, "0", maxduration = maxDuration, inDestinationId = inDestinationId, outDestinationId = outDestinationId, did = did, description = description, providerId = providerId.toString, providerGatewayAddress = providerGatewayAddress, providerPrefix = providerPrefix, rate = rate, rateBuy = rateBuy, inNumber = inNumber, outNumber = outNumber, inDestination = inDestination, outDestination = outDestination))
                                  }
                                  case _ => Nil
                                }
                              }
                              case _ => Nil
                            }
                          }
                          case _ => Nil
                        }
                      }
                      case _ => Nil
                    }

                  }
                  case _ => Nil
                }
              }
              case _ => Nil
            }
          }
          case _ => Nil
        }
      }
      case _ => Nil
    }
  }
  
  
  
}
