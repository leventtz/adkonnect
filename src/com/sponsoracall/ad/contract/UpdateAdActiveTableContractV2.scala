package com.sponsoracall.ad.contract

case class UpdateAdActiveTableContractV2(accountId:String,adId:String,impression:BigDecimal,impressionView:String,adPrice:BigDecimal,description:String,bypassSwitch:Boolean){
  def getImpressionAdAccountValue=impression+"+"+adId+"+"+accountId
  require(accountId!=null && adId!=null,"AccountId and AdId required")
  require(impressionView!=null ,"null impressionView")
  require(description!=null ,"null description")
}