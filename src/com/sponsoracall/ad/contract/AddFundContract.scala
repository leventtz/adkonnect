package com.sponsoracall.ad.contract

import com.sponsoracall.account.payment.PaymentContract


case class AddFundContract(accountId:String,adId:String,paymentContract:PaymentContract)