package com.sponsoracall.ad.contract
import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.Json
import com.sponsoracall.ad.data.DesignData

object UpdateDesignContractV2 {
  import play.api.libs.json._
  implicit val jsonDesignDataReader = Json.reads[DesignData]
  val jsonReader = Json.reads[UpdateDesignContractV2]
  def parse(value: String): Option[UpdateDesignContractV2] = {
    try {
      Some(jsonReader.reads(Json.parse(value)).get)
    } catch {
      case t: Throwable => {
        None
      }
    }
  }
}



case class UpdateDesignContractV2(accountId:String,adId:String,designData:DesignData) {
  require(accountId!=null && adId!=null && designData!=null, "Missing Account,AdId,DesignData")
  
} 

