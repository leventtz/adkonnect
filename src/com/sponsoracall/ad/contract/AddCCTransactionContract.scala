package com.sponsoracall.ad.contract

case class AddCCTransactionContract(accountId:String,orderId:String,amount:BigDecimal,authCode:String)