package com.sponsoracall.ad.data

object InvoiceItemDataV2 {
  import play.api.libs.json._
  val jsonReader = Json.reads[InvoiceItemDataV2]
  def parse(value: String): Option[InvoiceItemDataV2] = {
    try {
      Some(jsonReader.reads(Json.parse(value)).get)
    } catch {
      case t: Throwable => {
        None
      }
    }
  }
}

case class InvoiceItemDataV2(name:String,impression:BigDecimal,budget:BigDecimal) {
  import play.api.libs.json._
  val jsonWriter=Json.writes[InvoiceItemDataV2]
  override def toString={
    jsonWriter.writes(this).toString
  }     
}