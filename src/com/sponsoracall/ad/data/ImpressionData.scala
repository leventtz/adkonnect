package com.sponsoracall.ad.data
import play.api.libs.json._

case class ImpressionViewData(accountId:String,adId:String,impressionView:String) {
    override def toString = {
    val jsonWriter = Json.writes[ImpressionViewData]
    jsonWriter.writes(this).toString
  }
}

case class ImpressionData(accountId: String, adId: String, impression:BigDecimal,impressionView: String,adPrice:BigDecimal) {
  override def toString = {
    val jsonWriter = Json.writes[ImpressionData]
    jsonWriter.writes(this).toString
  }
} 