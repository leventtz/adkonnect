package com.sponsoracall.ad.data


object OrderTotalDataV2 {
  import play.api.libs.json._
  val jsonReader = Json.reads[OrderTotalDataV2]
  def parse(value: String): Option[OrderTotalDataV2] = {
    try {
      Some(jsonReader.reads(Json.parse(value)).get)
    } catch {
      case t: Throwable => {
        None
      }
    }
  }
}

case class OrderTotalDataV2 (feeItemMessageId:String,total:BigDecimal) {
  require(feeItemMessageId!=null && !feeItemMessageId.isEmpty(),"feeItemMessageId required")
import play.api.libs.json._
  val jsonWriter=Json.writes[OrderTotalDataV2]
  override def toString={
    jsonWriter.writes(this).toString
  }     
}