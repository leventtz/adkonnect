package com.sponsoracall.ad.data

//transaction type O order CC creditcard

case class TransactionDataV2(accountId:String,orderId:String,date:Long,amount:BigDecimal,transactionType:String,detail:String,accountDetail:String) {
  require(accountId!=null && accountId.nonEmpty,"accountId required")
  require(orderId!=null && orderId.nonEmpty,"AccountId orderId")
  import play.api.libs.json._
  val jsonWriter=Json.writes[TransactionDataV2]
  override def toString={
    jsonWriter.writes(this).toString
  }
}