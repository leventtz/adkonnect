package com.sponsoracall.ad.data

import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.JsValue

object PickAdData {
  import play.api.libs.json._
  implicit val jsonReader = Json.reads[PickAdData]
  implicit val jsonWriter = Json.writes[PickAdData]
  def parse(value: JsValue): Option[PickAdData] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => t.printStackTrace();None
    }
  }
}

case class PickAdData(
    id:String, 
    accountId: String,
    name:String,
    adId: String,
    medialocation: String,
    ivrmedialocation:String,
    ivrid:String,
    maxduration:Int,
    inDestinationId:Int,
    outDestinationId:Int,
    did:String,
    description: String, 
    providerId:String,
    providerGatewayAddress:String,
    providerPrefix:String,
    rate:BigDecimal,
    rateBuy:BigDecimal,
    inNumber:String,
    var outNumber:String,
    inDestination:String,
    outDestination:String) {
  import play.api.libs.json._
  override def toString = {
    PickAdData.jsonWriter.writes(this).toString
  }
}