package com.sponsoracall.ad.data

import play.api.libs.functional.syntax.toFunctionalBuilderOps

case class DesignData(impressionView: String, mediaId: String, header: String, adWords: String, websiteLink: String, websiteLinkPrompt: String, phoneLink: String, phoneLinkPrompt: String) {
  val description = {
    val forbiddenTags = List("<script>", "</script>")
    def isDesignAllowed(description: String): Boolean = {
      forbiddenTags.map(t => description.indexOf(t)).sum < 0
    }

    val headerHtml = if (header != null && header.nonEmpty) {
      "<h2>" + header + "</h2>"
    } else {
      ""
    }

    val imageHtml = "<img src='" + mediaId + "'/>"

    val adWordsHtml = if (adWords != null && adWords.nonEmpty) {
      "<h2>" + adWords + "</h2>"
    } else {
      ""
    }

    val webLinkHtml = if (websiteLink != null && websiteLink.nonEmpty) {
      val websiteLinkPromptHtml = if (websiteLinkPrompt != null && websiteLinkPrompt.nonEmpty) {
        websiteLinkPrompt
      } else {
        ""
      }
      "<h3><a href='" + websiteLink + "'>" + websiteLinkPromptHtml + "</a></h3>"
    } else {
      ""
    }

    val phoneLinkHtml = if (phoneLink != null && phoneLink.nonEmpty) {
      val phoneLinkPromptHtml = if (phoneLinkPrompt != null && phoneLinkPrompt.nonEmpty) {
        phoneLinkPrompt
      } else {
        ""
      }
      "<h3><a href='tel:" + phoneLink + "'>" + phoneLinkPromptHtml + "</a></h3>"
    } else {
      ""
    }

    val descriptionHtml= headerHtml + "<br>" + imageHtml + "<br>" + adWords + "<br>" + webLinkHtml + "<br>" + phoneLinkHtml;
    
    
    

    if (isDesignAllowed(descriptionHtml))
      descriptionHtml
    else
      "Invalid HTML Tags"
  }

  require(mediaId != null && impressionView != null, "Media and Impression View Required")
  import play.api.libs.json._
  val jsonWriter = Json.writes[DesignData]
  override def toString = {
    jsonWriter.writes(this).toString
  }
}