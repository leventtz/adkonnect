//
//  TermsViewController.m
//  Dialer
//
//  Created by Levent Tinaz on 5/9/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "TermsViewController.h"

@interface TermsViewController ()

@end

@implementation TermsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

   

        self.loadIndicator.hidden=NO;
        [self.loadIndicator startAnimating];
        [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
        NSString *urlString=[DialerUtil urlString:@"terms"];
        NSURL *termsUrl=[NSURL URLWithString:urlString];
        NSURLRequest *request=[NSURLRequest requestWithURL:termsUrl];
        [self.webView loadRequest:request];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self stopIndicator];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [self stopIndicator];
}


- (void)stopIndicator {
    self.loadIndicator.hidden=YES;
    [self.loadIndicator stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
