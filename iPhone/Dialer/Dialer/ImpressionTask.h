//
//  ImpressionTask.h
//  Dialer
//
//  Created by Levent Tinaz on 9/17/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBUtil.h"
#import "DialerUtil.h"
#import "AdData.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
@interface ImpressionTask : NSObject

{
    NSMutableData *nsData;
    NSString *dialNumber;
}

@property int statusCode;
@property NSArray *impressions;
@property AdData *adData;
@property NSError *error;
-(void)retrieveImpression:(NSString *)dialNumber;
@end
