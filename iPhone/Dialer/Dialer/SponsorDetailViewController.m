//
//  SponsorDetailViewController.m
//  Dialer
//
//  Created by Levent Tinaz on 5/7/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "SponsorDetailViewController.h"

@interface SponsorDetailViewController ()

@end

@implementation SponsorDetailViewController {
    NSTimer *timer;
}

@synthesize adData=_adData;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.webView loadHTMLString:self.adData.description baseURL:nil];
    if(self.adData.adId==nil){
        self.buttonOk.hidden=YES;
        self.buttonCancel.hidden=YES;
        self.buttonThanks.hidden=YES;

    } else if([self.adData.adId isEqualToString:@"0"]) {
       self.buttonThanks.hidden=NO;
        self.buttonOk.hidden=YES;
        self.buttonCancel.hidden=YES;
        
    } else {
        self.buttonThanks.hidden=YES;
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    if(self.adData.did!=NULL) {
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        BOOL dontShowDurationDialog=[standardUserDefaults valueForKey:@"dontShowDurationDialog"];
        if(!dontShowDurationDialog && self.adData.maxDuration>0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Call Duration"
                                                        message:[NSString stringWithFormat:@"You have %d minutes.",self.adData.maxDuration]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:@"Don't Show Again",nil];
        [alert show];
        }
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {

    if (buttonIndex == 1) {
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [standardUserDefaults setBool:YES forKey:@"dontShowDurationDialog"];
        [standardUserDefaults synchronize];
    }
}
- (void)viewWillDisappear:(BOOL)animated {

}


- (IBAction)buttonOkAction:(UIButton *)sender {
    
    if([[self.adData adCrypto] length]!=0){
        NSString *serviceName=@"devicecall";
        if(self.adData.maxDuration==0){
            serviceName=@"devicecalbypass";
        }
        
        [[DialerDialTask alloc] initWithDialNumber:self.adData.dialNumber withCryptoAd:self.adData.adCrypto withServiceName:serviceName];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeStyle:NSDateFormatterMediumStyle];
        [dateFormat setDateStyle:NSDateFormatterMediumStyle];
        
        NSDate *now = [[NSDate alloc] init];
        DBUtil *db=[[DBUtil alloc] init];
        [db insertCallDateTime:[NSString stringWithFormat:@"%@",[dateFormat stringFromDate:now]] withDialNumber:self.adData.dialNumber withAdId:self.adData.adId];
        [db insertAd:self.adData.adId withCompanyName:self.adData.name withMediaDescription:self.adData.description withMediaLocation:self.adData.mediaLocation];
        
        
        
        NSString *urlString=[NSString stringWithFormat:@"tel://%@",self.adData.did];
        if(self.adData.maxDuration==0) {
            urlString=[NSString stringWithFormat:@"tel://%@",self.adData.dialNumber];
        }
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        
        self.buttonOk.hidden=YES;
        self.buttonCancel.hidden=YES;
        self.buttonThanks.hidden=NO;
    }
}

@end
