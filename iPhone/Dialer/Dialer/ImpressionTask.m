//
//  ImpressionTask.m
//  Dialer
//
//  Created by Levent Tinaz on 9/17/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "ImpressionTask.h"
@implementation ImpressionTask
@synthesize impressions;
@synthesize adData;


-(void)retrieveImpression:(NSString *)_dialNumber {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    dialNumber=_dialNumber;

    NSDictionary *dict=[DBUtil getRegisterInfo];
    NSString *countryCode=[dict valueForKey:@"countryCode"];
    NSString *email=[dict valueForKey:@"email"];
    NSString *phoneNumber=[dict valueForKey:@"phone"];
    NSString *deviceId=[dict valueForKey:@"device"];
    NSString *postalCode=[dict valueForKey:@"postalCode"];
    nsData=[[NSMutableData alloc] init];
    
    
    
    NSString *urlString=[DialerUtil urlString:@"deviceImpression"];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];
    
    [request setHTTPMethod:@"POST"];
    
    id uniqueDeviceId=[DialerUtil getUniqueDeviceId:deviceId];
    
    NSString *body=[NSString stringWithFormat:@"dial=%@&country=%@&email=%@&phone=%@&device=%@&uniquedeviceid=%@&postalCode=%@",dialNumber,countryCode,email,phoneNumber,deviceId,uniqueDeviceId,postalCode];
    
    [request setHTTPBody:[NSData dataWithBytes:[body UTF8String] length:[body length]]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}

-(void)connection:(NSConnection *)connection didFailWithError:(NSError *)error {
    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    [alertView show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    self.error=error;
    
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSHTTPURLResponse* httpResponse=(NSHTTPURLResponse *)response;
    
    self.statusCode=[httpResponse statusCode];
    
    nsData=[[NSMutableData alloc] init];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [nsData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *) connection {
    
    NSError *myError = nil;
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:nsData options:NSJSONReadingMutableContainers error:&myError];
    
    
    self.impressions=[res valueForKey:@"impressions"];
    if(self.impressions==NULL) {
        NSString *adString=[res valueForKeyPath:@"ad"];
        if(adString!=NULL) {
            NSDictionary *noAd=[NSJSONSerialization JSONObjectWithData:[adString dataUsingEncoding:NSUTF8StringEncoding]  options:NSJSONReadingMutableContainers error:&myError];
            self.adData=[[AdData alloc] init];
            self.adData.description=[noAd valueForKey:@"description"];
            self.adData.adId=@"0";
        }    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

}
@end

