//
//  RegisterViewController.h
//  Dialer
//
//  Created by Levent Tinaz on 5/5/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DialerUtil.h"
#import "RegisterTask.h"
#import "DBUtil.h"

@interface RegisterViewController : UIViewController
{
    NSMutableData *nsdata;
    NSMutableArray *countries;

}
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;
@property (weak, nonatomic) IBOutlet UIPickerView *countryPicker;
@property (weak, nonatomic) IBOutlet UITextField *textCountryCode;
@property (weak, nonatomic) IBOutlet UITextField *textEmail;
@property (weak, nonatomic) IBOutlet UITextField *textPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *textPostalCode;

- (IBAction)SubmitAction:(UIButton *)sender;


@end



