//
//  ImpressionViewController.m
//  Dialer
//
//  Created by Levent Tinaz on 9/17/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "ImpressionViewController.h"

@interface ImpressionViewController ()
   @end

@implementation ImpressionViewController
@synthesize adData;
@synthesize impressions;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setContentInset:UIEdgeInsetsMake(20,
                                                     self.tableView.contentInset.left,
                                                     self.tableView.contentInset.bottom,
                                                     self.tableView.contentInset.right)];
    self.tableView.rowHeight=93;
    self.tableView.hidden=YES;
}




-(void)viewDidAppear:(BOOL)animated {
    if(adData!=NULL)
        [self performSegueWithIdentifier:@"fromImpressionView" sender:self.tableView];
    
    self.tableView.hidden=NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - collection view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [impressions count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *impressionString=[impressions objectAtIndex:indexPath.row];
    NSError *myError = nil;
    NSData *impressionData=[NSData dataWithBytes:[impressionString UTF8String] length:[impressionString length]];
    NSDictionary *impression=[NSJSONSerialization JSONObjectWithData:impressionData options:NSJSONReadingMutableContainers error:&myError];
    
    NSString *accountId=[impression valueForKey:@"accountId"];
    NSString *adId=[impression valueForKeyPath:@"adId"];

    DialerAdTask *adTask=[[DialerAdTask alloc] init];
    [adTask retrieveAd:accountId withAdId:adId withDialNumber:self.strippedString];
    while (adTask.adData==nil) {
        // If A job is finished, a flag should be set. and the flag can be a exit condition of this while loop
        
        // This executes another run loop.
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        
    }
    adData=adTask.adData;
    
    [self performSegueWithIdentifier:@"fromImpressionView" sender:tableView];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    
    NSString *impressionString=[impressions objectAtIndex:indexPath.row];
    NSError *myError = nil;
    NSData *impressionData=[NSData dataWithBytes:[impressionString UTF8String] length:[impressionString length]];
    NSDictionary *impression=[NSJSONSerialization JSONObjectWithData:impressionData options:NSJSONReadingMutableContainers error:&myError];
    
    NSString *impressionView=[impression valueForKey:@"impressionView"];
    NSURL *url = [NSURL URLWithString:impressionView];
    NSData *data = [[NSData alloc]initWithContentsOfURL:url ];
    UIImage *img = [[UIImage alloc]initWithData:data ];
    //UIImageView *imageView=(UIImageView *)[cell viewWithTag:100];
    //imageView.image=img;
    
    cell.backgroundView=[[UIImageView alloc] initWithImage:img];
    //cell.selectedBackgroundView=selectedImageView;
    
    
    return cell;
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"fromImpressionView"]) {
        SponsorDetailViewController *controller=segue.destinationViewController;
        controller.adData=adData;
    }
}



@end