//
//  SPCViewController.m
//  Dialer
//
//  Created by Levent Tinaz on 5/5/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "SPCViewController.h"

@interface SPCViewController ()

@end

@implementation SPCViewController {
    NSString *strippedString;
    ImpressionTask *impressionTask;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(self.dialNumber!=nil && self.dialNumber.length!=0) {
        self.dialLabel.text=self.dialNumber;
        self.backspacebutton.hidden=NO;
    } else {
        self.backspacebutton.hidden=YES;
    }
    self.view.hidden=NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.dialNumber=self.dialLabel.text;
    self.view.hidden=YES;

}
-(IBAction)callNumber:(UIButton *)button {
    NSString *originalString = self.dialLabel.text;
    if(originalString.length>8) {
    
    
     strippedString = [DialerUtil stripString:originalString];
        
    impressionTask=[[ImpressionTask alloc] init];
    [impressionTask retrieveImpression:strippedString];
    while (impressionTask.impressions==nil && impressionTask.adData==nil && impressionTask.error==nil) {
        // If A job is finished, a flag should be set. and the flag can be a exit condition of this while loop
        
        // This executes another run loop.
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        
    }
    
    }
    
}

-(IBAction)digit:(UIButton *)button {
    
    if(button.tag==13 && self.dialLabel.text.length>0) {
        self.dialLabel.text=[self.dialLabel.text substringWithRange:NSMakeRange(0,self.dialLabel.text.length-1)];
        
    } else if(self.dialLabel.text.length<18) {
        NSString *value;
         NSString *fileName;
        switch(button.tag) {
            case 10:
                value=[NSString stringWithFormat:(@"%@%@"),self.dialLabel.text,@"*"];
                fileName=@"starwav.wav";
                break;
            case 11:
                value=[NSString stringWithFormat:(@"%@%@"),self.dialLabel.text,@"#"];
                fileName=@"poundwav.wav";
                break;
            default:
                value=[NSString stringWithFormat:(@"%@%d"),self.dialLabel.text,button.tag];
                fileName=[NSString stringWithFormat:@"%d.wav",button.tag];
                
        }
        self.dialLabel.text=value;
        
        NSString *path = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], fileName];
        
        SystemSoundID soundID;
        
        NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
        
        //Use audio sevices to create the sound
        //
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);

        AudioServicesPlaySystemSound(soundID);

        
    }
    if(self.dialLabel.text.length==0) {
        self.backspacebutton.hidden=YES;
    } else {
        self.backspacebutton.hidden=NO;
    }
    
   
    
    
    
    
    
}

-(void)deleteAll:(UILongPressGestureRecognizer *)gestureRecognizer
{
    self.dialLabel.text=@"";
    self.backspacebutton.hidden=YES;
}

-(void)addPlus:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if(![self.dialLabel.text hasPrefix:@"+"]) {
        self.dialLabel.text=[NSString stringWithFormat:@"%@%@",@"+",self.dialLabel.text];
        self.backspacebutton.hidden=NO;
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UILongPressGestureRecognizer *deleteAll = [[UILongPressGestureRecognizer alloc]
                                               initWithTarget:self action:@selector(deleteAll:)];
    [self.backspacebutton addGestureRecognizer:deleteAll];
    
    
    UILongPressGestureRecognizer *plus = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(addPlus:)];
    [self.zeroButton addGestureRecognizer:plus];
    
    self.backspacebutton.hidden=YES;
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ImpressionViewController *controller=segue.destinationViewController;
    controller.strippedString=strippedString;
    controller.adData=impressionTask.adData;
    controller.impressions=impressionTask.impressions;
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    return self.dialLabel.text.length>4 && impressionTask!=nil && impressionTask.error==nil;
}



@end
