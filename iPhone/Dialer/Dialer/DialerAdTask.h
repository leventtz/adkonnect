//
//  DialerAdTask.h
//  Dialer
//
//  Created by Levent Tinaz on 5/9/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBUtil.h"
#import "DialerUtil.h"
#import "AdData.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
@interface DialerAdTask : NSObject

{
    NSMutableData *nsData;
}

@property int statusCode;
@property AdData *adData;
-(void)retrieveAd:(NSString *)accountId withAdId:(NSString *)adId withDialNumber:(NSString *)dialNumber;
-(void)addPhoneToContacts:(NSString *)number;
@end
