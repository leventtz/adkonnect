//
//  SponsorsTableViewController.m
//  Dialer
//
//  Created by Levent Tinaz on 5/7/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "SponsorsTableViewController.h"
#import "SponsorDetailViewController.h"

@interface SponsorsTableViewController ()

@end

@implementation SponsorsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setContentInset:UIEdgeInsetsMake(20,
                                                     self.tableView.contentInset.left,
                                                     self.tableView.contentInset.bottom,
                                                     self.tableView.contentInset.right)];
    
    UILongPressGestureRecognizer *rec=[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    
    
    
    [self.tableView addGestureRecognizer:rec];
    
    
}
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    DBUtil *db=[[DBUtil alloc] init];
    if(buttonIndex==1) {
        NSString *value=[self.entries objectAtIndex:alertView.tag];
        [db deleteCompany:value];
    }
    self.entries=[db getAllCompanies];
    [self.tableView reloadData];
    [db closeDB];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    if(gestureRecognizer.state==UIGestureRecognizerStateBegan){
        CGPoint p=[gestureRecognizer locationInView:self.tableView];
        NSIndexPath *indexPath=[self.tableView indexPathForRowAtPoint:p];
        if(indexPath!=nil){
            //DBUtil *db=[[DBUtil alloc] init];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Sponsor" message:@"Do you wish to delete this sponsor?"
                                                           delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
            NSLog(@"row %d",indexPath.row);
            [alert setTag:indexPath.row];
            [alert show];
        }
        
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    DBUtil *db=[[DBUtil alloc] init];

    self.entries=[db getAllCompanies];
    
    
    [db closeDB];
    
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.entries count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"%d",indexPath.row);
    static NSString *cellIdentifier=@"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    //cell.textLabel.text=[[self.entries objectAtIndex:indexPath.row] objectAtIndex:1];
    cell.textLabel.text=[self.entries objectAtIndex:indexPath.row];
    return cell;
    
}



 #pragma mark - Navigation
 
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
     if([segue.identifier isEqualToString:@"SponsorDetail"]) {
         NSIndexPath *indexPath=[self.tableView indexPathForSelectedRow];
         SponsorDetailViewController *controller=segue.destinationViewController;
         NSString *name=[self.entries objectAtIndex:indexPath.row];
         DBUtil *db=[[DBUtil alloc] init];
         NSArray *array=[db getAd:name];
         
         if([array count]!=0){
             NSString *name=[[array objectAtIndex:0] objectAtIndex:1];
             NSString *description=[[array objectAtIndex:0] objectAtIndex:2];
             NSString *mediaLocation=[[array objectAtIndex:0] objectAtIndex:3];
             
             AdData *data=[[AdData alloc] init];
             data.name=name;
             data.description=description;
             data.mediaLocation=mediaLocation;
             
             controller.adData=data;
         }
         [db closeDB];
         
        
     }
 }


@end

