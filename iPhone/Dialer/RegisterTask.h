//
//  RegisterTask.h
//  Dialer
//
//  Created by Levent Tinaz on 5/8/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegisterTask : NSObject

{
    NSMutableData *nsData;
}

@property int statusCode;
@property NSString *statusString;
-(void)registerUserPhone:(NSString *)phoneNumber withCountryCode:(NSString *)countryCode withDeviceId:(NSString *)deviceId withEmail:(NSString *)email withPostalCode:(NSString *)postalCode;



@end
