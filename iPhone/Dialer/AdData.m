//
//  AdData.m
//  Dialer
//
//  Created by Levent Tinaz on 5/9/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "AdData.h"

@implementation AdData

@synthesize adId;
@synthesize name;
@synthesize mediaLocation;
@synthesize description;
@synthesize did;
@synthesize adCrypto;
@synthesize dialNumber;
@synthesize maxDuration;


@end
