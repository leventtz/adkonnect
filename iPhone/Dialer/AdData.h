//
//  AdData.h
//  Dialer
//
//  Created by Levent Tinaz on 5/9/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdData : NSObject

@property(nonatomic,copy)  NSString *did;
@property(nonatomic,copy)  NSString *adCrypto;
@property(nonatomic,copy)  NSString *name;
@property(nonatomic,copy)  NSString *mediaLocation;
@property(nonatomic,copy)  NSString *description;
@property(nonatomic,copy)  NSString *adId;
@property(nonatomic,copy)  NSString *dialNumber;
@property  int maxDuration;

@end
