import com.amazonaws.services.dynamodbv2.model.ScanRequest
import java.util.HashMap
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import java.util.ArrayList
import com.amazonaws.services.dynamodbv2.model.WriteRequest
import com.amazonaws.services.dynamodbv2.model.DeleteRequest
import com.amazonaws.services.dynamodbv2.model.BatchWriteItemRequest
import com.amazonaws.services.dynamodbv2.model.ReturnConsumedCapacity
import com.sponsoracall.db.DynamoDBEngine

object DynamoDBUtil extends App {
  def truncateCDR() {
    
    val scanRequest = new ScanRequest().withTableName("AccountCDRTable").withAttributesToGet("accountId", "cdrId").withLimit(15)
    val deleteList = new ArrayList[WriteRequest]
    val items = DynamoDBEngine.client.scan(scanRequest).getItems();
    for (i <- 0 until items.size()) {
      val accountId = items.get(i).get("accountId").getS()
      val cdrId = items.get(i).get("cdrId").getS()
      println(accountId + " " + cdrId)
      val deleteKey = new HashMap[String, AttributeValue]
      deleteKey.put("accountId", new AttributeValue(accountId))
      deleteKey.put("cdrId", new AttributeValue(cdrId))
      val writeRequest = new WriteRequest().withDeleteRequest(new DeleteRequest().withKey(deleteKey))
      deleteList.add(writeRequest)
    }
    if (!deleteList.isEmpty()) {
    	val requestItems = new HashMap[String, java.util.List[WriteRequest]]();      
        requestItems.put("AccountCDRTable", deleteList)
      
      val batchWriteItemRequest = new BatchWriteItemRequest()
        .withReturnConsumedCapacity(ReturnConsumedCapacity.TOTAL).withRequestItems(requestItems)
        val result=DynamoDBEngine.client.batchWriteItem(batchWriteItemRequest)
        println(result)
    }

  }
  truncateCDR
}