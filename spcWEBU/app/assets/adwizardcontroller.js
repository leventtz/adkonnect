var appControllers = angular.module('adwizardcontroller', []);


appControllers.controller('AdWizardController', ['Account','$scope', '$resource', '$http','$routeParams','$modal',function (accountService,$scope,$resource,$http,$routeParams,$modal) {
	
	$scope.clearTemplateData=function() {
		$scope.selectedAds=new Array();
		$scope.adTemplates=new Array();	
		$scope.selectedDestinations=new Array();
		$scope.adSearch='';
		$scope.name='';
		$scope.designData={};
		$scope.designData.mediaId="0";
		$scope.designData.impressionView="0";
		$scope.designData.header="";
		$scope.designData.adWords="";
		$scope.designData.websiteLink="";
		$scope.designData.websiteLinkPrompt="";
		$scope.designData.phoneLink="";
		$scope.designData.phoneLinkPrompt="";
		
		$scope.saveError="";
		
			
		
	};

	$scope.clearTemplateData();
	$scope.page=1;
	
	$scope.retrieveAdTemplate=function() {
		$scope.adTemplates=new Array();
		var searchString=$scope.adSearch==''?'all':$scope.adSearch;
		
		$http.get("adtemplates/"+searchString).success(
			function(data) {
				for(var i=0;i<data.adTemplates.length;i++){
					$scope.adTemplates[i]=angular.fromJson(data.adTemplates[i]);
					$scope.adTemplates[i].qty=0;
				}
			}
		).error(
			
		)
	}
	
	$('#newAd-modal-lg').on('show.bs.modal', function (e) {
		$scope.retrieveAdTemplate();
		$scope.selectedAds=new Array();
	})
	
	$scope.selectedAd=function(adTemplate){
		var index=$scope.selectedAds.indexOf(adTemplate);
		if(index==-1) {
			$scope.selectedAds.push(adTemplate);
			adTemplate.impression=0.01;
			adTemplate.budget=15;
		} else {
			$scope.selectedAds.splice(index,1);
		}
	}
	
	$scope.isSelectedAd=function(adTemplate){
		return $scope.selectedAds.indexOf(adTemplate)!=-1;
	}
	

	$scope.retrieveAccountMedia=function() {
		$scope.accountMediaListIVR=new Array();
		$scope.accountMediaListMedia=new Array();
		
		$http.get("account/"+$scope.ACCOUNT_ID+"/media").success(function(data) {
			for(var i=0;i<data.media.length;i++){
				var value=angular.fromJson(data.media[i]);
				if(value.mediaTypeId=='prompt')
					$scope.accountMediaListIVR.push(value);
				else if(value.mediaTypeId=='picture')
					$scope.accountMediaListMedia.push(value);
			}
			
		}).error(function(data,status){
			if(status!==404)
				alert(data)
		})
	}
	

	$scope.getLineTotal=function(adTemplate){
		return adTemplate.budget;
	}

	
	$scope.getOrderTotal=function() {
		var total=0;
		for(var i=0;i<$scope.selectedAds.length;i++){
			total=total+$scope.getLineTotal($scope.selectedAds[i]);
		}
		return total;
	}


	$scope.saveOrder=function() {
		var lineItems=new Array();

		if($scope.selectedLocations.length==0) {
			$scope.selectedLocations.push(AllUS)
		}
		var selectedLocationsName=new Array();
		for(var i=0;i<$scope.selectedLocations.length;i++){
			selectedLocationsName.push($scope.selectedLocations[i]);
		}			

		for(var i=0;i<$scope.selectedAds.length;i++){
			lineItem={};
			lineItem.accountId=$scope.ACCOUNT_ID;
			lineItem.name=$scope.name;
			lineItem.templateId=$scope.selectedAds[i].id;
			lineItem.budget=$scope.selectedAds[i].budget;
			lineItem.impression=$scope.selectedAds[i].impression;
			lineItem.unitPrice=$scope.selectedAds[i].templateUnitPrice;
			lineItem.promptCost=$scope.isPrompt?$scope.selectedAds[i].templatePromptCost:0;
			lineItem.mediaId=$scope.designData.mediaId;
			lineItem.impressionView=$scope.designData.impressionView;			
			lineItem.mediaIVRId=$scope.mediaIVRId;
			lineItem.maxDuration=$scope.selectedAds[i].maxDuration;
			lineItem.templateName=$scope.selectedAds[i].name;
			lineItem.bypassSwitch=$scope.selectedAds[i].bypassSwitch;
			lineItem.allowRecharge=false;
			lineItem.rechargeAmount=0;
			lineItem.rechargeThreshold=0;
			lineItem.designData=$scope.designData;
			lineItem.incoming=selectedLocationsName;
			lineItems.push(lineItem);
		}		
		var order={};
		order.lineItems=lineItems;
		$scope.cc.expDate=$scope.cc.month+$scope.cc.year;
		$scope.cc.address=$scope.cc.address1;
		
		if(!$scope.cc.address2) {
			$scope.cc.address=$scope.cc.address+" "+$scope.cc.address2;
		}
		$scope.cc.description=$scope.name+" Sponsoracall.com ";
		order.payment=$scope.cc;
		$http.post("ad",angular.toJson(order)).success(function(data){
			$scope.clearCC();
			$scope.cc.address1=undefined;
			$scope.cc.address2=undefined;
			$scope.cc.month=undefined;
			$scope.cc.year=undefined;
			$scope.nextPage();
			$scope.clearTemplateData();
		}).error(function(data){
			$scope.saveError=data;
		})		
	}
	
	$scope.calculateOrder=function() {
		var lineItems=new Array();

		if($scope.selectedLocations.length==0) {
			$scope.selectedLocations.push(AllUS)
		}
		var selectedLocationsName=new Array();
		for(var i=0;i<$scope.selectedLocations.length;i++){
			selectedLocationsName.push($scope.selectedLocations[i]);
		}			

		for(var i=0;i<$scope.selectedAds.length;i++){
			lineItem={};
			lineItem.accountId=$scope.ACCOUNT_ID;
			lineItem.name=$scope.name;
			lineItem.templateId=$scope.selectedAds[i].id;
			lineItem.budget=$scope.selectedAds[i].budget;
			lineItem.impression=$scope.selectedAds[i].impression;
			lineItem.unitPrice=$scope.selectedAds[i].templateUnitPrice;
			lineItem.promptCost=$scope.isPrompt?$scope.selectedAds[i].templatePromptCost:0;
			lineItem.mediaId=$scope.designData.mediaId;
			lineItem.impressionView=$scope.designData.impressionView;			
			lineItem.mediaIVRId=$scope.mediaIVRId;
			lineItem.maxDuration=$scope.selectedAds[i].maxDuration;
			lineItem.templateName=$scope.selectedAds[i].name;
			lineItem.bypassSwitch=$scope.selectedAds[i].bypassSwitch;
			lineItem.allowRecharge=false;
			lineItem.rechargeAmount=0;
			lineItem.rechargeThreshold=0;
			lineItem.designData=$scope.designData;
			lineItem.incoming=selectedLocationsName;
			lineItems.push(lineItem);
		}		

		$http.post("calculateOrder",angular.toJson(lineItems)).success(function(data){
				$scope.totals=new Array();
				for(var i=0;i<data.totals.length;i++){
					$scope.totals[i]=angular.fromJson(data.totals[i]);
				}
				
				$scope.adTotal=$scope.totals[$scope.totals.length-1];
				$scope.cc.amount=$scope.adTotal.total;
		}).error(function(data){

		})		
	}
	$scope.closeWizard= function() {
		$scope.page=1;
		$scope.retrieveAccountAd('Z');
	}
	
	$scope.isPrompt=false;
	$scope.mediaIVRId="0";
	
	$scope.nextPage=function() {
		$scope.page=$scope.page+1;
		if($scope.page==4 ) {
			$scope.retrieveAccountMedia();
		} else if($scope.page==5) {
			$scope.calculateOrder();
		}
	}
	
	$scope.previousPage=function() {
		$scope.page=$scope.page-1;	
	}

	$scope.isShowWizardNavigation=function() {
		if($scope.page==3) {
			return false;
		} 
		return true;
	}
	
	$scope.isNext=function() {
		
		for(var i=0;i<$scope.selectedAds.length;i++) {
			if($scope.selectedAds[i].impression===undefined || $scope.selectedAds[i].budget===undefined) {
				return false;
			}
		}
		
		if(!$scope.name) {
			return false;
		}
		
		if($scope.page==1 && $scope.selectedAds.length==0) {
			return false;
		} else if($scope.page==2 && $scope.selectedLocations.length==0) {
			return false;
		} else if($scope.page==4 && ($scope.designData.mediaId==='0' ||
									$scope.designData.impressionView==='0')) {
			return false;
		} else if($scope.page>5) {
			return false;
		}
		return true;
	}

	$scope.isPrevious=function() {
		if($scope.page==1) {
			return false;
		} else if($scope.page>6) {
			return false;
		}

		return true;
	}
	
	$scope.isSubmitable=function() {
		if($scope.page==6 && 
				$scope.name && 
				$scope.cc.cardNum && 
				$scope.cc.cvv && 
				$scope.cc.month && 
				$scope.cc.year &&
				$scope.cc.firstName && 
				$scope.cc.lastName && 
				$scope.cc.address1 && 
				$scope.cc.city && 
				$scope.cc.state && 
				$scope.cc.zip && 
				$scope.cc.phone)
			return true;
		else
			return false;
	}

	
}]);





