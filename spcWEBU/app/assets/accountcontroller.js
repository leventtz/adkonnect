/* Controllers */

var appControllers = angular.module('accountControllers', []);

appControllers.controller('LoadingCtrl',['$scope',function($scope) {
    $scope.$on('LOAD',function($scope){
    	$scope.loading=true;
    	$("body").css("cursor", "wait");
    });
    $scope.$on('UNLOAD',function($scope){
    	$scope.loading=false;
    	$("body").css("cursor", "default");
    });
		
}]);


appControllers.controller('AccountPasswordActionCtrl', ['$scope','$http',function ($scope,$http,$routeParams) {
	$scope.passwordaction={};    
	$scope.accountpasswordaction = function (type) {
    	var challengeField = $('input#recaptcha_challenge_field').val(),
        responseField  = $('input#recaptcha_response_field').val();    	
        $scope.passwordaction.recaptcha_challenge_field=challengeField;
        $scope.passwordaction.recaptcha_response_field=responseField;    	

        if($scope.passwordaction.recaptcha_response_field=='' || $scope.passwordaction.email=='' ) {
        	return;
        }
        
        $http.post("accountpasswordaction/"+type,angular.toJson($scope.passwordaction))
    	.success(
    		function(data){
    			if(type==0) {
    				window.open("/resetpassword","_self")
    			} else {
        			$scope.errorMessage=data;
        			Recaptcha.reload();    				
    			}
    		}
    	).error(
    		function(errorData,responseCode){
    			if(responseCode=='401') {
    				window.open("/","_self")
    			} else {
    				Recaptcha.reload();
    				$scope.errorMessage=errorData;
    			}
    		}
    	)
    } 
    
}]);


appControllers.controller('ResetPasswordCtrl', ['$scope','$http','validationRegex',function ($scope,$http,validationRegex) {
	$scope.passwordaction={}; 
    $scope.validationRegex = validationRegex;
	
    $scope.accountpasswordaction = function () {
    	var challengeField = $('input#recaptcha_challenge_field').val(),
        responseField  = $('input#recaptcha_response_field').val();    	
        $scope.passwordaction.recaptcha_challenge_field=challengeField;
        $scope.passwordaction.recaptcha_response_field=responseField;
        

        
        $http.post("resetPasswordAction",angular.toJson($scope.passwordaction))
    	.success(
    		function(data){
    			window.open("/","_self");  				
    		}
    	).error(
    		function(errorData,responseCode){
    			if(responseCode=='401') {
    				window.open("/","_self")
    			} else {
    				Recaptcha.reload();
    				$scope.errorMessage=errorData;
    			}
    		}
    	)
    } 
    
}]);



appControllers.controller('LoginCtrl', ['$scope','$http',function ($scope,$http,$routeParams) {
	
	$scope.login={}
	$scope.login.emailPhone='';
	$scope.login.password='';
	$scope.requestSent=false;
	
    $scope.callLogin = function () {

    	$scope.login.email = $("#email").val();
    	$scope.login.password = $("#password").val();
    	$scope.requestSent=true;
    	$http.post("login",$scope.login)
    	.success(
    		function(data){
    			window.open("/","_self")
    		}
    	)
    	.error(
    		function(data, status, headers, config){
   	    			$scope.requestSent=false;
    		}
    	)
    }
    
    
    $scope.shouldDisabled=function() {
    	return $scope.requestSent || $scope.login.emailPhone==null || $scope.login.password==null;
    };
    
    
}]);



appControllers.controller('RegisterCtrl', ['$http','$scope','validationRegex',function ($http,$scope,validationRegex) {
	$scope.login={}
	
	$scope.account={};
	
	$scope.requestSent=false;
		
    $scope.account.companyName = "";
    $scope.account.middleName = "";
    $scope.account.address2 = "";
    $scope.account.phone2 = "";
    $scope.account.password = "";
    $scope.account.country = "US";
    $scope.account.state = "";
    $scope.account.accountType = "A";
    $scope.account.gender = "";
    $scope.account.birthday="";
    $scope.account.active=true;
    $scope.alert = false;
    $scope.validationRegex = validationRegex;

    $scope.save = function () {
    	
        var challengeField = $('input#recaptcha_challenge_field').val(),
        responseField  = $('input#recaptcha_response_field').val();    	
        $scope.account.recaptcha_challenge_field=challengeField;
        $scope.account.recaptcha_response_field=responseField;
    	$http.post("/register",$scope.account)
    		.success(function(data) {
    			window.open('registerconfirm','_self');
    		})
    		.error(function(data) {
        		$scope.errorMessage=data;
        		$scope.serverError=true;
        		$scope.requestSent=false
  				Recaptcha.reload();
    		});
    		$scope.requestSent=true
    }
    
}]);





appControllers.controller('AccountDetailCtrl', ['Account','$scope', '$resource', 'validationRegex',function (accountService,$scope, $resource,validationRegex) {
	$scope.cc={};
	$scope.account={};
	$scope.retrieveAccount=function() {
		accountService.get({accountId:$scope.ACCOUNT_ID},function(account) {
			$scope.account=account;
			$scope.cc.firstName=account.firstName;
			$scope.cc.lastName=account.lastName;
			$scope.cc.country=account.country;
			$scope.cc.address1=account.address1;
			$scope.cc.address2=account.address2;
			$scope.cc.city=account.city;
			$scope.cc.country=account.country;
			$scope.cc.state=account.state;
			$scope.cc.zip=account.postalCode;
			$scope.cc.phone=account.phone1;
		})
	};
	
	
	$scope.clearCC=function() {
		$scope.cc={};
		$scope.cc.firstName=$scope.account.firstName;
		$scope.cc.lastName=$scope.account.lastName;
		$scope.cc.country=$scope.account.country;
		$scope.cc.address1=$scope.account.address1;
		$scope.cc.address2=$scope.account.address2;
		$scope.cc.city=$scope.account.city;
		$scope.cc.country=$scope.account.country;
		$scope.cc.state=$scope.account.state;
		$scope.cc.zip=$scope.account.postalCode;
		$scope.cc.phone=$scope.account.phone1;		
	}
	
	
	
	$scope.retrieveAccount();
	
	$scope.save = function () {
		accountService.save($scope.ACCOUNT_ID,$scope.account,function() {
    		$('#updateAccountModal').modal('hide');
    		$scope.retrieveAccount();
    	},function(error) {
    		$scope.errorMessage=error.data;
    		$scope.serverError=true;
    	});
    }

	
	
	
    $scope.validationRegex=validationRegex;
    

}]);







