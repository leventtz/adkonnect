package controllers

import java.math.BigInteger
import java.security.MessageDigest
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Success
import org.apache.commons.codec.binary.Base64
import com.sponsoracall.account.contract.AccountContractV2
import com.sponsoracall.ad.contract.PickAdContractV2
import com.sponsoracall.ad.data.ImpressionViewData
import com.sponsoracall.ad.data.PickAdData
import com.sponsoracall.ad.data.PickAdData
import com.sponsoracall.ad.service.AdServiceRegistry
import com.sponsoracall.cdr.contract.CreateCDRContract
import com.sponsoracall.cdr.contract.UpdateCDRSummaryContractTable
import com.sponsoracall.cdr.service.CDRServiceRegistry
import com.sponsoracall.exception.PhoneExistException
import com.sponsoracall.util.AdUtil
import model.DeviceAdData
import play.Logger
import play.api.Play.current
import play.api.i18n.Messages
import play.api.libs.Crypto
import play.api.libs.concurrent.Akka
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller
import play.api.mvc.Cookie
import plugins.DestinationUtil
import com.sponsoracall.account.service.AccountServiceRegistry
import com.sponsoracall.ad.contract.PickAdFromContractV2
import com.sponsoracall.gatewayaccount.service.GatewayAccountServiceRegistry

object ApplicationDevice extends Controller {
  def createHit(adData: PickAdData) {
    Akka.system.scheduler.scheduleOnce(0.milliseconds) {
      try {
        CDRServiceRegistry.service.createCDR(CreateCDRContract(adData.accountId, adData.id, adData))
      } catch {
        case e: Throwable => Logger.error(e.toString())
      }
    }
  }

  def updateCDRSummaryTable(adData: PickAdData, billDuration: Int, hit: Int) {
    Akka.system.scheduler.scheduleOnce(0.milliseconds) {
      try {
        CDRServiceRegistry.service.updateCDRSummaryTable(new UpdateCDRSummaryContractTable(adData, billDuration, hit));
      } catch {
        case e: Throwable => Logger.error(e.toString())
      }
    }
  }

  def deviceCountries() = Action { request =>
    Ok(Json.toJson(Map("countryList" -> DestinationUtil.countries.filter(_.countryCode != null).map(_.toString))))
  }

  def deviceRegister() = Action { request =>
    try {
      val postData = request.body.asFormUrlEncoded;
      postData match {
        case Some(postValue) => {
          val countryOption = postValue.get("country");
          val emailOption = postValue.get("email");
          val postalCodeOption = postValue.get("postalCode");
          val phoneOption = postValue.get("phone");
          val deviceOption = postValue.get("device");
          val uniqueDeviceIdOption = postValue.get("uniquedeviceid");
          val phoneType = postValue.get("phoneType");

          val email: String = emailOption.get(0);
          val device: String = deviceOption.get(0);
          val country = countryOption.get(0);
          val uniqueDeviceId: String = uniqueDeviceIdOption.get(0);
          val postalCode: String = postalCodeOption.get(0);
          val phone: String = DestinationUtil.normalizeUserPhoneCode(phoneOption.get(0), country);
          val accountId = (country + "-" + phone)

          val digestValue = doubleSaltIt(device, request.headers.get("hash"));

          (uniqueDeviceId == digestValue) match {
            case true => {
              CDRServiceRegistry.service.retrieveBestMatchDestinationCountryCode(phone) match {
                case Success(Some(bestCountryCode)) => {
                  CDRServiceRegistry.service.retrieveLocationsFromZip(postalCode) match {
                    case Some(x :: xs) => {
                      val contract = AccountContractV2(id = accountId, accountType = "U", companyName = "",
                        firstName = "-", middleName = "", lastName = "-", address1 = "-", address2 = "-",
                        city = "-", state = "-", postalCode = postalCode, country = country,
                        email = email, phone1 = phone, phone2 = phoneType.toString, password = "", birthday = "", gender = "")
                      val result = AccountServiceRegistry.service.addMobileUserAccount(contract)
                      result match {
                        case Success(Some(id)) => Created
                        case Failure(p: PhoneExistException) => Created
                        case _ => BadRequest
                      }
                    }
                    case _ => Conflict("UnknownPostalCode")

                  }
                }
                case _ => Conflict("WrongCountryCode")
              }
            }
            case false => BadRequest
          }
        }
        case _ => BadRequest
      }
    } catch {
      case t: Throwable => BadRequest
    }
  }

  def deviceAd() = Action { request =>
    try {
      val postData = request.body.asFormUrlEncoded;
      postData match {
        case Some(postValue) => {
          val countryOption = postValue.get("country");
          val phoneOption = postValue.get("phone");
          val deviceOption = postValue.get("device");
          val uniqueDeviceIdOption = postValue.get("uniquedeviceid");
          val dialOption = postValue.get("dial");

          val device: String = deviceOption.get(0);
          val country = countryOption.get(0);
          val uniqueDeviceId: String = uniqueDeviceIdOption.get(0);
          val phone: String = DestinationUtil.normalizeUserPhoneCode(phoneOption.get(0), country);
          val adAccountId = postValue.get("adAccountId").get(0)
          val adId = postValue.get("adId").get(0)
          val accountId = (country + "-" + phone)

          if (uniqueDeviceId == doubleSaltIt(device, request.headers.get("hash"))) {
            val contract = PickAdFromContractV2(phone, AdUtil.normalizeDialForAd(dialOption.get(0).replaceAll("-", ""), country), accountId, adAccountId, adId)
            AdServiceRegistry.service.pickAdsFrom(contract) match {
              case Nil => noAd
              case adList: List[PickAdData] => {
                val randomAdIndex = System.currentTimeMillis().toInt % adList.size
                val ad = adList(randomAdIndex)
                ad.outNumber = AdUtil.normalizeDialForDial(dialOption.get(0).replaceAll("-", ""), country)
                updateCDRSummaryTable(ad, 0, 1)
                createHit(ad)

                Ok(Json.toJson(Map("did" -> ad.did, "maxDuration" -> ad.maxduration.toString, "adcrypto" -> Crypto.encryptAES(ad.toString), "ad" -> DeviceAdData(ad.adId, ad.name, ad.description, ad.medialocation).toString)))
                  .withCookies(Cookie("XSRF-TOKEN", Crypto.encryptAES(ad.toString), httpOnly = false))
              }
              case _ => noAd
            }
          } else {
            noAd
          }
        }
        case _ => noAd
      }

    } catch {
      case t: Throwable => t.printStackTrace(); noAd
    }
  }

  def deviceCall() = Action { request =>
    try {
      val postData = request.body.asFormUrlEncoded;
      postData match {
        case Some(postValue) => {
          val countryOption = postValue.get("country");
          val phoneOption = postValue.get("phone");
          val deviceOption = postValue.get("device");
          val uniqueDeviceIdOption = postValue.get("uniquedeviceid");
          val dialOption = postValue.get("dial");
          val cryptoAdOption = postValue.get("cryptoad");

          val device: String = deviceOption.get(0);
          val country = countryOption.get(0);
          val uniqueDeviceId: String = uniqueDeviceIdOption.get(0);
          val phone: String = DestinationUtil.normalizeUserPhoneCode(phoneOption.get(0), country);
          val dial = AdUtil.normalizeDialForDial(dialOption.get(0).replaceAll("-", ""), country);
          val cryptoad = cryptoAdOption.get(0);
          val accountId = (country + "-" + phone)
          if (uniqueDeviceId == doubleSaltIt(device, request.headers.get("hash"))) {
            val jsonDec = Crypto.decryptAES(cryptoad)
            cacheCallRedis(dial, jsonDec)
            Ok("")
          } else {
            Ok("")
          }

        }
        case _ => Ok("")
      }
    } catch {
      case t: Throwable => t.printStackTrace(); Ok("")
    }

  }

  def deviceGetRate() = Action { request =>
    try {
      val postData = request.body.asFormUrlEncoded;
      postData match {
        case Some(postValue) => {
          val countryOption = postValue.get("country");
          val phoneOption = postValue.get("phone");
          val deviceOption = postValue.get("device");
          val uniqueDeviceIdOption = postValue.get("uniquedeviceid");
          val dialOption = postValue.get("dial");

          val device: String = deviceOption.get(0);
          val country = countryOption.get(0);
          val uniqueDeviceId: String = uniqueDeviceIdOption.get(0);
          val dial = AdUtil.normalizeDialForAd(dialOption.get(0).replaceAll("-", ""), country)

          
          CDRServiceRegistry.service.retrieveBestMatchDestination(dial) match {
            case Success(Some((destinationId,destinationName))) => {
              GatewayAccountServiceRegistry.service.retrieveSellRateByDestination(destinationId) match {
                case Success(Some(rateData)) => Ok(Json.toJson(Map("rate" -> rateData.toString)))
                case  _ => BadRequest
              }
            } 
            case _=> BadRequest
          }
        }
        case _ => BadRequest
      }
    } catch {
      case t: Throwable => t.printStackTrace(); BadRequest
    }

  }  
  
  
  def deviceCallBypassSwitch() = Action { request =>
    try {
      val postData = request.body.asFormUrlEncoded;
      postData match {
        case Some(postValue) => {
          val countryOption = postValue.get("country");
          val phoneOption = postValue.get("phone");
          val deviceOption = postValue.get("device");
          val uniqueDeviceIdOption = postValue.get("uniquedeviceid");
          val dialOption = postValue.get("dial");
          val cryptoAdOption = postValue.get("cryptoad");

          val device: String = deviceOption.get(0);
          val country = countryOption.get(0);
          val uniqueDeviceId: String = uniqueDeviceIdOption.get(0);
          val phone: String = DestinationUtil.normalizeUserPhoneCode(phoneOption.get(0), country);
          val dial = AdUtil.normalizeDialForDial(dialOption.get(0).replaceAll("-", ""), country);
          val cryptoad = cryptoAdOption.get(0);
          val accountId = (country + "-" + phone)
          if (uniqueDeviceId == doubleSaltIt(device, request.headers.get("hash"))) {
            val jsonDec = Crypto.decryptAES(cryptoad)
          }
          Ok("")
        }
        case _ => Ok("")
      }
    } catch {
      case t: Throwable => t.printStackTrace(); Ok("")
    }

  }

  def createCDR(jsonDec: String) {
    Akka.system.scheduler.scheduleOnce(0.milliseconds) {
      try {

        PickAdData.parse(Json.parse(jsonDec)) match {
          case Some(data) => CDRServiceRegistry.service.createCDR(CreateCDRContract(data.accountId, data.id, data))
          case None => //
        }
      } catch {
        case e: Throwable => Logger.error(e.toString())
      }
    }
  }

  def cacheCallRedis(dial: String, jsonDec: String) {
    //    Akka.system.scheduler.scheduleOnce(0.milliseconds) {
    try {
      AdServiceRegistry.service.cacheCallRedis(dial, jsonDec) match {
        case Success(()) => //Nice
        case Failure(t) => {
          Logger.error(t.getMessage())
        }
      }
    } catch {
      case e: Throwable => Logger.error(e.toString())
    }
    //    }
  }

  def deviceImpression() = Action.async { request =>
    val future = scala.concurrent.Future {
      try {
        val postData = request.body.asFormUrlEncoded;
        postData match {
          case Some(postValue) => {
            val countryOption = postValue.get("country");
            val phoneOption = postValue.get("phone");
            val deviceOption = postValue.get("device");
            val uniqueDeviceIdOption = postValue.get("uniquedeviceid");
            val dialOption = postValue.get("dial");
            val postalCodeOption = postValue.get("postalCode");

            val device: String = deviceOption.get(0);
            val country = countryOption.get(0);
            val uniqueDeviceId: String = uniqueDeviceIdOption.get(0);
            val phone: String = DestinationUtil.normalizeUserPhoneCode(phoneOption.get(0), country);
            val dial = AdUtil.normalizeDialForAd(dialOption.get(0).replaceAll("-", ""), country)
            val postalCode: String = postalCodeOption.get(0)
            val accountId = (country + "-" + phone)
            if (uniqueDeviceId == doubleSaltIt(device, request.headers.get("hash"))) {
              val splitValues = dialOption.get(0).split("\\#")
              if (splitValues.length == 2 && splitValues(0) == "8888") {
                AdServiceRegistry.service.retriveAd(splitValues(1)) match {
                  case Some(ad) => {
                    val adList: List[ImpressionViewData] = List.fill(5)(ImpressionViewData(ad.accountId, ad.adId, ad.impressionView));
                    Ok(Json.toJson(Map("impressions" -> adList.map(_.toString)))).withCookies(Cookie("XSRF-TOKEN", Crypto.encryptAES(1.toString), httpOnly = false))
                  }
                  case None => noAd
                }
              } else {
                AdServiceRegistry.service.retrieveImpressions(phone, dial, postalCode) match {
                  case Nil => noAd
                  case adList: List[ImpressionViewData] =>
                    Ok(Json.toJson(Map("impressions" -> adList.map(_.toString)))).withCookies(Cookie("XSRF-TOKEN", Crypto.encryptAES(1.toString), httpOnly = false))
                  case _ => noAd
                }
              }
            } else {
              noAd
            }
          }
          case _ => noAd
        }
      } catch {
        case t: Throwable => noAd
      }
    }
    future.map(result => result)
  }

  def noAd = {
    NotFound(Json.toJson(Map("did" -> "", "adcrypto" -> "", "ad" -> DeviceAdData("0", "", "<img width='300' height='122' src='https://s3.amazonaws.com/com.dimwire.media/spcfiles/noadlogo.jpg'/>" + "<div><h4>" + Messages("no_ad") + "</h4></div>", "https://s3.amazonaws.com/com.dimwire.media/spcfiles/noad.jpg").toString)))
  }

  def doubleSaltIt(value: String, header: Option[String]): String = {
    val digestValue = header match {
      case None => new BigInteger(1, MessageDigest.getInstance("MD5").digest(("Izmir'inKavaklariDokulurYapraklari" + value).getBytes("UTF-8"))).toString(16)
      case Some(_) => {
        //Base64.encodeBase64(MessageDigest.getInstance("MD5").digest(("Izmir'inKavaklariDokulurYapraklari"+value).getBytes("UTF-8")))
        val digestValue = MessageDigest.getInstance("MD5").digest(("Izmir'inKavaklariDokulurYapraklari" + value).getBytes)
        new String(Base64.encodeBase64(digestValue));
      }
    }
    digestValue
  }

}