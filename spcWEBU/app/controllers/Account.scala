package controllers

import java.io.File
import java.util.UUID
import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Success
import com.amazonaws.services.s3.model.AccessControlList
import com.amazonaws.services.s3.model.DeleteObjectRequest
import com.amazonaws.services.s3.model.GroupGrantee
import com.amazonaws.services.s3.model.Permission
import com.amazonaws.services.s3.model.PutObjectRequest
import com.sponsoracall.account.contract.AccountContractV2
import com.sponsoracall.account.contract.AccountMediaContractV2
import com.sponsoracall.account.contract.ChangePasswordContractV2
import com.sponsoracall.account.data.AccountDataV2
import com.sponsoracall.account.data.AccountMediaDataV2
import javax.naming.ConfigurationException
import model.SessionData
import play.api.Play
import play.api.Play.current
import play.api.i18n.Messages
import play.api.libs.Crypto
import play.api.libs.concurrent.Akka
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller
import play.api.mvc.Cookie
import play.api.mvc.Result
import play.api.mvc.SimpleResult
import play.api.templates.Html
import plugins.S3ScalaPlugin
import util.Auth
import util.SessionUtil
import com.sponsoracall.util.MD5DigestV2
import com.sponsoracall.util.EmailService
import com.sponsoracall.account.service.AccountServiceRegistry
import net.authorize.Environment
import net.authorize.Merchant
import net.authorize.TransactionType
import net.authorize.aim.Transaction
import net.authorize.data._
import net.authorize.data.creditcard._
import java.io.BufferedReader
import java.net.URL
import java.util.Hashtable
import java.io.DataOutputStream
import java.util.Enumeration


object Account extends Controller {
  def addAccount() = Action(parse.json) { request =>
    val parseValue = Json.parse(request.body.toString)
    val accountType = parseValue.\("accountType").as[String];
    val address1 = parseValue.\("address1").as[String];
    val address2 = parseValue.\("address2").as[String];
    val city = parseValue.\("city").as[String];
    val companyName = parseValue.\("companyName").as[String];
    val country = parseValue.\("country").as[String];
    val email = parseValue.\("email").as[String];
    val firstName = parseValue.\("firstName").as[String];
    val lastName = parseValue.\("lastName").as[String];
    val middleName = parseValue.\("middleName").as[String];
    val password = parseValue.\("password").as[String];
    val repassword = parseValue.\("repassword").as[String];
    val phone1 = parseValue.\("phone1").as[String];
    val phone2 = parseValue.\("phone2").as[String];
    val postalCode = parseValue.\("postalCode").as[String];
    val state = parseValue.\("state").as[String];
    val gender = parseValue.\("gender").as[String];
    val birthday = parseValue.\("birthday").as[String];
    val challange = parseValue.\("recaptcha_challenge_field").as[String];
    val response = parseValue.\("recaptcha_response_field").as[String];
    val remoteAddr = request.remoteAddress;
    if (password != repassword) {
      Conflict(Messages("password_does_not_match"))
    } else if (!Auth.validateCaptcha(remoteAddr, challange, response)) {
      Conflict(Messages("invalid_captcha"))
    } else {

      val id = UUID.randomUUID().toString().replace("-", "");
      val result = AccountServiceRegistry.service.createAccount(AccountContractV2(id, accountType, companyName, firstName, middleName, lastName, address1, address2, city, state, postalCode, country, email, phone1, phone2, password, birthday, gender))
      result match {
        case Success(id) => {
          sendConfirmEmail(email, id)
          Ok(views.html.register.registerconfirm())
        }
        case Failure(t) => t.printStackTrace(); Conflict(Messages("account_email_exist"))
      }
    }
  }

  def sendConfirmEmail(toEmail: String, accountId: String) {
    val emailFrom = Play.current.configuration.getString("email.from") match {
      case Some(value) => value
      case None => throw new ConfigurationException("Cant Find Email from")
    }

    val emailSubject = Messages("account_confirm_email_subject")

    val link = Play.current.configuration.getString("confirm.link") match {
      case Some(value) => value
      case None => throw new ConfigurationException("Cant Find confirm link")
    }

    val enId = Crypto.encryptAES(accountId)

    val message = Messages("account_confirm_email_body")

    val emailBody = s"$message <a href='$link$enId'>$link$enId</a>"

    Akka.system.scheduler.scheduleOnce(0.milliseconds) {
      EmailService.sendEmail(emailFrom, toEmail, emailSubject, emailBody)
    }
  }

  def logout = Action {
    val sessionData = new SessionData(UUID.randomUUID().toString.replaceAll("-", ""), "Connection")
    Ok(views.html.register.register())
      .withSession(sessionData.cookieName -> Crypto.encryptAES(sessionData.toString))
      .withCookies(Cookie("XSRF-TOKEN", Crypto.encryptAES(sessionData.cookieName), httpOnly = false))
  }

  def registerconfirm = Action {
    request =>
      Ok(views.html.register.registerconfirm())
  }

  def acc(email: String) = Action {
    AccountServiceRegistry.service.retrieveAccountByEmail(email) match {
      case Success(Some(accountData)) => {
        accountData.password = "";
        val sessionData = new SessionData(UUID.randomUUID().toString.replaceAll("-", ""), accountData.id)
        Redirect("/").withSession(sessionData.cookieName -> Crypto.encryptAES(sessionData.toString))
          .withCookies(Cookie("XSRF-TOKEN", Crypto.encryptAES(sessionData.cookieName), httpOnly = false))
      }
      case _ => NotFound("not found")
    }
  }

  def login() = Action(parse.json) {
    implicit request =>
      val parseValue = Json.parse(request.body.toString)
      val email = parseValue.\("email").as[String];
      val password = parseValue.\("password").as[String];
      if (email.length() == 0 || password.length == 0) {
        BadRequest
      } else {
        AccountServiceRegistry.service.retrieveAccountByEmail(email) match {
          case Success(Some(accountData)) if accountData.password.equals(MD5DigestV2.digest(password)) && accountData.active => {
            accountData.password = "";
            val sessionData = new SessionData(UUID.randomUUID().toString.replaceAll("-", ""), accountData.id)
            Ok("").withSession(sessionData.cookieName -> Crypto.encryptAES(sessionData.toString))
              .withCookies(Cookie("XSRF-TOKEN", Crypto.encryptAES(sessionData.cookieName), httpOnly = false))
          }
          case Success(Some(accountData)) if accountData.password.equals(MD5DigestV2.digest(password)) && !accountData.active => {
            //sendConfirmEmail(accountData.email, accountData.id)
            BadRequest(Messages("account.not.active.email.confirm"))
          }
          case Success(Some(accountData)) if !accountData.password.equals(MD5DigestV2.digest(password)) && accountData.active => Unauthorized("login")
          case Success(None) => Unauthorized("login")
          case _ => Unauthorized("login")
        }
      }
  }

  def confirmEmail(linkParam: String) = Action {
    request =>
      try {
        val decValue = Crypto.decryptAES(linkParam)
        AccountServiceRegistry.service.toogleAccount(decValue, "1");
        Ok(views.html.register.registerconfirmthanks())
      } catch {
        case e: Throwable => Ok(views.html.register.registercannotconfirm())
      }
  }

  def accountpasswordaction(passwordActionType: Int) = Action(parse.json) { request =>
    val parseValue = Json.parse(request.body.toString)
    val email = parseValue.\("email").as[String];
    val challange = parseValue.\("recaptcha_challenge_field").as[String];
    val response = parseValue.\("recaptcha_response_field").as[String];
    val remoteAddr = request.remoteAddress;
    if (!Auth.validateCaptcha(remoteAddr, challange, response)) {
      Conflict(Messages("invalid_captcha"))
    } else {
      AccountServiceRegistry.service.retrieveAccountByEmail(email) match {
        case Success(Some(accountData)) => {
          if (passwordActionType == 0) sendResetPassword(email)
          else {
            sendConfirmEmail(accountData.email, accountData.id)
            Ok(Messages("account.not.active.email.confirm"))
          }
        }
        case Success(None) => Conflict(Messages("unknown.email"))
        case Failure(t) => Conflict("unknown_error")
      }
    }
  }

  def resetPasswordAction() = Action(parse.json) { request =>
    val parseValue = Json.parse(request.body.toString)
    val email = parseValue.\("email").as[String];
    val tempPassword = parseValue.\("tempPassword").as[String];
    val newPassword = parseValue.\("newPassword").as[String];

    val challange = parseValue.\("recaptcha_challenge_field").as[String];
    val response = parseValue.\("recaptcha_response_field").as[String];
    val remoteAddr = request.remoteAddress;
    if (!Auth.validateCaptcha(remoteAddr, challange, response)) {
      Conflict(Messages("invalid_captcha"))
    } else {
      AccountServiceRegistry.service.retrieveAccountByEmail(email) match {
        case Success(Some(accountData)) => {
          AccountServiceRegistry.service.changePassword(ChangePasswordContractV2(accountData.id, tempPassword, newPassword)) match {
            case Success(()) => Ok(Messages("change_password_success"))
            case Failure(t) => t.printStackTrace(); Ok(Messages("invalid_temp_password"))
          }
        }
        case Success(None) => Conflict(Messages("unknown.email"))
        case Failure(t) => Conflict("unknown_error")
      }
    }
  }

  def resetPassword() = Action {
    request =>
      Ok(views.html.register.resetPassword())
  }

  private def sendResetPassword(email: String): Result = {
    val randomPassword = UUID.randomUUID().toString().replace("-", "").substring(0, 6).capitalize + "-" + UUID.randomUUID().toString().replace("-", "").substring(0, 8).capitalize;
    AccountServiceRegistry.service.resetPassword(email, randomPassword) match {
      case Success(()) => {
        SessionUtil.sendResetPassword(email, randomPassword)
        Ok(Messages("thank.you.please.check.your.email"))
      }
      case _ => Conflict(Messages("unknown.email"))
    }

  }

  def retrieveAccount(accountId: String) = Action {
    request =>
      if (SessionUtil.checkAccountId(request, accountId)) {
        val result = AccountServiceRegistry.service.retrieveAccount(accountId)
        result match {
          case Success(Some(s)) => Ok(s.toString)
          case Success(None) => NotFound
          case Failure(f) => NotFound
          case _ => NotFound
        }
      } else {
        Application.authRequiredSimpleResult
      }
  }

  def updateAccount(accountId: String) = Action(parse.json) { request =>
    if (SessionUtil.checkAccountId(request, accountId)) {
      val parseValue = Json.parse(request.body.toString)
      val accountType = parseValue.\("accountType").as[String];
      val address1 = parseValue.\("address1").as[String];
      val address2 = parseValue.\("address2").as[String];
      val city = parseValue.\("city").as[String];
      val companyName = parseValue.\("companyName").as[String];
      val country = parseValue.\("country").as[String];
      val email = parseValue.\("email").as[String];
      val firstName = parseValue.\("firstName").as[String];
      val lastName = parseValue.\("lastName").as[String];
      val middleName = parseValue.\("middleName").as[String];
      val password = parseValue.\("password").as[String];
      val repassword = parseValue.\("repassword").as[String];
      val phone1 = parseValue.\("phone1").as[String];
      val phone2 = parseValue.\("phone2").as[String];
      val postalCode = parseValue.\("postalCode").as[String];
      val state = parseValue.\("state").as[String];
      val gender = parseValue.\("gender").as[String];
      val birthday = parseValue.\("birthday").as[String];
      val remoteAddr = request.remoteAddress;
      if (password != repassword) {
        Conflict(Messages("password_does_not_match"))
      } else {
        val result = AccountServiceRegistry.service.updateAccount(AccountContractV2(accountId, accountType, companyName, firstName, middleName, lastName, address1, address2, city, state, postalCode, country, email, phone1, phone2, password, birthday, gender))
        result match {
          case Success(id) => Ok("");
          case Failure(t) => t.printStackTrace(); Conflict(Messages("account_email_exist"))

        }
      }
    } else {
      Application.authRequiredSimpleResult
    }

  }

  def listAccountMedia(accountId: String) = Action { request =>
    if (SessionUtil.checkAccountId(request, accountId)) {
      val approveParam = request.getQueryString("approved");
      val approve = approveParam match {
        case None => false
        case Some(_) => true
      }
      val result = AccountServiceRegistry.service.listAccountMedia(accountId)

      val finalResult = approveParam match {
        case Some("true") | Some("false") => result.filter(accountMedia => accountMedia.approved == approveParam.get)
        case _ => result
      }
      Ok(Json.toJson(Map("media" -> finalResult.map(_.toString))))
    } else {
      Application.authRequiredSimpleResult
    }
  }

  def accountMedia() = Action(parse.multipartFormData) { request =>
    try {
      val postData = request.body.asFormUrlEncoded
      val accountId = postData.get("accountId").get(0)
      val mediaType = postData.get("mediaType").get(0)
      val mediaDescription = postData.get("mediaDescription").get(0)
      val mediaIdParam = postData.get("mediaId")
      val mediaApprovedValue = "0"
      if (SessionUtil.checkAccountId(request, accountId)) {

        request.body.file("media").map { mediaFile =>
          import java.io.File
          val filename = mediaFile.filename
          val contentType = mediaFile.contentType

          val result: Option[String] = {
            var mediaId = "";
            if (mediaIdParam.get(0).isEmpty) {
              mediaId = UUID.randomUUID().toString.replace("-", "")
              AccountServiceRegistry.service.addAccountMedia(AccountMediaDataV2(accountId, mediaId, mediaDescription, mediaType, mediaApprovedValue));
            } else {
              mediaId = mediaIdParam.get(0);
              AccountServiceRegistry.service.updateAccountMedia(AccountMediaDataV2(accountId, mediaId, mediaDescription, mediaType, mediaApprovedValue));
            }
            Some(mediaId);
          }

          result match {
            case Some(mediaId) => {
              val fileExtension = filename.split("\\.")(1)
              val file = File.createTempFile(s"/temp/$mediaId", fileExtension)
              mediaFile.ref.moveTo(file, true)
              val fileName = s"$mediaId.$fileExtension"
              val putObjectRequest = new PutObjectRequest(S3ScalaPlugin.s3Bucket + "/accountfiles", fileName, file)

              val acl = new AccessControlList()
              acl.grantPermission(GroupGrantee.AllUsers, Permission.Read);
              putObjectRequest.setAccessControlList(acl)

              val result = S3ScalaPlugin.s3Client.get.putObject(putObjectRequest);
              file.delete()
              val contract = AccountMediaDataV2(accountId, mediaId, mediaDescription, mediaType, mediaApprovedValue)
              contract.mediaLocation = "https://s3.amazonaws.com/" + S3ScalaPlugin.s3Bucket + "/accountfiles/" + fileName;
              AccountServiceRegistry.service.updateAccountMedia(contract)
              Ok(views.html.account.UploadStatus(Messages("upload_status_yes")))
            }
            case None => Ok(views.html.account.UploadStatus(Messages("upload_status_yes")))
          }
        }.getOrElse {
          Ok(views.html.account.UploadStatus(Messages("upload_status_no")))
        }
      } else {
        Application.authRequiredSimpleResult
      }
    } catch {
      case t: Throwable => Ok(views.html.account.UploadStatus(Messages("upload_status_no")))
    }
  }

  def retrieveAccountMedia(accountId: String, mediaId: String) = Action { request =>
    if (SessionUtil.checkAccountId(request, accountId)) {
      val result = AccountServiceRegistry.service.retrieveAccountMedia(AccountMediaContractV2(accountId, mediaId));
      result match {
        case Some(s) => Ok(Json.toJson(Map("accountMedia" -> s.toString)))
        case None => BadRequest
      }
    } else {
      Application.authRequiredSimpleResult
    }
  }

  def deleteAccountMedia(accountId: String, mediaId: String) = Action { request =>
    if (SessionUtil.checkAccountId(request, accountId)) {
      val contract = AccountMediaContractV2(accountId, mediaId)
      AccountServiceRegistry.service.retrieveAccountMedia(contract) match {
        case Some(data) => {
          val objectName = data.mediaLocation.substring(data.mediaLocation.lastIndexOf("/") + 1);
          val deleteObjectRequest = new DeleteObjectRequest(S3ScalaPlugin.s3Bucket + "/accountfiles", objectName);
          val result = S3ScalaPlugin.s3Client.get.deleteObject(deleteObjectRequest);

          AccountServiceRegistry.service.deleteAccountMedia(contract)
          Ok("")
        }
        case None => Ok("")
      }
    } else {
      Application.authRequiredSimpleResult
    }
  }

  def forgotPassword = Action {
    request =>
      Ok(views.html.register.forgotPassword())
  }

  def changePassword() = Action(parse.json) { request =>
    try {
      val changePasswordContract = ChangePasswordContractV2.parse(Json.parse(request.body.toString))
      if (SessionUtil.checkAccountId(request, changePasswordContract.id)) {
        AccountServiceRegistry.service.changePassword(changePasswordContract) match {
          case Success(()) => Ok("")
          case Failure(t) => t.printStackTrace(); BadRequest(Messages("invalid.password"))
        }
      } else {
        Application.authRequiredSimpleResult
      }
    } catch {
      case t: Throwable => t.printStackTrace(); BadRequest(Messages("invalid.password"))
    }
  }

  def pay() {
//    val apiLoginID = "8tngD2BH6M";
//     val transactionKey = "28429sf397JFpJh7";
     
     //https://developer.authorize.net/integration/gettingstarted/
    //levent18 11QQ
    
     val apiLoginID="4tzu2EP784"
     val transactionKey="7n55xwS2Kas3J9cM"
     val secretquestion="Simon"
       
  }
  
}