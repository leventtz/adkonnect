package controllers

import scala.util.Failure
import scala.util.Success
import com.sponsoracall.account.payment.PaymentContract
import com.sponsoracall.ad.contract.ActivateAdContractV2
import com.sponsoracall.ad.contract.CreateAdContractV2
import com.sponsoracall.ad.contract.UpdateDesignContractV2
import com.sponsoracall.ad.contract.UpdateIncomingContractV2
import com.sponsoracall.ad.data.DesignData
import com.sponsoracall.ad.data.InvoiceDataV2
import com.sponsoracall.ad.data.OrderTotalDataV2
import com.sponsoracall.ad.data.TransactionDataV2
import com.sponsoracall.ad.service.AdServiceRegistry
import com.sponsoracall.cdr.data.LocationData
import com.sponsoracall.cdr.service.CDRServiceRegistry
import com.sponsoracall.gatewayaccount.service.GatewayAccountServiceRegistry
import play.api.i18n.Messages
import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller
import util.SessionUtil
import com.sponsoracall.ad.contract.CompleteOrderContract
import play.api.libs.json.JsObject
import com.sponsoracall.ad.contract.AddFundContract


object AccountAd extends Controller {
    implicit val locationReader = Json.reads[LocationData]
    implicit val designDataReader = Json.reads[DesignData]
    implicit val reader = Json.reads[CreateAdContractV2]
  
  def updateImpression()  = Action(parse.json) { 
      request =>
      val parseValue = Json.parse(request.body.toString)
      val accountId = parseValue.\("accountId").as[String];
      val adId = parseValue.\("adId").as[String];
      val impression = parseValue.\("impression").as[Double];
      
      AdServiceRegistry.service.updateAdImpression(accountId, adId, impression) 
      Ok("")
  }
    

  def updateName()  = Action(parse.json) { 
      request =>
      val parseValue = Json.parse(request.body.toString)
      val accountId = parseValue.\("accountId").as[String];
      val adId = parseValue.\("adId").as[String];
      val name = parseValue.\("name").as[String];
      AdServiceRegistry.service.updateAdName(accountId, adId, name) 
      Ok("")
  }    
    
  def calculateOrder = Action(parse.json) { request =>
    try {
    val parseValue = Json.parse(request.body.toString)
     val lineItems: List[CreateAdContractV2] = parseValue.as[List[CreateAdContractV2]]
    
    if (lineItems.foldRight(true)((contract, value) => value && SessionUtil.checkAccountId(request, contract.accountId))) {
      val list = AdServiceRegistry.service.calculateOrderTotal(lineItems);
      val listi18 = list.map(f => new OrderTotalDataV2(Messages(f.feeItemMessageId), f.total))
      Ok(Json.toJson(Map("totals" -> listi18.map(_.toString))))
    } else {
      Application.authRequiredSimpleResult
    }
    }catch {
      case t:Throwable => t.printStackTrace();BadRequest
    }
  }
 
  def saveOrder = Action(parse.json) { request =>
    val parseValue = Json.parse(request.body.toString)
    val paymentValues=parseValue.\("payment");
    val paymentContract:PaymentContract=PaymentContract.parse(paymentValues).get;
    val lineItemsValue=parseValue.\("lineItems");
    val lineItems: List[CreateAdContractV2] = lineItemsValue.as[List[CreateAdContractV2]]
    if (lineItems.foldRight(true)((contract, value) => value && SessionUtil.checkAccountId(request, contract.accountId))) {
      AdServiceRegistry.service.saveOrder(CompleteOrderContract(lineItems,paymentContract)) match {
        case Success(data) => Ok(Json.toJson(Map("data" -> data.toString)))
        case Failure(t) => BadRequest(t.getMessage())
      }
    } else {
      Application.authRequiredSimpleResult
    } 
  }

  

  
  def accountAdsView = Action {
    request =>
    Ok(views.html.ad.AccountAdsView())
  }

  def retrieveAdTemplates(searchString: String) = Action {
    request =>
      val searchStringOption: Option[String] = if (searchString == "") None else Some(searchString)
      val result = GatewayAccountServiceRegistry.service.listTemplates(searchStringOption);
      result match {
        case Success(x :: xs) => Ok(Json.toJson(Map("adTemplates" -> result.get.map(_.toString))))
        case Success(List()) => NotFound
        case Failure(f) => BadRequest
        case _ => BadRequest
      }
  }
  def retrieveIncomingDestination() = Action { request =>
    val result = CDRServiceRegistry.service.retriveIncomingDestinations
    result match {
      case Success(x :: xs) => Ok(Json.toJson(Map("incoming" -> result.get.map(_.toString))))
      case _ => NotFound
    }
  }

  def retrieveAccounAds(accountId: String, adId: String) = Action {
    request =>
      Ok(Json.toJson(Map("accountAd" -> AdServiceRegistry.service.retrieveAccounAds(accountId, adId).map(_.toString))))
  }

  def toggleAd = Action(parse.json) { request =>
    ActivateAdContractV2.parse(request.body.toString) match {
      case None => BadRequest
      case Some(contract) => if (SessionUtil.checkAccountId(request, contract.accountId)) AdServiceRegistry.service.toogleAd(contract) else Application.authRequiredSimpleResult
    }
    Ok("")
  }

  def updateIncoming() = Action(parse.json) { request =>

    UpdateIncomingContractV2.parse(request.body.toString) match {
      case None => BadRequest
      case Some(contract) => {
        if (SessionUtil.checkAccountId(request, contract.accountId)) 
        {
          AdServiceRegistry.service.updateIncoming(contract)
          Ok("")
        }
          else Application.authRequiredSimpleResult
      }
    }
    
  }
  
  def updateDesign() = Action(parse.json) { request =>
    UpdateDesignContractV2.parse(request.body.toString) match {
      case Some(contract) => {
        if (SessionUtil.checkAccountId(request, contract.accountId)) {
          AdServiceRegistry.service.updateDesign(contract)
          Ok("");
        } else Application.authRequiredSimpleResult
      }
      case _ => BadRequest
    }
  }

  def addFund() = Action(parse.json) { request =>
    val parseValue = Json.parse(request.body.toString)
    val accountId=parseValue.\("accountId").as[String]
    val adId=parseValue.\("adId").as[String]
    val paymentValues=parseValue.\("payment");
    PaymentContract.parse(paymentValues) match {
      case Some(paymentContract) => {
        AdServiceRegistry.service.addFund(AddFundContract(accountId,adId,paymentContract)) match {
            case Success(data) => Ok(Json.toJson(Map("data" -> data.toString)))
            case Failure(t) => BadRequest(t.getMessage()) 
        }
      }
      case _ => BadRequest("Parse Error")
    }
  }
  
  
  def retrieveTransactions(accountId:String,orderId:String) = Action  {
      request =>
        val transactionDataV2=AdServiceRegistry.service.retrieveTransactions(accountId, orderId).map(t=>{
          if(t.transactionType=="O") {
        	 InvoiceDataV2.parse(t.detail) match {
        	   case Some(invoiceData) => {
        	     val listi18 = invoiceData.totalItems.map(f => new OrderTotalDataV2(Messages(f.feeItemMessageId), f.total))
        	     TransactionDataV2(accountId=t.accountId,orderId=t.orderId,transactionType=t.transactionType,date=t.date,amount=t.amount,detail=InvoiceDataV2(invoiceData.lineItems,listi18).toString,accountDetail=t.accountDetail)
        	   }
        	   case _ => t
        	 }
        	 
          } else {
            t
          }
        })
      Ok(Json.toJson(Map("accountTransaction" -> transactionDataV2.map(_.toString))))
  }
  
}