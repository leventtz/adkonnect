package controllers

import play.api.data.Form
import play.api.data.Forms.text
import play.api.data.Forms.tuple
import play.api.mvc.Action
import play.api.mvc.Controller
import play.api.libs.Crypto
import views.html.defaultpages.unauthorized
import play.api.mvc.Cookie
import java.util.UUID
import model.SessionData
import play.Logger
import util.SessionUtil



object Application extends Controller {

  def authRequiredSimpleResult = {
    val sessionData = new SessionData(UUID.randomUUID().toString.replaceAll("-", ""), "Connection")
    Unauthorized(views.html.register.register())
      .withSession(sessionData.cookieName -> Crypto.encryptAES(sessionData.toString))
      .withCookies(Cookie("XSRF-TOKEN", Crypto.encryptAES(sessionData.cookieName), httpOnly = false))
  }

    
  
  def authRequired = Action {
    request =>
    authRequiredSimpleResult
  }

  def redirectToHttps = Action {
    request =>
    Redirect("https://www.adkonnect.com")
  }  
  
  def sessionExpired = Action {
    request =>
    Unauthorized("sessionexpired")
  }  
  
  def index =
    Action {
      implicit request =>
        SessionUtil.getSessionData(request) match {
          case Some(data) => Ok(views.html.index(data.accountId))
          case _ => {
        	  val sessionData = new SessionData(UUID.randomUUID().toString.replaceAll("-", ""), "Connection")
        	  Ok(views.html.register.register())
        	  	.withSession(sessionData.cookieName -> Crypto.encryptAES(sessionData.toString))
        	  	.withCookies(Cookie("XSRF-TOKEN", Crypto.encryptAES(sessionData.cookieName), httpOnly = false))            
          }
        }
    }
  
}