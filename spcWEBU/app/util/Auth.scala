package util

import model.SPCWebData
import play.api.libs.Crypto
import play.api.libs.json.JsError
import play.api.libs.json.JsSuccess
import play.api.mvc.Controller
import play.api.mvc.Cookie
import play.api.mvc.RequestHeader
import net.tanesha.recaptcha.ReCaptchaImpl

object Auth extends Controller {

  def validateSPCCookie(request: RequestHeader): Option[SPCWebData] = {
    request.cookies.get("X-SPCMOBILE") match {
      case None => None
      case Some(spcCookie) if spcCookie != "" => {
        SPCWebData.parse(Crypto.decryptAES(spcCookie.value)) match {
          case JsSuccess(spcWebData, _) => {
            if (spcWebData.remoteAddress != request.remoteAddress)
              None
            else
              Some(spcWebData)
          }
          case JsError(_) => None
        }
      }
    }
  }

  def validateSession(request: RequestHeader): Boolean = {
    request.session.get("connected") match {
      case None => false
      case Some(connectedValue) => if (Crypto.decryptAES(connectedValue).length == 32) true else false
    }
  }

  def validateSessionAndXSRF(request: RequestHeader): Boolean = {
    request.session.get("connected") match {
      case None => false
      case Some(connectedValue) => {
        request.headers.get("X-XSRF-TOKEN") match {
          case None => false
          case Some(tokenValue) => {
            val value = Crypto.decryptAES(connectedValue)
            val token = Crypto.decryptAES(tokenValue)
            value + value == token
          }
        }
      }
    }
  }

  def xhrOkResponse(response: String, id: String, phoneNumber: String, request: RequestHeader): play.api.mvc.Result = {
    println(SPCWebData(id, request.remoteAddress, phoneNumber));
    Ok(response)
      .withSession("connected" -> Crypto.encryptAES(id))
      .withCookies(Cookie("XSRF-TOKEN", Crypto.encryptAES(id), httpOnly = false))
      .withCookies(Cookie("X-SPCMOBILE", Crypto.encryptAES(SPCWebData(id, request.remoteAddress, phoneNumber).toString), maxAge = Some(31 * 24 * 60 * 60)))
  }

  def redirectIndex(id: String, phoneNumber: String, request: RequestHeader): play.api.mvc.Result = {
    Redirect("/")
      .withSession("connected" -> Crypto.encryptAES(id))
      .withCookies(Cookie("XSRF-TOKEN", Crypto.encryptAES(id), httpOnly = false))
      .withCookies(Cookie("X-SPCMOBILE", Crypto.encryptAES(SPCWebData(id, request.remoteAddress, phoneNumber).toString), maxAge = Some(31 * 24 * 60 * 60)))
  }

  def badRequestWithNoSession(msg: String = ":("): play.api.mvc.Result = {
    BadRequest(msg)
      .withNewSession
      .withCookies(Cookie("XSRF-TOKEN", "", maxAge = Some(-10), httpOnly = false))
      .withCookies(Cookie("X-SPCMOBILE", "", maxAge = Some(-10), httpOnly = false))
  }

  def unauthorizedWithNoSession(msg: String = ":("): play.api.mvc.Result = {
    Unauthorized(msg)
      .withNewSession
      .withCookies(Cookie("XSRF-TOKEN", "", maxAge = Some(-10), httpOnly = false))
      .withCookies(Cookie("X-SPCMOBILE", "", maxAge = Some(-10), httpOnly = false))
  }

  def logout(msg: String): play.api.mvc.Result = {
    Ok(msg)
      .withNewSession
      .withCookies(Cookie("XSRF-TOKEN", "", maxAge = Some(-10), httpOnly = false))
      .withCookies(Cookie("X-SPCMOBILE", "", maxAge = Some(-10), httpOnly = false))
  }

  def validateCaptcha(remoteAddr: String, challenge: String, response: String): Boolean = {
    val reCaptcha = new ReCaptchaImpl();
    reCaptcha.setPrivateKey(System.getProperty("private_captcha_key"));
    val reCaptchaResponse = reCaptcha.checkAnswer(
      remoteAddr, challenge, response);
    reCaptchaResponse.isValid()

  }

}