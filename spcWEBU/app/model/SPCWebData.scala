package model

import java.util.UUID

object SPCWebData {
  import play.api.libs.json._
  val jsonReader = Json.reads[SPCWebData]
  def parse(jsonString: String): JsResult[SPCWebData] = {
    try {
      jsonReader.reads(Json.parse(jsonString))
    } catch {
      case t:Throwable => {t.printStackTrace();JsError("SPCWeb ParseError->"+jsonString)}
    }
  }
}
case class SPCWebData(accountId:String,remoteAddress:String,phoneNumber:String,timestamp:Long=System.currentTimeMillis()) {
  val id=UUID.randomUUID().toString.replaceAll("-","")	
  require(accountId!=null && accountId.length()==32,"userid missing")
  require(remoteAddress!=null && remoteAddress.length()>0,"remoteAddress missing")
  require(phoneNumber!=null && phoneNumber.length()>0,"phonenumber missing")
  
  import play.api.libs.json._
  val jsonWriter=Json.writes[SPCWebData]
  override def toString={
    jsonWriter.writes(this).toString
  }  
}