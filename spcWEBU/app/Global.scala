import java.util.UUID
import scala.util.Failure
import scala.util.Success
import controllers.Application
import play.api.Application
import play.api.GlobalSettings
import play.api.Logger
import play.api.Play
import play.api.libs.Crypto
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api._
import play.api.mvc._
import play.api.mvc.Results._
import util.SessionUtil
import com.sponsoracall.db.DynamoDBEngine
import com.sponsoracall.cdr.service.CDRServiceRegistry
import scala.concurrent.Future


object LoggingFilter extends EssentialFilter {
  def apply(nextFilter: EssentialAction) = new EssentialAction {
    def apply(requestHeader: RequestHeader) = {
      val startTime = System.currentTimeMillis
      nextFilter(requestHeader).map { result =>
        val endTime = System.currentTimeMillis
        val requestTime = endTime - startTime
        if(requestHeader.headers.get("X-Forwarded-For")!=None) {
        	Logger.info(s"ClientIP: ${requestHeader.headers.get("X-Forwarded-For")} Method:${requestHeader.method} URI:${requestHeader.uri} Time:${requestTime}ms Result:${result.header.status}")
        }
        result.withHeaders("Request-Time" -> requestTime.toString)
          .withCookies(Cookie("X-SESSION-DEVAM", Crypto.encryptAES(System.currentTimeMillis().toString), httpOnly = false))
          .withCookies(Cookie("X-SESSION-REM", Crypto.encryptAES(requestHeader.host), httpOnly = false))

      }
    }
  }
}

object Global extends GlobalSettings {
  override def onError(request: RequestHeader, ex: Throwable) = {
    Logger.error(ex.getMessage(),ex)
	Future.successful(BadRequest)
  }	
  override def doFilter(next: EssentialAction): EssentialAction = {
    Filters(super.doFilter(next), LoggingFilter)
  }

  override def onRouteRequest(request: RequestHeader): Option[Handler] = {
    play.api.Play.current.mode match {
      case play.api.Mode.Dev => {
        super.onRouteRequest(request);
      }
      case _ => {
        if (request.path.startsWith("/terms")) {
          super.onRouteRequest(request);
        } else {
          request.headers.get("x-forwarded-proto") match {
            case Some(header) if ("http" == header) => Some(controllers.Application.redirectToHttps)
            case Some(header) if ("https" == header) => determineRoute(request)
            case _ => Some(controllers.Application.authRequired)
          }
        }
      }
    }
  }

  def determineRoute(request: RequestHeader): Option[Handler] = {
    val accessValues = (SessionUtil.isExpired(request), request.method, SessionUtil.validateSessionUserNotConnected(request), SessionUtil.validateSessionUser(request), SessionUtil.validateAngularPost(request), request.path);
    val route = (accessValues) match {
      //index	
      case p @ (_, "GET", _, _, _, _) if request.path == "/" => super.onRouteRequest(request);
      case p @ (_, "GET", _, _, _, _) if request.path == "/0" => super.onRouteRequest(request);

      //login
      case p @ (_, "POST", true, _, true, _) if request.path.startsWith("/login") => super.onRouteRequest(request);
      case p @ (_, "GET", _, _, _, _) if request.path == "/logout" => super.onRouteRequest(request);

      //register, confirm,password reset
      case p @ (_, "GET", _, _, _, _) if request.path.startsWith("/registerconfirm") => super.onRouteRequest(request);
      case p @ (_, "GET", _, _, _, _) if request.path.startsWith("/confirm") => super.onRouteRequest(request);
      case p @ (_, "POST", true, _, true, _) if request.path.startsWith("/accountpasswordaction") => super.onRouteRequest(request);
      case p @ (_, "POST", _, true, true, _) if request.path.startsWith("/accountcp") => super.onRouteRequest(request);
      case p @ (_, "GET", true, _, _, _) if request.path == "/forgotpassword" => super.onRouteRequest(request);
      case p @ (_, "GET", true, _, _, _) if request.path == "/resetpassword" => super.onRouteRequest(request);
      case p @ (_, "POST", true, _, true, _) if request.path.startsWith("/resetPasswordAction") => super.onRouteRequest(request);

      //account
      case p @ (false, "GET", _, true, true, _) if request.path.startsWith("/account") => super.onRouteRequest(request);
      case p @ (false, "POST", _, true, true, _) if request.path.startsWith("/account/") => super.onRouteRequest(request);
      case p @ (false, "POST", true, _, true, _) if request.path.startsWith("/register") => super.onRouteRequest(request);

      //account media
      case p @ (false, "POST", _, true, _, _) if request.path.startsWith("/accountMedia") => super.onRouteRequest(request);
      case p @ (false, "GET", _, true, true, _) if request.path.startsWith("/accountMedia") => super.onRouteRequest(request);
      case p @ (false, "DELETE", _, true, true, _) if request.path.startsWith("/accountMedia") => super.onRouteRequest(request);

      //ad
      case p @ (false, "POST", _, true, true, _) if request.path.startsWith("/ad") => super.onRouteRequest(request);
      case p @ (false, "GET", _, true, true, _) if request.path.startsWith("/ad") => super.onRouteRequest(request);
      case p @ (false, "GET", _, true, true, _) if request.path.startsWith("/activeAds") => super.onRouteRequest(request);
      case p @ (_, "GET", _, _, _, _) if request.path.startsWith("/adtemplates") => super.onRouteRequest(request);
      
      case p @ (false, "GET", _, true, true, _) if request.path.startsWith("/incomingdestination") => super.onRouteRequest(request);
      case p @ (false, "GET", _, true, true, _) if request.path.startsWith("/location") => super.onRouteRequest(request);
      case p @ (false, "GET", _, true, true, _) if request.path.startsWith("/adincoming") => super.onRouteRequest(request);
      case p @ (false, "POST", _, true, true, _) if request.path.startsWith("/adincoming") => super.onRouteRequest(request);
      case p @ (false, "DELETE", _, true, true, _) if request.path.startsWith("/adincoming") => super.onRouteRequest(request);
      case p @ (false, "GET", _, true, true, _) if request.path.startsWith("/AccountAds") => super.onRouteRequest(request);
      case p @ (false, "POST", _, true, true, _) if request.path.startsWith("/calculateOrder") => super.onRouteRequest(request);
      case p @ (false, "POST", _, true, true, _) if request.path.startsWith("/togglead") => super.onRouteRequest(request);
      case p @ (false, "POST", _, true, true, _) if request.path.startsWith("/updateIncoming") => super.onRouteRequest(request);
      case p @ (false, "POST", _, true, true, _) if request.path.startsWith("/updatedesign") => super.onRouteRequest(request);
      case p @ (false, "POST", _, true, true, _) if request.path.startsWith("/updateimpression") => super.onRouteRequest(request);
      case p @ (false, "POST", _, true, true, _) if request.path.startsWith("/updatename") => super.onRouteRequest(request);      
      case p @ (false, "POST", _, true, true, _) if request.path.startsWith("/addfund") => super.onRouteRequest(request);      
      
      case p @ (_, "GET", _, true, true, _) if request.path.startsWith("/destination") => super.onRouteRequest(request);

      case p @ (_, "GET", _, _, _, _) if request.path.startsWith("/assets") => super.onRouteRequest(request);
      case p @ (_, "GET", _, _, _, _) if request.path.startsWith("/favicon.ico") => super.onRouteRequest(request);

      case p @ (_, "GET", _, _, _, _) if request.path.startsWith("/terms") => super.onRouteRequest(request);
      case p @ (_, "GET", _, _, _, _) if request.path.startsWith("/devicecountries") => super.onRouteRequest(request);
      case p @ (_, "POST", _, _, _, _) if request.path.startsWith("/deviceregister") => super.onRouteRequest(request);
      case p @ (_, "POST", _, _, _, _) if request.path.startsWith("/devicead") => super.onRouteRequest(request);
      case p @ (_, "POST", _, _, _, _) if request.path.startsWith("/deviceImpression") => super.onRouteRequest(request);      
      case p @ (_, "POST", _, _, _, _) if request.path.startsWith("/devicecall") => super.onRouteRequest(request);


      
      
      
      case p @ _ if (SessionUtil.isExpired(request)) => Some(controllers.Application.sessionExpired)
      case p @ _ => Some(controllers.Application.authRequired)
    }

    route;
  }

  override def onStart(app: Application) {
    super.onStart(app)
    Logger.info("Starting...")
    init
    testDB
    testDynamoDB
  }

  def init {
    System.setProperty("private_captcha_key", Play.current.configuration.getString("private_captcha_key").get)
    System.setProperty("public_captcha_key", Play.current.configuration.getString("public_captcha_key").get)

    
    
    System.setProperty("accessKey", Play.current.configuration.getString("accessKey").get)
    System.setProperty("secretKey", Play.current.configuration.getString("secretKey").get)
    System.setProperty("SessionIdleTimeoutDurationInMinute", Play.current.configuration.getString("SessionIdleTimeoutDurationInMinute").get)
    System.setProperty("dynamoDBEndPoint", Play.current.configuration.getString("dynamoDBEndPoint").get)
    System.setProperty("redisEndPoint", Play.current.configuration.getString("redisEndPoint").get)    	
    
    
    System.setProperty("url", Play.current.configuration.getString("url").get)
    System.setProperty("maxActive", Play.current.configuration.getString("maxActive").get)
    System.setProperty("maxIdle", Play.current.configuration.getString("maxIdle").get)
    System.setProperty("minIdle", Play.current.configuration.getString("minIdle").get)
    
  }

  override def onHandlerNotFound(request: RequestHeader) = {
    Future.successful(NotFound("(: :)"))    
  }  

  def testDB {
    Logger.info("Test DB " + System.getProperty("url"))
    CDRServiceRegistry.service.listDIDs match {
      case Success(_) => {}
      case Failure(t) => throw t
    }
  }

  def testDynamoDB {
    Logger.info("Test DynamoDB " + DynamoDBEngine.client.listTables())
  }

}