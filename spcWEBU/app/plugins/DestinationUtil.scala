package plugins

import com.sponsoracall.cdr.service.CDRServiceRegistry


object DestinationUtil {
  val countries = {
    CDRServiceRegistry.service.retrieveCountries.get
  }  
    

  val countryOutCode=Map("United States of America"->"011","Australia"->"0011")
  def getOutCode(countrName:String)=countryOutCode.getOrElse("United States of America", "00");
  
  
  def normalizeUserPhoneCode(phone: String, countryCode: String): String = {
    val stripPlus=phone.replace("+", "").replace("-", "").replace("(", "").replace(")", "")
    if (stripPlus.startsWith(countryCode)) {
      stripPlus
    } else {
      countryCode + stripPlus
    }
  }  
  
}