'use strict';

/* Services */

var spcServices = angular.module('spcAppServices', [ 'ngResource' ]);

spcServices.factory('Account', [ '$resource', function($resource) {
	return $resource('/account/:accountId', {accountId : '@id'});
}]);

spcServices.factory('AccountAd', [ '$resource', function($resource) {
	return $resource('/account/:accountId/ad', {accountId : '@id'});
}]);

spcServices.factory('AccountMedia', [ '$resource', function($resource) {
	return $resource('/account/:accountId/media', {accountId : '@id',approved:'@approved'});
}]);

spcServices.factory('Media', [ '$resource', function($resource) {
	return $resource('/accountMedia/:accountId/media/:mediaId', {accountId: '@accountId', mediaId : '@mediaId'});
}]);


