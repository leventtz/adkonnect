CREATE TABLE CITY(
   ID   SERIAL,	
   COUNTRYCODE	TEXT ,
   STATE  TEXT,	
   CITY TEXT,
   PRIMARY KEY(ID)
);


CREATE INDEX "City"
  ON CITY
  USING btree (City COLLATE pg_catalog."default");


