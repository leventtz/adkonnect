﻿-- Table: rate

-- DROP TABLE rate;

CREATE TABLE rate
(
  id serial NOT NULL,
  accountid character(32),
  destinationid integer NOT NULL,
  rate numeric(7,4),
  ratetypeid text DEFAULT 'S'::text,
  CONSTRAINT rate_pkey PRIMARY KEY (id),
  CONSTRAINT rate_destinationid_fkey FOREIGN KEY (destinationid)
      REFERENCES destination (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "UniqueRate" UNIQUE (accountid, destinationid, ratetypeid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE rate
  OWNER TO dimwireaccess;
