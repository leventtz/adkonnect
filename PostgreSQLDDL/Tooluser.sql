
-- DROP TABLE tooluser;

CREATE TABLE tooluser  
(
  id char(32) NOT NULL primary key,
  firstname TEXT,
  lastname TEXT,
  email TEXT NOT NULL unique,
  password TEXT NOT NULL,
  active boolean DEFAULT false
)

