CREATE TABLE DIDTable(
   DID  TEXT,	
   GATEWAYID INT references gateway(id),
   PROVIDERID TEXT references account(id), 	
   PRIMARY KEY(DID)
);

