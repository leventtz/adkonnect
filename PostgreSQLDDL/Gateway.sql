CREATE TABLE Gateway(
   ID   SERIAL,	
   IP	TEXT ,
   DID  TEXT,	
   PROVIDERID TEXT,
   NAME TEXT ,
   hitcount int default 0,	
   active boolean default true,
   PRIMARY KEY(ID),
   UNIQUE(IP,DID)
);

