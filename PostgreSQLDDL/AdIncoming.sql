CREATE TABLE Adincoming (
  AdId             integer NOT NULL references Ad(id), 
  DestinationId    Integer NOT NULL references destination(id),
  exclude	   boolean default false,
  PRIMARY KEY(AdId,DestinationId)
) WITH (
    OIDS = FALSE
);