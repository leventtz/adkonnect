CREATE TABLE AdDA(
  AdId             integer NOT NULL references Ad(id), 
  DestinationId    Integer NOT NULL references destination(id),
  rate		   NUMERIC(7, 4),
  exclude	   boolean default false,
  PRIMARY KEY(AdId,DestinationId)
) WITH (
    OIDS = FALSE
);