CREATE TABLE Country (
  Id    integer NOT NULL PRIMARY KEY,
  Name  TEXT NOT NULL,
  countrycode text, 
  countryexitcode text,
Unique(name)
) WITH (
    OIDS = FALSE
  );
