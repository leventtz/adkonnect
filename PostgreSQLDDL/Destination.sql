CREATE TABLE Destination (
  Id             integer NOT NULL PRIMARY KEY,
  Name           TEXT NOT NULL,
  CountryId      integer references country(id),
  incomingdestination boolean default false,
  Unique(name)	
) WITH (
    OIDS = FALSE
  );
