CREATE TABLE DIALINGCODE(
   DestinationId  Integer references destination(id), 
   DA             TEXT  NOT NULL PRIMARY KEY
) WITH (
    OIDS = FALSE
  );
