-- Table: ad

-- DROP TABLE ad;

CREATE TABLE ad
(
  id serial NOT NULL,
  accountid character(32) NOT NULL,
  name text NOT NULL,
  mediaid integer,
  mediaivrid integer,
  maxduration integer,
  description text,
  active boolean NOT NULL DEFAULT false,
  budget numeric(7,2),
  allowrecharge boolean DEFAULT false,
  rechargeamount numeric(7,2),
  rechargethreshold numeric(7,2),
  numberofcalls integer DEFAULT 0,
  numberofminutes integer DEFAULT 0,
  balance numeric(7,2) DEFAULT 0,
  adtime timestamp without time zone DEFAULT timezone('utc'::text, now()),
  templatedefaultqty integer,
  templateunitprice numeric,
  templatepromptcost numeric,
  bypassswitch boolean DEFAULT false,
  CONSTRAINT ad_pkey PRIMARY KEY (id),
  CONSTRAINT ad_accountid_fkey FOREIGN KEY (accountid)
      REFERENCES account (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ad_mediaid_fkey FOREIGN KEY (mediaid)
      REFERENCES accountmedia (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ad_mediaivrid_fkey FOREIGN KEY (mediaivrid)
      REFERENCES accountmedia (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ad
  OWNER TO dimwireaccess;

