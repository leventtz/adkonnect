DROP TABLE location;

CREATE TABLE location
(
  id serial primary key,
  parentId integer NOT NULL,
  locationname text NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE location
  OWNER TO dimwireaccess;

CREATE INDEX location_parentid_index ON Location (parentid);
CREATE INDEX location_name_index ON Location (locationname);