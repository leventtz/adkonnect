CREATE TABLE AccountPhoneContacts(
  AccountId      char(32) NOT NULL references Account(id), 
  PhoneNumber    TEXT,
  description	 TEXT,
  Primary Key(AccountId,PhoneNumber)
) WITH (
    OIDS = FALSE
);
