CREATE TABLE AccountCreditCard (
  FirstName         TEXT NOT NULL,
  MiddleName        TEXT,
  LastName          TEXT NOT NULL,
  Address1          TEXT NOT NULL,
  Address2          TEXT,
  City              TEXT NOT NULL,
  PostalCode        TEXT,
  Country           integer NOT NULL,
  CreditCardNumber  TEXT NOT NULL PRIMARY KEY,
  ExpDate           TEXT,
  SecurityCode      TEXT,
  InActive          boolean DEFAULT false,
  AccountId         char(32) NOT NULL
) WITH (
    OIDS = FALSE
  );
