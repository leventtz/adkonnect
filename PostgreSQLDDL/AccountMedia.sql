CREATE TABLE AccountMedia (
  Id           serial NOT NULL PRIMARY KEY,
  AccountId    TEXT references Account(id), 
  Description  TEXT,
  MediaTypeId  TEXT,
  Approved     boolean NOT NULL DEFAULT false,
  MediaLocation TEXT,
  Unique(AccountId,description)
) WITH (
    OIDS = FALSE
);
