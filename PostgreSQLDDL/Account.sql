-- Table: account

-- DROP TABLE account;

CREATE TABLE account
(
  id varchar(32) NOT NULL,
  accounttype TEXT NOT NULL,
  companyname TEXT,
  firstname TEXT NOT NULL,
  middlename TEXT,
  lastname TEXT NOT NULL,
  address1 TEXT NOT NULL,
  address2 TEXT,
  city TEXT NOT NULL,
  state TEXT NOT NULL,
  postalcode TEXT,
  country TEXT NOT NULL,
  email TEXT NOT NULL,
  phone1 TEXT NOT NULL,
  phone2 TEXT,
  password TEXT NOT NULL,
  birthday TEXT,
  gender TEXT,
  active boolean DEFAULT false,
  CONSTRAINT account_pkey PRIMARY KEY (id),
  CONSTRAINT account_email_key UNIQUE (email),
  CONSTRAINT account_phone1_key UNIQUE (phone1)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE account
  OWNER TO dimwireaccess;

-- Index: "CompanyName"

-- DROP INDEX "CompanyName";

CREATE INDEX "CompanyName"
  ON account
  USING btree
  (companyname COLLATE pg_catalog."default");

-- Index: "LastName"

-- DROP INDEX "LastName";

CREATE INDEX "LastName"
  ON account
  USING btree
  (lastname COLLATE pg_catalog."default");

-- Index: phone1

-- DROP INDEX phone1;

CREATE INDEX phone1
  ON account
  USING btree
  (phone1 COLLATE pg_catalog."default");



