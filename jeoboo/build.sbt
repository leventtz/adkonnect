import net.ground5hark.sbt.concat._
import com.typesafe.sbt.web.SbtWeb
import play.PlayJava
import sbt._

name := """jeoboo"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala,SbtWeb)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  "org.scala-lang.modules" %% "scala-async"       % "0.9.2",
  "com.typesafe.play"   %% "play"               % "2.3.4",
  "com.typesafe.akka"   %% "akka-actor"         % "2.3.9",
  "com.typesafe.akka"   %% "akka-contrib"         % "2.3.9",
  "com.typesafe.akka"   %% "akka-testkit"         % "2.3.9",
  "org.scalaz"          %% "scalaz-core"        % "7.0.6",
  "com.typesafe"         % "config"             % "1.2.1",
  "org.typelevel"       %% "scalaz-contrib-210" % "0.2",
  "commons-pool"         % "commons-pool"       % "1.6",
  "commons-dbcp"         % "commons-dbcp"       % "1.4",
  "junit"                % "junit"              % "4.11"     % "test",
  "org.scalatest"       %% "scalatest"          % "2.2.1"    % "test",
  "org.scalatestplus"   %% "play"               % "1.2.0"    % "test",
  "org.scalacheck"      %% "scalacheck"         % "1.11.5"   % "test"
)

libraryDependencies += "com.amazonaws" % "aws-java-sdk" % "1.9.16"

libraryDependencies += "redis.clients" % "jedis" % "2.6.2"

libraryDependencies += "net.tanesha.recaptcha4j" % "recaptcha4j" % "0.0.7"

libraryDependencies += "net.authorize" % "anet-java-sdk" % "1.8.3"

pipelineStages in Assets := Seq(uglify, digest, gzip)

Concat.groups := Seq(
  "style-group.css" -> group(Seq("css/style1.css", "css/style2.css")),
  "script-group.js" -> group(Seq("js/script1.js", "js/script2.js")),
  "style-group2.css" -> group((sourceDirectory.value / "assets" / "style") * "*.css")
)