// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

var jeoBooApp = angular.module('jeobooApp', [
  'ui.bootstrap'
  ]);
var url;
var wsHost="ws://www.jeoboo.com/ws";

document.addEventListener('DOMContentLoaded', function () {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    var current = tabs[0];
    url = current.url;
  });
});

jeoBooApp.controller('JeobooController', ['$scope','$http','$timeout',function ($scope,$http,$timeout) {
  $scope.messageItems=[];
  $scope.message="";
  
  var ws = new WebSocket(wsHost);

  $scope.onMessage=function(event) {
   var objectItem=angular.fromJson(event.data);
   var dateItem=new Date(objectItem.timeStamp*1000);
   objectItem.date=dateItem.toLocaleDateString();
   objectItem.time=dateItem.toLocaleTimeString();
       $scope.$apply(function () {
            $scope.messageItems.push(objectItem); 
        });
  }

  ws.onmessage = function(event) {
    $scope.onMessage(event);
  }


 setTimeout(function() {
  $scope.retrieveMessages();
 },100);  

 $scope.addMessage=function() {
  $http.post(httpHost+encodeURIComponent(url),angular.toJson($scope.message))
  .success(
    function(data){
      $scope.message="";
    }
    ).error(
    function(errorData,responseCode){
      document.getElementById("fillThis").innerHTML=errorData;
    }
    );
  }
  
  $scope.retrieveMessages=function() {
    $http.get(httpHost+encodeURIComponent(url))
    .success(
      function(data){
        $scope.messageItems=[];
        var previousDate="";
        data.reverse().forEach(function(item) {
         var objectItem=angular.fromJson(item);
         var dateItem=new Date(objectItem.timeStamp*1000);
         objectItem.date=dateItem.toLocaleDateString();
         objectItem.time=dateItem.toLocaleTimeString();
         if(previousDate === objectItem.date) {
           objectItem.showDate=false;
         } else {
          objectItem.showDate=true;
          previousDate=objectItem.date
        }
        $scope.messageItems.push(objectItem);
      }
      );
      }
      ).error(
      function(errorData,responseCode){
        document.getElementById("fillThis").innerHTML=errorData;
      }
      );
    }

  }
  ]);












