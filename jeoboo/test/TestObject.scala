import scala.collection.mutable

/**
 * Created by leventtinaz on 3/14/15.
 */
object TestObject extends App{

  import scala.collection.mutable.{TreeSet,HashMap}

  val hashMap=new mutable.HashMap[Int,mutable.HashMap[Int,Ac]]()

class Ac(val urlString:String,val remoteAddress:String) {
override def hashCode=(urlString+remoteAddress).hashCode
override def equals(that:Any) = that.hashCode() == hashCode
}

  val ac1=new Ac("http","192.168.1.2")
  val ac2=new Ac("https","192.168.1.2")
  val ac3=new Ac("http","192.168.1.2")



  def addActor(ac:Ac): Unit = {
    hashMap.get(ac.urlString.hashCode) match {
      case Some(map)  => map +=(ac.hashCode -> ac)
      case None => hashMap += (ac.urlString.hashCode -> mutable.HashMap(ac.hashCode -> ac))
    }
    hashMap.foreach(f=> hashMap.size+" "+println(f))
    println("----------")
  }

  def removeActor(ac:Ac): Unit = {
    hashMap.get(ac.urlString.hashCode) match {
      case Some(map)  => {
        map -= ac.hashCode
        if(map.size==0)
          hashMap.remove(ac.urlString.hashCode)
      }
      case None => //Hmm its not here.
    }
    hashMap.foreach(f=> hashMap.size+" "+println(f))
    println("----------")
  }

  addActor(ac1)
  addActor(ac2)
  addActor(ac3)
  removeActor(ac3)
  removeActor(ac2)
}
