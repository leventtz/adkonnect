var jeoBooApp = angular.module('jeobooApp', ['ngAnimate']);

jeoBooApp.controller('jeobooindex', ['$scope',function($scope) {

    $scope.topicNamePress = function(keyEvent) {
        $scope.urlViewParam="urlview/"+encodeURIComponent($scope.topicName);
        if (keyEvent.which === 13 && $scope.topicName.length>0)
        location.href=$scope.urlViewParam;
    };
}]);

jeoBooApp.directive('onLongPress', function($timeout) {
	return {
		restrict: 'A',
		link: function($scope, $elm, $attrs) {
			$elm.bind('touchstart', function(evt) {
				// Locally scoped variable that will keep track of the long press
				$scope.longPress = true;

				// We'll set a timeout for 600 ms for a long press
				$timeout(function() {
					if ($scope.longPress) {
						// If the touchend event hasn't fired,
						// apply the function given in on the element's on-long-press attribute
						$scope.$apply(function() {
							$scope.$eval($attrs.onLongPress)
						});
					}
				}, 600);
			});

			$elm.bind('touchend', function(evt) {
				// Prevent the onLongPress event from firing
				$scope.longPress = false;
				// If there is an on-touch-end function attached to this element, apply it
				if ($attrs.onTouchEnd) {
					$scope.$apply(function() {
						$scope.$eval($attrs.onTouchEnd)
					});
				}
			});
		}
	};
})

jeoBooApp.controller('JeobooController', ['$scope','$interval', function($scope,$interval) {


    $scope.hideError=true;
    $scope.hideSpinner=true;



    createDropZone("#tableContainer",false);
    createDropZone("#uploadFileButton",true);
        createDropZone("#uploadFileButtonIcon",true);


    function createDropZone(id,clickable) {
        var myDropzone = new Dropzone(id, {
            url: "/upload",
            maxFilesize: 2,
            clickable:clickable,
            previewsContainer:'.previewsContainerNone',
            acceptedFiles: 'image/*'
        });

        myDropzone.on("complete", function(complete) {
          console.log(complete);
          setTimeout(function() {$scope.hideSpinner=true;},200);
          if(complete.status == 'error') {
            $scope.errorMessage=complete.previewTemplate.innerText;
            $scope.hideError=false;
          }
          myDropzone.removeFile(complete);
        });

        myDropzone.on("processing", function(complete) {
        console.log("test");
            setTimeout(function() {$scope.hideSpinner=false;},200);
        });
    }


    var app = document.getElementById("app");
    $scope.urlString = app.getAttribute("data-url");
    var urlHashCode = app.getAttribute("data-url-hashcode");
    var wsServer = app.getAttribute("data-ws-server");
    var ws;

    $scope.messageItems = [];
    $scope.message = "";


function checkStatus() {
    return function() {
    if (angular.isUndefined(ws) || ws.readyState > 1) {
        ws = new WebSocket(wsServer + encodeURIComponent($scope.urlString));

        ws.onmessage = function(event) {

            $scope.onMessage(event);
        };
    }
    }
}

checkStatus()();

setTimeout(function() {
    var service = {};
    service.serviceName = 'retrieve';
    service.urlString = $scope.urlString;
    service.message = "";
    ws.send(angular.toJson(service));

},1000)


$interval(checkStatus(), 5000);
    $scope.messageKeyPress = function(keyEvent) {
        if (keyEvent.which === 13)
            $scope.addMessage();
    };

    function formatMessageItem(eventData) {
        var objectItem = angular.fromJson(eventData);
        var dateItem = new Date(objectItem.timeStamp * 1000);
        objectItem.date = dateItem.toLocaleDateString();
        objectItem.time = dateItem.toLocaleTimeString();

        if (objectItem.messageType === 1) {
            objectItem.file = objectItem.message;
        }
        if ((objectItem.message.lastIndexOf('http://', 0) === 0) || (objectItem.message.lastIndexOf('https://', 0) === 0)) {
           objectItem.link=true;
        }
        return objectItem;
    }

    $scope.onMessage = function(event) {
        var data = angular.fromJson(event.data);
        switch (data.actionType) {
            case "add_message":
            case "retrieve_messages":
                data.messages.forEach(function(eventData) {
                    var objectItem = formatMessageItem(eventData);
                    if (urlHashCode == parseInt(objectItem.urlHashCode)) {
                        $scope.messageItems.push(objectItem);
                    }
                    scrollTable();
                });
                break;
            case "retrieve_messages_history":
                data.messages.forEach(function(eventData) {
                    var objectItem = formatMessageItem(eventData);
                    if (urlHashCode == parseInt(objectItem.urlHashCode)) {
                         $scope.messageItems.unshift(objectItem);
                    }
                });
                break;
            case "delete_message":
                data.messages.forEach(function(eventData) {
                    var objectItem = formatMessageItem(eventData);
                    var originalItem=findItem(objectItem);
                    if(originalItem) {
                        var index = $scope.messageItems.indexOf(originalItem);
                        $scope.messageItems.splice(index, 1);
                    }
                });
                break;
            case "rate_message":
                data.messages.forEach(function(eventData) {
                    var objectItem = formatMessageItem(eventData);
                    var originalItem=findItem(objectItem);
                    var index = $scope.messageItems.indexOf(originalItem);
                    if(originalItem) {
                        $scope.messageItems[index]=objectItem;
                    }
                });
                break;
        case "delete_message_failed":
            alert("Delete failed! Are you sure you are deleting the message that I've post?");
            break;
    }

    function findItem(item) {
     for(var i=0;i<$scope.messageItems.length;i++) {
          var data=$scope.messageItems[i];
          if(data.urlString===item.urlString && data.timeStamp === item.timeStamp) {
             return data;
          }
     }
    }

    $scope.messageItems.sort(function(item1, item2){return item1.timeStamp-item2.timeStamp;});
    var tempDate='';
    $scope.messageItems.forEach(function(data) {
        data.showDate=true;
        if(data.date === tempDate) {
            data.showDate=false;
        } else {
            tempDate=data.date;
            data.showDate=true;
        }
    });

    setTableHeight();


    $scope.$apply(function() {
        //fire angular.
    });

};

function setTableHeight() {
    var height=jQuery( document ).height();
    var tableHeight=height*0.65
    $scope.tableStyle={'height':tableHeight+'px','overflow':'auto'};
}

function scrollTable() {
    var scrollBottom = Math.max($('#messageTable').height() - $('#myDiv').height() + 20, 0);
    jQuery('#tableContainer').scrollTop(scrollBottom);
}


$scope.addMessage = function() {
    var service = {};
    service.serviceName = 'add';
    service.urlString = $scope.urlString;
    service.message = $scope.message;
    ws.send(angular.toJson(service));
    $scope.message = "";
    $scope.filter= "";
};


jQuery("#tableContainer").scroll(function() {
    if (jQuery("#tableContainer").scrollTop() < 5) {
        if ($scope.messageItems.length > 0) {
            var firstItem = $scope.messageItems[0];
            var firstTimeStamp = firstItem.timeStamp;
            retrieveHistory(firstTimeStamp);
        }
    }
});


var retrieveHistory = function(timeStamp) {
    var service = {};
    service.serviceName = 'retrieve_history';
    service.urlString = $scope.urlString;
    service.timeStamp = timeStamp;
    service.message = $scope.message;
    ws.send(angular.toJson(service));
    $scope.message = "";
};


$scope.deleteMessage = function(messageItem) {
    var delConfirm = confirm("Do you wish to delete this message?");
    if (delConfirm === true) {
        var service = {};
        service.serviceName = 'delete';
        service.urlString = messageItem.urlString;
        service.timeStamp = messageItem.timeStamp;
        ws.send(angular.toJson(service));
    }
};

$scope.tumbMessage = function(messageItem,isUp) {
    var service = {};
    service.serviceName = isUp ? 't_Up':'t_Down';
    service.urlString = messageItem.urlString;
    service.timeStamp = messageItem.timeStamp;
    ws.send(angular.toJson(service));
};


$scope.editMessage = function(messageItem) {
    $scope.message = messageItem.message;
};


$scope.itemOnLongPress = function(id) {

}

$scope.itemOnTouchEnd = function(id) {

}


}]);