package model
import play.api.libs.json.Json
/**
 * Created by leventtinaz on 2/27/15.
 */
object Model {
  implicit val messageItemReader=Json.reads[MessageItem]
  implicit val messageItemWriter=Json.writes[MessageItem]
  implicit val messageItemActionWriter=Json.writes[MessageItemAction]


  //message type 0 message,1 file,2 link,etc
  case class MessageItem(timeStamp:Long,message:String,urlHashCode:Int,urlString:String,messageType:Int,tUp:Int,tDown:Int)
  case class MessageItemAction(actionType:String,messages:List[MessageItem],normalizedUrlString:String);

}
