package service

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient
import com.amazonaws.{Protocol, ClientConfiguration}
import com.amazonaws.auth.BasicAWSCredentials

/**
 * Created by leventtinaz on 2/27/15.
 */
object DynamoDBEngine {
  val conf = new ClientConfiguration();

  //  conf.setProxyPort(8080);
  //val cr = new BasicAWSCredentials("AKIAJPUHRZLVFVIP43OQ", "hQqydgvAlWlq6r9edhGvEjQ/sbP5dZcpHgm/ROoX");
  private val cr = new BasicAWSCredentials(GlobalSettings.awsAccessKey, GlobalSettings.awsSecretKey);

  conf.setProtocol(Protocol.HTTPS);
  conf.setUserAgent(GlobalSettings.userAgent);
  val client = new AmazonDynamoDBClient(cr, conf)
  client.setEndpoint(GlobalSettings.dynamoDBEndPoint)
}
