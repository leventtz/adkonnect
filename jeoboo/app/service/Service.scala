package service

import java.util
import java.util.{ArrayList, HashMap}

import com.amazonaws.services.dynamodbv2.model._
import com.amazonaws.services.s3.model._
import play.Play
import scala.collection.mutable.ListBuffer
import scala.util.Failure
import scala.util.Success
import java.time.Instant
import model.Model._
import scalaz._
import Scalaz._
import scala.async.Async._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import actors.BoardActor
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client

object Service {

  val tumbTotal=play.Play.application.configuration.getInt("tumb_total")
  val tumbDiff=play.Play.application.configuration.getInt("tumb_diff")


  private def isMessageValid(item:MessageItem): Boolean = {
    val messageTumbTotal=item.tUp+item.tDown
    if(messageTumbTotal<tumbTotal)
      true
    else {
      val messageTumbDiff=item.tUp-item.tDown
      if(messageTumbDiff<tumbDiff)
        false
      else
        true
    }
  }


  def retrieveMessageItemsLast(urlString:String) : Future[\/[Throwable,List[MessageItem]]] = async {
    \/.fromTryCatch {
      retrieveMessageItems(urlString, Instant.now.getEpochSecond())
    }
  }

  def retrieveMessageItemsHistory(normalizedUrlString:String,timeStamp:Long) : Future[\/[Throwable,List[MessageItem]]] = async {
    \/.fromTryCatch {
      retrieveMessageItems(normalizedUrlString, timeStamp)
    }
  }

  private def retrieveMessageItems(urlString:String,timeStamp:Long):List[MessageItem] = {
    require(!urlString.isEmpty,"Invalid UrlString")
    val queryRequest = new QueryRequest()
      val keyConditions = new HashMap[String, Condition]
      keyConditions.put("UrlStringNormalized", new Condition().withComparisonOperator(ComparisonOperator.EQ).withAttributeValueList(new AttributeValue(normalizeUrl(urlString))))
      keyConditions.put("TimeStamp", new Condition().withComparisonOperator(ComparisonOperator.LT).withAttributeValueList(new AttributeValue().withN(timeStamp.toInt.toString))) 
    queryRequest.withTableName("MessageTable").withKeyConditions(keyConditions)
      queryRequest.setScanIndexForward(false)
      queryRequest.setLimit(20)
      val result = DynamoDBEngine.client.query(queryRequest)
      val items = result.getItems()

      val listBuffer = new ListBuffer[MessageItem]()
      for (i <- 0 until items.size()) {
        val messageItem=createMessageItem(items.get(i))
        if(isMessageValid(messageItem)) {
          listBuffer.append(messageItem)
        }
      }

    listBuffer.reverse.toList
  }
  private def createMessageItem(item:java.util.Map[String,AttributeValue]): MessageItem = {
    val urlStringNormalized=item.get("UrlStringNormalized").getS
    val timeStamp: String = item.get("TimeStamp").getN
    val message = item.get("Message").getS
    val messageType = item.get("MessageType").getN.toInt
    val remoteAddress = item.get("RemoteAddress").getS
    val urlString = item.get("UrlString").getS

    val tUp:Int= {
      val tUpGet=item.get("tUp")
      if(tUpGet!=null)
        tUpGet.getN.toInt
      else
        0
    }
    val tDown:Int= {
      val tDownGet=item.get("tDown")
      if(tDownGet!=null)
        tDownGet.getN.toInt
       else
        0
    }

    MessageItem(timeStamp.toInt, message, urlStringNormalized.hashCode, urlString, messageType, tUp, tDown)
  }

  def serviceRate(urlString:String,timeStamp:Long,tUp:Int,tDown:Int) = async {
    \/.fromTryCatch {
      require(!urlString.isEmpty, "Invalid UrlString")

      val keys = new util.HashMap[String, AttributeValue]()
      keys.put("UrlStringNormalized", new AttributeValue(normalizeUrl(urlString)))
      keys.put("TimeStamp", new AttributeValue().withN(timeStamp.toString))
      val normalizedUrlString=normalizeUrl(urlString)

      val updateItemRequest = new UpdateItemRequest().withTableName("MessageTable").withReturnValues(ReturnValue.ALL_NEW).withKey(keys)
        .addAttributeUpdatesEntry(
          "tUp", new AttributeValueUpdate()
        .withValue(new AttributeValue().withN(tUp.toString))
        .withAction(AttributeAction.ADD))
        .addAttributeUpdatesEntry(
          "tDown", new AttributeValueUpdate()
        .withValue(new AttributeValue().withN(tDown.toString))
        .withAction(AttributeAction.ADD))
      val result = DynamoDBEngine.client.updateItem(updateItemRequest)

      val messageItem = createMessageItem(result.getAttributes)
      if (isMessageValid(messageItem))
        BoardActor() ! MessageItemAction("rate_message", List(messageItem),normalizedUrlString)
      else
        BoardActor() ! MessageItemAction("delete_message", List(messageItem),normalizedUrlString)
    }
  }

  private def rateAllowed(normalizedUrlString:String,timeStamp:Long,remoteIP:String):Boolean = {
    true
  }

  def addMessage(urlString:String,message:String,messageType:Int=0)(implicit remoteAddress:String) = async {
    \/.fromTryCatch {
      require(!urlString.isEmpty,"Invalid UrlString")
      require(!message.isEmpty,"Invalid message")
      require(!remoteAddress.isEmpty,"Invalid remoteAddress")
      val normalizedUrlString=normalizeUrl(urlString)
      val javaList = new ArrayList[String]() 
      val timeStamp=Instant.now().getEpochSecond()
      val messageItem = new HashMap[String, AttributeValue]() 
      messageItem.put("UrlStringNormalized", new AttributeValue().withS(normalizedUrlString))
      messageItem.put("TimeStamp", new AttributeValue().withN(timeStamp.toString))
      messageItem.put("Message", new AttributeValue().withS(message)) 
      messageItem.put("UrlString", new AttributeValue().withS(urlString)) 
      messageItem.put("RemoteAddress",new AttributeValue(remoteAddress))
      messageItem.put("MessageType",new AttributeValue().withN(messageType.toString))
      val putItemRequest = new PutItemRequest().withTableName("MessageTable").withItem(messageItem)
      DynamoDBEngine.client.putItem(putItemRequest) 
      val item=MessageItem(timeStamp,message,normalizedUrlString.hashCode,urlString,messageType,0,0)
      BoardActor() ! MessageItemAction("add_message",List(item),normalizedUrlString)
    }
  }

  def deleteMessage(urlString:String,timeStamp:Long)(implicit remoteAddress:String)  =  async {
    \/.fromTryCatch {
      require(!urlString.isEmpty, "Invalid urlString")
      val normalizedUrlString=normalizeUrl(urlString)

      try {
        val deleteItemRequest = new DeleteItemRequest()
        val keys = new util.HashMap[String, AttributeValue]()
        keys.put("UrlStringNormalized", new AttributeValue(normalizedUrlString))
        keys.put("TimeStamp", new AttributeValue().withN(timeStamp.toString))
        val expectedAttributes = new util.HashMap[String, ExpectedAttributeValue]()
        val expectedValue = new ExpectedAttributeValue()
        expectedValue.setValue(new AttributeValue(remoteAddress))
        expectedValue.setComparisonOperator(ComparisonOperator.EQ)
        expectedAttributes.put("RemoteAddress", expectedValue)
        deleteItemRequest.setKey(keys)
        deleteItemRequest.setTableName("MessageTable")
        deleteItemRequest.setExpected(expectedAttributes)
        deleteItemRequest.setReturnValues(ReturnValue.ALL_OLD)
        val result = DynamoDBEngine.client.deleteItem(deleteItemRequest)

        val messageItem = MessageItem(timeStamp, result.getAttributes().get("Message").getS, normalizedUrlString.hashCode, urlString, result.getAttributes().get("MessageType").getN.toInt, 0, 0)

        if (result.getAttributes() != null && result.getAttributes().get("MessageType").getN.toString.equals("1")) {
          val fileName = result.getAttributes().get("Message").getS
          val objectName = fileName.substring(fileName.indexOf("/files/") + 7)
          val deleteObjectRequest = new DeleteObjectRequest(s3Bucket + "/files", objectName)

        }
        BoardActor() ! MessageItemAction("delete_message", List(messageItem),normalizedUrlString)

      } catch {
        case t: Throwable => {
          BoardActor() ! MessageItemAction("connection_closed", List(),normalizedUrlString)
        }
      }
    }

  }


  val s3Bucket = Play.application.configuration.getString("aws.s3.bucket") 
  val s3Client:AmazonS3Client = {
    val secretKey = Play.application.configuration.getString("aws.secret.key") 
    val accessKey= Play.application.configuration.getString("aws.access.key") 
    val awsCredentials = new BasicAWSCredentials(accessKey, secretKey) 
    val amazonS3=new AmazonS3Client(awsCredentials) 
    amazonS3.createBucket(s3Bucket) 
    amazonS3
  }

  def uploadFile(urlString:String,file:java.io.File)(implicit remoteAddress:String) =  async {
    \/.fromTryCatch {
      require(!urlString.isEmpty, "Invalid urlString")
      val putObjectRequest = new PutObjectRequest(s3Bucket + "/files", file.getName, file)
      val acl = new AccessControlList()
      acl.grantPermission(GroupGrantee.AllUsers, Permission.Read)
      putObjectRequest.setAccessControlList(acl)
      val result = s3Client.putObject(putObjectRequest)
      val mediaLocation = "https://s3.amazonaws.com/" + s3Bucket + "/files/" + file.getName
      addMessage(urlString, mediaLocation, 1)
      file.delete()
    }
  }

  def normalizeUrl(urlString:String) = urlString.replaceAll("https:","").replaceAll("http:","").replaceAll("//","").replaceAll("\\.","").replaceAll("www","").toLowerCase

}
