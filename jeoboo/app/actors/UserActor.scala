package actors

import akka.actor.Actor
import akka.actor.ActorLogging
import scala.concurrent.duration._
import akka.actor.ActorSystem
import akka.actor.Props
import akka.contrib.throttle.Throttler._
import akka.contrib.throttle.TimerBasedThrottler
import akka.event.LoggingReceive
import model.Model.MessageItem
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import akka.actor.ActorRef
import akka.actor.Props
import service.Service
import play.api.libs.json.Json
import model.Model._
import akka.event.Logging
import play.libs.Akka
import scala.concurrent.ExecutionContext.Implicits.global

class UserActor(val remoteAddress: String, val urlString:String,board: ActorRef, out: ActorRef) extends Actor with ActorLogging {
  private implicit val clientRemoteAddress = remoteAddress
  private var lastActionTimeStamp = System.currentTimeMillis()

  override def preStart() = {
    BoardActor() ! Subscribe(normalizedUrl,remoteAddress)
  }

  override def postStop() = {

  }
  def receive = LoggingReceive {
    case m:MessageItemAction if sender == board => out ! Json.toJson(m)
    case service:JsValue => {
      try {
        runServices(service)
      } catch {
        case _ => //eat the error
      }
    }
    case t@_ => //do nothing
  }

  def runServices(jsValue: JsValue): Unit = {
    val serviceName = jsValue.\("serviceName").as[String]
    val urlString = jsValue.\("urlString").as[String]
    serviceName match {
      case "delete" if serviceAllowed => Service.deleteMessage(urlString, jsValue.\("timeStamp").as[Long])
      case "retrieve" => {
        Service.retrieveMessageItemsLast(urlString).map {
          f =>
            f.fold(x =>  out ! Json.toJson(MessageItemAction("retrieve_messages", List(),Service.normalizeUrl(urlString))),
              list =>    out ! Json.toJson(MessageItemAction("retrieve_messages", list,Service.normalizeUrl(urlString)))
            )
        }
      }
      case "retrieve_history" if serviceAllowed => {
        val timeStamp = jsValue.\("timeStamp").as[Long]
        Service.retrieveMessageItemsHistory(urlString,timeStamp).map {
          f =>
            f.fold(x =>  sender ! Json.toJson(MessageItemAction("retrieve_messages_history", List(),Service.normalizeUrl(urlString))),
            list =>    sender ! Json.toJson(MessageItemAction("retrieve_messages_history", list,Service.normalizeUrl(urlString)))
            )
        }
      }
      case "add" if serviceAllowed => Service.addMessage(urlString, jsValue.\("message").as[String])
      case "t_Up" if serviceAllowed =>  Service.serviceRate(urlString,jsValue.\("timeStamp").as[Long],1,0)
      case "t_Down" if serviceAllowed =>  Service.serviceRate(urlString,jsValue.\("timeStamp").as[Long],0,1)

      case _ => //do nothing
    }
  }


  val normalizedUrl=Service.normalizeUrl(urlString)

  def serviceAllowed: Boolean = {
    if ((System.currentTimeMillis() - lastActionTimeStamp) / 1000 > UserActor.serviceThrottle) {
      lastActionTimeStamp = System.currentTimeMillis();
      true
    } else {
      false
    }
  }

  override def hashCode():Int = (remoteAddress+normalizedUrl).hashCode

}

object UserActor {
  def props(remoteAddress: String, urlString: String)(out: ActorRef) = {
    Props(new UserActor(remoteAddress, urlString, BoardActor(), out))
  }

  val serviceThrottle = play.Play.application.configuration.getInt("service_throttle")
  val actorPerUrl = play.Play.application.configuration.getInt("actor_per_url")
}
