package actors

import akka.actor._
import akka.event.LoggingReceive
import model.Model.MessageItem
import play.libs.Akka
import model.Model._
import scala.collection.mutable.HashMap

case class ActorRefData(normalizedUrlString:String,remoteAddress:String,sender:ActorRef)

class BoardActor extends Actor with ActorLogging {
  var users: Set[ActorRef] = Set.empty[ActorRef]
  var hashMap: HashMap[String, HashMap[Int, ActorRefData]] = HashMap.empty[String, HashMap[Int, ActorRefData]]
  var senderMap: HashMap[ActorRef, ActorRefData] = HashMap.empty[ActorRef, ActorRefData]

  def receive = LoggingReceive {
    case Subscribe(normalizedUrlString: String, remoteAddress: String) => {
      addActor(ActorRefData(normalizedUrlString, remoteAddress, sender))
      context watch sender
    }
    case m: MessageItemAction => {
      sendMessage(m)
    }
    case Terminated(user) => {
      removeActor(user)
    }
  }

  def addActor(ac: ActorRefData): Unit = {
    hashMap.get(ac.normalizedUrlString) match {
      case Some(map) => {
        map += (ac.hashCode -> ac)
      }
      case None => hashMap += (ac.normalizedUrlString -> HashMap(ac.hashCode -> ac))
    }
    senderMap += (ac.sender -> ac)
  }

  def removeActor(sender: ActorRef): Unit = {
    senderMap.get(sender) match {
      case Some(ac) => {
        hashMap.get(ac.normalizedUrlString) match {
          case Some(map) => {
            map -= ac.hashCode()
            if (map.size == 0)
              hashMap.remove(ac.normalizedUrlString)
              senderMap.remove(sender)
          }
          case None => //Hmm its not here.
        }
      }
      case None => //hmmm again
    }
  }

  def sendMessage(m: MessageItemAction): Unit = {
      hashMap.get(m.normalizedUrlString) match {
        case Some(map) => map.foreach(f=>f._2.sender ! m)
        case _ => //hmmmm
      }
  }
}

object BoardActor {
  lazy val board = Akka.system().actorOf(Props[BoardActor])
  def apply() = board
}

case class Subscribe(normalizedUrlString:String,remoteAddress:String)