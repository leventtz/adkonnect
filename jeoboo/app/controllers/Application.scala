package controllers

import java.util.UUID

import actors.UserActor
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.{Action, Controller, WebSocket}
import service._
import scala.concurrent.duration._
import scalaz.Scalaz._
import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}
import akka.actor.ActorSystem
import scala.concurrent.ExecutionContext.Implicits.global
import java.util.concurrent.TimeUnit._
import scala.concurrent.duration.{Duration, FiniteDuration}
import play.api.Play
import play.api.Play.current
import scala.concurrent.Future
import play.api.libs.Crypto
import play.api.mvc._

object Application extends Controller {

  def index = Action { request =>
    Ok(views.html.index())
  }

  def url(urlString: String) = Action { implicit request =>
    create(urlString)
  }

  def create(urlString:String,random:Boolean=false)(implicit request:RequestHeader):play.api.mvc.Result =  {
    val forwardAddress: String = getForwardedAddress

    val dataSession: String = Crypto.encryptAES(forwardAddress)

    val wsServer: String = Play.current.mode match {
      case play.api.Mode.Prod => s"ws://www.jeoboo.com:1453/ws/$dataSession/"
      case play.api.Mode.Dev => s"ws://192.168.04:9000/ws/$dataSession/"
    }

    Ok(views.html.url(urlString, Service.normalizeUrl(urlString).hashCode, wsServer,random))
      .withSession((request.session +
      ("session-args" -> Crypto.encryptAES(forwardAddress))) +
      ("x-session-args"->Crypto.encryptAES(urlString)))
  }

  def upload = Action.async(parse.multipartFormData) { implicit request =>
    request.session.get("x-session-args").map {
      urlEncrypted =>
        val urlString=Crypto.decryptAES(urlEncrypted)
        request.session.get("session-args") match {
          case Some(value) if getForwardedAddress == Crypto.decryptAES(value) => {
            request.body.file("file").map { file =>
              file.contentType match {
                case Some(fileConentType) if fileConentType.startsWith("image") => {
                  val fileName="/tmp/"+UUID.randomUUID().toString+file.filename
                  val tempFile=new java.io.File(fileName)
                  tempFile.deleteOnExit
                  file.ref.moveTo(tempFile)
                  Service.uploadFile(urlString,tempFile)(getForwardedAddress)
                  Future.successful(Ok("File uploaded"))
                }
                case _ => {
                  Future.successful(BadRequest)
                }
              }
            }.getOrElse {
              Future.successful(BadRequest)
            }
          }
          case _ => Future.successful(BadRequest)
        }

    }.getOrElse {
      Future.successful(BadRequest)
    }
  } 

  def getForwardedAddress(implicit request:RequestHeader): String = {
     val forwardAddress=request.headers.get("X-Forwarded-For") match {
      case Some(address) => address
      case _ => request.remoteAddress;
    }
    forwardAddress
  }

  def ws(urlString: String, dataSession: String) = WebSocket.tryAcceptWithActor[JsValue, JsValue] { implicit request =>
    Future.successful(Right(UserActor.props(Crypto.decryptAES(dataSession), urlString)))
  }

}