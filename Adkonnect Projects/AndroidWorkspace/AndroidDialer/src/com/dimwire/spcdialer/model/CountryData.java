package com.dimwire.spcdialer.model;

public class CountryData {
	private final String countryName;
	private final String countryCode;
	public CountryData(String countryName,String countryCode) {
		this.countryName=countryName;
		this.countryCode=countryCode;
	}
	public String toString() {
		return countryName;
	}
	public String getCountryName() {
		return countryName;
	}
	public String getCountryCode() {
		return countryCode;
	}
	 
}
