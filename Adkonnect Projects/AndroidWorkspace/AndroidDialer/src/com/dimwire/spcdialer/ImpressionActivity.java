package com.dimwire.spcdialer;

import static com.dimwire.spcdialer.util.DialerUtil.DIALER_PREF_NAME;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;

import com.dimwire.spcdialer.http.GenericPostTask;
import com.dimwire.spcdialer.http.GenericResponse;
import com.dimwire.spcdialer.util.DialerUtil;

public class ImpressionActivity extends Activity {
	private ProgressDialog mDialog;
	private ImageView imageViews[];
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.activity_impression);
			mDialog = new ProgressDialog(this);
			mDialog.setMessage(getResources().getString(R.string.loading));
			mDialog.setCancelable(false);			
			mDialog.show();
			
			Map<String, String> map = new HashMap<String, String>(2);

			map.put("phone",
					getSharedPreferences(DIALER_PREF_NAME, Context.MODE_PRIVATE)
							.getString("phone", ""));
			map.put("device",
					getSharedPreferences(DIALER_PREF_NAME, Context.MODE_PRIVATE)
							.getString("device", ""));
			map.put("uniquedeviceid",
					getSharedPreferences(DIALER_PREF_NAME, Context.MODE_PRIVATE)
							.getString("uniquedeviceid", ""));
			map.put("dial",
					getSharedPreferences(DIALER_PREF_NAME, Context.MODE_PRIVATE)
							.getString("dial", "").replaceAll("-", "")
							.replaceAll("\\)", "").replaceAll("\\(", ""));
			map.put("country",
					getSharedPreferences(DIALER_PREF_NAME, Context.MODE_PRIVATE)
							.getString("country", ""));

			map.put("postalCode",
					getSharedPreferences(DIALER_PREF_NAME, Context.MODE_PRIVATE)
							.getString("postalCode", ""));			
			
			
			String serverAddress = DialerUtil.getServerAddress(this);
			String urlString = serverAddress + "deviceImpression";
			
			
			final GenericPostTask adTask = new GenericPostTask(urlString, map) {
				@Override
				protected void onPostExecute(GenericResponse result) {
					super.onPostExecute(result);
					final LinearLayout layout=(LinearLayout)findViewById(R.id.imageLinearLayout);
					if (result != null) {
						try {
							JSONObject object = new JSONObject(
									result.getResponseString());
							
							JSONArray impressions = object.getJSONArray("impressions");
							
							imageViews = new ImageView[impressions.length()];
							
							for(int i=0;i<impressions.length();i++) {
								ImageView imageView=new ImageView(ImpressionActivity.this);
								int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 94, getResources().getDisplayMetrics());
								imageView.setMinimumHeight(px);
								ViewGroup.LayoutParams imageViewParams=new TableRow.LayoutParams(
								        ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
								
								imageView.setLayoutParams(imageViewParams);
								imageView.setVisibility(ViewGroup.INVISIBLE);
								
								layout.addView(imageView);
								imageViews[i]=imageView;
								JSONObject impressionjson = new JSONObject(impressions.getString(i));
								String url=impressionjson.getString("impressionView");
								String adId=impressionjson.getString("adId");
								String adAccountId=impressionjson.getString("accountId");
								new ImageDownloader(new ImpressionViewData(imageView,adId,adAccountId)).execute(url);
							}
						} catch (JSONException e) {
							try {
								JSONObject object = new JSONObject(
										result.getResponseString());
								JSONObject objectAd = new JSONObject(
										object.getString("ad"));
								String mediaDescription = objectAd
										.getString("description");
								
								WebView webView=new WebView(ImpressionActivity.this);
								webView.loadData(mediaDescription, "text/html",
										null);

								webView.setWebViewClient(new WebViewClient() {
									public void onPageFinished(WebView view,
											String url) {
										mDialog.dismiss();
									}

									@Override
									public void onReceivedSslError(WebView view,
											SslErrorHandler handler, SslError error) {
										handler.proceed(); // Ignore SSL certificate
									}
								});								
								
								layout.addView(webView);
								mDialog.dismiss();
								
							}catch(Exception ex1) {
								Log.e("ImpressionActivity", ex1.getMessage());
								mDialog.dismiss();
							}
						}
					} else {
						mDialog.dismiss();
						DialerUtil.showNetworkErrorDialog(ImpressionActivity.this);
					}
				}
			};
			adTask.execute();
		} catch (Exception ex) {
			Log.e("ImpressionActivity", ex.getMessage(), ex);
			mDialog.dismiss();
			DialerUtil.showNetworkErrorDialog(ImpressionActivity.this);
		}
	}


	private class AdClick implements View.OnClickListener {
		private ImpressionViewData impressionViewData;
		AdClick(ImpressionViewData impressionViewData) {
			this.impressionViewData=impressionViewData;
		}
		@Override
		public void onClick(View v) {
			SharedPreferences pref = getApplicationContext()
					.getSharedPreferences(DIALER_PREF_NAME,
							Context.MODE_PRIVATE);
			Editor editor = pref.edit();
			editor.putString("adId", impressionViewData.adId);
			editor.putString("adAccountId", impressionViewData.adAccountId);
						
			editor.commit();

			Intent myIntent = new Intent(ImpressionActivity.this,
					AdActivity.class);
			startActivity(myIntent);

		}
	}	

	class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
		private ImpressionViewData impressionViewData;
		  public ImageDownloader(ImpressionViewData impressionViewData) {
		      this.impressionViewData = impressionViewData;
		  }

		  protected Bitmap doInBackground(String... urls) {
		      String url = urls[0];
		      Bitmap mIcon = null;
		      try {
		        InputStream in = new java.net.URL(url).openStream();
		        mIcon = BitmapFactory.decodeStream(in);
		      } catch (Exception e) {
		          Log.e("Error", e.getMessage());
		      }
		      return mIcon;
		  }

		  protected void onPostExecute(Bitmap result) {
			  impressionViewData.imageView.setImageBitmap(result);
			  impressionViewData.imageView.setVisibility(View.VISIBLE);
			  impressionViewData.imageView.setOnClickListener(new AdClick(impressionViewData));			
			  for(int i=0;i<imageViews.length*0.6;i++){
				  if(!imageViews[i].isShown()) {
					  return;
				  }
			  }
			  mDialog.dismiss();
		  }
	}
	
	private class ImpressionViewData {
		final ImageView imageView;
		final String adId;
		final String adAccountId;
		ImpressionViewData(ImageView imageView,String adId,String adAccountId){
			this.imageView=imageView;
			this.adId=adId;
			this.adAccountId=adAccountId;
		}
	}
}



