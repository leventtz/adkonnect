package com.dimwire.spcdialer;

import android.app.Activity;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.util.Log;

public class CallTimeoutThread {
	private static final int VIB_MIN = 1; // vibrate before last minute
	private static  String phoneState="";
	public static void setPhoneState(String newPhoneState) {
		phoneState=newPhoneState;
	}
	
	public static void createVibrator(final int durationInMinutes,
			final Activity activity) {
		if(durationInMinutes<2) {
			return;
		}
		new Thread(new Runnable() {
			Vibrator v = (Vibrator) activity
					.getSystemService(Context.VIBRATOR_SERVICE);

			@Override
			public void run() { 
				try { 
					if(phoneState.equals("IDLE")) {
						return;
					}
					Thread.sleep((durationInMinutes - VIB_MIN)*60 * 1000);

					if(phoneState.equals("IDLE")) {
						return;
					}
					
					Log.i("CallTimeoutThread","phoneState:"+phoneState+" "+phoneState.equals("IDLE"));
					
					v.vibrate(300);
					Thread.sleep(1000);
					v.vibrate(300);
					Thread.sleep(1000);
					v.vibrate(300);
					try {
					    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
					    Ringtone r = RingtoneManager.getRingtone(activity, notification);
					    r.play();
					} catch (Exception e) {
					    Log.e("CallTimeoutThread", e.getMessage());
					}					
				} catch (Exception ex) {
					Log.e("CallTimeoutThread", ex.getMessage());
				}

			}
		}).start();
	}
	
}
