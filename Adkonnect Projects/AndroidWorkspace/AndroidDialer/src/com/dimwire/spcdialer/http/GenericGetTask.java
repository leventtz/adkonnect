package com.dimwire.spcdialer.http;

import org.apache.http.client.methods.HttpGet;

import android.util.Log;

public class GenericGetTask extends GenericHttpTask {

	public GenericGetTask(String urlString) {
		super(urlString);
	}

	@Override
	public GenericResponse doInBackground(String... params) {
		GenericResponse response=null;
		try {
			response=createGenericResponse(client.execute(new HttpGet(urlString)));
		} catch (Exception e) {
			Log.e("GenericGetTask",e.getMessage(),e);
		}
		if(this.isCancelled())
			return null;
		else 
		   return response;
	}
	
}