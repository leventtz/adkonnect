package com.dimwire.spcdialer.http;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

public class GenericPostTask extends GenericHttpTask {
	private final Map<String, String> formValues;
	public GenericPostTask(String urlString, Map<String, String> formValues) {
		super(urlString);
		if (formValues == null)
			throw new IllegalArgumentException("Null Parameters!");

		this.formValues = formValues;
	}

	@Override
	public GenericResponse doInBackground(String... params) {
		GenericResponse genericResponse=null;
		try {
			List<NameValuePair> postData = new ArrayList<NameValuePair>(
					formValues.size());
			for (String key : formValues.keySet()) {
				postData.add(new BasicNameValuePair(key, formValues.get(key)));
			}
			HttpPost post = new HttpPost(urlString);
			post.addHeader("hash",UUID.randomUUID().toString());
			post.setEntity(new UrlEncodedFormEntity(postData));
			genericResponse = createGenericResponse(client
					.execute(post));
		} catch (Exception e) {
			Log.e("GenericPostTask",e.getMessage(),e);
		}
		return genericResponse;
	}

}