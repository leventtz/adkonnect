//
//  main.m
//  Dialer
//
//  Created by Levent Tinaz on 5/5/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SPCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SPCAppDelegate class]));
    }
}
 