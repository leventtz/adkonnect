//
//  WelcomeViewController.h
//  Dialer
//
//  Created by Levent Tinaz on 5/9/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPCViewController.h"
#import "DBUtil.h"

@interface WelcomeViewController : UIViewController

@end
