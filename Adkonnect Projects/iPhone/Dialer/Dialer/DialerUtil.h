//
//  DialerUtil.h
//  Dialer
//
//  Created by Levent Tinaz on 5/8/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DialerUtil : NSObject
+ (NSString *) md5:(NSString *)value;
+ (NSString *) getUniqueDeviceId:(NSString *)value;
+ (BOOL) IsValidEmail:(NSString *)checkString;
+ (BOOL) isNumber:(NSString *)string;
+ (NSString *) urlString:(NSString *)string;
+ (NSString *) stripString:(NSString *)originalString;
@end
