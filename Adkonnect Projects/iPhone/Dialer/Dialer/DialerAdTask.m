//
//  DialerAdTask.m
//  Dialer
//
//  Created by Levent Tinaz on 5/9/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "DialerAdTask.h"

@implementation DialerAdTask {
    NSString *dialNumber;
}
@synthesize adData;
#pragma mark Adding person to the contacts
- (ABRecordRef)addPhoneToContacts:(NSString *)number inAddressBook:(ABAddressBookRef)addressBook
{
    ABRecordRef result = NULL;
    CFErrorRef error = NULL;
    NSString *companyName=@"sponsoracall";
    NSArray *allContacts = (__bridge_transfer NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBook);
    
    NSUInteger i = 0; for (i = 0; i < [allContacts count]; i++)
    {
        ABRecordRef contactPerson = (__bridge ABRecordRef)allContacts[i];
        NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(contactPerson,
                                                                              kABPersonFirstNameProperty);
        if([companyName isEqualToString:firstName]) {
            result=contactPerson;
            break;
        }
    }
    if(result==NULL) {
        result = ABPersonCreate();
        ABRecordSetValue(result, kABPersonFirstNameProperty, (__bridge CFTypeRef)companyName, &error);
        ABAddressBookAddRecord(addressBook, result, &error);
    }
    
    
    
    // Adding phone numbers
    ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFTypeRef)(number), (CFStringRef)@"iPhone", NULL);
    ABRecordSetValue(result, kABPersonPhoneProperty, phoneNumberMultiValue, nil);
    CFRelease(phoneNumberMultiValue);
    
    //3
    BOOL couldAddPerson = ABAddressBookAddRecord(addressBook, result, &error);
    
    if (couldAddPerson) {
        NSLog(@"Successfully added the person.");
    } else {
        NSLog(@"Failed to add the person.");
        CFRelease(result);
        result = NULL;
        return result;
    }
    
    if (ABAddressBookHasUnsavedChanges(addressBook)) {
        BOOL couldSaveAddressBook = ABAddressBookSave(addressBook, &error);
        
        if (couldSaveAddressBook) {
            NSLog(@"Succesfully saved the address book.");
        } else {
            NSLog(@"Failed.");
        }
    }
    
    return result;
}

-(void)addPhoneToContacts:(NSString *)number {
    
    ABAddressBookRef addressBook = NULL;
    CFErrorRef error = NULL;
    
    switch (ABAddressBookGetAuthorizationStatus()) {
        case kABAuthorizationStatusAuthorized: {
            addressBook = ABAddressBookCreateWithOptions(NULL, &error);
            
            if (addressBook != NULL) [self addPhoneToContacts:number inAddressBook:addressBook];
            break;
        }
        case kABAuthorizationStatusDenied: {
            NSLog(@"Access denied to address book");
            break;
        }
        case kABAuthorizationStatusNotDetermined: {
            addressBook = ABAddressBookCreateWithOptions(NULL, &error);
            ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                if (granted) {
                    NSLog(@"Access was granted");
                    [self addPhoneToContacts:number inAddressBook:addressBook];
                }
                else NSLog(@"Access was not granted");
                if (addressBook != NULL) CFRelease(addressBook);
            });
            break;
        }
        case kABAuthorizationStatusRestricted: {
            NSLog(@"access restricted to address book");
            break;
        }
    }

}


-(void)retrieveAd:(NSString *)accountId withAdId:(NSString *)adId withDialNumber:(NSString *)dial;
 {
     dialNumber=dial;
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
     
  
    NSDictionary *dict=[DBUtil getRegisterInfo];
  
    

   
    NSString *countryCode=[dict valueForKey:@"countryCode"];
    NSString *email=[dict valueForKey:@"email"];
    NSString *phoneNumber=[dict valueForKey:@"phone"];
    NSString *deviceId=[dict valueForKey:@"device"];
    
    nsData=[[NSMutableData alloc] init];
    
    
    
    NSString *urlString=[DialerUtil urlString:@"devicead"];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];
    
    [request setHTTPMethod:@"POST"];
    
    id uniqueDeviceId=[DialerUtil getUniqueDeviceId:deviceId];
    
    NSString *body=[NSString stringWithFormat:@"adAccountId=%@&adId=%@&dial=%@&country=%@&email=%@&phone=%@&device=%@&uniquedeviceid=%@",accountId,adId,dialNumber,countryCode,email,phoneNumber,deviceId,uniqueDeviceId];
    
    [request setHTTPBody:[NSData dataWithBytes:[body UTF8String] length:[body length]]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}

-(void)connection:(NSConnection *)connection didFailWithError:(NSError *)error {
    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    [alertView show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSHTTPURLResponse* httpResponse=(NSHTTPURLResponse *)response;
    
    self.statusCode=[httpResponse statusCode];
    
    nsData=[[NSMutableData alloc] init];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [nsData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *) connection {
    
    NSError *myError = nil;
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:nsData options:NSJSONReadingMutableContainers error:&myError];
    NSString *did=[res objectForKey:@"did"];
    NSString *maxDurationString=[res objectForKey:@"maxDuration"];
    NSString *adcrypto=[res objectForKey:@"adcrypto"];
    NSString *adObject=[res objectForKey:@"ad"];
    
    NSData *dataAd=[NSData dataWithBytes:[adObject UTF8String] length:[adObject length]];
    NSDictionary *ad=[NSJSONSerialization JSONObjectWithData:dataAd options:NSJSONReadingMutableContainers error:&myError];
    
    NSString *adId=[ad valueForKey:@"adId"];
    NSString *companyName=[ad valueForKey:@"companyName"];
    NSString *name=[ad valueForKey:@"name"];
    NSString *mediaLocation=[ad valueForKey:@"mediaLocation"];
    NSString *description=[ad valueForKey:@"description"];
    
    self.adData=[[AdData alloc] init];
    adData.adId=adId;
    adData.name=name;
    adData.mediaLocation=mediaLocation;
    adData.description=description;
    adData.did=did;
    adData.maxDuration=[maxDurationString intValue];
    adData.dialNumber=dialNumber;
    adData.adCrypto=adcrypto;
        
    [self addPhoneToContacts:did];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
   
}
@end

