//
//  DialerDialTask.h
//  Dialer
//
//  Created by Levent Tinaz on 5/16/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBUtil.h"
#import "DialerUtil.h"


@interface DialerDialTask : NSObject

{
    NSMutableData *nsData;
}
@property int statusCode;
-(void)initWithDialNumber:dialNumber withCryptoAd:(NSString *)cryptoAd withServiceName:(NSString *)serviceName;
@end
