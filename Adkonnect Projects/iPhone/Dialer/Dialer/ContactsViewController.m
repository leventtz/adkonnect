//
//  ContactsViewController.m
//  Dialer
//
//  Created by Levent Tinaz on 5/6/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "ContactsViewController.h"



@interface ContactsViewController ()

@end

@implementation ContactsViewController {
    NSString *strippedString;
    ImpressionTask *impressionTask;
}




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated {
    if(picker==NULL) {
    picker = [[ABPeoplePickerNavigationController alloc] init];
    [picker setPeoplePickerDelegate:self];
    
    //embed the view
    [self addChildViewController:picker];
    [self.view addSubview:picker.view];
    //[self.navigationController setNavigationBarHidden:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    NSLog(@"viewdie");
    self.view.hidden=NO;
}


- (void)peoplePickerNavigationControllerDidCancel:

(ABPeoplePickerNavigationController *)peoplePicker

{
    
    [self dismissViewControllerAnimated:NO completion:^{
        int controllerIndex=0;
        
        [(UITabBarController*)self.tabBarController setSelectedIndex:controllerIndex];
    }];
    
    
    
}




- (BOOL)peoplePickerNavigationController:

(ABPeoplePickerNavigationController *)peoplePicker

      shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    
    return YES;
    
}



- (BOOL)peoplePickerNavigationController:

(ABPeoplePickerNavigationController *)peoplePicker

      shouldContinueAfterSelectingPerson:(ABRecordRef)person

                                property:(ABPropertyID)property

                              identifier:(ABMultiValueIdentifier)identifier

{
    if(property==kABPersonPhoneProperty) {
        
        ABMultiValueRef multiPhones=ABRecordCopyValue(person, kABPersonPhoneProperty);
        if(identifier==ABMultiValueGetIndexForIdentifier(multiPhones, identifier)){
            dialNumber=(__bridge NSString *)ABMultiValueCopyValueAtIndex(multiPhones,identifier);
        }
    }
    

    strippedString=[DialerUtil stripString:dialNumber];
    impressionTask=[[ImpressionTask alloc] init];
    [impressionTask retrieveImpression:strippedString];
    while (impressionTask.impressions==nil && impressionTask.adData==nil && impressionTask.error==nil) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        
    }
    [self performSegueWithIdentifier:@"ImpressionContact" sender:self];
    
    return NO;

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    ImpressionViewController *controller=segue.destinationViewController;
    controller.strippedString=strippedString;
    controller.adData=impressionTask.adData;
    controller.impressions=impressionTask.impressions;
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    return impressionTask!=nil && impressionTask.error==nil && sender==self;
}

@end
