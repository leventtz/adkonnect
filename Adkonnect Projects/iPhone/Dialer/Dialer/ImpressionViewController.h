//
//  ImpressionViewController.h
//  Dialer
//
//  Created by Levent Tinaz on 9/17/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdData.h"
#import "ImpressionTask.h"
#import "DialerAdTask.h"

#import "SponsorDetailViewController.h"


@interface ImpressionViewController : UITableViewController
@property (strong,nonatomic) NSArray *impressions;
@property (strong,nonatomic) AdData  *adData;
@property(strong,nonatomic) NSString *strippedString;
@end


