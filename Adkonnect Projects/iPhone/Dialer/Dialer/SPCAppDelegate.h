//
//  SPCAppDelegate.h
//  Dialer
//
//  Created by Levent Tinaz on 5/5/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SPCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
