//
//  WelcomeViewController.m
//  Dialer
//
//  Created by Levent Tinaz on 5/9/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "WelcomeViewController.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{

    [super viewDidLoad];
     self.view.hidden=YES;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSDictionary *dict=[DBUtil getRegisterInfo];
    if([dict valueForKey:@"phone"] && [dict valueForKeyPath:@"postalCode"]) {
        [self performSegueWithIdentifier:@"MainTabController" sender:self];
    } else {
        self.view.hidden=NO;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
