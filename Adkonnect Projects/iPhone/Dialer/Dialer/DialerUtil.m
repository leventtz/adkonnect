//
//  DialerUtil.m
//  Dialer
//
//  Created by Levent Tinaz on 5/8/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "DialerUtil.h"
#import <CommonCrypto/CommonDigest.h>

@implementation DialerUtil

+ (NSString *)getUniqueDeviceId:(NSString *)value {
    return [DialerUtil md5:[NSString stringWithFormat:@"%@%@",@"Izmir'inKavaklariDokulurYapraklari",value]];
}


+ (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest );
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

+ (BOOL) IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(BOOL) isNumber:(NSString *)string {
    NSScanner *scanner = [NSScanner scannerWithString:string];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
    return isNumeric;
}

+ (NSString *) urlString:(NSString *)string {
    const NSString *serverAddress=@"https://www.sponsoracall.com/";
    // NSString *serverAddress=@"http://54.89.225.140:9000";
    NSString *urlString=[NSString stringWithFormat:@"%@/%@",serverAddress,string];
    return urlString;
}

+ (NSString *) stripString:(NSString *)originalString {
    NSMutableString *strippedString = [NSMutableString stringWithCapacity:originalString.length];
    
    NSScanner *scanner = [NSScanner scannerWithString:originalString];
    NSCharacterSet *numbers = [NSCharacterSet
                               characterSetWithCharactersInString:@"0123456789#"];
    
    while ([scanner isAtEnd] == NO) {
        NSString *buffer;
        if ([scanner scanCharactersFromSet:numbers intoString:&buffer]) {
            [strippedString appendString:buffer];
            
        } else {
            [scanner setScanLocation:([scanner scanLocation] + 1)];
        }
    }
    return (NSString *)strippedString ;
}


@end
