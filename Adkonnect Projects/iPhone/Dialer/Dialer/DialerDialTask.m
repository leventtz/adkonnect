//
//  DialerDialTask.m
//  Dialer
//
//  Created by Levent Tinaz on 5/16/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "DialerDialTask.h"

@implementation DialerDialTask

-(void)initWithDialNumber:dialNumber withCryptoAd:(NSString *)cryptoAd withServiceName:(NSString *)serviceName {
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    

    NSDictionary *dict=[DBUtil getRegisterInfo];
    
    
    
    NSString *countryCode=[dict valueForKey:@"countryCode"];
    NSString *email=[dict valueForKey:@"email"];
    NSString *phoneNumber=[dict valueForKey:@"phone"];
    NSString *deviceId=[dict valueForKey:@"deviceId"];
    
    nsData=[[NSMutableData alloc] init];
    
    
    
    NSString *urlString=[DialerUtil urlString:serviceName];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];
    
    [request setHTTPMethod:@"POST"];
    
    id uniqueDeviceId=[DialerUtil getUniqueDeviceId:deviceId];
    
    NSString *body=[NSString stringWithFormat:@"cryptoad=%@&dial=%@&country=%@&email=%@&phone=%@&device=%@&uniquedeviceid=%@",cryptoAd,dialNumber,countryCode,email,phoneNumber,deviceId,uniqueDeviceId];
    
    [request setHTTPBody:[NSData dataWithBytes:[body UTF8String] length:[body length]]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

-(void)connection:(NSConnection *)connection didFailWithError:(NSError *)error {
    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    [alertView show];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSHTTPURLResponse* httpResponse=(NSHTTPURLResponse *)response;
    
    self.statusCode=[httpResponse statusCode];
    
    nsData=[[NSMutableData alloc] init];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [nsData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *) connection {
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}

@end
