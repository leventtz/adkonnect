package com.dimwire.spcdialer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class AdDetailActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addetail);

		final WebView webView = (WebView) findViewById(R.id.webViewAdDetail);

		final ProgressDialog mDialog = new ProgressDialog(this);
		mDialog.setMessage(getResources().getString(R.string.please_wait));
		mDialog.setCancelable(false);
		mDialog.show();

		Intent intent=getIntent();
		String mediaDescription=intent.getStringExtra("mediaDescription");
		
		webView.loadDataWithBaseURL(null,mediaDescription,"text/html", "UTF-8",null);
		
		webView.setWebViewClient(new WebViewClient() {
			   public void onPageFinished(WebView view, String url) {
				   mDialog.dismiss();
			    }
		});		
		
		
	}

}
