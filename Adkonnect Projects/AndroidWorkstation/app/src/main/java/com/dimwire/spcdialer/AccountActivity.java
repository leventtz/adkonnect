package com.dimwire.spcdialer;

import static com.dimwire.spcdialer.util.DialerUtil.DIALER_PREF_NAME;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.dimwire.spcdialer.http.GenericPostTask;
import com.dimwire.spcdialer.http.GenericResponse;
import com.dimwire.spcdialer.util.DialerUtil;

public class AccountActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_myaccount);
		
		final ProgressDialog mDialog = new ProgressDialog(this);
		mDialog.setMessage(getResources().getString(R.string.first_time));
		mDialog.setCancelable(false);
		mDialog.show();		
		
		String serverAddress = DialerUtil.getServerAddress(this);
		String urlString = serverAddress + "deviceGetRate";

		mDialog.show();
		Map<String, String> map = new HashMap<String, String>(2);
		map.put("phone",
				getSharedPreferences(DIALER_PREF_NAME,
						Context.MODE_PRIVATE).getString("phone", ""));
		map.put("device",
				getSharedPreferences(DIALER_PREF_NAME,
						Context.MODE_PRIVATE).getString("device", ""));
		map.put("uniquedeviceid",
				getSharedPreferences(DIALER_PREF_NAME,
						Context.MODE_PRIVATE).getString(
						"uniquedeviceid", ""));
		map.put("dial",
				getSharedPreferences(DIALER_PREF_NAME,
						Context.MODE_PRIVATE).getString("dial", null));
		map.put("country",
				getSharedPreferences(DIALER_PREF_NAME,
						Context.MODE_PRIVATE).getString("country", ""));

		
		
		String password=getSharedPreferences(DIALER_PREF_NAME,
				Context.MODE_PRIVATE).getString("password", null);
		
		
		if(password==null) {
			final EditText textEditPin=(EditText)findViewById(R.id.editTextPassword);
			
		}
		
		

		final TextView textViewDestinationName=(TextView)findViewById(R.id.textViewDestinationName);
		final TextView textViewRate=(TextView)findViewById(R.id.textViewRate);
		

		
	
		GenericPostTask sellRateTask = new GenericPostTask(urlString, map) {
			@Override
			protected void onPostExecute(GenericResponse result) {
				super.onPostExecute(result);
				try {
					JSONObject object = new JSONObject(
							result.getResponseString());
					JSONObject objectRate = new JSONObject(
							object.getString("rate"));

					
					textViewDestinationName.setText(objectRate.getString("destinationName"));
					textViewRate.setText(getResources().getString(R.string.currency)+objectRate.getString("rate")+"/"+getResources().getString(R.string.minute));
					
				} catch (JSONException e) {
					DialerUtil.showNetworkErrorDialog(AccountActivity.this);
				}
				mDialog.dismiss();
			}
		};
		
		
		sellRateTask.execute();		
		
	}

}
