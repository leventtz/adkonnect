package com.dimwire.spcdialer;

import static com.dimwire.spcdialer.util.DialerUtil.DIALER_PREF_NAME;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.dimwire.spcdialer.http.GenericGetTask;
import com.dimwire.spcdialer.http.GenericPostTask;
import com.dimwire.spcdialer.http.GenericResponse;
import com.dimwire.spcdialer.model.CountryData;
import com.dimwire.spcdialer.util.DialerUtil;

public class RegisterActivity extends Activity {
	

	@Override
	protected void onStop() {
		super.onStop();
		finish();
	}
 
	public String getUserName(String accountType) {
		AccountManager manager = AccountManager.get(this);
		Account[] accounts = manager.getAccountsByType(accountType);
		List<String> possibleEmails = new LinkedList<String>();

		for (Account account : accounts) {
			possibleEmails.add(account.name);
		}

		if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
			String email = possibleEmails.get(0);
			String[] parts = email.split("@");
			if (parts.length > 0 && parts[0] != null)
				return parts[0];
			else
				return null;
		} else
			return null;
	}

	private void showMainActivity() {
		Intent myIntent = new Intent(RegisterActivity.this, DialActivity.class);
		startActivity(myIntent);
		finish();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		popluateCountrySpinner();
		initRegisterValues();	
		((EditText) findViewById(R.id.editTextEmail)).setImeOptions(EditorInfo.IME_ACTION_DONE);
	}
	
	private void popluateCountrySpinner() {
		final Spinner spinnerCountry = (Spinner) findViewById(R.id.spinnerCountry);
		final TextView txtPhoneNumber=(TextView) findViewById(R.id.textPhoneNumber);
		
		spinnerCountry.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				CountryData data=(CountryData)parent.getSelectedItem();
				final String devicePhoneNumber = DialerUtil
						.getPhoneNumber(RegisterActivity.this);
				
				txtPhoneNumber.setText("+"+data.getCountryCode()+" "+PhoneNumberUtils.formatNumber(devicePhoneNumber));
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}});
		
		final ProgressDialog mDialog = new ProgressDialog(this);
		mDialog.setMessage(getResources().getString(R.string.first_time));
		mDialog.setCancelable(false);
		mDialog.show();

		String serverAddress = DialerUtil.getServerAddress(this);
		String urlString = serverAddress + "devicecountries";
		GenericGetTask countryTask = new GenericGetTask(urlString) {
			public void onPostExecute(GenericResponse response) {
				super.onPostExecute(response);
				mDialog.dismiss();
				if (response == null) {
					DialerUtil.showNetworkErrorDialog(RegisterActivity.this);
					return;
				}
				
				if (response.getStatusCode() == 200) {
					ArrayAdapter<CountryData> spinnerArrayAdapter = null;
					try {
						JSONObject jsonObject = new JSONObject(
								response.getResponseString());
						JSONArray result = jsonObject
								.getJSONArray("countryList");

						if (result != null) {
							CountryData[] values = new CountryData[result.length()];
							for (int i = 0; i < values.length; i++) {
								JSONObject jsonCountry = new JSONObject(result.get(i).toString());
								values[i] = new CountryData(jsonCountry.getString("name"),jsonCountry.getString("countryCode")); 
							}
							spinnerArrayAdapter = new ArrayAdapter<CountryData>(
									RegisterActivity.this,
									android.R.layout.simple_spinner_item,
									values);
							spinnerCountry.setAdapter(spinnerArrayAdapter);
							Locale locale=Locale.getDefault();
							String displayCountry=locale.getDisplayCountry();
							spinnerCountry.setAdapter(spinnerArrayAdapter);
							for(int i=0;i<values.length;i++) {
								if(values[i].getCountryName().equals(displayCountry)){
									spinnerCountry.setSelection(i);
									break;
								}
							}
							
						}
					} catch (Exception ex) {
						Log.e("RegisterActivity", ex.getMessage(), ex);
					}
					
					
					
				}
				mDialog.dismiss();
			}
		};
		countryTask.execute();
	}

	private void initRegisterValues() {
		
		final EditText email = (EditText) findViewById(R.id.editTextEmail);
		final EditText postalCode = (EditText) findViewById(R.id.editTextPostalCode);

		

		Button registerButton = (Button) findViewById(R.id.buttonRegisterSubmit);

		registerButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (email.getEditableText().length() == 0
						|| !android.util.Patterns.EMAIL_ADDRESS.matcher(
								email.getText().toString()).matches()) {
					email.setError(getResources().getString(
							R.string.invalid_email));
					return;
				}

				
				if (postalCode.getEditableText().length() == 0) {
					postalCode.setError(getResources().getString(
							R.string.invalid_postal_code));
					return;
				}
				
				
				final Spinner countrySpinner = (Spinner) findViewById(R.id.spinnerCountry);
				
				final String deviceId = DialerUtil
						.uniqueDeviceId(RegisterActivity.this);
				final String countryCode=((CountryData)countrySpinner.getSelectedItem()).getCountryCode();
				final String phoneNumber=DialerUtil.getPhoneNumber(RegisterActivity.this);
				
				final String uniqueDeviceId=DialerUtil.uniqueDeviceId(deviceId);
				final String postalCodeValue=postalCode.getEditableText().toString();
				
				Map<String, String> hashMap = new HashMap<String, String>();
				
				hashMap.put("country", countryCode);
				hashMap.put("email", email.getText().toString());
				hashMap.put("phone",  phoneNumber);
				hashMap.put("postalCode", postalCodeValue);				
				hashMap.put("device", deviceId);
				hashMap.put("uniquedeviceid", uniqueDeviceId);
				hashMap.put("phoneType", "Android");
				
				String serverAddress = DialerUtil
						.getServerAddress(RegisterActivity.this);
				String urlString = serverAddress + "deviceregister";
				
				final ProgressDialog mDialog = new ProgressDialog(RegisterActivity.this);
				mDialog.show();
				GenericPostTask register = new GenericPostTask(urlString,
						hashMap) {
					public void onPostExecute(GenericResponse response) {
						super.onPostExecute(response);
						mDialog.dismiss();
						if (response == null) {
							DialerUtil
									.showNetworkErrorDialog(RegisterActivity.this);
							return;
						}
						if (response.getStatusCode() == 409 && response.getResponseString().equals("WrongCountryCode")) {
							DialerUtil.showWrongCountryDialog(RegisterActivity.this);
							return;
						}
						if (response.getStatusCode() == 409 && response.getResponseString().equals("UnknownPostalCode")) {
							DialerUtil.showUnknowPostalCodeDialog(RegisterActivity.this);
							return;
						}
						if (response.getStatusCode() == 400) {
							DialerUtil.showNetworkErrorDialog(RegisterActivity.this);
							return;
						}						

						SharedPreferences preferences = getSharedPreferences(DIALER_PREF_NAME,Context.MODE_PRIVATE);
						Editor editor = preferences.edit();
						editor.putBoolean("registered", true);
						editor.putString("device", deviceId);
						editor.putString("country", countryCode);
						editor.putString("phone", phoneNumber);
						editor.putString("postalCode", postalCodeValue);
						editor.putString("uniquedeviceid", uniqueDeviceId);
						editor.commit();
						showMainActivity();
					}
				};
				register.execute();
			}
		});

	}

}
