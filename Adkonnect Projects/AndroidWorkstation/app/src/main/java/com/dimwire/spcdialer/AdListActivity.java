package com.dimwire.spcdialer;

import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.dimwire.spcdialer.db.DBUtils;
public class AdListActivity extends ListActivity {
	
	Intent intent;
	TextView textCallDateTime;
	TextView textDialNumber;
	
	DBUtils dbUtils=new DBUtils(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ads);
		findViewById(R.id.tableRowEmptyAd).setVisibility(View.INVISIBLE);
		final List<Map<String,String>> companyList=dbUtils.getAllCompanies();
		
		if(companyList.size() !=0) {
			ListView listView=getListView();
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					 
				}
			});
		} else {
			findViewById(R.id.tableRowEmptyAd).setVisibility(View.VISIBLE);
		}
		
	
		final Map<String,String> values[]=new Map[companyList.size()];
		final AdListAdapter adapter = new AdListAdapter(this,companyList.toArray(values));
		setListAdapter(adapter);
		
		ListView listView=this.getListView();
		listView.setLongClickable(true);
		listView.setOnItemLongClickListener(new OnItemLongClickListener() {
		    public boolean onItemLongClick(AdapterView<?> parent, View v, final int position, long id) {
			    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			        @Override
			        public void onClick(DialogInterface dialog, int which) {
			            switch (which){
			            case DialogInterface.BUTTON_POSITIVE:
			            	new DBUtils(AdListActivity.this).deleteCompany(values[position].get("companyName"));
			            	values[position].put("deleted", "1");
			            	adapter.notifyDataSetChanged();
			            case DialogInterface.BUTTON_NEGATIVE:
			            	dialog.dismiss();
			                break;
			            }
			        }
			    };		    	
			    AlertDialog.Builder builder = new AlertDialog.Builder(AdListActivity.this);
			    builder.setMessage(R.string.delete_company).setPositiveButton(R.string.yes, dialogClickListener)
			        .setNegativeButton(R.string.no, dialogClickListener).show();
		        return true;
		    }
		});		
		
		
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				
				Intent intent=new Intent(AdListActivity.this,AdDetailActivity.class);
				
				List<Map<String, String>> list = new DBUtils(AdListActivity.this).getAds(values[position].get("companyName"));
				if(list.size()>0) {
					Map<String, String> ad = list.get(0);
					intent.putExtra("mediaLocation", ad.get("mediaLocation"));
					intent.putExtra("mediaDescription", ad.get("mediaDescription"));
					startActivity(intent);
				}
			}
		});
		
			
		
	}
}
