package com.dimwire.spcdialer;

import static com.dimwire.spcdialer.util.DialerUtil.DIALER_PREF_NAME;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Data;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.dimwire.spcdialer.db.DBUtils;
import com.dimwire.spcdialer.http.GenericPostTask;
import com.dimwire.spcdialer.http.GenericResponse;
import com.dimwire.spcdialer.util.DialerUtil;

public class AdActivity extends Activity {
	private String objectAdCrypto;
	private String did;
	protected String adId;
	protected int maxDuration = 0;
	protected boolean bypassSwitch;
	private Button buttonOk;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final ProgressDialog mDialog = new ProgressDialog(this);
		mDialog.setMessage(getResources().getString(R.string.loading));
		mDialog.setCancelable(false);
		try {
			setContentView(R.layout.activity_ad);
			final WebView webView = (WebView) findViewById(R.id.webViewAdDetail);
			buttonOk = (Button) findViewById(R.id.buttonOk);

			buttonOk.setOnClickListener(new OKClickListener());
			buttonOk.setVisibility(ViewGroup.INVISIBLE);
			
			mDialog.show();
			Map<String, String> map = new HashMap<String, String>(2);

			map.put("phone",
					getSharedPreferences(DIALER_PREF_NAME, Context.MODE_PRIVATE)
							.getString("phone", ""));
			map.put("device",
					getSharedPreferences(DIALER_PREF_NAME, Context.MODE_PRIVATE)
							.getString("device", ""));
			map.put("uniquedeviceid",
					getSharedPreferences(DIALER_PREF_NAME, Context.MODE_PRIVATE)
							.getString("uniquedeviceid", ""));
			map.put("dial",
					getSharedPreferences(DIALER_PREF_NAME, Context.MODE_PRIVATE)
							.getString("dial", "").replaceAll("-", "")
							.replaceAll("\\)", "").replaceAll("\\(", ""));
			map.put("country",
					getSharedPreferences(DIALER_PREF_NAME, Context.MODE_PRIVATE)
							.getString("country", ""));

			map.put("adId",
					getSharedPreferences(DIALER_PREF_NAME, Context.MODE_PRIVATE)
							.getString("adId", ""));

			map.put("adAccountId",
					getSharedPreferences(DIALER_PREF_NAME, Context.MODE_PRIVATE)
							.getString("adAccountId", ""));

			String serverAddress = DialerUtil.getServerAddress(this);
			String urlString = serverAddress + "devicead";

			final GenericPostTask adTask = new GenericPostTask(urlString, map) {
				@Override
				protected void onPostExecute(GenericResponse result) {
					super.onPostExecute(result);
					if (result != null) {
						try {
							JSONObject object = new JSONObject(
									result.getResponseString());
							JSONObject objectAd = new JSONObject(
									object.getString("ad"));

							objectAdCrypto = object.getString("adcrypto");

							did = object.getString("did");

							adId = objectAd.getString("adId");

							bypassSwitch = false;
							if (did.equals("bypass")) {
								bypassSwitch = true;
								did = getSharedPreferences(DIALER_PREF_NAME,
										Context.MODE_PRIVATE)
										.getString("dial", "")
										.replaceAll("-", "")
										.replaceAll("\\)", "")
										.replaceAll("\\(", "");
							}

							try {
								maxDuration = Integer.parseInt(object
										.getString("maxDuration"));
							} catch (Exception ex) {
								Log.e("AdActivity", ex.getMessage(), ex);
							}

							String adName = objectAd.getString("name");
							String mediaDescription = objectAd
									.getString("description");
							String mediaLocation = objectAd
									.getString("mediaLocation");

							if (!adId.equals("0")) {
								new DBUtils(getApplication())
										.insertAd(adId, adName,
												mediaDescription, mediaLocation);
							}
							if(!bypassSwitch) {
								addDid();
							}

							webView.loadDataWithBaseURL(null,mediaDescription,"text/html", "UTF-8",null);

							webView.setWebViewClient(new WebViewClient() {
								public void onPageFinished(WebView view,
										String url) {
									mDialog.dismiss();

									final Handler handler = new Handler();
									handler.postAtTime(new Runnable() {
										@Override
										public void run() {
											buttonOk.setVisibility(ViewGroup.VISIBLE);
										}

									}, SystemClock.uptimeMillis()+1000);

								}

								@Override
								public void onReceivedSslError(WebView view,
										SslErrorHandler handler, SslError error) {
									handler.proceed(); // Ignore SSL certificate
								}
							});
						} catch (JSONException e) {
							Log.e("AdActivity", e.getMessage(), e);
							mDialog.dismiss();
							switchToDialPad();
						}
						if (result.getStatusCode() == 404) {
							buttonOk.setVisibility(View.INVISIBLE);
						}
					} else {
						mDialog.dismiss();
						DialerUtil.showNetworkErrorDialog(AdActivity.this);
					}
				}
			};
			adTask.execute();
		} catch (Exception ex) {
			Log.e("AdActivity", ex.getMessage(), ex);
			mDialog.dismiss();
			DialerUtil.showNetworkErrorDialog(AdActivity.this);
		}
	}

	private void addDid() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				if (!getSharedPreferences(DIALER_PREF_NAME,
						Context.MODE_PRIVATE).contains(did)) {
					insertDID(did);
					Editor editor = getSharedPreferences(DIALER_PREF_NAME,
							Context.MODE_PRIVATE).edit();
					editor.putInt(did, 1);
					editor.commit();
				}

			}
		}).start();
	}

	private void switchToDialPad() {
		Intent myIntent = new Intent(AdActivity.this, DialActivity.class);
		startActivity(myIntent);

	}

	private class OKClickListener implements View.OnClickListener {
		private final long start = System.currentTimeMillis();
		private final long WAIT_TIME = 3 * 1000;

		@Override
		public void onClick(final View v) {
			if (adId.equals("0")) {
				switchToDialPad();
				return;
			}

			final ProgressDialog mDialog = new ProgressDialog(AdActivity.this);
			mDialog.setMessage(getResources().getString(R.string.please_wait));
			mDialog.setCancelable(false);
			try {
				mDialog.show();
				Map<String, String> map = new HashMap<String, String>(2);
				map.put("phone",
						getSharedPreferences(DIALER_PREF_NAME,
								Context.MODE_PRIVATE).getString("phone", ""));
				map.put("device",
						getSharedPreferences(DIALER_PREF_NAME,
								Context.MODE_PRIVATE).getString("device", ""));
				map.put("uniquedeviceid",
						getSharedPreferences(DIALER_PREF_NAME,
								Context.MODE_PRIVATE).getString(
								"uniquedeviceid", ""));
				map.put("dial",
						getSharedPreferences(DIALER_PREF_NAME,
								Context.MODE_PRIVATE).getString("dial", null));
				map.put("country",
						getSharedPreferences(DIALER_PREF_NAME,
								Context.MODE_PRIVATE).getString("country", ""));

				map.put("cryptoad", objectAdCrypto);

				Map<String, String> call = new HashMap<String, String>();

				call.put("callDateTime",
						String.valueOf(System.currentTimeMillis()));
				call.put(
						"dialNumber",
						getSharedPreferences(DIALER_PREF_NAME,
								Context.MODE_PRIVATE).getString("dial", null));
				call.put("adId", adId);

				new DBUtils(getApplication()).insertCall(call);

				String serverAddress = DialerUtil
						.getServerAddress(AdActivity.this);

				String urlString = serverAddress
						+ (bypassSwitch ? "deviceCallBypassSwitch"
								: "devicecall");

				GenericPostTask callTask = new GenericPostTask(urlString, map) {
					@Override
					protected void onPostExecute(GenericResponse result) {
						super.onPostExecute(result);
						if (result != null) {
							callGateway(v, mDialog);
						} else {
							mDialog.dismiss();
							DialerUtil.showNetworkErrorDialog(AdActivity.this);
						}
					}
				};

				callTask.execute();
			} catch (Exception ex) {
				Log.e("AdActivity", ex.getMessage(), ex);
				mDialog.dismiss();
				DialerUtil.showNetworkErrorDialog(AdActivity.this);
			}
		}

	}

	private void callGateway(final View v, final Dialog mDialog) {
		final SharedPreferences settings = getSharedPreferences(
				DialerUtil.DIALER_PREF_NAME, 0);
		Editor editor = settings.edit();
		editor.putString("dial", "");
		editor.commit();

		callIntent(v, mDialog);

//		Resources res = getResources();
//		String message = String.format(
//				res.getString(R.string.duration_message), maxDuration);
//		final AlertDialog.Builder alert = new AlertDialog.Builder(this);
//		alert.setTitle(R.string.duration_title);
//		alert.setMessage(message);
//		alert.setNegativeButton(R.string.close,
//				new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						dialog.cancel();
//						callIntent(v, mDialog);
//					}
//				});
//
//		alert.setPositiveButton(R.string.ok,
//				new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						Editor editor = settings.edit();
//						editor.putBoolean("dontShowDuration", true);
//						editor.commit();
//						dialog.cancel();
//						callIntent(v, mDialog);
//					}
//				});
//		alert.show();
	}

	private void callIntent(View v, Dialog mDialog) {
		String uri = "tel:" + did;
		Intent intent = new Intent(Intent.ACTION_CALL);
		intent.setData(Uri.parse(uri));
		startActivity(intent);
		if (maxDuration > 1) {
			CallTimeoutThread.createVibrator(maxDuration, AdActivity.this);
		}
		v.setVisibility(View.INVISIBLE);
		mDialog.dismiss();

	}

	private String[] getRawContactIdFromNumber(String givenNumber) {
		List<String> rawIds = new ArrayList<String>();
		Cursor phones = getContentResolver()
				.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
						new String[] { ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID },
						ContactsContract.CommonDataKinds.Phone.NUMBER + "='"
								+ givenNumber + "'", null,
						ContactsContract.CommonDataKinds.Phone.NUMBER);

		while (phones.moveToNext()) {
			rawIds.add(phones.getString(phones
					.getColumnIndex(ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID)));
			Log.v("contacts", "Given Number: " + givenNumber + "Raw ID: "
					+ rawIds.get(rawIds.size() - 1));
		}
		phones.close();
		String[] ret = new String[0];

		return rawIds.toArray(ret);
	}

	private void insertDID(String did) {
		if (getRawContactIdFromNumber(did).length == 0) {
			String name = "AdKonnect";
			String phone = did;
			String email = "support@adkonnect.com";

			ArrayList<ContentProviderOperation> op_list = new ArrayList<ContentProviderOperation>();
			op_list.add(ContentProviderOperation
					.newInsert(ContactsContract.RawContacts.CONTENT_URI)
					.withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
					.withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
					// .withValue(RawContacts.AGGREGATION_MODE,
					// RawContacts.AGGREGATION_MODE_DEFAULT)
					.build());

			// first and last names
			op_list.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
					.withValueBackReference(Data.RAW_CONTACT_ID, 0)
					.withValue(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE)
					.withValue(StructuredName.DISPLAY_NAME, name).build());

			op_list.add(ContentProviderOperation
					.newInsert(Data.CONTENT_URI)
					.withValueBackReference(Data.RAW_CONTACT_ID, 0)
					.withValue(
							ContactsContract.Data.MIMETYPE,
							ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
					.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,
							phone)
					.withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
							Phone.TYPE_MAIN).build());
			op_list.add(ContentProviderOperation
					.newInsert(Data.CONTENT_URI)
					.withValueBackReference(Data.RAW_CONTACT_ID, 0)

					.withValue(
							ContactsContract.Data.MIMETYPE,
							ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
					.withValue(ContactsContract.CommonDataKinds.Email.DATA,
							email)
					.withValue(ContactsContract.CommonDataKinds.Email.TYPE,
							Email.TYPE_WORK).build());

			try {
				getContentResolver().applyBatch(ContactsContract.AUTHORITY,
						op_list);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}
}
