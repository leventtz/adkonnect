package com.dimwire.spcdialer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.http.SslError;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dimwire.spcdialer.util.DialerUtil;

public class TermsActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_terms);
		WebView view=(WebView)findViewById(R.id.webViewTerms);
		
		String serverAddress = DialerUtil.getServerAddress(this);
		String urlString = serverAddress + "terms";		
		
		view.loadUrl(urlString);
		
		
		final ProgressDialog mDialog = new ProgressDialog(this);
		mDialog.setMessage(getResources().getString(R.string.first_time));
		mDialog.setCancelable(false);
		mDialog.show();		
		
	    
		
		
		view.setWebViewClient(new WebViewClient() {
			   public void onPageFinished(WebView view, String url) {
				   mDialog.dismiss();
			    }
	            @Override
	            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
	                handler.proceed(); // Ignore SSL certificate errors
	            }			   
		});
	}
}
