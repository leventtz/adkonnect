package com.dimwire.spcdialer.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

import com.dimwire.spcdialer.R;

public class DialerUtil {
	
	public static final String DIALER_PREF_NAME = "DialerPref";
	
	public static String getMetaData(Activity activity, String metaDataNanme) {
		String value = null;
		ApplicationInfo aplicationInfo;
		try {

			aplicationInfo = activity.getPackageManager().getApplicationInfo(
					activity.getPackageName(), PackageManager.GET_META_DATA);
			value = aplicationInfo.metaData.get(metaDataNanme).toString();
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		return value;
	}

	public static String getServerAddress(Activity activity) {
		return getMetaData(activity, "server");
	}

	public static String uniqueDeviceId(String value) {
		try {
			
			return Base64.encodeToString(MessageDigest.getInstance("MD5")
					.digest(("Izmir'inKavaklariDokulurYapraklari"+value).getBytes()), Base64.NO_WRAP);			
		} catch (NoSuchAlgorithmException e) {
			Log.e("DialerUtil",e.getMessage(),e);
		} 
		return "";
	}
	
	
	public static String uniqueDeviceId(Activity activity) {
		final TelephonyManager tm = (TelephonyManager) activity
				.getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

		final String tmDevice, tmSerial, androidId;
		tmDevice = "" + tm.getDeviceId();
		tmSerial = "" + tm.getSimSerialNumber();
		androidId = ""
				+ android.provider.Settings.Secure.getString(
						activity.getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);

		UUID deviceUuid = new UUID(androidId.hashCode(),
				((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
		return deviceUuid.toString();
	}

	public static String getPhoneNumber(Activity activity) {
		TelephonyManager tMgr = (TelephonyManager) activity
				.getSystemService(Context.TELEPHONY_SERVICE);
		return tMgr.getLine1Number();
	}

	public static void showErrorDialog(Activity activity, int title,
			int message, final boolean fatal) {
		final AlertDialog.Builder alert = new AlertDialog.Builder(activity);
		alert.setTitle(title);
		alert.setMessage(message);
		alert.setPositiveButton(R.string.ok,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						if (fatal) {
							System.exit(-1);
						}
					}
				});
		alert.show();
	}

	public static void showNetworkErrorDialog(Activity activity) {
		showErrorDialog(activity, R.string.error_dialog_title,
				R.string.network_error_try_again, true);
	}

	public static void showPhoneConflictDialog(Activity activity) {
		showErrorDialog(activity, R.string.error_dialog_title,
				R.string.conflict_phone, true);
	}

	public static void showDeviceConflictDialog(Activity activity) {
		showErrorDialog(activity, R.string.error_dialog_title,
				R.string.conflict_device, true);
	}

	public static void showWrongCountryDialog(Activity activity) {
		showErrorDialog(activity, R.string.error_dialog_title,
				R.string.wrong_country_code, false);
	}

	public static void showUnknowPostalCodeDialog(Activity activity) {
		showErrorDialog(activity, R.string.error_dialog_title,
				R.string.unknown_postal_code, false);
	}	
	
}
