package com.dimwire.spcdialer;

import static com.dimwire.spcdialer.util.DialerUtil.DIALER_PREF_NAME;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class DialActivity extends Activity {

	private class BackClick implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			String dialString = textDial.getText().toString();
			if (dialString.length() > 0) {
				textDial.setText(dialString.toString().substring(0,
						dialString.length() - 1));
			}
			v.startAnimation(AnimationUtils.loadAnimation(DialActivity.this,
					R.anim.buttonanim));
			vib.vibrate(VIB_MS);
		}
	}

	private class CallClick implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			v.startAnimation(AnimationUtils.loadAnimation(DialActivity.this,
					R.anim.buttonanim));
			vib.vibrate(VIB_MS);

			SharedPreferences pref = getApplicationContext()
					.getSharedPreferences(DIALER_PREF_NAME,
							Context.MODE_PRIVATE);
			Editor editor = pref.edit();
			editor.putString("dial", textDial.getText().toString());
			editor.commit();

			Intent myIntent = new Intent(DialActivity.this,
					ImpressionActivity.class);
			startActivity(myIntent);

		}
	}

	private class ContactClick implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(Intent.ACTION_PICK,
					ContactsContract.Contacts.CONTENT_URI);
			intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
			startActivityForResult(intent, 100);

			v.startAnimation(AnimationUtils.loadAnimation(DialActivity.this,
					R.anim.buttonanim));
			vib.vibrate(VIB_MS * 2);
		}
	}

	private class CallsClick implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			v.startAnimation(AnimationUtils.loadAnimation(DialActivity.this,
					R.anim.buttonanim));
			vib.vibrate(VIB_MS * 2);

			Intent intent = new Intent(getApplication(), CallsActivity.class);
			startActivity(intent);
		}

	}

	private class AdsClick implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			v.startAnimation(AnimationUtils.loadAnimation(DialActivity.this,
					R.anim.buttonanim));
			vib.vibrate(VIB_MS * 2);

			Intent intent = new Intent(getApplication(), AdListActivity.class);
			startActivity(intent);
		}

	}

	private class LongPlusClick implements View.OnLongClickListener {

		@Override
		public boolean onLongClick(View v) {
			textDial.setText("+" + textDial.getText());
			v.startAnimation(AnimationUtils.loadAnimation(DialActivity.this,
					R.anim.buttonanim));
			vib.vibrate(VIB_MS * 3);
			return true;
		}

	}

	private class LongBackClick implements View.OnLongClickListener {

		@Override
		public boolean onLongClick(View v) {
			textDial.setText("");
			v.startAnimation(AnimationUtils.loadAnimation(DialActivity.this,
					R.anim.buttonanim));
			vib.vibrate(VIB_MS * 3);
			return true;
		}

	}

	private class NumberClick implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			ImageView button = (ImageView) v;
			String dialString = textDial.getText().toString();
			String digit = button.getContentDescription().toString();
			textDial.setText(dialString + digit);
			v.startAnimation(AnimationUtils.loadAnimation(DialActivity.this,
					R.anim.buttonanim));
			vib.vibrate(VIB_MS);
		}
	}

	private Vibrator vib;

	private TextView textDial;

	private final int VIB_MS = 30;

	private ImageView back;

	private ImageView call;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			Uri uri = data.getData();

			if (uri != null) {
				Cursor c = null;
				try {
					c = getContentResolver().query(uri, null, null, null, null);

					if (c != null && c.moveToFirst()) {
						String contactPhoneNumber = c.getString(22);
						if (contactPhoneNumber != null) {
							SharedPreferences pref = getApplicationContext()
									.getSharedPreferences(DIALER_PREF_NAME,
											Context.MODE_PRIVATE);
							Editor editor = pref.edit();
							editor.putString("dial", contactPhoneNumber);
							editor.commit();

							Intent myIntent = new Intent(DialActivity.this,
									ImpressionActivity.class);
							startActivity(myIntent);
						}

					}
				} finally {
					if (c != null && !c.isClosed())
						c.close();
				}
			}
		}

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStart();
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				DIALER_PREF_NAME, Context.MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.putString("dialNumber", ((TextView) findViewById(R.id.textDial))
				.getText().toString());
		editor.commit();
	}

	@Override
	protected void onStart() {
		super.onStart();
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				DIALER_PREF_NAME, Context.MODE_PRIVATE);
		String phoneNumber = pref.getString("dial", "");
		TextView textDial = ((TextView) findViewById(R.id.textDial));
		textDial.setText(phoneNumber);

		String postalCode = getSharedPreferences(DIALER_PREF_NAME,
				Context.MODE_PRIVATE).getString("postalCode", "");
		if ("" == postalCode) {
			Editor editor = getSharedPreferences(DIALER_PREF_NAME,
					Context.MODE_PRIVATE).edit();
			editor.clear();
			editor.commit();
			Intent myIntent = new Intent(DialActivity.this,
					WelcomeActivity.class);
			startActivity(myIntent);
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dial_pad);

		textDial = (TextView) findViewById(R.id.textDial);
		vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

		NumberClick numberClick = new NumberClick();
		ImageView b0 = (ImageView) findViewById(R.id.b0);
		b0.setOnClickListener(numberClick);
		b0.setOnLongClickListener(new LongPlusClick());

		ImageView b1 = (ImageView) findViewById(R.id.b1);
		b1.setOnClickListener(numberClick);

		ImageView b2 = (ImageView) findViewById(R.id.b2);
		b2.setOnClickListener(numberClick);

		ImageView b3 = (ImageView) findViewById(R.id.b3);
		b3.setOnClickListener(numberClick);

		ImageView b4 = (ImageView) findViewById(R.id.b4);
		b4.setOnClickListener(numberClick);

		ImageView b5 = (ImageView) findViewById(R.id.b5);
		b5.setOnClickListener(numberClick);

		ImageView b6 = (ImageView) findViewById(R.id.b6);
		b6.setOnClickListener(numberClick);

		ImageView b7 = (ImageView) findViewById(R.id.b7);
		b7.setOnClickListener(numberClick);

		ImageView b8 = (ImageView) findViewById(R.id.b8);
		b8.setOnClickListener(numberClick);

		ImageView b9 = (ImageView) findViewById(R.id.b9);
		b9.setOnClickListener(numberClick);

		ImageView bpound = (ImageView) findViewById(R.id.bpound);
		bpound.setOnClickListener(numberClick);

		ImageView bStar = (ImageView) findViewById(R.id.bstar);
		bStar.setOnClickListener(numberClick);

		call = (ImageView) findViewById(R.id.bcall);
		call.setOnClickListener(new CallClick());

		back = (ImageView) findViewById(R.id.bback);
		back.setOnClickListener(new BackClick());
		back.setOnLongClickListener(new LongBackClick());

		ImageView contact = (ImageView) findViewById(R.id.bcontact);
		contact.setOnClickListener(new ContactClick());

		ImageView calls = (ImageView) findViewById(R.id.bpreviouscalls);
		calls.setOnClickListener(new CallsClick());

		ImageView sponsors = (ImageView) findViewById(R.id.bsponsors);
		sponsors.setOnClickListener(new AdsClick());

		textDial.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (textDial.length() == 0) {
					back.setVisibility(View.INVISIBLE);
					call.setEnabled(false);
				} else {
					back.setVisibility(View.VISIBLE);
					call.setEnabled(true);
				}
				if (textDial.length() > 15) {
					textDial.setTextSize(28);
				} else {
					textDial.setTextSize(30);
				}
			}

		});

	}

}
