package com.dimwire.spcdialer;

import static com.dimwire.spcdialer.util.DialerUtil.DIALER_PREF_NAME;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class WelcomeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);
		SharedPreferences preferences = getSharedPreferences(DIALER_PREF_NAME,
				Context.MODE_PRIVATE);
		if (preferences.getBoolean("registered", false)) {
			Intent myIntent = new Intent(this, DialActivity.class);
			startActivity(myIntent);
			finish();
		} else {
			findViewById(R.id.textAccept).setOnClickListener(
					new OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(WelcomeActivity.this,
									RegisterActivity.class);
							startActivity(intent);
							finish();
						}
					});
			
			findViewById(R.id.textTerms).setOnClickListener(
					new OnClickListener() {
						@Override 
						public void onClick(View v) {
							Intent intent = new Intent(WelcomeActivity.this,
									TermsActivity.class);
							startActivity(intent);
						}
					});			
			
		}
	}
}
