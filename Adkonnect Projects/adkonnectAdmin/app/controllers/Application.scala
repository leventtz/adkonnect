package controllers

import java.io.File
import java.util.UUID
import scala.util.Failure
import scala.util.Success
import scala.util.Try
import com.amazonaws.services.s3.model.AccessControlList
import com.amazonaws.services.s3.model.GroupGrantee
import com.amazonaws.services.s3.model.Permission
import com.amazonaws.services.s3.model.PutObjectRequest
import com.sponsoracall.cdr.contract.AddDIDContract
import com.sponsoracall.cdr.contract._
import com.sponsoracall.cdr.service.CDRServiceRegistry
import com.sponsoracall.gatewayaccount.contract._
import com.sponsoracall.gatewayaccount.service.GatewayAccountServiceRegistry
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.mvc.Action
import play.api.mvc.Controller
import plugins.S3ScalaPlugin
import com.sponsoracall.gatewayaccount.contract.DeleteAccountMediaContract
import com.sponsoracall.gatewayaccount.contract.RetrieveAccountMediaContract
import com.amazonaws.services.s3.model.DeleteObjectRequest
import com.sponsoracall.gatewayaccount.data.TemplateDAData
import com.sponsoracall.account.service.AccountServiceRegistry
import com.sponsoracall.ad.contract.ApproveAdContract
import play.api.libs.Crypto
import play.api.Play
import play.api.i18n.Messages
import play.libs.Akka
import scala.concurrent.duration._
import javax.naming.ConfigurationException
import com.sponsoracall.util.EmailService
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import com.sponsoracall.ad.contract.UpdateDesignDescriptionContractV2
import scala.concurrent.Future
import java.util.Calendar
import com.sponsoracall.ad.service.AdServiceRegistry
object Application extends Controller {

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def getPartials(partialName: String) = Action {
    partialName match {
      case "dashboard" => Ok(views.html.dashboard())
      case "destinations" => Ok(views.html.destination.destinations())
      case "deleteConfirmationDialog" => Ok(views.html.deleteConfirmationDialog())
      case "confirmationDialog" => Ok(views.html.confirmationDialog())
      case "newDialingCodeDialog" => Ok(views.html.destination.newDialingCodeDialog())
      case "newRateDialog" => Ok(views.html.destination.newRateDialog())
      case "gateway" => Ok(views.html.gateway.gateway())
      case "gatewayDialog" => Ok(views.html.gateway.gatewayDialog())
      case "providerGateway" => Ok(views.html.gateway.providerGatewayDialog())
      case "addRouteDialog" => Ok(views.html.destination.addRouteDialog())
      case "accountDialog" => Ok(views.html.gateway.accountDialog())
      case "templates" => Ok(views.html.templates.templates())
      case "templateDADialog" => Ok(views.html.templates.templateDADialog())
      case "uploadMediaDialog" => Ok(views.html.templates.uploadMediaDialog())
      case "templateDialog" => Ok(views.html.templates.templateDialog())
      case "accounts" => Ok(views.html.account.accounts())
      case "editDesignDialog" => Ok(views.html.account.editDesignDialog())
      case _ => Ok("")
    }

  }

  def service(serviceName: String) = Action.async(parse.json) {
    implicit request =>
Future.successful(      
      try {
        val parseValue = Json.parse(request.body.toString)
        val contractValues = Json.parse(parseValue.\("contractValues").as[String]).as[JsObject];
        play.api.Logger.info(request.body.toString + " ServiceName:" + serviceName + " " + contractValues)
        runService(serviceName, contractValues)
      } catch {
        case t: Throwable => t.printStackTrace; cantParseContract
      }
  )
  }

  def runService(serviceName: String, contract: JsValue): play.api.mvc.SimpleResult = {
    serviceName match {
      case "RetrieveCountries" => retrieveCountries(contract)
      case "RetrieveDestinations" => retrieveDestinationsByCountry(contract)
      case "RetrieveDialingCodes" => retrieveDialingCodesByDestination(contract)
      case "RetrieveRatesByDestination" => retrieveRatesByDestination(contract)
      case "DeleteDialingCode" => deleteDialingCode(contract)
      case "CreateDialingCode" => createDialingCode(contract)
      case "ListActiveProviders" => listActiveProviders()
      case "ListAllProviders" => listAllProviders()
      case "DeleteRate" => deleteRate(contract)
      case "AddRate" => addRate(contract)
      case "ListGateways" => listGateways()
      case "ToggleGateway" => toggleGateway(contract)
      case "CreateGateway" => createGateway(contract)
      case "UpdateGateway" => UpdateGateway(contract)
      case "DeleteGateway" => deleteGateway(contract)
      case "ResetGateway" => resetGateway(contract)
      case "ListDIDs" => listDIDs(contract)
      case "DeleteDID" => deleteDID(contract)
      case "AddDID" => addDID(contract)
      case "ListDIDsByProvider" => listDIDsByProvider(contract)
      case "CreateProviderGateway" => createProviderGateway(contract)
      case "UpdateProviderGateway" => updateProviderGateway(contract)
      case "DeleteProviderGateway" => deleteProviderGateway(contract)
      case "ToggleProviderGateway" => toggleProviderGateway(contract)
      case "ListProviderGateways" => listProviderGateways()
      case "ListRoutes" => listRoutes(contract)
      case "AddRoute" => addRoute(contract)
      case "DeleteRoute" => deleteRoute(contract)
      case "ToggleRoute" => toggleRoute(contract)
      case "ToggleProvider" => toggleProvider(contract)
      case "AddProvider" => addProvider(contract)
      case "UpdateProvider" => updateProvider(contract)
      case "DeleteProvider" => deleteProvider(contract)
      case "ListAllTemplates" => listAllTemplates()
      case "ToogleTemplate" => toggleTemplate(contract)
      case "ToggleBypassSwitch" => toggleBypassSwitch(contract)
      case "DeleteTemplate" => deleteTemplate(contract)
      case "ListTemplateDAWithDestination" => listTemplateDAWithDestination(contract)
      case "AddTemplateDa" => addTemplateDa(contract)
      case "RemoveTemplateDa" => removeTemplateDa(contract)
      case "ListAccountMedia" => listAccountMedia();
      case "DeleteMedia" => deleteMedia(contract);
      case "CreateTemplate" => createTemplate(contract);
      case "UpdateTemplate" => updateTemplate(contract);
      case "ListUnApprovedAd" => listUnApprovedAd;
      case "TogggleApprove" => togggleApprove(contract);
      case "SendApprovedEmail" => sendApprovedEmail(contract);
      case "SendDeniedEmail" => sendDeniedEmail(contract);
      case "UpdateDesign" => updateDesign(contract);
      case "CDRSummary" => cdrSummary(contract);
      
      case _ => BadRequest("Unknown Service")
    }
  }

  def togggleApprove(contract: JsValue): play.api.mvc.SimpleResult = {
    ApproveAdContract.parse(contract) match {
      case Some(contractValue) => {
        AccountServiceRegistry.service.retrieveAccount(contractValue.accountId) match {
          case Success(Some(accountData)) => {
            AdServiceRegistry.service.togggleApprove(contractValue) match {
              case Success(()) => {
                if (contractValue.approve) {
                  println("Sending Email...")
                  sendApprovedEmail(accountData.email)
                }
                Ok("")
              }
              case Failure(t) => BadRequest
            }
          }
          case _ => BadRequest
        }
      }
      case None => cantParseContract
    }
    Ok("")
  }

  def updateDesign(contract: JsValue): play.api.mvc.SimpleResult = {
   UpdateDesignDescriptionContractV2.parse(contract.toString) match {
     case Some(contractValue) =>{
       AdServiceRegistry.service.updateDesign(contractValue)
       Ok("")
     }
     case None => cantParseContract
   }
  }
  
  def sendApprovedEmail(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val accountId = contract.\("accountId").as[String]
      AccountServiceRegistry.service.retrieveAccount(accountId) match {
        case Success(Some(accountData)) => {
          sendApprovedEmail(accountData.email)
          Ok("")
        }
        case Success(None) => BadRequest
        case Failure(t) => BadRequest
      }
      Ok("")
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def sendDeniedEmail(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val accountId = contract.\("accountId").as[String]
      AccountServiceRegistry.service.retrieveAccount(accountId) match {
        case Success(Some(accountData)) => {
          sendDeniedEmail(accountData.email)
          Ok("")
        }
        case Success(None) => BadRequest
        case Failure(t) => BadRequest
      }
      Ok("")
    } catch {
      case t: Throwable => cantParseContract
    }
  }  
  
  def listUnApprovedAd(): play.api.mvc.SimpleResult = {
    returnListResult(AdServiceRegistry.service.listUnApprovedAd)
  }

  def createTemplate(contract: JsValue): play.api.mvc.SimpleResult = {
    CreateTemplateContract.parse(contract) match {
      case Some(contractValue) => {
        GatewayAccountServiceRegistry.service.createTemplate(contractValue)
        Ok("")
      }
      case None => cantParseContract
    }
  }

  def updateTemplate(contract: JsValue): play.api.mvc.SimpleResult = {
    UpdateTemplateContract.parse(contract) match {
      case Some(contractValue) => {
        GatewayAccountServiceRegistry.service.updateTemplate(contractValue)
        Ok("")
      }
      case None =>cantParseContract
    }
  }

  def listAccountMedia(): play.api.mvc.SimpleResult = {
    returnListResult(GatewayAccountServiceRegistry.service.listAccountMedia(ListAccountMediaContract("1")))
  }

  def listTemplateDAWithDestination(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = contract.\("adId").as[Int]
      returnListResult(GatewayAccountServiceRegistry.service.listTemplateDAWithDestination(id))
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def addTemplateDa(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val adId = contract.\("adId").as[Int]
      val destinationId = contract.\("destinationId").as[Int]
      val rate = contract.\("rate").as[BigDecimal]
      val exclude = contract.\("exclude").as[Boolean]
      GatewayAccountServiceRegistry.service.addTemplateDa(TemplateDAData(adId, destinationId, rate, exclude)) match {
        case Success(()) => Ok("")
        case Failure(t) => BadRequest(t.getMessage())
      }
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def removeTemplateDa(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val adId = contract.\("adId").as[Int]
      val destinationId = contract.\("destinationId").as[Int]
      GatewayAccountServiceRegistry.service.removeTemplateDa(adId, destinationId) match {
        case Success(()) => Ok("")
        case Failure(t) => BadRequest(t.getMessage())
      }
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def toggleTemplate(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = contract.\("id").as[Int]
      GatewayAccountServiceRegistry.service.toggleTemplate(id) match {
        case Success(()) => Ok("")
        case Failure(t) => BadRequest(t.getMessage())
      }
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def toggleBypassSwitch(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = contract.\("id").as[Int]
      GatewayAccountServiceRegistry.service.toggleBypassSwitch(id) match {
        case Success(()) => Ok("")
        case Failure(t) => BadRequest(t.getMessage())
      }
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def deleteTemplate(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = contract.\("id").as[Int]
      GatewayAccountServiceRegistry.service.deleteTemplate(id) match {
        case Success(()) => Ok("")
        case Failure(t) => BadRequest(t.getMessage())
      }
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def listAllTemplates(): play.api.mvc.SimpleResult = {
    returnListResult(GatewayAccountServiceRegistry.service.listAllTemplates(None))
  }

  def deleteProvider(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = contract.\("id").as[String]
      GatewayAccountServiceRegistry.service.deleteAccount(DeleteAccountContract(id)) match {
        case Success(()) => Ok("")
        case Failure(t) => BadRequest(t.getMessage())
        case _ => BadRequest
      }
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def addProvider(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = UUID.randomUUID().toString().replace("-", "");
      val accountType = "P";
      val companyName = contract.\("companyName").as[String]
      val firstName = contract.\("firstName").as[String]
      val middleName = contract.\("middleName").as[String]
      val lastName = contract.\("lastName").as[String]
      val address1 = contract.\("address1").as[String]
      val address2 = contract.\("address2").as[String]
      val city = contract.\("city").as[String]
      val state = contract.\("state").as[String]
      val postalCode = contract.\("postalCode").as[String]
      val country = contract.\("country").as[String]
      val email = contract.\("email").as[String]
      val phone1 = contract.\("phone1").as[String]
      val phone2 = contract.\("phone2").as[String]
      val password = "Test1234_";
      val birthDay = "";
      val gender = "";
      val contractValue = new AddAccountContract(id, accountType, companyName, firstName, middleName, lastName, address1, address2, city, state, postalCode, country, email, phone1, phone2, password, birthDay, gender)
      GatewayAccountServiceRegistry.service.addAccount(contractValue) match {
        case Success(Some(id)) => Ok(id);
        case Failure(f) => BadRequest("Duplicate:" + f.getMessage());
        case _ => BadRequest
      }
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def updateProvider(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = contract.\("id").as[String]
      val accountType = "P";
      val companyName = contract.\("companyName").as[String]
      val firstName = contract.\("firstName").as[String]
      val middleName = contract.\("middleName").as[String]
      val lastName = contract.\("lastName").as[String]
      val address1 = contract.\("address1").as[String]
      val address2 = contract.\("address2").as[String]
      val city = contract.\("city").as[String]
      val state = contract.\("state").as[String]
      val postalCode = contract.\("postalCode").as[String]
      val country = contract.\("country").as[String]
      val email = contract.\("email").as[String]
      val phone1 = contract.\("phone1").as[String]
      val phone2 = contract.\("phone2").as[String]
      val password = "Test1234_";
      val birthDay = "";
      val gender = "";
      val active = contract.\("active").as[Boolean]
      val contractValue = new UpdateAccountContract(id, accountType, companyName, firstName, middleName, lastName, address1, address2, city, state, postalCode, country, email, phone1, phone2, birthDay, gender, active)
      GatewayAccountServiceRegistry.service.updateAccount(contractValue) match {
        case Success(()) => Ok("");
        case Failure(f) => BadRequest("Duplicate:" + f.getMessage());
        case _ => BadRequest
      }
    } catch {
      case t: Throwable => t.printStackTrace(); cantParseContract
    }
  }

  def toggleProvider(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = contract.\("id").as[String]
      GatewayAccountServiceRegistry.service.toogleAccount(id) match {
        case Success(()) => Ok("")
        case Failure(t) => BadRequest(t.getMessage())
      }
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def addRoute(contract: JsValue): play.api.mvc.SimpleResult = {
    RouteContract.parse(contract) match {
      case Some(contractValue) => {
        CDRServiceRegistry.service.addRoute(contractValue)
        Ok("")
      }
      case None => cantParseContract
    }
  }

  def deleteRoute(contract: JsValue): play.api.mvc.SimpleResult = {
    RouteContract.parse(contract) match {
      case Some(contractValue) => {
        CDRServiceRegistry.service.deleteRoute(contractValue)
        Ok("")
      }
      case None => cantParseContract
    }
  }

  def toggleRoute(contract: JsValue): play.api.mvc.SimpleResult = {
    RouteContract.parse(contract) match {
      case Some(contractValue) => {
        CDRServiceRegistry.service.toggleRoute(contractValue)
        Ok("")
      }
      case None => cantParseContract
    }
  }

  def listRoutes(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val destinationId = contract.\("destinationId").as[Int]
      returnListResult(com.sponsoracall.cdr.service.CDRServiceRegistry.service.listRoutes(destinationId))
    } catch {
      case t: Throwable => cantParseContract
    }
  }
  def createProviderGateway(contract: JsValue): play.api.mvc.SimpleResult = {
    CreateProviderGatewayContract.parse(contract) match {
      case Some(contractValue) => {
        CDRServiceRegistry.service.createProviderGateway(contractValue) match {
          case Success(()) => Ok("")
          case Failure(t) => BadRequest(t.getMessage())
        }
      }
      case None => cantParseContract
    }
  }

  def updateProviderGateway(contract: JsValue): play.api.mvc.SimpleResult = {
    UpdateProviderGatewayContract.parse(contract) match {
      case Some(contractValue) => {
        CDRServiceRegistry.service.updateProviderGateway(contractValue) match {
          case Success(()) => Ok("")
          case Failure(t) => BadRequest(t.getMessage())
        }
      }
      case None => cantParseContract
    }
  }

  def deleteProviderGateway(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = contract.\("id").as[Int]
      CDRServiceRegistry.service.deleteProviderGateway(id) match {
        case Success(()) => Ok("")
        case Failure(t) => BadRequest(t.getMessage())
      }
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def toggleProviderGateway(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = contract.\("id").as[Int]
      CDRServiceRegistry.service.toggleProviderGateway(id) match {
        case Success(()) => Ok("")
        case Failure(t) => BadRequest(t.getMessage())
      }
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def listProviderGateways(): play.api.mvc.SimpleResult = {
    try {
      returnListResult(com.sponsoracall.cdr.service.CDRServiceRegistry.service.listProviderGateways())
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def addDID(contract: JsValue): play.api.mvc.SimpleResult = {
    AddDIDContract.parse(contract) match {
      case Some(contractValue) => {
        CDRServiceRegistry.service.addDID(contractValue) match {
          case Success(()) => Ok("")
          case Failure(t) => BadRequest(t.getMessage())
        }
      }
      case None => cantParseContract
    }
  }

  def deleteDID(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val did = contract.\("DID").as[String]
      CDRServiceRegistry.service.deleteDID(did) match {
        case Success(()) => Ok("")
        case Failure(t) => BadRequest(t.getMessage())
      }
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def listDIDs(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = contract.\("id").as[Int]
      returnListResult(com.sponsoracall.cdr.service.CDRServiceRegistry.service.listDIDs(id))
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def listDIDsByProvider(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val providerId = contract.\("providerId").as[String]
      returnListResult(com.sponsoracall.cdr.service.CDRServiceRegistry.service.listDIDsByProvider(providerId))
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def resetGateway(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = contract.\("id").as[Int]
      CDRServiceRegistry.service.resetGatewayHitCount(id)
      Ok("")
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def deleteGateway(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = contract.\("id").as[Int]
      CDRServiceRegistry.service.deleteGateway(id) match {
        case Success(()) => Ok("")
        case Failure(t) => BadRequest(t.getMessage())
      }
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def toggleGateway(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val id = contract.\("id").as[Int]
      CDRServiceRegistry.service.toggleGateway(id) match {
        case Success(()) => Ok("")
        case Failure(t) => BadRequest(t.getMessage())
      }
    } catch {
      case t: Throwable => cantParseContract
    }

  }

  def UpdateGateway(contract: JsValue): play.api.mvc.SimpleResult = {
    UpdateGatewayContract.parse(contract) match {
      case Some(contractValue) => {
        CDRServiceRegistry.service.updateGateway(contractValue) match {
          case Success(()) => Ok("")
          case Failure(t) => BadRequest(t.getMessage())
        }
      }
      case None => cantParseContract
    }
  }

  def createGateway(contract: JsValue): play.api.mvc.SimpleResult = {
    CreateGatewayContract.parse(contract) match {
      case Some(contractValue) => {
        CDRServiceRegistry.service.createGateway(contractValue) match {
          case Success(()) => Ok("")
          case Failure(t) => BadRequest(t.getMessage())
        }
      }
      case None => cantParseContract
    }
  }

  def addRate(contract: JsValue): play.api.mvc.SimpleResult = {
    RateContract.parse(contract) match {
      case Some(contractValue) => {
        GatewayAccountServiceRegistry.service.addRate(contractValue)
        Ok("")
      }
      case None => cantParseContract
    }
  }

  def listGateways(): play.api.mvc.SimpleResult = {
    returnListResult(com.sponsoracall.cdr.service.CDRServiceRegistry.service.listGateways)
  }

  def listActiveProviders(): play.api.mvc.SimpleResult = {
    returnListResult(GatewayAccountServiceRegistry.service.listActiveProviders)
  }

  def listAllProviders(): play.api.mvc.SimpleResult = {
    returnListResult(GatewayAccountServiceRegistry.service.listAllProviders())
  }

  def deleteRate(contract: JsValue): play.api.mvc.SimpleResult = {
    DeleteRateContract.parse(contract) match {
      case Some(contractValue) => {
        GatewayAccountServiceRegistry.service.deleteRate(contractValue)
        Ok("")
      }
      case None => cantParseContract
    }
  }

  def createDialingCode(contract: JsValue): play.api.mvc.SimpleResult = {
    CreateDialingCodeContract.parse(contract) match {
      case Some(contractValue) => {
        CDRServiceRegistry.service.createDialingCode(contractValue)
        Ok("")
      }
      case None => cantParseContract
    }
  }

  def deleteDialingCode(contract: JsValue): play.api.mvc.SimpleResult = {
    DeleteDialingCodeContract.parse(contract) match {
      case Some(contractValue) => {
        CDRServiceRegistry.service.deleteDialingCode(contractValue)
        Ok("")
      }
      case None => cantParseContract
    }
  }
  def retrieveCountries(contract: JsValue): play.api.mvc.SimpleResult = {
    returnListResult(CDRServiceRegistry.service.retrieveCountries)
  }

  def retrieveDestinationsByCountry(contract: JsValue): play.api.mvc.SimpleResult = {
    RetrieveDestinationsByCountryContract.parse(contract) match {
      case Some(contractValue) => returnListResult(CDRServiceRegistry.service.retriveDestinationsByCountry(contractValue))
      case None => cantParseContract
    }
  }

  def retrieveDialingCodesByDestination(contract: JsValue): play.api.mvc.SimpleResult = {
    RetrieveDialingCodeByDestinationContract.parse(contract) match {
      case Some(contractValue) => returnListResult(CDRServiceRegistry.service.retrieveDialingCodeByDestination(contractValue))
      case None => cantParseContract
    }
  }

  def retrieveRatesByDestination(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val destinationId = contract.\("destinationId").as[Int];
      returnListResult(GatewayAccountServiceRegistry.service.listRateByDestination(destinationId))
    } catch {
      case t: Throwable => cantParseContract
    }
  }

  def returnListResult[T](result: Try[List[T]]): play.api.mvc.SimpleResult = {
    result match {
      case Success(Nil) => NotFound
      case Success(list) => Ok(Json.toJson(Map("list" -> list.map(_.toString))).toString)
      case Failure(t) => BadRequest(t.getMessage)
    }
  }

  def accountMedia() = Action(parse.multipartFormData) { request =>
    try {
      val postData = request.body.asFormUrlEncoded
      val mediaDescription = postData.get("mediaDescription").get(0)
      val mediaIdParam = postData.get("mediaId")
      val accountId = "1";
      request.body.file("media").map { mediaFile =>
        import java.io.File
        val filename = mediaFile.filename
        val contentType = mediaFile.contentType

        val result: Option[Int] = {
          var mediaId = 0
          if (mediaIdParam.get(0).isEmpty) {
            mediaId = GatewayAccountServiceRegistry.service.addAccountMedia(AddAccountMediaContract(accountId, mediaDescription, "M", true)).get.get.id
          } else {
            mediaId = mediaIdParam.get(0).toInt;
            GatewayAccountServiceRegistry.service.updateAccountMedia(UpdateAccountMediaContract(mediaId, mediaDescription, "M", true));
          }
          Some(mediaId);
        }

        result match {
          case Some(mediaId) => {
            val fileExtension = filename.split("\\.")(1)
            val file = File.createTempFile(s"/temp/$mediaId", fileExtension)
            mediaFile.ref.moveTo(file, true)
            val fileName = s"$mediaId.$fileExtension"
            val putObjectRequest = new PutObjectRequest(S3ScalaPlugin.s3Bucket + "/accountfiles", fileName, file)

            val acl = new AccessControlList()
            acl.grantPermission(GroupGrantee.AllUsers, Permission.Read);
            putObjectRequest.setAccessControlList(acl)

            val result = S3ScalaPlugin.s3Client.get.putObject(putObjectRequest);
            file.delete()
            val contract = UpdateMediaLocationContract(mediaId, "https://s3.amazonaws.com/" + S3ScalaPlugin.s3Bucket + "/accountfiles/" + fileName)
            GatewayAccountServiceRegistry.service.updateAccountMediaLocation(contract)
            Ok("")
          }
          case None => Ok("")
        }
      }.getOrElse {
        BadRequest
      }

    } catch {
      case t: Throwable => BadRequest
    }
  }

  def deleteMedia(contract: JsValue): play.api.mvc.SimpleResult = {
    val id = contract.\("id").as[Int]
    GatewayAccountServiceRegistry.service.retrieveAccountMedia(RetrieveAccountMediaContract(id)) match {
      case Success(Some(data)) => {
        GatewayAccountServiceRegistry.service.deleteAccountMedia(DeleteAccountMediaContract(id)) match {
          case Success(()) => {
            val objectName = data.mediaLocation.substring(data.mediaLocation.lastIndexOf("/") + 1);
            val deleteObjectRequest = new DeleteObjectRequest(S3ScalaPlugin.s3Bucket + "/accountfiles", objectName);
            val result = S3ScalaPlugin.s3Client.get.deleteObject(deleteObjectRequest);
          }
          case _ => BadRequest
        }
      }
      case _ => BadRequest
    }
    Ok("")

  }

  def sendDeniedEmail(toEmail:String) {
    try {
      val emailFrom = Play.current.configuration.getString("email.from") match {
        case Some(value) => value
        case None => throw new ConfigurationException("Cant Find Email from")
      }

      val emailSubject = Messages("your.ad.denied.subject")

      val emailBody = Messages("your.ad.denied")

      Akka.system.scheduler.scheduleOnce(0.milliseconds) {
        val emailResult = EmailService.sendEmail(emailFrom, toEmail, emailSubject, emailBody)
      }
      Success(())
    } catch {
      case t: Throwable => Failure(t)
    }    
  }
  
  def sendApprovedEmail(toEmail: String): Try[Unit] = {
    try {
      val emailFrom = Play.current.configuration.getString("email.from") match {
        case Some(value) => value
        case None => throw new ConfigurationException("Cant Find Email from")
      }

      val emailSubject = Messages("your.ad.approved.subject")

      val emailBody = Messages("your.ad.approved")

      Akka.system.scheduler.scheduleOnce(0.milliseconds) {
        val emailResult = EmailService.sendEmail(emailFrom, toEmail, emailSubject, emailBody)
      }
      Success(())
    } catch {
      case t: Throwable => Failure(t)
    }
  }

  def cdrSummary(contract: JsValue): play.api.mvc.SimpleResult = {
    try {
      val startDate = parseFromDate(contract.\("startDate").as[String])
      val endDate = parseFromDate(contract.\("endDate").as[String])
      val providerIdPostvalue = contract.\("providerId").as[String]
      val countryIdPostValue = contract.\("countryId").as[Int]
      val destinationIdPostValue = contract.\("destinationId").as[Int]
      val reportNamePostValue= contract.\("reportName").as[String];
      
      val providerId=if(providerIdPostvalue.isEmpty()) None else Some(providerIdPostvalue)
      val countryId=if(countryIdPostValue==0) None else Some(countryIdPostValue)
      val destinationId=if(destinationIdPostValue==0) None else Some(destinationIdPostValue)
      val reportName=if(reportNamePostValue==None) "date" else reportNamePostValue  
      
      
      val result = CDRServiceRegistry.service.retrieveCDRSummary(CDRSummaryContract(reportName, CDRFilter(startDate=startDate, endDate=endDate, adId=None, providerId=providerId, country=countryId, inDest=None, outDest=destinationId,accountId=None)))
      result match {
        case Success(cdrSummaryData) => Ok(Json.toJson(Map(
            "metadata" -> cdrSummaryData.metaData.map(Json.toJson(_)),
            "values" -> cdrSummaryData.values.map(Json.toJson(_))
            )))
        case Failure(t) => t.printStackTrace();BadRequest
      } 
    } catch {
      case t:Throwable => t.printStackTrace();BadRequest
    }
  }

  def parseFromDate(s: String): Option[Int] = {
    parseDate(s)
  }

  def parseDate(s: String): Option[Int] = {
    try {
      val sp = s.replace("\"", "").split("/")
      val year = sp(2).toInt
      val month = sp(0).toInt
      val date = sp(1).substring(0, 2).toInt

      val c = Calendar.getInstance()
      c.clear()
      c.set(Calendar.YEAR, year)
      c.set(Calendar.MONTH, month - 1)
      c.set(Calendar.DAY_OF_MONTH, date)

      Some((c.getTimeInMillis() / 1000 / 60 / 60).toInt)
    } catch {
      case t: Throwable => { None; }
    }
  }

  def parseToDate(s: String): Option[Int] = {
    parseDate(s) match {
      case None => None
      case Some(v) => Some(v + 24)
    }
  }

 
  def cantParseContract = BadRequest("can't parse contract!")

}
