package com.sponsoracall.gatewayaccount.contract

import java.sql.Date

object UpdateAccountContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[UpdateAccountContract]
  def parse(value: JsValue): Option[UpdateAccountContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class UpdateAccountContract(
  id: String,
  accountType: String,
  companyName: String,
  firstName: String,
  middleName: String,
  lastName: String,
  address1: String,
  address2: String,
  city: String,
  state: String,
  postalCode: String,
  country: String,
  email: String,
  phone1: String,
  phone2: String,
  birthday:String,
  gender:String,  
  active: Boolean = true) {

  require(id != null && id.length() != 0, "Invalid AccountId")

  require(accountType != null && accountType.length() > 0, "Missing accountType")
  require(firstName != null && firstName.length() > 0, "Missing FirstName")
  require(lastName != null && lastName.length() > 0, "Missing Last Name")
  require(country != null && country.length() > 0, "Missing country")
  require(phone1 != null && phone1.length() > 0, "Missing Phone Number")
  require(email.matches(AccountContract.validEmailRegex), "Invalid Email")
  
}