package com.sponsoracall.gatewayaccount.contract

case class ListRateByAccountContract(accountId:String) {
  require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
}