package com.sponsoracall.gatewayaccount.contract

case class UpdateEndPointContract(id:Int,accountId:String,description:String,endpoint:String,userName:String,password:String,registrationRequired:Boolean=false) {
  require(id>0,"Missing id")
  require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
  require(description!=null && !description.isEmpty(),"Missing Description")
  require(endpoint!=null && !endpoint.isEmpty(),"Missing endpoint")
  require(userName!=null && !userName.isEmpty(),"Missing userName")
  require(password!=null && !password.isEmpty(),"Missing password")
}