package com.sponsoracall.gatewayaccount.service

import com.sponsoracall.gatewayaccount.integration.GatewayAccountIntegrationComponent



trait GatewayAccountServiceComponent {
  self: GatewayAccountIntegrationComponent =>
  val service: GatewayAccountService
}

