package com.sponsoracall.gatewayaccount.data

import java.sql.Date

case class RateData(id:Int,accountId:String,accountName:String,destinationId:Int,destinationName:String,rate:BigDecimal,rateTypeId:String){
  require(id>0,"Missing Id")
  require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
  require(destinationId>0,"Missing Account Id")
  require(rateTypeId.length()==1,"Missing rateTypeId")
  require(rate>=0,"Missing rate")
  
  import play.api.libs.json._
  val jsonWriter=Json.writes[RateData]
  override def toString={
    jsonWriter.writes(this).toString
  }  
  
}