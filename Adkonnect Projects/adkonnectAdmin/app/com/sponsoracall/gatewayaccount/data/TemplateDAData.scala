package com.sponsoracall.gatewayaccount.data

case class TemplateDAData(adId:Int,destinationId:Int,rate:BigDecimal,exclude:Boolean=false) {
    import play.api.libs.json._
  val jsonWriter=Json.writes[TemplateDAData]
  override def toString={
    jsonWriter.writes(this).toString
  }  
  
}
case class ListTemplateDAData(adId:Int,destinationId:Int,destinationName:String,rate:BigDecimal,exclude:Boolean=false) {
    import play.api.libs.json._
  val jsonWriter=Json.writes[ListTemplateDAData]
  override def toString={
    jsonWriter.writes(this).toString
  }  
  
}