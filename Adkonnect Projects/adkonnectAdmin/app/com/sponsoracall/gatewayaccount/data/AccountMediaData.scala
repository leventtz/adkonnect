package com.sponsoracall.gatewayaccount.data

case class AccountMediaData(id:Int,accountId:String,description:String,mediaTypeId:String,approved:Boolean,mediaLocation:String) {
	require(id>0,"id required")
	require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
	require(description!=null && !description.isEmpty(),"Invalid Description")
	require(description!=null && !description.isEmpty(),"Invalid Media Location")
	
  import play.api.libs.json._
  val jsonWriter = Json.writes[AccountMediaData]
  override def toString = {
    jsonWriter.writes(this).toString
  }	
}