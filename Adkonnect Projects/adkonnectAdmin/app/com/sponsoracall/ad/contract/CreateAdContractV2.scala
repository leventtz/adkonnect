package com.sponsoracall.ad.contract

import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.Json
import com.sponsoracall.cdr.data.LocationData
import com.sponsoracall.ad.data.DesignData

object CreateAdContractV2 {
  import play.api.libs.json._
  implicit val jsonLocationReader = Json.reads[LocationData]
  implicit val jsonDesignDataReader = Json.reads[DesignData]
  val jsonReader = Json.reads[CreateAdContractV2]
  def parse(value: String): Option[CreateAdContractV2] = {
    try {
      Some(jsonReader.reads(Json.parse(value)).get)
    } catch {
      case t: Throwable => {
        None
      }
    }
  }
}


case class CreateAdContractV2 (
 accountId:String,
 name:String,
 templateId:Int,
 templateName:String,
 impression:BigDecimal,
 budget:BigDecimal,
 unitPrice:BigDecimal,
 promptCost:BigDecimal,
 mediaIVRId:String,
 maxDuration:Int,
 bypassSwitch:Boolean,
 allowRecharge:Boolean,
 rechargeAmount:BigDecimal,
 rechargeThreshold:BigDecimal,
 incoming:List[LocationData],
 designData:DesignData
) {
  require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
  require(name != null && name.length() > 0, "Missing Ad Name")
  require(incoming !=null && incoming.size>0, "Missing Location Info") 
}