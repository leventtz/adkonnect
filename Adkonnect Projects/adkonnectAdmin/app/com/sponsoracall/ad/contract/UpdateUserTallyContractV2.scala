package com.sponsoracall.ad.contract

case class UpdateUserTallyContractV2(from: String, to: String, accountId: String, adId: String, durationInSec: Int)