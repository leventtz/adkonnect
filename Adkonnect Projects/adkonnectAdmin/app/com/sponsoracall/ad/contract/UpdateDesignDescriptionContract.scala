package com.sponsoracall.ad.contract
import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.Json

object UpdateDesignDescriptionContractV2 {
  import play.api.libs.json._
  val jsonReader = Json.reads[UpdateDesignDescriptionContractV2]
  def parse(value: String): Option[UpdateDesignDescriptionContractV2] = {
    try {
      Some(jsonReader.reads(Json.parse(value)).get)
    } catch {
      case t: Throwable => {
        None
      }
    }
  }
}
case class UpdateDesignDescriptionContractV2(accountId:String,adId:String,description:String) {
  require(accountId!=null && adId!=null && description!=null,"Missing Account,AdId,description")
  
  
} 

