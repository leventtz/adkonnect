package com.sponsoracall.ad.contract

case class ExcludeAccount(accountId:String,adId:String)
case class RefundContractV2(number: String,excludeAccount:Option[ExcludeAccount])