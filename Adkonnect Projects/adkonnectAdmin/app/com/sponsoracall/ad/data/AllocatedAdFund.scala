package com.sponsoracall.ad.data
import play.api.libs.json._

object AllocatedAdFund {
  val jsonReader = Json.reads[AllocatedAdFund]
  def parse(value: String): Option[AllocatedAdFund] = {
    try {
      Some(jsonReader.reads(Json.parse(value)).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class AllocatedAdFund(accountId: String, adId: String, allocatedAmount: BigDecimal, timestamp: Long) {
  val jsonWriter = Json.writes[AllocatedAdFund]
  override def toString = {
    jsonWriter.writes(this).toString
  }
}