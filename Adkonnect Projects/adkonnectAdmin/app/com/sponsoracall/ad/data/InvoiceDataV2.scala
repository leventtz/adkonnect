package com.sponsoracall.ad.data

import play.api.libs.json.Json
object InvoiceDataV2 {
  import play.api.libs.json._
  implicit val j1 = Json.reads[OrderTotalDataV2]
  implicit val j2 = Json.reads[InvoiceItemDataV2]
  val jsonReader = Json.reads[InvoiceDataV2]
  def parse(value: String): Option[InvoiceDataV2] = {
    try {
      val l1=Json.parse(value).\("lineItems")
      val l2= Json.parse(value).\("totalItems")
      val totalItems=Json.parse(value).\("totalItems").as[List[OrderTotalDataV2]]
      val lineItems=Json.parse(value).\("lineItems").as[List[InvoiceItemDataV2]]
      
      Some(InvoiceDataV2(lineItems,totalItems))
    } catch {
      case t: Throwable => {
        None
      }
    }
  }
}

case class InvoiceDataV2(lineItems:List[InvoiceItemDataV2],totalItems:List[OrderTotalDataV2]) {
  import play.api.libs.json._
  implicit val w1=Json.writes[OrderTotalDataV2]
  implicit val w2=Json.writes[InvoiceItemDataV2]
  val jsonWriter=Json.writes[InvoiceDataV2]
  override def toString={
    jsonWriter.writes(this).toString
  }     
}