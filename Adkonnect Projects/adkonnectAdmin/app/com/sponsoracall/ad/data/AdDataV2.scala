package com.sponsoracall.ad.data

import com.sponsoracall.cdr.data.LocationData
case class AdDataV2 (
    accountId:String,
    name:String,
    adId:String,
    adDateTime:Long,
    templateId:Int,
    mediaIVRId:String,
    mediaId:String,
    description:String,
    designData:String,    
    impressionView:String,
    maxDuration:Int,
    bypassSwitch:Boolean=false,
    impression:BigDecimal,
    budget:BigDecimal=0,
    balance:BigDecimal=0,
    adPrice:BigDecimal,
    incoming:List[LocationData],
    templateName:String,    
    approved:Boolean=false,
    active:Boolean=false
    ) {
  
  
  require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
  require(name != null && name.length() > 0, "Missing Ad Name")
  require(budget >=0 , "Invalid Budget")

  import play.api.libs.json._
  implicit val jsonLocationWriter=Json.writes[LocationData]
  val jsonWriter=Json.writes[AdDataV2]
  override def toString={
    jsonWriter.writes(this).toString
  }   
}