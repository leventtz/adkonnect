package com.sponsoracall.ad.data

case class UserCalls(accountId:String,adId:String,from:String,to:String,calldate:Long,callDuration:Int,maxDuration:Int,outDestinationId:Int)