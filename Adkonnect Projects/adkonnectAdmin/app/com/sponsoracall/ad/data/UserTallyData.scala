package com.sponsoracall.ad.data

import play.api.libs.json._
import com.sponsoracall.util.GlobalSettings
 
object UserTallyData {
  implicit val recentCallsReader = Json.reads[RecentCalls]
  implicit val recentAdsReader = Json.reads[RecentAds]
  val jsonReader = Json.reads[UserTallyData]
  def parse(value: String): Option[UserTallyData] = {
    try {
      Some(jsonReader.reads(Json.parse(value)).get)
    } catch {
      case t: Throwable => t.printStackTrace();None
    }
  }
}


object RecentCalls {
  val jsonReader = Json.reads[RecentCalls]
  def parse(value: String): Option[RecentCalls] = {
    try {
      Some(jsonReader.reads(Json.parse(value)).get)
    } catch {
      case t: Throwable => t.printStackTrace();None
    }
  }
}

object RecentAds {
  val jsonReader = Json.reads[RecentAds]
  def parse(value: String): Option[RecentAds] = {
    try {
      Some(jsonReader.reads(Json.parse(value)).get)
    } catch {
      case t: Throwable => t.printStackTrace();None
    }
  }
}


case class RecentCalls(to:String,timeStamp: Long) {
  val jsonWriter = Json.writes[RecentCalls]
  override def toString = {
    jsonWriter.writes(this).toString
  }  
}

case class RecentAds(accountId: String, adId: String, to:String,timeStamp: Long) {
  val jsonWriter = Json.writes[RecentAds]
  override def toString = {
    jsonWriter.writes(this).toString
  }    
}

case class UserTallyData(dailyTotalMinutes: Int, dailyTotalMinutesTimeStamp: Long, monthlyTotalMinutes: Int, monthlyTotalMinutesTimeStamp: Long, recentAds: List[RecentAds],recentCalls: List[RecentCalls]) {
  val isDailyLimit=dailyTotalMinutes<GlobalSettings.totalDailyMinutesAllowed
  val isMonthlyLimit=monthlyTotalMinutes<GlobalSettings.totalMonthlyMinutesAllowed
  implicit val jsonRecentCallsWriter = Json.writes[RecentCalls]
  implicit val jsonRecentAdsWriter = Json.writes[RecentAds]
  val jsonWriter = Json.writes[UserTallyData]
  override def toString = {
    jsonWriter.writes(this).toString
  }
}

