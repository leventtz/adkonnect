package com.sponsoracall.ad.service

import scala.util.Try
import com.sponsoracall.ad.contract.ActivateAdContractV2
import com.sponsoracall.ad.contract.ApproveAdContract
import com.sponsoracall.ad.contract.CreateAdContractV2
import com.sponsoracall.ad.contract.PickAdContractV2
import com.sponsoracall.ad.contract.UpdateAdActiveTableContractV2
import com.sponsoracall.ad.contract.UpdateDesignContractV2
import com.sponsoracall.ad.contract.UpdateDesignDescriptionContractV2
import com.sponsoracall.ad.contract.UpdateIncomingContractV2
import com.sponsoracall.ad.data.AdDataV2
import com.sponsoracall.ad.data.ImpressionViewData
import com.sponsoracall.ad.data.OrderTotalDataV2
import com.sponsoracall.ad.data.OrderTotalFeesDataV2
import com.sponsoracall.ad.data.PickAdData
import com.sponsoracall.ad.data.TransactionDataV2
import com.sponsoracall.ad.contract.PickAdFromContractV2
import com.sponsoracall.ad.contract.RefundContractV2
import com.sponsoracall.ad.contract.UpdateUserTallyContractV2
import com.sponsoracall.ad.contract.AddFundContract
import com.sponsoracall.ad.contract.AddCCTransactionContract
import com.sponsoracall.ad.contract.CompleteOrderContract
import com.sponsoracall.account.payment.PaymentData

trait AdService {

  def toogleAd(contract: ActivateAdContractV2)

  def updateBalance(accountId: String, adId: String,amount:BigDecimal)
  def calculateOrderTotal(lineItems: List[CreateAdContractV2]): List[OrderTotalDataV2]
  def listOrderTotalFees(): Try[List[OrderTotalFeesDataV2]]
  def retrieveAccountAd(accountId: String, adId: String): Option[AdDataV2]
  def retriveAd(adId: String): Option[AdDataV2]

  def retrieveAccounAds(accountId: String, adId: String): List[AdDataV2]

  def saveOrder(contract:CompleteOrderContract): Try[PaymentData]

  def updateIncoming(contract: UpdateIncomingContractV2)

  def updateDesign(contract: UpdateDesignContractV2)

  def updateDesign(contract: UpdateDesignDescriptionContractV2)

  def addItemstoAdActiveTable(contract: UpdateAdActiveTableContractV2): Try[Unit]

  def removeItemsFromAdActiveTable(contract: UpdateAdActiveTableContractV2): Try[Unit]

  def retrieveInvoice(accountId: String, orderId: String)

  def updateBalance(accountId: String, adId: String): Try[Unit]

  def addTransaction(contract: TransactionDataV2): Try[Unit]

  def retrieveTransactions(accountId: String, orderId: String): List[TransactionDataV2]

  def retrieveCachedCallRedis(number: String): Try[PickAdData]
  def cacheCallRedis(number: String, adPickDataJson: String): Try[Unit]

  def cacheCall(number: String, adPickDataJson: String): Try[Unit]

  def retrieveCachedCall(number: String): Try[PickAdData]

  def retrieveCachedCallForCDR(number: String): Try[PickAdData]

  def listUnApprovedAd(): Try[List[AdDataV2]]

  def togggleApprove(contract: ApproveAdContract): Try[Unit]

  def retrieveImpressions(from: String, to: String, postalCode: String): List[ImpressionViewData]
  
  def pickAdsFrom(contract:PickAdFromContractV2): List[PickAdData]

  def xPickAdsFrom(contract:PickAdFromContractV2): List[PickAdData]

  def returnFunds(contract:RefundContractV2)
  
  def updateAdName(accountId:String,adId:String,name:String)
  
  def updateAdImpression(accountId:String,adId:String,impression:BigDecimal):Try[Unit]

  def updateUserTally(contract:UpdateUserTallyContractV2)
  
  def addFund(contract:AddFundContract):Try[PaymentData]
  
  def addCCTransaction(contract:AddCCTransactionContract): Try[Unit]
}