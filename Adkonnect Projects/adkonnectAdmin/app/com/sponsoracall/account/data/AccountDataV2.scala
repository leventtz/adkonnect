package com.sponsoracall.account.data

import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.Json
import com.sponsoracall.util.MD5DigestV2


object AccountDataV2 {
  import play.api.libs.json._
  val jsonReader = Json.reads[AccountDataV2]
  def parse(value: String): Option[AccountDataV2] = {
    try {
      Some(jsonReader.reads(Json.parse(value)).get)
    } catch {
      case t: Throwable => {
        None
      }
    }
  }
}

case class AccountDataV2(
  id: String,
  accountType: String,
  companyName: String,
  firstName: String,
  middleName: String,
  lastName: String,
  address1: String,
  address2: String,
  city: String,
  state:String,
  postalCode: String,
  country: String,
  var email: String,
  phone1: String,
  phone2: String,
  birthday:String,
  var password:String,
  gender:String) {
  require(id!=null && id.length()!=0,"Missing AccountId")
  require(accountType != null && accountType.length() > 0, "Missing accountType")
  require(firstName != null && firstName.length() > 0, "Missing FirstName")
  require(lastName != null && lastName.length() > 0, "Missing Last Name")
  require(address1 != null && address1.length() > 0, "Missing Address1")
  require(city != null && city.length() > 0, "Missing City")
  require(country != null && country.length() > 0, "Missing country")
  require(phone1 != null && phone1.length() > 0, "Missing Phone Number")
  require(email != null && email.length() > 0, "Missing email") 
  
  var active:Boolean=false;
  import play.api.libs.json._
  val jsonWriter=Json.writes[AccountDataV2]
  override def toString={
    jsonWriter.writes(this).toString
  }
  
}