package com.sponsoracall.account.data

import play.api.libs.functional.syntax.toFunctionalBuilderOps
case class AccountMediaDataV2(accountId:String,mediaId:String,description:String,mediaTypeId:String,var mediaLocation:String="NA",approved:String="0") {
    require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
    require(mediaId!=null && accountId.length()!=0,"Invalid mediaId")
	require(description!=null && !description.isEmpty(),"Invalid Description")
  import play.api.libs.json._
  val jsonWriter=Json.writes[AccountMediaDataV2]
  override def toString={
    jsonWriter.writes(this).toString
  }
}
