package com.sponsoracall.account.payment;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Hashtable;
 
import com.sponsoracall.util.GlobalSettings;

public class Payment {
	public static PaymentData pay(PaymentContract contract) throws Exception{
		// By default, this sample code is designed to post to our test server for
		// developer accounts: https://test.authorize.net/gateway/transact.dll
		// for real accounts (even in test mode), please make sure that you are
		// posting to: https://secure.authorize.net/gateway/transact.dll
		
		URL post_url = new URL(GlobalSettings.ccServer());
		Hashtable<String,String> post_values = new Hashtable<String,String>();
		// the API Login ID and Transaction Key must be replaced with valid values
		post_values.put ("x_login", GlobalSettings.x_login_id());
		post_values.put ("x_tran_key", GlobalSettings.x_tran_key());
		  
		post_values.put ("x_version", "3.1");
		post_values.put ("x_delim_data", "TRUE");
		post_values.put ("x_delim_char", "|");
		post_values.put ("x_relay_response", "FALSE");

		post_values.put ("x_type", "AUTH_CAPTURE");
		post_values.put ("x_method", "CC");
		post_values.put ("x_card_code", contract.cvv());
		post_values.put ("x_card_num", contract.cardNum());
		post_values.put ("x_exp_date", contract.expDate());

		post_values.put ("x_amount", contract.amount().toString());
		post_values.put ("x_description", contract.description());

		post_values.put ("x_first_name", contract.firstName());
		post_values.put ("x_last_name", contract.lastName());
		post_values.put ("x_address", contract.address());
		post_values.put ("x_state",contract.state());
		post_values.put ("x_zip", contract.zip());
		post_values.put ("x_phone", contract.phone());
		
		StringBuffer post_string = new StringBuffer();
		Enumeration<String> keys = post_values.keys();
		while( keys.hasMoreElements() ) {
		  String key = URLEncoder.encode(keys.nextElement().toString(),"UTF-8");
		  String value = URLEncoder.encode(post_values.get(key).toString(),"UTF-8");
		  post_string.append(key + "=" + value + "&");
		}


		// Open a URLConnection to the specified post url
		URLConnection connection = post_url.openConnection();
		connection.setDoOutput(true);
		connection.setUseCaches(false);

		// this line is not necessarily required but fixes a bug with some servers
		connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

		// submit the post_string and close the connection
		DataOutputStream requestObject = new DataOutputStream( connection.getOutputStream() );
		requestObject.write(post_string.toString().getBytes());
		requestObject.flush();
		requestObject.close();

		// process and read the gateway response
		BufferedReader rawResponse = new BufferedReader(new InputStreamReader(connection.getInputStream()));

		String responseData = rawResponse.readLine();
		rawResponse.close();	                     // no more data

		// split the response into an array
		String [] responses = responseData.split("\\|");
		
		return new PaymentData(responses[0],responses[3],responses[4]);
		
	}
}
