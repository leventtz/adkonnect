package com.sponsoracall.account.contract

import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.JsValue
import com.sponsoracall.util.MD5DigestV2


object AccountContractV2 {
   val validEmailRegex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
  val validPasswordRegex = "^.*(?=.{6,})(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).*$"
  import play.api.libs.json._
  val jsonReader = Json.reads[AccountContractV2]
  def parse(value: JsValue): Option[AccountContractV2] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => t.printStackTrace();None
    }
  }
}



case class AccountContractV2(
  id:String,  
  accountType: String,
  companyName: String,
  firstName: String,
  middleName: String,
  lastName: String,
  address1: String,
  address2: String,
  city: String,
  state: String,
  postalCode: String,
  country: String,
  email: String,
  phone1: String,
  phone2: String,
  var password: String,
  birthday:String,
  gender:String) {
  
  
  require(accountType != null && accountType.length() > 0, "Missing accountType")
  require(firstName != null && firstName.length() > 0, "Missing FirstName")
  require(lastName != null && lastName.length() > 0, "Missing Last Name")
  require(address1 != null && address1.length() > 0, "Missing Address1")
  require(city != null && city.length() > 0, "Missing City")
  require(country != null && country.length() > 0, "Missing country")
  require(phone1 != null && phone1.length() > 9, "Invalid Phone Number")
  
  require(email != null && email.length() > 0, "Missing email") 
  require(email.matches(AccountContractV2.validEmailRegex), "Invalid Email")
  
  password=MD5DigestV2.digest(password)
  
  import play.api.libs.json._
  val jsonWriter = Json.writes[AccountContractV2]
  override def toString = {
    jsonWriter.writes(this).toString
  }

}