package com.sponsoracall.account.service

import com.sponsoracall.account.integration.AccountIntegrationComponent
import com.sponsoracall.account.integration.AccountIntegrationImpl

import scala.util.Try

import com.sponsoracall.account.contract.AccountContractV2
import com.sponsoracall.account.contract.AccountMediaContractV2
import com.sponsoracall.account.contract.AccountSearchContract
import com.sponsoracall.account.contract.ChangePasswordContractV2
import com.sponsoracall.account.data.AccountDataV2
import com.sponsoracall.account.data.AccountMediaDataV2

object AccountServiceRegistry extends AccountServiceComponent with AccountIntegrationComponent {
  val integration = new AccountIntegrationImpl
  val service = new AccountService() {
    def addMobileUserAccount(contract: AccountContractV2): Try[Option[String]] = integration.addMobileUserAccount(contract)

    def deleteAccountAll(email: String) = integration.deleteAccountAll(email)
    def deleteAccount(accountId: String) = integration.deleteAccount(accountId)
    def addEmail(email: String, accountId: String): Try[Unit] = integration.addEmail(email, accountId)
    def deleteEmail(email: String): Try[Unit] = integration.deleteEmail(email)

    def updateEmail(oldEmail: String, newEmail: String, accountId: String): Try[Unit] = integration.updateEmail(oldEmail, newEmail, accountId)

    def getAccountId(email: String): Try[Option[String]] = integration.getAccountId(email)

    def createAccount(contract: AccountContractV2): Try[String] = integration.createAccount(contract)

    def toogleAccount(accountId: String, active: String) = integration.toogleAccount(accountId, active)

    def retrieveAccountByEmail(email: String): Try[Option[AccountDataV2]] = integration.retrieveAccountByEmail(email)
    def retrieveAccount(accountId: String): Try[Option[AccountDataV2]] = integration.retrieveAccount(accountId)

    def updateAccount(contract: AccountContractV2): Try[Unit] = integration.updateAccount(contract)

    def resetPassword(email: String, password: String): Try[Unit] = integration.resetPassword(email, password)

    def changePassword(contract: ChangePasswordContractV2): Try[Unit] = integration.changePassword(contract)

    def search(contract: AccountSearchContract) = integration.search(contract)

    def searchBy(searchString: String, indexName: String): Try[List[AccountDataV2]] = integration.searchBy(searchString, indexName)

    def searchByEmail(searchString: String): Try[List[AccountDataV2]] = integration.searchByEmail(searchString)

    def searchByAccountId(searchString: String): Try[List[AccountDataV2]] = integration.searchByAccountId(searchString)
    def retrieveAccountMedia(contract: AccountMediaContractV2): Option[AccountMediaDataV2] = integration.retrieveAccountMedia(contract)

    def addAccountMedia(contract: AccountMediaDataV2) = integration.addAccountMedia(contract)

    def deleteAccountMedia(contract: AccountMediaContractV2) = integration.deleteAccountMedia(contract)

    def updateAccountMedia(contract: AccountMediaDataV2) = integration.updateAccountMedia(contract)

    def listAccountMedia(accountId: String): List[AccountMediaDataV2] = integration.listAccountMedia(accountId)
  }
}