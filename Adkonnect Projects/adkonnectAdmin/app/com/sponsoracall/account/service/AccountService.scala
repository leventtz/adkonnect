package com.sponsoracall.account.service

import scala.util.Try

import com.sponsoracall.account.contract.AccountContractV2
import com.sponsoracall.account.contract.AccountMediaContractV2
import com.sponsoracall.account.contract.AccountSearchContract
import com.sponsoracall.account.contract.ChangePasswordContractV2
import com.sponsoracall.account.data.AccountDataV2
import com.sponsoracall.account.data.AccountMediaDataV2

trait AccountService {

  def addMobileUserAccount(contract:AccountContractV2): Try[Option[String]] 
  
  def deleteAccountAll(email: String) 
  def deleteAccount(accountId: String) 
  def addEmail(email: String, accountId: String): Try[Unit] 
  def deleteEmail(email: String): Try[Unit] 

  def updateEmail(oldEmail: String, newEmail: String, accountId: String): Try[Unit] 

  def getAccountId(email: String): Try[Option[String]] 

  def createAccount(account: AccountContractV2): Try[String]

  def toogleAccount(accountId: String, active: String) 

  def retrieveAccountByEmail(email: String): Try[Option[AccountDataV2]]
  def retrieveAccount(accountId: String): Try[Option[AccountDataV2]] 
 
  def updateAccount(contract: AccountContractV2): Try[Unit] 

  def resetPassword(email: String, password: String): Try[Unit] 

  def changePassword(contract: ChangePasswordContractV2): Try[Unit] 

  def search(contract: AccountSearchContract)

  def searchBy(searchString: String, indexName: String): Try[List[AccountDataV2]] 

  def searchByEmail(searchString: String): Try[List[AccountDataV2]]

  def searchByAccountId(searchString: String): Try[List[AccountDataV2]] 
  def retrieveAccountMedia(contract: AccountMediaContractV2): Option[AccountMediaDataV2] 

  def addAccountMedia(contract: AccountMediaDataV2) 

  def deleteAccountMedia(contract: AccountMediaContractV2)

  def updateAccountMedia(contract: AccountMediaDataV2)

  def listAccountMedia(accountId: String): List[AccountMediaDataV2] 
}
