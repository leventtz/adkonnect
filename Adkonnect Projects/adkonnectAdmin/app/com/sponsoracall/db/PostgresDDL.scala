package com.sponsoracall.db
import scala.slick.jdbc.{ StaticQuery => Q }
import org.slf4j.LoggerFactory
import scala.slick.jdbc.{ StaticQuery => Q }
object PostgresDDL {
  val log = LoggerFactory.getLogger(getClass())
  val root = "C:/Dev/git/sponsoracall/PostgreSQLDDL/"
  val postFix = ".sql";
  
  def main(string: Array[String]) {
    runDDL
    runScript("insertcountries");
    runScript("insertdestinations");
    runScript("insertdialingcodes");
    runScript("insertdefaultaccounts");
    runScript("insertrates");
    runScript("insertotherobjects");
  }


  def runScript(fileName: String) {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val source = scala.io.Source.fromFile(root + fileName + postFix)
          source.getLines.filterNot(p=>p.isEmpty()).foreach(f=>Q.updateNA(f.mkString).execute)
          
          source.close
        } catch {
          case t: Throwable =>
            t.printStackTrace();
            log.info(t + " " + fileName)
            System.exit(-1)
        }
    }
  }

  def runDDL() {
    PostgresDBEngine.database.withSession {
      implicit session =>
        val files = List("Country", "Destination","DialingCode","Account", "AccountCreditCard", "AccountPhoneContacts", "AccountMedia","Ad", "AdIncoming", "AdDA", "Rate", "CDR",
           "Gateway","DIDTable","ProviderGateway","cdrsummary","Tooluser")

        files.foreach(fileName => {
          val source = scala.io.Source.fromFile(root + fileName + postFix)
          try {

            val dropTableString = "drop table " + fileName + " CASCADE "
            log.info(dropTableString)
            Q.updateNA(dropTableString).execute
            source.close()
          } catch {
            case t: Throwable => {
              log.error(t.getMessage(), t)
            }
          }
        })
        
        files.foreach(fileName => {        
          try {
            val source = scala.io.Source.fromFile(root + fileName + postFix)
            Q.updateNA(source.mkString).execute
            log.info(source.mkString)
            source.close()
          } catch {
            case t: Throwable =>
              log.info(t + " " + fileName)
              System.exit(-1)
          }
          
        })
    }
  }
}