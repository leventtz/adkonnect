package com.sponsoracall.cdr.data

import com.sponsoracall.ad.data.PickAdData




object CDRAccountData {
  import play.api.libs.json._
  val jsonReader = Json.reads[CDRAccountData]
  def parse(value: JsValue): Option[CDRAccountData] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class CDRAccountData(accountId:String,timeStamp:Long,data:PickAdData) {
    import play.api.libs.json._
  val jsonWriter = Json.writes[CDRAccountData]
  override def toString = {
    jsonWriter.writes(this).toString
  }
}