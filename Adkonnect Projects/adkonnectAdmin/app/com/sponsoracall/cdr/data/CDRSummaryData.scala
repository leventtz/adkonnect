package com.sponsoracall.cdr.data

import scala.collection.mutable.MutableList


case class CDRSummaryData(metaData:List[String],values:List[List[String]]) {
  import play.api.libs.json._
  val jsonWriter=Json.writes[CDRSummaryData]
  override def toString={
    jsonWriter.writes(this).toString
  } 
} 