package com.sponsoracall.cdr.data


object LocationData {
  
  import play.api.libs.json._
  val jsonReader = Json.reads[LocationData]
  def parse(value: String): LocationData = {
      jsonReader.reads(Json.parse(value)).get
  }
}

case class LocationData(id:Int,parentId:Int,locationName:String) {
    import play.api.libs.json._
  	val jsonWriter=Json.writes[LocationData]  
    override def toString={
    jsonWriter.writes(this).toString
  }   
}


