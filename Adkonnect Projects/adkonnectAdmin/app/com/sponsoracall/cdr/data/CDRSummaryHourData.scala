package com.sponsoracall.cdr.data

import scala.collection.mutable.MutableList



case class CDRSummaryByHourData(calldatehour:Int,calls:Int,ccalls:Int,hit:Int,duration:BigDecimal,maxDuration:Int,adTotal:BigDecimal,providerTotal:BigDecimal) {
  import play.api.libs.json._
  val jsonWriter=Json.writes[CDRSummaryByHourData]
  override def toString={
    jsonWriter.writes(this).toString
  } 
} 

