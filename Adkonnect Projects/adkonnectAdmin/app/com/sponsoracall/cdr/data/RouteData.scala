package com.sponsoracall.cdr.data

case class RouteData(providerGatewayId:Int,destinationId:Int,active:Boolean) {
    import play.api.libs.json._
  	val jsonWriter=Json.writes[RouteData]  
    override def toString={
    jsonWriter.writes(this).toString
  }   
}