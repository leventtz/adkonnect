package com.sponsoracall.cdr.data

case class ProviderGatewayData(id:Int,endpointip:String,providerid:String,prefix:String,username:String,password:String,active:Boolean)  {
     require(endpointip!=null && endpointip.length()>0, "Missing endpointIP")
     require(providerid!=null && providerid.length()>0, "Missing providerId")
  
    import play.api.libs.json._
  	val jsonWriter=Json.writes[ProviderGatewayData]  
    override def toString={
    jsonWriter.writes(this).toString
  }  
}