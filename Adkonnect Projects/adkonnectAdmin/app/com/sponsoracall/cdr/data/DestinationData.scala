package com.sponsoracall.cdr.data

case class DestinationData(id:Int,name:String,countryId:Int,incomingDestination:Boolean) {
  require(id>0,"Missing Id")
  require(countryId>0,"Missing countryId")
  require(name!=null && name.length>0,"Missing Name");
  
  import play.api.libs.json._
  val jsonWriter=Json.writes[DestinationData]
  override def toString={
    jsonWriter.writes(this).toString
  }    
}