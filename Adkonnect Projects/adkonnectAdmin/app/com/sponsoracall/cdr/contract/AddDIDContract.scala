package com.sponsoracall.cdr.contract


object AddDIDContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[AddDIDContract]
  def parse(value: JsValue): Option[AddDIDContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class AddDIDContract(DID:String,gatewayId:Int,providerId:String) {
  require(DID!=null && DID.length()!=0,"DID is required")
}