package com.sponsoracall.cdr.contract

case class UpdateCDRContract(accountId:String,cdrId:String,billDuration:Int,callDuration:Int,reason:String)