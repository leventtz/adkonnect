package com.sponsoracall.cdr.contract


object UpdateProviderGatewayContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[UpdateProviderGatewayContract]
  def parse(value: JsValue): Option[UpdateProviderGatewayContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}


case class UpdateProviderGatewayContract(id:Int,endpointip:String,providerid:String,prefix:String,username:String,password:String,active:Boolean) {
  require(endpointip!=null && endpointip.length()>0,"endpointip required")
  require(providerid!=null && providerid.length()>0,"providerid required")
}