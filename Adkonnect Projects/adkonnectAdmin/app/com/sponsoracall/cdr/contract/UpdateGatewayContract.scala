package com.sponsoracall.cdr.contract

object UpdateGatewayContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[UpdateGatewayContract]
  def parse(value: JsValue): Option[UpdateGatewayContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class UpdateGatewayContract(id:Int,ip:String,did:String,providerId:String,name:String,active:Boolean) {
  require(ip!=null && ip.length()>0,"IP required")
  require(did!=null && did.length()>0,"DID required")
  require(name!=null && name.length()>0,"name required")
  require(providerId!=null && providerId.length()>0,"provider required")
}