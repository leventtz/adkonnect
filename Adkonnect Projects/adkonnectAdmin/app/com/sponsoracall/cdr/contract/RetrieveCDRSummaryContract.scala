package com.sponsoracall.cdr.contract

import java.sql.Date

case class CDRFilter(startDate:Option[Int],endDate:Option[Int],adId:Option[Int],providerId:Option[String],country:Option[Int],inDest:Option[Int],outDest:Option[Int],accountId:Option[String]) {
	def this() = this(None,None,None,None,None,None,None,None)
}
case class CDRSummaryContract(reportType:String,filter:CDRFilter)



