package com.sponsoracall.cdr.contract

object RetrieveDialingCodeByDestinationContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[RetrieveDialingCodeByDestinationContract]
  def parse(value: JsValue): Option[RetrieveDialingCodeByDestinationContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class RetrieveDialingCodeByDestinationContract(destinationId:Int)