package com.sponsoracall.cdr.contract

object CreateDestinationContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[CreateDestinationContract]
  def parse(value: JsValue): Option[CreateDestinationContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class CreateDestinationContract(name:String,countryId:Int) {
  require(name!=null && name.length>0,"Country Name Required")
}