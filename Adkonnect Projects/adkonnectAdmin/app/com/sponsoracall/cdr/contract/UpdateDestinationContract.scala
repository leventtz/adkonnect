package com.sponsoracall.cdr.contract

object UpdateDestinationContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[UpdateDestinationContract]
  def parse(value: JsValue): Option[UpdateDestinationContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class UpdateDestinationContract(id:Int,name:String) {
  require(name!=null && name.length>0,"Country Name Required")
}