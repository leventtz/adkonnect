package com.sponsoracall.cdr.contract

import com.sponsoracall.ad.data.PickAdData

case class CreateCDRContract(accountId:String,cdrId:String,data:PickAdData) 