package com.sponsoracall.exception

class EmailExistException(email:String) extends Exception(email)
class PhoneExistException(phone:String) extends Exception(phone)
class WrongPasswordException extends Exception
class UnknownAccount extends Exception
class NotEnoughFund extends Exception