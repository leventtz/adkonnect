package com.sponsoracall.util

object AdUtil {
  val countryExitCodes = Map("1" -> "011", "61" -> "0011", "592" -> "001", "852" -> "001", "81" -> "010", "692" -> "011", "691" -> "011", "976" -> "001", "234" -> "009", "680" -> "011", "65" -> "001", "82" -> "001", "886" -> "002", "255" -> "000", "66" -> "001", "256" -> "000")

  def stripInternationalExitCode(dial: String): String = {
    val normalizedPhone = dial.replaceAll("\\+", "");
    if (normalizedPhone.startsWith("011")) {
      normalizedPhone.replaceFirst("011", "");
    } else if (normalizedPhone.startsWith("0011")) {
      normalizedPhone.replaceFirst("0011", "");
    } else if (normalizedPhone.startsWith("00")) {
      normalizedPhone.replaceFirst("00", "");
    } else if (normalizedPhone.startsWith("000")) {
      normalizedPhone.replaceFirst("000", "");
    } else if (normalizedPhone.startsWith("001")) {
      normalizedPhone.replaceFirst("001", "");
    } else if (normalizedPhone.startsWith("010")) {
      normalizedPhone.replaceFirst("010", "");
    } else {
      normalizedPhone
    }
  }

  def isInternational(dial: String): Boolean = {
    dial != stripInternationalExitCode(dial)
  }

  def isDomestic(dial: String): Boolean = {
    dial == stripInternationalExitCode(dial)
  }

  def isUSCall(dial: String): Boolean = {
    if (isDomestic(dial) && dial.startsWith("0")) {
      false;
    } else {
      true;
    }
  }

  def normalizeDialForAd(dial: String, callerCountryCode: String): String = {
    if (isInternational(dial)) {
      return stripInternationalExitCode(dial)
    } else if (callerCountryCode == "1" && dial.startsWith("1")) {
      return dial
    } else if (callerCountryCode == "1" && !dial.startsWith("1")) {
      return "1" + dial
    } else if (callerCountryCode != "1" && dial.startsWith("0")) {
      return callerCountryCode + dial.replaceFirst("0", "");
    } else {
      return callerCountryCode + dial
    }
  }

  def normalizeDialForDial(dial: String, callerCountryCode: String): String = {
    if (dial.startsWith("+")) {
      return countryExitCodes.get(callerCountryCode) match {
        case None => "00" + dial.replaceFirst("\\+", "")
        case Some(exitCode) => exitCode + dial.replaceFirst("\\+", "")
      }
    } else if (!isInternational(dial)) {
      if (callerCountryCode == "1" && !dial.startsWith("1")) {
        return "1" + dial;
      } else if (callerCountryCode != "1" && !dial.startsWith("0")) {
        return "0" + dial
      } else {
        return dial
      }

    } else {
      return dial
    }
  }
}