package com.sponsoracall.util

import com.sponsoracall.ad.data.ImpressionData

object GlobalSettings {
	val redisEndPoint=System.getProperty("redisEndPoint","")
	val callExpire=System.getProperty("CallExpire","60").toInt //default 30 seconds
	val cdrExpire=System.getProperty("CDRExpire","3600").toInt //default 1 hour
	val forgotPasswordExpire=System.getProperty("CDRExpire","18000").toInt //default 5 hour
	val awsAccessKey=System.getProperty("AWSAccessKey","AKIAJPUHRZLVFVIP43OQ")
	val awsSecretKey=System.getProperty("AWSSecretKey","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")
	val dynamoDBEndPoint=System.getProperty("dynamoDBEndPoint","https://dynamodb.us-east-1.amazonaws.com/")
	val userAgent="spcWEB"
	
	  
	  
	  
	val forgotPasswordEmailSubject=System.getProperty("ForgotPasswordEmailSubject","Reset Password")
    
	val emailBody = "<br>Dear User, Please go to the following URL to reset your password:"	 
    
	val forgotPasswordEmailBody=System.getProperty("ForgotPasswordEmailEmailBody",emailBody)
	val forgotPasswordFrom=System.getProperty("ForgotPasswordFrom","levent.tz@gmail.com")  
	val noReplyAddress=System.getProperty("NoReplyAddress","noReply@sponsoracall.com")
	val resetPasswordLink=System.getProperty("ResetPasswordLink","https://www.dimwire.com/resetpasswordL/")
	
	
	val totalDailyMinutesAllowed:Int=System.getProperty("TotalDailyMinutesAllowed",50.toString).toInt //50 minutes default
	val totalMonthlyMinutesAllowed:Int=System.getProperty("TotalMonthlyMinutesAllowed",1500.toString).toInt //1000 minutes default
	val minutesBetweenSameCalls:Int=System.getProperty("MinutesBetweenSameCalls",(1).toString()).toInt //1 minute
	val minutesBetweenSameAd:Int=System.getProperty("MinutesBetweenSameAd",(2*24*60).toString()).toInt //2 days 
	val averageCallDuration:Int=System.getProperty("AverageCallDuration",10.toString()).toInt //10 Minutes
	val numberOfItemsLimit:Int=System.getProperty("NumberOfItemsLimit",5.toString()).toInt //5 items per locationtype (zip,city,state,country,etc)

	//Test Server
	//val x_login_id:String=System.getProperty("x_login_id","4tzu2EP784")
	//val x_tran_key:String=System.getProperty("x_tran_key","7n55xwS2Kas3J9cM")
	//val ccServer="https://test.authorize.net/gateway/transact.dll"

	//Production
	val x_login_id:String=System.getProperty("x_login_id","8tngD2BH6M")
	val x_tran_key:String=System.getProperty("x_tran_key","792r96v4QS9FmzWR")
	val ccServer:String="https://secure.authorize.net/gateway/transact.dll"

}