/* jshint ignore:start */



angular.module('spcAdminDestinationControllers', [])
    .controller('DestinationCtrl', ['$scope', 'SpcService', 'BuildServiceCallJson', '$modal',
        function ($scope, $spcService, $buildServiceCallJson, $modal) {

            $scope.countryChanged = function () {
                var contractValues = {}
                contractValues.countryId = $scope.selectedCountry.id;
                $spcService.save({serviceName: 'RetrieveDestinations'}, $buildServiceCallJson('RetrieveDestinations', contractValues), function (data) {
                    $scope.destinations = new Array();
                    for (var i = 0; i < data.list.length; i++) {
                        $scope.destinations.push(angular.fromJson(data.list[i]))
                    }
                });

                $scope.dialingCodes = new Array();
                $scope.rates = new Array();
                $scope.selectedDestination=null;
            }
            
            $scope.destinationChanged = function () {
                $scope.dialingCodes = new Array();
            	var contractValues = {}
                contractValues.destinationId = $scope.selectedDestination.id;
                $spcService.save({serviceName: 'RetrieveDialingCodes'}, $buildServiceCallJson('RetrieveDialingCodes', contractValues), function (data) {
                    for (var i = 0; i < data.list.length; i++) {
                        $scope.dialingCodes.push(angular.fromJson(data.list[i]))
                    }
                });
                $scope.retrieveRates();
                $scope.retrieveRoutes();
            }

            $scope.retrieveRates=function() {
            	if($scope.selectedDestination===undefined) {
            		return;
            	}
                $scope.rates = new Array();
                var contractValues = {}
                contractValues.destinationId = $scope.selectedDestination.id;
                $spcService.save({serviceName: 'RetrieveRatesByDestination'}, $buildServiceCallJson('RetrieveRatesByDestination', contractValues), function (data) {
                    for (var i = 0; i < data.list.length; i++) {
                        $scope.rates.push(angular.fromJson(data.list[i]))
                    }
                });            	
            }
            

            
            
            $spcService.save({serviceName: 'RetrieveCountries'}, $buildServiceCallJson('RetrieveCountries', {}), function (data) {
                $scope.countries = new Array();
                for (var i = 0; i < data.list.length; i++) {
                    $scope.countries.push(angular.fromJson(data.list[i]))
                }
            });

            $scope.deleteDialingCode = function (da, index) {
                var modalInstance = $modal.open({
                    templateUrl: 'partials/deleteConfirmationDialog',
                    controller: 'OkCancelCtrl'
                });

                modalInstance.result.then(function () {
                    var contractValues = {}
                    contractValues.da = da;
                    $spcService.save({
                        serviceName: 'DeleteDialingCode'
                    }, $buildServiceCallJson('DeleteDialingCode', contractValues), function (data) {
                        $scope.dialingCodes.splice(index, 1);
                    });
                }, function () {
                    //cancelled
                });
            };


            
            $scope.createDialingCode = function () {
                var modalInstance = $modal.open({
                    templateUrl: 'partials/newDialingCodeDialog',
                    controller: 'NewDialingCodeCtrl'
                });

                modalInstance.result.then(function (da) {
    				  var contractValues={}
    				  contractValues.da=da;
    				  contractValues.destinationId=$scope.selectedDestination.id;
    					  $spcService.save({serviceName:'CreateDialingCode'},$buildServiceCallJson('CreateDialingCode',contractValues),function(data) {
    						  $scope.dialingCodes.splice(0,0,{destinationId:$scope.selectedDestinationid,da:da});
    			        	});			  
                }, function () {
                    //cancelled
                });



            };
            
            
            
            $scope.retrieveRoutes=function() {
            	if($scope.selectedDestination===undefined) {
            		return;
            	}
                $scope.routes = new Array();
                var contractValues = {}
                contractValues.destinationId = $scope.selectedDestination.id;
            	$scope.providers = new Array();
                
                $spcService.save({serviceName: 'ListRoutes'}, $buildServiceCallJson('ListRoutes', contractValues), function (data) {
                    for (var i = 0; i < data.list.length; i++) {
                        $scope.routes.push(angular.fromJson(data.list[i]))
                    }
                    $scope.providergateways=new Array();
                	$spcService.save({	serviceName: 'ListProviderGateways'}, $buildServiceCallJson('ListProviderGateways', {}), function (data) {
                		for (var i = 0; i < data.list.length; i++) {
                			var providergateway=angular.fromJson(data.list[i]);
                			$scope.providergateways.push(providergateway)
                		}
                    	for(var i=0;i<$scope.routes.length;i++) {
                    		for(var j=0;j<$scope.providergateways.length;j++) {
                    			if($scope.routes[i].providerGatewayId==$scope.providergateways[j].id) {
                    				$scope.routes[i].providerGatewayIP=$scope.providergateways[j].endpointip+" Active:"+$scope.providergateways[j].active;
                    			}
                    		}
                    	}                		
                	});

                    
                });            	
            };           
            
            $scope.addRoute = function () {
                var modalInstance = $modal.open({
                    templateUrl: 'partials/addRouteDialog',
                    controller: 'AddRouteCtrl'
                });

                modalInstance.result.then(function (data) {
    				  var contractValues={}
    				  contractValues.providerGatewayId=data.providergateway.id;
    				  contractValues.destinationId=$scope.selectedDestination.id;
    					  $spcService.save({serviceName:'AddRoute'},$buildServiceCallJson('AddRoute',contractValues),function(data) {
    						  $scope.retrieveRoutes();
    			        	});			  
                }, function () {
                    //cancelled
                });



            };            
            
            
            $scope.toggleRoute=function(route) {
                var modalInstance = $modal.open({
                    templateUrl: 'partials/confirmationDialog',
                    controller: 'OkCancelCtrl'
                });

                modalInstance.result.then(function () {
                    var contractValues = {}
                    contractValues.providerGatewayId = route.providerGatewayId;
                    contractValues.destinationId = route.destinationId;
                    $spcService.save({serviceName: 'ToggleRoute'}, $buildServiceCallJson('ToggleRoute', contractValues), function (data) {
                    	$scope.retrieveRoutes();
                    });
                }, function () {
                    //cancelled
                });              	
            } ;           
            
            
            $scope.deleteRoute=function(route) {
                var modalInstance = $modal.open({
                    templateUrl: 'partials/deleteConfirmationDialog',
                    controller: 'OkCancelCtrl'
                });

                modalInstance.result.then(function () {
                    var contractValues = {}
                    contractValues.providerGatewayId = route.providerGatewayId;
                    contractValues.destinationId = route.destinationId;
                    $spcService.save({serviceName: 'DeleteRoute'}, $buildServiceCallJson('DeleteRoute', contractValues), function (data) {
                    	$scope.retrieveRoutes();
                    });
                }, function () {
                    //cancelled
                });            	
            } ;             
            
            
            $scope.deleteRate = function (id, index) {
                var modalInstance = $modal.open({
                    templateUrl: 'partials/deleteConfirmationDialog',
                    controller: 'OkCancelCtrl'
                });

                modalInstance.result.then(function () {
                    var contractValues = {}
                    contractValues.id = id;
                    $spcService.save({serviceName: 'DeleteRate'}, $buildServiceCallJson('DeleteRate', contractValues), function (data) {
                        $scope.rates.splice(index, 1);
                    });
                }, function () {
                    //cancelled
                });
            };            
            
            
           
            $scope.addRate = function () {
                var modalInstance = $modal.open({
                    templateUrl: 'partials/newRateDialog',
                    controller: 'NewRateCtrl'
                });
                modalInstance.result.then(function (addRate) {
    				  var contractValues={}
    				  contractValues.accountId=addRate.account.id;
    				  contractValues.destinationId=$scope.selectedDestination.id;
    				  contractValues.rate=addRate.rate;
    				  contractValues.rateTypeId=addRate.rateTypeId;
    					  $spcService.save({serviceName:'AddRate'},$buildServiceCallJson('AddRate',contractValues),function(data) {
    						  $scope.retrieveRates();
    			        	});			  
                }, function () {
                	retrieveRates();
                });
            };            
            

        }
    ])

.controller('OkCancelCtrl', ['$modalInstance', '$scope',
    function ($modalInstance, $scope) {
        $scope.ok = function () {
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
])

.controller('NewDialingCodeCtrl', ['$modalInstance', '$scope',
    function ($modalInstance, $scope) {
		$scope.da={data:''};
        $scope.ok = function () {
            $modalInstance.close($scope.da.data);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
])

.controller('NewRateCtrl', ['$modalInstance', '$scope','SpcService', 'BuildServiceCallJson',
    function ($modalInstance, $scope,$spcService, $buildServiceCallJson) {
	    $scope.addRate={};
    	$scope.providers = new Array();
    	$spcService.save({	serviceName: 'ListActiveProviders'}, $buildServiceCallJson('ListActiveProviders', {}), function (data) {
    		for (var i = 0; i < data.list.length; i++) {
    			$scope.providers.push(angular.fromJson(data.list[i]))
    		}
    	});	
		$scope.addData={data:''};
        $scope.ok = function () {
            $modalInstance.close($scope.addRate);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
])

.controller('AddRouteCtrl', ['$modalInstance', '$scope','SpcService', 'BuildServiceCallJson',
    function ($modalInstance, $scope,$spcService, $buildServiceCallJson) {
	$scope.providergateways=new Array();
	$spcService.save({serviceName: 'ListProviderGateways'}, $buildServiceCallJson('ListProviderGateways', {}), function (data) {
		for (var i = 0; i < data.list.length; i++) {
			var providergateway=angular.fromJson(data.list[i]);
			$scope.providergateways.push(providergateway)
		}
	});
	
		$scope.data={};
        $scope.ok = function () {
            $modalInstance.close($scope.data);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

]);
/* jshint ignore:end */