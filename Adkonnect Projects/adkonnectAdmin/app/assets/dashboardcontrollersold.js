/* jshint ignore:start */



angular.module('spcAdminDashboardController', [])
.controller('DashboardCtrl', ['$scope', 'SpcService', 'BuildServiceCallJson', '$modal',
    function ($scope, $spcService, $buildServiceCallJson, $modal) {
	$( "#startDate" ).datepicker(); 
	$( "#endDate" ).datepicker();

    $scope.startDate = "";
    $scope.endDate = "";
	$scope.selectedCountry={id:0,name:''};
	$scope.selectedDestination={id:0,name:''};
	$scope.selectedProvider={id:"",companyName:''};
	
    $spcService.save({serviceName: 'RetrieveCountries'}, $buildServiceCallJson('RetrieveCountries', {}), function (data) {
        $scope.countries = new Array();
        $scope.countries.push($scope.selectedCountry);
        for (var i = 0; i < data.list.length; i++) {
            $scope.countries.push(angular.fromJson(data.list[i]))
        }
    });
    
    
    $scope.countryChanged = function () {
    	
    	$scope.destinations = new Array();
    	$scope.selectedDestination={id:0,name:''};
    	if($scope.selectedCountry.id==0) {
    		return;
    	}
        var contractValues = {}
        contractValues.countryId = $scope.selectedCountry.id;
        $spcService.save({serviceName: 'RetrieveDestinations'}, $buildServiceCallJson('RetrieveDestinations', contractValues), function (data) {
            for (var i = 0; i < data.list.length; i++) {
                $scope.destinations.push(angular.fromJson(data.list[i]))
            }
        });
        
    }    
	
	$spcService.save({	serviceName: 'ListActiveProviders'}, $buildServiceCallJson('ListActiveProviders', {}), function (data) {
		$scope.providers = new Array();
		$scope.providers.push($scope.selectedProvider);
		for (var i = 0; i < data.list.length; i++) {
			$scope.providers.push(angular.fromJson(data.list[i]))
		}
	});	

	



	
	$scope.retrieveCDRSummary=function(reportName) {
        var contractValues = {}
        contractValues.startDate=$scope.startDate;
        contractValues.endDate=$scope.endDate;
        contractValues.countryId=$scope.selectedCountry.id;
        contractValues.destinationId=$scope.selectedDestination.id;
        contractValues.providerId=$scope.selectedProvider.id;
        contractValues.countryId = $scope.selectedCountry.id;
        contractValues.reportName=reportName;
        $scope.hitCount=0;
        $scope.callCount=0
        $scope.completedCallCount=0
        $scope.actualDurationTotal=0
        $scope.billedDurationTotal=0
        $scope.buyTotal=0
        $scope.sellTotal=0
        
        var offSet = new Date().getTimezoneOffset()*1000*60;
        $spcService.save({	serviceName: 'CDRSummary'}, $buildServiceCallJson('CDRSummary', contractValues), function (data) {
			$scope.summaryData=new Array();
			for(var i=0;i<data.summaryData.length;i++) {
				$scope.summaryData.push(angular.fromJson(data.summaryData[i]))
			}
			
			for(var i=0;i<$scope.summaryData.length;i++) {
				$scope.summaryData[i].calldatehour=$scope.summaryData[i].calldatehour*1000*60*60
		        $scope.hitCount=$scope.hitCount+$scope.summaryData[i].hit
		        $scope.callCount=$scope.callCount+$scope.summaryData[i].calls
		        $scope.completedCallCount=$scope.completedCallCount+$scope.summaryData[i].ccalls
		        $scope.actualDurationTotal=$scope.actualDurationTotal+$scope.summaryData[i].duration
		        $scope.billedDurationTotal=$scope.billedDurationTotal+$scope.summaryData[i].maxDuration
		        $scope.buyTotal=$scope.buyTotal+$scope.summaryData[i].providerTotal
		        $scope.sellTotal=$scope.sellTotal+$scope.summaryData[i].adTotal
			}
			
		});	
	}
	
	$scope.retrieveCDRSummary("date");
	
    }
	
]);

/* jshint ignore:end */
