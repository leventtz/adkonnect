/* jshint ignore:start */


// Declare app level module which depends on filters, and services
angular.module('spcAdminApp', [
  'ui.bootstrap',
  'ngRoute',
  'spcAdminServices',
  'spcAdminGatewayControllers',
  'spcAdminDestinationControllers',
  'spcAdminTemplateControllers',
  'spcAdminAccountControllers',
  'spcAdminDashboardController'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/templates', {templateUrl: 'partials/templates', controller: 'TemplateCtrl'});
  $routeProvider.when('/gateway', {templateUrl: 'partials/gateway', controller: 'GatewayCtrl'});
  $routeProvider.when('/destinations', {templateUrl: 'partials/destinations', controller: 'DestinationCtrl'});
  $routeProvider.when('/dashboard', {templateUrl: 'partials/dashboard', controller: 'DashboardCtrl'});
  $routeProvider.when('/accounts', {templateUrl: 'partials/accounts', controller: 'AccountsCtrl'});
  $routeProvider.otherwise({redirectTo: '/dashboard'});
}]);

/* jshint ignore:end */