package model



object AdWebData {
  import play.api.libs.json._
  val jsonReader = Json.reads[AdWebData]
  def parse(jsonString: String): JsResult[AdWebData] = {
    try {
      jsonReader.reads(Json.parse(jsonString))
    } catch {
      case t:Throwable => {t.printStackTrace();JsError("SPCWeb ParseError->"+jsonString)}
    }
  }
}
case class AdWebData(description:String,mediaLocation:String) {
  import play.api.libs.json._
  val jsonWriter=Json.writes[AdWebData]
  override def toString={
    jsonWriter.writes(this).toString
  }  
}