package model



object DeviceAdData {
  import play.api.libs.json._
  val jsonReader = Json.reads[DeviceAdData]
  def parse(jsonString: String): JsResult[DeviceAdData] = {
    try {
      jsonReader.reads(Json.parse(jsonString))
    } catch {
      case t:Throwable => {t.printStackTrace();JsError("SPCWeb ParseError->"+jsonString)}
    }
  }
}
case class DeviceAdData(adId:String,name:String,description:String,mediaLocation:String) {
  import play.api.libs.json._
  val jsonWriter=Json.writes[DeviceAdData]
  override def toString={
    jsonWriter.writes(this).toString
  }  
}