package model

import play.api.Play

object SessionData {
  val SessionTimeoutDuration = Play.current.configuration.getString("SessionIdleTimeoutDurationInMinute").get.toLong * 60 * 1000
  import play.api.libs.json._
  val jsonReader = Json.reads[SessionData]
  def parse(value: JsValue): Option[SessionData] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => {
        None
      }
    }
  }
}

case class SessionData(cookieName: String, accountId: String) {
  require(cookieName != null, "Missing Cookie Name")
  require(accountId != null, "accountId")

  import play.api.libs.json._
  val jsonWriter = Json.writes[SessionData]
  override def toString = {
    jsonWriter.writes(this).toString
  }

}