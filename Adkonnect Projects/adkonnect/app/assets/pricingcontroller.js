/* jshint ignore:start */
var appControllers = angular.module('pricingcontroller', []);


appControllers.controller('PricingCtrl', ['$http','$scope',function ($http,$scope) {
	$scope.retrieveAdTemplate=function() {
		$scope.adTemplates=new Array();
		var searchString=$scope.adSearch==''?'all':$scope.adSearch;
		
		$http.get("adtemplates/"+searchString).success(
			function(data) {
				for(var i=0;i<data.adTemplates.length;i++){
					$scope.adTemplates[i]=angular.fromJson(data.adTemplates[i]);
				}
			}
		).error(
			
		)
	}
	$scope.retrieveAdTemplate();
	
}]);

/* jshint ignore:end */