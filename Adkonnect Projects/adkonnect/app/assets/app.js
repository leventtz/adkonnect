/* jshint ignore:start */
'use strict';

var spcApp = angular.module('spcApp', [
    'ui.bootstrap',
    'ngRoute',
    'spcAppServices',    
    'accountadcontroller',
    'accountControllers',
    'accountMediaControllers',
    'adwizardcontroller',
    'pricingcontroller'
]);



spcApp.factory('errorInterceptor', ['$q', '$location',
    function ($q, $location) {
        return {
            request: function (config) {
            	$('#mydiv').show();
                return config || $q.when(config);
            },
            requestError: function (request) {
            	$('#mydiv').show();
                return $q.reject(request);
            },
            response: function (response) {
            	$('#mydiv').hide();
                return response || $q.when(response);
            },
            responseError: function (response) {
            	$('#mydiv').hide();
                if (response && response.status === 401 && response.data=='login') {
                	$('#logingErrorModal').modal('show'); 
                } else if (response && response.status === 401 && response.data=='sessionexpired') {
                	$('#SessionExpiredModal').modal('show');
                	window.setTimeout(function(){window.open("logout","_self");}, 500);
                }
                return $q.reject(response);
            }
        };
    }
]);

spcApp.config(['$httpProvider',
    function ($httpProvider) {
        $httpProvider.interceptors.push('errorInterceptor');
    }
]);

spcApp.config(['$httpProvider',
    function ($httpProvider) {
        //initialize get if not there
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }
        //disable IE ajax request caching
        $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
    }
]);

spcApp.constant('validationRegex', {
    digitDialingCode: /^[0-9]{4,}$/,
    digitPhone: /^[0-9]{10,}$/,
    password: /^.*(?=.{6,})(?=.*[a-z])(?=.*[A-Z])(?=.*\d).*$/
});

/* jshint ignore:end */
