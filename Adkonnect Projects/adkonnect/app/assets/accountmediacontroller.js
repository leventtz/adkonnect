/* jshint ignore:start */
var appControllers = angular.module('accountMediaControllers', []);

appControllers.controller('AccountMediaCtrl', ['AccountMedia','Media','$scope', '$resource', '$http',function (accountMediaService,mediaService,$scope, $resource,$http) {

	$scope.posting = function () {
    	$scope.isPosting=true;
    	$('#postAnim_design1').show();
	};	
	
	
    $scope.refreshAccountMedia=function() {
    	$scope.isPosting=false;
    	$('#postAnim_design1').hide();
    	
    	$scope.mediaList=new Array();
   		accountMediaService.get({accountId:$scope.ACCOUNT_ID},function(jsonData) {
   				var i=0;
   				jsonData.media.forEach(function(entry) {
   				$scope.mediaList[i++]=angular.fromJson(entry);
   			});
   		})
    }
    
    
	$scope.deleteMedia=function(mediaId) {
		$scope.mediaWillBeDeletedId=mediaId;
	}

	$scope.deleteConfirmed=function() {
		$http({method:"DELETE",url:'/accountMedia/'+$scope.ACCOUNT_ID+'/media/'+$scope.mediaWillBeDeletedId}).success(function(){$scope.refreshAccountMedia();}).error(function(){$scope.refreshAccountMedia();})
		
	}
	
	$scope.mediaTabSelect = function() {
	    setTimeout(function() {
	    	$scope.refreshAccountMedia();
	    });
	  };	
	
	
	  
	  
}]);

/* jshint ignore:end */



