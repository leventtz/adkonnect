/* jshint ignore:start */

var appControllers = angular.module('accountadcontroller', []);

appControllers.controller('AccountAdCtrl', ['$scope', '$http','$routeParams',function ($scope,$http,$routeParams) {
	var offSet = new Date().getTimezoneOffset()*1000*60;
	$scope.selectedLocations=new Array();
	var AllUS={id:1,parentId:0,locationName:'United States of America'};
	$scope.selectedLocations.push(AllUS);
	
	$scope.accountAds=new Array();
	
	$scope.designData={};
	$scope.designData.mediaId="0";
	$scope.designData.impressionView="0";
	$scope.designData.header="";
	$scope.designData.adWords="";
	$scope.designData.websiteLink="";
	$scope.designData.websiteLinkPrompt="";
	$scope.designData.phoneLink="";
	$scope.designData.phoneLinkPrompt="";	
	
	
	$scope.retrieveAccountAd=function(adId) {
		if(adId=='Z') {
			$scope.accountAds=new Array();
		}
		$http.get("account/"+$scope.ACCOUNT_ID+"/ad/"+adId).success(function(data) {
			for(var i=0;i<data.accountAd.length;i++){
				var value=angular.fromJson(data.accountAd[i]);
				value.designData=angular.fromJson(value.designData)
				value.localtime=value.adDateTime-offSet;
				value.selectedLocations=angular.fromJson(value.incoming);
				
				angular.forEach(value.incomingName,function(item) {
					value.selectedLocations.push(angular.fromJson(item));
				});
				
				$scope.accountAds.push(value);
				
			}
		}).error(function(data,status){
			if(status!==404)
				alert(data)
		})
	}
	
	$scope.retrieveAccountAd('Z');
	
	$scope.retrieveOlder=function() {
		if($scope.accountAds.length!=0) {
			$scope.retrieveAccountAd($scope.accountAds[$scope.accountAds.length-1].adId);
		} else {
			$scope.retrieveAccountAd('Z');
		}
	}
	
	
	$scope.retrieveAccountTransactions=function(orderId) {
		if(orderId=='Z') {
			$scope.accountTransactions=new Array();
		}
	    var offSet = new Date().getTimezoneOffset()*1000*60;
		$http.get("account/"+$scope.ACCOUNT_ID+"/transaction/"+orderId).success(function(data) {
			for(var i=0;i<data.accountTransaction.length;i++){
				var value=angular.fromJson(data.accountTransaction[i]);
				value.locatime=value.date-offSet;
				if(value.accountDetail) {
					value.accountDetail=angular.fromJson(value.accountDetail);
					value.lineItems=angular.fromJson(angular.fromJson(value.detail).lineItems);
					value.totalItems=angular.fromJson(angular.fromJson(value.detail).totalItems);
				} else {
					value.accountDetail={};
					value.lineItems={};
					value.totalItems={};
				}
				
				$scope.accountTransactions.push(value);
			}
		}).error(function(data,status){
			if(status!==404)
				alert(data)
		})
	}
	
	
	$scope.transactionsTabSelect=function() {
		$scope.retrieveAccountTransactions('Z');	
	}
	
	
	
	$scope.retrieveOlderTransactions=function() {
		if($scope.accountTransactions.length!=0) {
			$scope.retrieveAccountTransactions($scope.accountTransactions[$scope.accountTransactions.length-1].orderId);
		} else {
			$scope.retrieveAccountTransactions('Z');
		}
	}
	
	
	
	$scope.toogleAdConfirmed=function(ad) {
		var toogleAd={};
		toogleAd.accountId=ad.accountId;
		toogleAd.adId=ad.adId;
		toogleAd.active=!ad.active;
		$http.post("togglead",toogleAd).success(function(data) {
			ad.active=!ad.active;
		}).error(function(data,status){
			if(status!==404)
				alert(data)
		})		
	}
	
	$scope.toogleAd=function(ad) {
		
		$scope.adWillBeToggled=ad;
	}

	$scope.actionConfirmed=function() {
		console.log($scope.adWillBeToggled);
		$scope.toogleAdConfirmed($scope.adWillBeToggled)
	}
	
	
	$scope.openInvoiceDetailDialog=function(transaction) {
		$scope.selectedTransaction=transaction;
		$('#invoiceDialog').modal('show');
		
	}		
		
	$scope.printInvoice=function() {
		  var divToPrint=document.getElementById('printArea');
		  newWin= window.open("");
		  newWin.document.write(divToPrint.outerHTML);
	}
	
	
	
	
	//REPEATED CODE AdWizard.js
	$scope.onLocationSelect=function(selectedLocation) {
		//allow only us
		if(selectedLocation.id==1) {
			$scope.selectedLocations=new Array();
			$scope.selectedLocations.push(selectedLocation);
			return;
		}

		//remove unique
		for(var i=0; i<$scope.selectedLocations.length;i++){
			if($scope.selectedLocations[i].id==selectedLocation.id) {
				return;
			}
		}
		
		
		$scope.selectedLocations.push(selectedLocation);
		//remove US
		for(var i=0; i<$scope.selectedLocations.length;i++){
			if($scope.selectedLocations[i].id==1) {
				$scope.selectedLocations.splice(i,1)
				return;
			}
		}
		
		
		//remove child
		var tempChild=new Array();
		for(var i=0; i<$scope.selectedLocations.length;i++){
			if($scope.selectedLocations[i].parentId!=selectedLocation.id) {
				tempChild.push($scope.selectedLocations[i]);
			}
		}
		
		var tempParent=new Array();
		for(var i=0; i<tempChild.length;i++){
			if(tempChild[i].id!=selectedLocation.parentId) {
				tempParent.push(tempChild[i])
			}
		}
		
		
		$scope.selectedLocations=tempParent;
	}
	
	$scope.deleteSelectedLocation=function(location) {
		var index=$scope.selectedLocations.indexOf(location);
     	if(index>=0) {
     		$scope.selectedLocations.splice(index,1);
     	}
     	if($scope.selectedLocations.length==0){
     		$scope.selectedLocations.push(AllUS)
     	}
	}
	
	$scope.getLocation = function(val) {
		    return $http.get('location/'+val).then(function(res){
		      var addresses = [];
		      angular.forEach(res.data.location, function(item){
		        addresses.push(angular.fromJson(item));
		      });
		      return addresses;
		    });
	};	
	//REPEATED CODE AdWizard.js	

	$scope.openIncomingDestinationsDialog=function(ad) {
		$scope.selectedAd=ad;
		$scope.selectedLocations=angular.copy(ad.selectedLocations);
		$('#updateIncomingDestinationsDialog').modal('show');
	}		
	
	$scope.openImpressionDialog=function(ad) {
		$scope.selectedAd=ad;
		$scope.updateImpression=angular.copy(ad.impression);
		$('#updateImpressionDialog').modal('show');
	}		

	$scope.updateAdImpression=function() {
		var ad=$scope.selectedAd;
		var adWillbeUpdated={};
		adWillbeUpdated.accountId=ad.accountId;
		adWillbeUpdated.adId=ad.adId;
		adWillbeUpdated.impression=$scope.updateImpression
		$http.post("updateimpression",angular.toJson(adWillbeUpdated)).success(function(data){
			$scope.selectedAd.impression=$scope.updateImpression;			
			$('#updateImpressionDialog').modal('hide');
		}).error(function(data){
			alert(data);
		})		
	}	

	$scope.openApplyPaymentDialog=function(ad) {
		$scope.paymentDialog=true
		$scope.fundAmount=15;
		$scope.selectedAd=ad;
		$('#applyPaymentDialog').modal('show');
	}	
	
	
	$scope.submitPayment=function() {
		var ad=$scope.selectedAd;
		var adWillbeUpdated={};
		adWillbeUpdated.accountId=ad.accountId;
		adWillbeUpdated.adId=ad.adId;
		adWillbeUpdated.fundAmount=$scope.fundAmount
		$scope.cc.expDate=$scope.cc.month+$scope.cc.year;
		$scope.cc.address=$scope.cc.address1;
		
		if(!$scope.cc.address2) {
			$scope.cc.address=$scope.cc.address+" "+$scope.cc.address2;
		}
		$scope.cc.description=$scope.name+" Sponsoracall.com ";
		var fund={};
		$scope.cc.amount=$scope.fundAmount;		
		fund.payment=$scope.cc;
     	fund.accountId=$scope.selectedAd.accountId;
		fund.adId=$scope.selectedAd.adId;
		
		$http.post("addfund",angular.toJson(fund)).success(function(data){
			$scope.selectedAd.budget=$scope.selectedAd.budget+$scope.fundAmount;
			$scope.selectedAds={};
			$scope.clearCC();
			$scope.cc.address1=undefined;
			$scope.cc.address2=undefined;
			$scope.cc.month=undefined;
			$scope.cc.year=undefined;
			$('#applyPaymentDialog').modal('hide');			
		}).error(function(data){
			$scope.saveError=data;
		})		
	}
	
	
	$scope.isPaymentSubmitable=function() {
		if($scope.cc.cardNum && 
				$scope.cc.cvv && 
				$scope.cc.month && 
				$scope.cc.year &&
				$scope.cc.firstName && 
				$scope.cc.lastName && 
				$scope.cc.address1 && 
				$scope.cc.city && 
				$scope.cc.state && 
				$scope.cc.zip && 
				$scope.cc.phone &&
				$scope.fundAmount)
			return true;
		else
			return false;
			
	}	
	
	
	
	$scope.openNameDialog=function(ad) {
		$scope.selectedAd=ad;
		$scope.updateName=angular.copy(ad.name);
		$('#updateNameDialog').modal('show');
	}		


	$scope.updateAdName=function() {
		var ad=$scope.selectedAd;
		var adWillbeUpdated={};
		adWillbeUpdated.accountId=ad.accountId;
		adWillbeUpdated.adId=ad.adId;
		adWillbeUpdated.name=$scope.updateName
		$http.post("updatename",angular.toJson(adWillbeUpdated)).success(function(data){
			$scope.selectedAd.name=$scope.updateName;			
			$('#updateNameDialog').modal('hide');
		}).error(function(data){
			alert(data);
		})		
	}
	
	
	
	$scope.submitIncomingChanges=function() {
		var ad=$scope.selectedAd;
		var adWillbeUpdated={};
		adWillbeUpdated.accountId=ad.accountId;
		adWillbeUpdated.adId=ad.adId;
		adWillbeUpdated.incoming=$scope.selectedLocations
		$http.post("updateIncoming",angular.toJson(adWillbeUpdated)).success(function(data){
			$scope.selectedAd.selectedLocations=angular.copy($scope.selectedLocations);
			$('#updateIncomingDestinationsDialog').modal('hide');
		}).error(function(data){
			alert(data);
		})		
	}	
	
	$scope.openUpdateDesignDialog=function(ad) {
		function retrieveAccountMedia() {
			$scope.accountMediaListIVR=new Array();
			$scope.accountMediaListMedia=new Array();
			$http.get("account/"+$scope.ACCOUNT_ID+"/media").success(function(data) {
				for(var i=0;i<data.media.length;i++){
					var value=angular.fromJson(data.media[i]);
					if(value.mediaTypeId=='prompt')
						$scope.accountMediaListIVR.push(value);
					else if(value.mediaTypeId=='picture')
						$scope.accountMediaListMedia.push(value);
				}
				$scope.selectedAd=angular.copy(ad);
				$scope.designData=$scope.selectedAd.designData;
				$('#updateDesignDialog').modal('show');
				
			}).error(function(data,status){
				if(status!==404)
					alert(data)
			})
		}
		retrieveAccountMedia();
	}	

	$scope.submitDesignChanges=function() {
		var ad={};
		ad.accountId=$scope.selectedAd.accountId;
		ad.adId=$scope.selectedAd.adId;
		ad.designData=$scope.selectedAd.designData;
		$http.post("updatedesign",angular.toJson(ad)).success(function(data){
			for(var i=0;i<$scope.accountAds.length;i++) {
				if($scope.accountAds[i].adId==ad.adId) {
					$scope.accountAds[i].designData=angular.copy(ad.designData);
					$scope.accountAds[i].approved=false;
					$scope.accountAds[i].active=false;
					break;
				}
			}
			
			$('#updateDesignDialog').modal('hide');
		}).error(function(data){
			alert("Missing fields!");
		})		
	}	
	
	
	setTimeout(function(){	
		if($scope.accountAds.length==0) {
			$('#newAd-modal-lg').modal('show');
		}
	}, 700);	
	
}]);

/* jshint ignore:end */


