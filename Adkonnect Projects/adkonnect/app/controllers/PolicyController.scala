package controllers

import play.api.mvc.Action
import play.api.mvc.Controller

object PolicyController extends Controller {
  def terms = Action {
    Ok(views.html.terms())
  }
  def dataUsePolicy = Action {
    Ok(views.html.datausepolicy())
  }
  def cookiePolicy = Action {
    Ok(views.html.cookiepolicy())
  }
}