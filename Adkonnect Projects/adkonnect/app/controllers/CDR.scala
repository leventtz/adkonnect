package controllers

import scala.util.Failure
import scala.util.Success
import play.api.i18n.Messages
import play.api.libs.json._
import play.api.mvc.Action
import play.api.mvc.Controller
import java.util.Date
import java.util.Calendar
import play.api.mvc.Request
import com.sponsoracall.cdr.service.CDRServiceRegistry
import com.sponsoracall.cdr.contract.RetrieveDialingCodeByDestinationContract
import com.sponsoracall.cdr.contract.RetrieveDestinationsByCountryContract
import com.sponsoracall.cdr.contract.CDRSummaryContract
import com.sponsoracall.cdr.contract.CDRFilter
import com.sponsoracall.cdr.data.LocationData

object CDR extends Controller {
  def searchLocation(searchString:String) = Action {
    CDRServiceRegistry.service.retrieveLocations(searchString) match {
      case Success(list:List[LocationData]) => Ok(Json.toJson(Map("location"->list.map(_.toString))));
      case _ => Ok("")
    }
  }
	
  def countryList() = Action {
    val result = CDRServiceRegistry.service.retrieveCountries
    result match {
      case Success(x :: xs) => Ok(Json.toJson(Map("countryList" -> result.get.map(_.toString))))
      case Success(List()) => NotFound
      case Failure(f) => NotFound
      case _ => NotFound
    }
  }

  def destinationList() = Action {
    request =>
      val result = CDRServiceRegistry.service.retriveDestinations
      result match {
        case Success(x :: xs) => Ok(Json.toJson(Map("destinationList" -> result.get.map(_.toString))))
        case Success(List()) => NotFound
        case Failure(f) => NotFound
        case _ => NotFound
      }
  }


  def retrieveDestinationByCountry(countryId: Int) = Action {
    request =>
      val result = CDRServiceRegistry.service.retriveDestinationsByCountry(RetrieveDestinationsByCountryContract(countryId));
      result match {
        case Success(x :: xs) => Ok(Json.toJson(Map("destinationList" -> result.get.map(_.toString))))
        case _ => NotFound
      }
  }


  def retrieveDialingCodeByDestination(destinationId: Int) = Action {
    request =>
      val result = CDRServiceRegistry.service.retrieveDialingCodeByDestination(RetrieveDialingCodeByDestinationContract(destinationId))
      result match {
        case Success(x :: xs) => Ok(Json.toJson(Map("dialingcodelist" -> result.get.map(_.toString))))
        case Success(List()) => NotFound
        case Failure(f) => NotFound
        case _ => NotFound
      }
  }

  
  def cdrSummary = Action {
    request =>

      val startDate = request.getQueryString("fromDate") match {
        case None => None
        case Some(dt) => {
          parseFromDate(dt)
        }
      }

      val endDate = request.getQueryString("toDate") match {
        case None => None
        case Some(dt) => {
          parseToDate(dt)
        }
      }

      val providerId = request.getQueryString("providerId") match {
        case None => None
        case Some("0") => None
        case v @ _ => v
      }

      val countryId = request.getQueryString("countryId") match {
        case None => None
        case Some("0") => None
        case Some(id) => Some(id.toInt)
      }

      val inDestinationId = parseIntValue(request.getQueryString("inDestinationId"))
      val destinationId = parseIntValue(request.getQueryString("destinationId"))
      val adId = parseIntValue(request.getQueryString("adId"))
      val switchId = parseIntValue(request.getQueryString("switchId"))

      val result = CDRServiceRegistry.service.retrieveCDRSummaryByHour(CDRSummaryContract("date", CDRFilter(startDate, endDate, adId, providerId, countryId, inDestinationId, destinationId, None)))
      result match {
        case Success(cdrSummaryList) => Ok(Json.toJson(Map("cdrsummary" -> cdrSummaryList.map(_.toString))))
        case Failure(t) => {
          BadRequest
        }
      }
  }

  def parseFromDate(s: String): Option[Int] = {
    parseDate(s)
  }

  def parseDate(s: String): Option[Int] = {
    try {
      val sp = s.replace("\"", "").split("-")
      val year = sp(0).toInt
      val month = sp(1).toInt
      val date = sp(2).substring(0, 2).toInt

      val c = Calendar.getInstance()
      c.clear()
      c.set(Calendar.YEAR, year)
      c.set(Calendar.MONTH, month - 1)
      c.set(Calendar.DAY_OF_MONTH, date)

      Some((c.getTimeInMillis() / 1000 / 60 / 60).toInt)
    } catch {
      case t: Throwable => { None; }
    }
  }

  def parseToDate(s: String): Option[Int] = {
    parseDate(s) match {
      case None => None
      case Some(v) => Some(v + 24)
    }
  }

  def parseIntValue(s: Option[String]): Option[Int] = {
    s match {
      case None | Some("0") => None
      case Some(s) => {
        try {
          Some(s.toInt)
        } catch {
          case t: Throwable => None
        }
      }
    }
  }

}