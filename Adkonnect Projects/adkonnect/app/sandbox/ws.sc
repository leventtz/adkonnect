package sandbox
import scala.concurrent._
import ExecutionContext.Implicits.global

object ws {
val session = socialNetwork.createSessionFor("user", credentials)
val f: Future[List[Friend]] = future {
  session.getFriends()
}

}