package sandbox

import com.sponsoracall.gatewayaccount.contract.CreateTemplateContract
import com.sponsoracall.ad.contract.CreateAdContractV2
import scala.util.Try
import scala.util.Success
import java.sql.Date
import java.sql.Time
import com.amazonaws.services.dynamodbv2.model.QueryRequest
import java.util.HashMap
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.Condition
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator
import com.sponsoracall.db.DynamoDBEngine
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest
import com.amazonaws.services.dynamodbv2.model.ScanRequest



object ClearCDRSummary extends App {
    def clearSummary = {
      
      val scanRequest=new ScanRequest().withTableName("CDRSummaryTable")
      val items=DynamoDBEngine.client.scan(scanRequest).getItems();
      for(i<-0 until items.size()){
        val key=new HashMap[String,AttributeValue]
        key.put("id", items.get(i).get("id"))
        key.put("daterange", items.get(i).get("daterange"))
        val deleteItemRequest=new DeleteItemRequest().withTableName("CDRSummaryTable").withKey(key)
        println(key)
        DynamoDBEngine.client.deleteItem(deleteItemRequest);
      }
    }
    
    
    clearSummary
	
}