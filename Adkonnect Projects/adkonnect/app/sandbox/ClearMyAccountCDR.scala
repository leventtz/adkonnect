package sandbox

import com.sponsoracall.gatewayaccount.contract.CreateTemplateContract
import com.sponsoracall.ad.contract.CreateAdContractV2
import scala.util.Try
import scala.util.Success
import java.sql.Date
import java.sql.Time
import com.amazonaws.services.dynamodbv2.model.QueryRequest
import java.util.HashMap
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.Condition
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator
import com.sponsoracall.db.DynamoDBEngine
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest



object ClearMyAccountCDR extends App {
    def clearMyAccount = {
      
      val key=new HashMap[String,Condition]
      key.put("accountId", new Condition().withComparisonOperator(ComparisonOperator.EQ).withAttributeValueList(new AttributeValue("c92412c9f6344d1e9b06ec5f2ae463fa")))
      val queryRequest=new QueryRequest().withTableName("AccountCDRTable").withKeyConditions(key)
      val items=DynamoDBEngine.client.query(queryRequest).getItems();
      for(i<-0 until items.size()){
        val key=new HashMap[String,AttributeValue]
        key.put("accountId", new AttributeValue("c92412c9f6344d1e9b06ec5f2ae463fa"))
        key.put("cdrId", items.get(i).get("cdrId"))
        val deleteItemRequest=new DeleteItemRequest().withTableName("AccountCDRTable").withKey(key)
        println(key)
        DynamoDBEngine.client.deleteItem(deleteItemRequest);
      }
    }
    
    
    clearMyAccount
	
}