package plugins;

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import play.Application
import play.Logger
import play.Plugin;
import play.Play


object S3ScalaPlugin {
  	val AWS_S3_BUCKET = "aws.s3.bucket";
    val AWS_ACCESS_KEY = "aws.access.key";
    val AWS_SECRET_KEY = "aws.secret.key";
    
     val accessKey = Play.application.configuration.getString(AWS_ACCESS_KEY);
     val secretKey = Play.application.configuration.getString(AWS_SECRET_KEY);
     val s3Bucket = Play.application.configuration.getString(AWS_S3_BUCKET);
     
     val s3Client:Option[AmazonS3Client]= {
       try {
        val awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
        val amazonS3=new AmazonS3Client(awsCredentials);
        amazonS3.createBucket(s3Bucket);
        Some(amazonS3)
       }catch {
         case t:Throwable => None
       }
     }
     
}

class S3ScalaPlugin(application:Application) extends Plugin {
    override def onStart() {
      Logger.info(S3ScalaPlugin.s3Client+" started");
    }

    override def enabled():Boolean= {
        (application.configuration().keys().contains(S3ScalaPlugin.AWS_ACCESS_KEY) &&
                application.configuration().keys().contains(S3ScalaPlugin.AWS_SECRET_KEY) &&
                application.configuration().keys().contains(S3ScalaPlugin.AWS_S3_BUCKET));
    }

}
