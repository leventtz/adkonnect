package util

import java.util.Calendar

object Util  {
	val months=List("01","02","03","04","05","06","07","08","09","10","11","12")
	
	val years={
	  val year=Calendar.getInstance().get(Calendar.YEAR)
	  val yearStart=year-(year/100).toInt*100;
	  val yearEnd=yearStart+10
	  (for(i<-yearStart until yearEnd) yield i).toList
	}
	
	val monthOptions=months.map(f=>s"<option value='$f'>$f</option>").mkString
	val yearOptions=years.map(f=>s"<option value='$f'>$f</option>").mkString
	
}