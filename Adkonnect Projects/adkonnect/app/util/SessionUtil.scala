package util

import scala.concurrent.duration.DurationInt
import javax.naming.ConfigurationException
import model.SessionData
import play.api.Play
import play.api.Play.current
import play.api.i18n.Messages
import play.api.libs.Crypto
import play.api.libs.concurrent.Akka
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json
import play.api.mvc.RequestHeader
import play.api.templates.Html
import com.sponsoracall.util.EmailService


object SessionUtil {

  def sendResetPassword(toEmail: String, newPassword: String) {
    val emailFrom = Play.current.configuration.getString("email.from") match {
      case Some(value) => value
      case None => throw new ConfigurationException("Cant Find Email from")
    }

    val emailSubject = Messages("account_forgot_password_email_subject")
    val message = Messages("account_forgot_password_description")
    val emailBody = Html(message + "<br>" + newPassword)

    Akka.system.scheduler.scheduleOnce(0.milliseconds) {
      EmailService.sendEmail(emailFrom, toEmail, emailSubject, emailBody.toString)
    }
  }  
  
  
  def validateAngularPost(implicit request: RequestHeader): Boolean = {
    request.headers.get("X-XSRF-TOKEN") match {
      case None => false
      case Some(tokenValue) => {
        val cookieName = Crypto.decryptAES(tokenValue)
        request.session.get(cookieName) match {
          case None => {
            false
          }
          case Some(data) => {
            SessionData.parse(Json.parse(Crypto.decryptAES(data))) match {
              case None => false;
              case oV @ Some(sessionData) if oV.get.cookieName == cookieName => true
              case _ => false
            }
          }
        }
      }
    }
  }

  def validateSessionUser(implicit request: RequestHeader): Boolean = {
	 getSessionData(request) match {
	   case Some(data) =>true
	   case _ => false
	 }	  
  }
  
  def getSessionData(implicit request: RequestHeader): Option[SessionData] = {
    request.cookies.get("XSRF-TOKEN") match {
      case None => None
      case Some(tokenValue) => {
        val cookieName = Crypto.decryptAES(tokenValue.value)
        request.session.get(cookieName) match {
          case None => None
          case Some(data) => {
            SessionData.parse(Json.parse(Crypto.decryptAES(data))) match {
              case None => None;
              case oV @ Some(sessionData) if oV.get.cookieName == cookieName && oV.get.accountId != "Connection" => oV
              case _ => None
            }
          }
        }
      }
    }
  }
  
  def checkAccountId(implicit request: RequestHeader,accountId:String):Boolean= {
    getSessionData match {
      case Some(sessionData) => sessionData.accountId==accountId
      case _ => false
    }
  }
  
  def isExpired(implicit request:RequestHeader) : Boolean = {
    request.cookies.get("X-SESSION-DEVAM") match {
      case None => true
      case Some(time)=> {
    	val t=System.currentTimeMillis()>=(Crypto.decryptAES(time.value).toLong+System.getProperty("SessionIdleTimeoutDurationInMinute").toLong*1000*60)
        t 
      }
    }
  }
  def validateSessionUserNotConnected(implicit request: RequestHeader): Boolean = {
    request.cookies.get("XSRF-TOKEN") match {
      case None =>
         false
      case Some(tokenValue) => {
        val cookieName = Crypto.decryptAES(tokenValue.value)
        request.session.get(cookieName) match {
          case None => false
          case Some(data) => {
            SessionData.parse(Json.parse(Crypto.decryptAES(data))) match {
              case None => false;
              case oV @ Some(sessionData) if oV.get.cookieName == cookieName && oV.get.accountId == "Connection" => true
              case _ => false
            }
          }
        }
      }
    }

  }
  
}