import com.sponsoracall.ad.contract.PickAdContractV2
import plugins.DestinationUtil
import com.sponsoracall.cdr.service.CDRServiceRegistry
import controllers.Account
import util.TestPayment

object TestObjectU extends App {
	trait TestTrait {
		def name:String
	}
	trait TestTrait1 {
		protected val identity1:String="TestTrait1"
		def secondName:String
	}
	object TestObject extends TestTrait with TestTrait1 {
		identity1 =>
		def name="TestObject"
		def secondName=identity1
	}
	println(TestObject.secondName)
}