package com.sponsoracall.db

import java.util.Properties
import scala.slick.jdbc.JdbcBackend.Database
import org.apache.commons.dbcp.BasicDataSourceFactory
import javax.sql.DataSource
import scala.slick.jdbc.GetResult
import scala.slick.jdbc.{ StaticQuery => Q }
import scala.util.Failure
import scala.util.Success
import scala.util.Try
import org.slf4j.LoggerFactory

object PostgresDBEngine {
  val log = LoggerFactory.getLogger(getClass())

  val dataSource: DataSource = {
    val dsProperties = new Properties();
    dsProperties.setProperty("url", System.getProperty("url","jdbc:postgresql://54.82.47.189/dimwire"));
    dsProperties.setProperty("driverClassName", System.getProperty("driverClassName", "org.postgresql.Driver"));
    dsProperties.setProperty("username", System.getProperty("username", "dimwireaccess"));
    dsProperties.setProperty("password", System.getProperty("password", "`1qazxsw2"));
    dsProperties.setProperty("maxActive", System.getProperty("maxActive", "100"));
    dsProperties.setProperty("minIdle", System.getProperty("minIdle", "5"));
    dsProperties.setProperty("maxIdle", System.getProperty("maxIdle", "10"));
    dsProperties.setProperty("validationquery", System.getProperty("validationquery", "select 1"));
    BasicDataSourceFactory.createDataSource(dsProperties)
  }

  dataSource.getConnection().close()

  val database: Database = Database.forDataSource(dataSource)

  def listData[T](queryString: String, r: GetResult[T]): Try[List[T]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          Success(Q.queryNA[T](queryString)(r).list)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }

}