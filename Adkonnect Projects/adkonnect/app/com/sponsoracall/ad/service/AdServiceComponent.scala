package com.sponsoracall.ad.service

import com.sponsoracall.ad.integration.AdIntegrationComponent



trait AdServiceComponent {
	self: AdIntegrationComponent =>
	val service:AdService
}