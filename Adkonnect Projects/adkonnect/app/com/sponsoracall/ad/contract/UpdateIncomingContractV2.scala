package com.sponsoracall.ad.contract
import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.Json
import com.sponsoracall.cdr.data.LocationData

object UpdateIncomingContractV2 {
  
  import play.api.libs.json._
  implicit val locationReader = Json.reads[LocationData]
  val jsonReader = Json.reads[UpdateIncomingContractV2]
  def parse(value: String): Option[UpdateIncomingContractV2] = {
    try {
      Some(jsonReader.reads(Json.parse(value)).get)
    } catch {
      case t: Throwable => {
        None
      }
    }
  }
}
case class UpdateIncomingContractV2(accountId:String,adId:String,incoming:List[LocationData]) {
  require(accountId!=null && adId!=null,"Missing Account and AdId")
  require(incoming !=null && incoming.size>0, "Missing Location Info")
  
} 