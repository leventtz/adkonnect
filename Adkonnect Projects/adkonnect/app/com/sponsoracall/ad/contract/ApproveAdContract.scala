package com.sponsoracall.ad.contract

object ApproveAdContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[ApproveAdContract]
  def parse(value: JsValue): Option[ApproveAdContract] = {
    try {
      Some(jsonReader.reads(value).get) 
    } catch {
      case t: Throwable => t.printStackTrace();None
    }
  }
}

case class ApproveAdContract(accountId:String,adId:String,approve:Boolean) {
  require(accountId!=null && !accountId.isEmpty(),"AccountId Required")
  require(adId!=null && !adId.isEmpty(),"adId Required")
}