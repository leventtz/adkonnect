package com.sponsoracall.ad.contract
import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.Json

object ActivateAdContractV2 {
  import play.api.libs.json._
  val jsonReader = Json.reads[ActivateAdContractV2]
  def parse(value: String): Option[ActivateAdContractV2] = {
    try {
      Some(jsonReader.reads(Json.parse(value)).get)
    } catch {
      case t: Throwable => {
        None
      }
    }
  }
}
case class ActivateAdContractV2(accountId:String,adId:String,active:Boolean) {
    import play.api.libs.json._
  val jsonWriter=Json.writes[ActivateAdContractV2]
  override def toString={
    jsonWriter.writes(this).toString
  } 
}