package com.sponsoracall.ad.contract


case class LineItemContractV2(templateId:Int,qty:Int,templateUnitPrice:Float,templatePromptCost:Float,isPrompt:Boolean)

case class GetOrderTotalContractV2(accountId:String,lineItems:List[LineItemContractV2]) {
   require(accountId==null && accountId.isEmpty(),"accountId required")
}