package com.sponsoracall.ad.contract

import com.sponsoracall.account.payment.PaymentContract

case class CompleteOrderContract(lineItems: List[CreateAdContractV2],paymentContract:PaymentContract) {
  require(lineItems.length>0,"Empty Order Items");
  require(paymentContract!=null,"Null Contract"); 
}