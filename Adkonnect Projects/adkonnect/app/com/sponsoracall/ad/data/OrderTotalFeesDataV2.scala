package com.sponsoracall.ad.data

import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.Json


case class OrderTotalFeesDataV2(feeItemMessageId:String,isPercent:String,applyValue:BigDecimal) {
  require(feeItemMessageId!=null && !feeItemMessageId.isEmpty(),"feeItemMessageId required")
}

