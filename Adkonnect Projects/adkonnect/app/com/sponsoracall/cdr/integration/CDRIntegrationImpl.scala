package com.sponsoracall.cdr.integration

import java.util.Calendar
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer
import scala.slick.jdbc.GetResult
import scala.slick.jdbc.{ StaticQuery => Q }
import scala.util.Failure
import scala.util.Success
import scala.util.Try
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator
import com.amazonaws.services.dynamodbv2.model.Condition
import com.amazonaws.services.dynamodbv2.model.PutItemRequest
import com.amazonaws.services.dynamodbv2.model.QueryRequest
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest
import com.sponsoracall.ad.data.PickAdData
import com.sponsoracall.cdr.contract.AddDIDContract
import com.sponsoracall.cdr.contract.CDRFilter
import com.sponsoracall.cdr.contract.CDRSummaryContract
import com.sponsoracall.cdr.contract.CreateCDRContract
import com.sponsoracall.cdr.contract.CreateCountryContract
import com.sponsoracall.cdr.contract.CreateDestinationContract
import com.sponsoracall.cdr.contract.CreateDialingCodeContract
import com.sponsoracall.cdr.contract.CreateGatewayContract
import com.sponsoracall.cdr.contract.CreateProviderGatewayContract
import com.sponsoracall.cdr.contract.DeleteDestinationContract
import com.sponsoracall.cdr.contract.DeleteDialingCodeContract
import com.sponsoracall.cdr.contract.RetrieveDestinationsByCountryContract
import com.sponsoracall.cdr.contract.RetrieveDialingCodeByDestinationContract
import com.sponsoracall.cdr.contract.RouteContract
import com.sponsoracall.cdr.contract.UpdateCDRContract
import com.sponsoracall.cdr.contract.UpdateCountryContract
import com.sponsoracall.cdr.contract.UpdateDestinationContract
import com.sponsoracall.cdr.contract.UpdateGatewayContract
import com.sponsoracall.cdr.contract.UpdateProviderGatewayContract
import com.sponsoracall.cdr.data.BestMatchTemplateData
import com.sponsoracall.cdr.data.CDRAccountData
import com.sponsoracall.cdr.data.CDRSummaryByHourData
import com.sponsoracall.cdr.data.CDRSummaryData
import com.sponsoracall.cdr.data.CountryData
import com.sponsoracall.cdr.data.DIDData
import com.sponsoracall.cdr.data.DestinationData
import com.sponsoracall.cdr.data.DialingCodeData
import com.sponsoracall.cdr.data.GatewayData
import com.sponsoracall.cdr.data.ProviderGatewayData
import com.sponsoracall.cdr.data.RouteData
import com.sponsoracall.db.DynamoDBEngine
import com.sponsoracall.db.PostgresDBEngine
import play.api.libs.json.Json
import java.util.HashMap
import com.amazonaws.services.dynamodbv2.model.AttributeAction
import com.sponsoracall.cdr.contract.UpdateCDRSummaryContractTable
import org.slf4j.LoggerFactory
import com.sponsoracall.cdr.data.LocationData

class CDRIntegrationImpl extends CDRIntegration {
  val log = LoggerFactory.getLogger(getClass())
  def createCountry(contract: CreateCountryContract): Try[Option[CountryData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        val name = contract.name
        val ps = session.prepareStatement("INSERT INTO Country (id,name) VALUES (nextval('countryseq'),?)")
        ps.setString(1, name)
        ps.execute()
        Success(Q.queryNA[CountryData](s"select * from Country where name='$name'")(GetResult(r => CountryData(r.<<, r.<<, r.<<, r.<<))).firstOption)
    }
  }

  def updateCountry(contract: UpdateCountryContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update Country set name=? where id=?")
          ps.setString(1, contract.name)
          ps.setInt(2, contract.id)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def retrieveCountries: Try[List[CountryData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val l = Q.queryNA[CountryData]("select * from Country order by name")(GetResult(r => CountryData(r.<<, r.<<, r.<<, r.<<))).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def createDestination(contract: CreateDestinationContract) = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val name = contract.name
          val countryId = contract.countryId
          val ps = session.prepareStatement("INSERT INTO destination (id,name,countryId) VALUES (nextval('destseq'),?,?)")
          ps.setString(1, name)
          ps.setInt(2, countryId)
          ps.execute()
          Success(Q.queryNA[DestinationData](s"select * from destination where name='$name' and countryId=" + countryId + " order by destination.name")(GetResult(r => DestinationData(r.<<, r.<<, r.<<, r.<<))).firstOption)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def updateDestination(contract: UpdateDestinationContract) = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update destination set name=? where id=?")
          ps.setString(1, contract.name)
          ps.setInt(2, contract.id)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def retriveDestinations: Try[List[DestinationData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val l = Q.queryNA[DestinationData]("select id,name,countryid,incomingdestination from Destination order by destination.name")(GetResult(r => DestinationData(r.<<, r.<<, r.<<, r.<<))).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def retrieveLocations(searchString:String): Try[List[LocationData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val l = Q.queryNA[LocationData]("select id,parentId,locationName from location where upper(locationName) like upper('"+searchString+"%') order by locationname limit 15")(GetResult(r => LocationData(r.<<, r.<<, r.<<))).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }  
  
  
  def retriveIncomingDestinations: Try[List[DestinationData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val l = Q.queryNA[DestinationData]("select id,name,countryid,incomingdestination from Destination where incomingdestination=true order by destination.name")(GetResult(r => DestinationData(r.<<, r.<<, r.<<, r.<<))).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def retriveDestinationsByCountry(contract: RetrieveDestinationsByCountryContract): Try[List[DestinationData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val l = Q.queryNA[DestinationData](s"select id,name,countryid,incomingdestination from Destination where countryId=" + contract.countryId + " order by destination.name")(GetResult(r => DestinationData(r.<<, r.<<, r.<<, r.<<))).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def retrieveBestMatchDestination(number: String): Try[Option[(Int, String)]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val sql = s"select destinationid,destination.name,da from dialingcode,destination where destination.id=destinationid and (da=substring('$number',1,4) or da=substring('$number',1,5) or da=substring('$number',1,6) or da=substring('$number',1,7) or da=substring('$number',1,8) or da=substring('$number',1,9)) order by da desc limit 1 ";
          val ps = session.prepareStatement(sql)
          val rs = ps.executeQuery();
          if (rs.next()) {
            Success(Some(rs.getInt(1), rs.getString(2)))
          } else {
            Success(None)
          }
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def retrieveBestMatchTemplate(number: String): Try[Option[BestMatchTemplateData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val sql = s"select ad.id,destination.id,destination.name from ad,adda,destination where adda.destinationid=destination.id and ad.accountId='1' and adda.adid=ad.id and adda.destinationId=(select destinationid from dialingcode,destination where destination.id=destinationid and (da=substring('$number',1,4) or da=substring('$number',1,5) or da=substring('$number',1,6) or da=substring('$number',1,7) or da=substring('$number',1,8) or da=substring('$number',1,9)) order by da desc limit 1) ";
          val ps = session.prepareStatement(sql)
          val rs = ps.executeQuery()
          if (rs.next()) {
            val templateId = rs.getInt(1)
            val destinationId = rs.getInt(2)
            val destinationName = rs.getString(3)
            Success(Some(BestMatchTemplateData(templateId, destinationId, destinationName)))
          } else {
            Success(None)
          }
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def retrieveBestMatchDestinationCountryCode(number: String): Try[Option[String]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val sql = s"select country.countrycode from dialingcode,destination,country where country.id=destination.countryid and destination.id=destinationid and (da=substring('$number',1,4) or da=substring('$number',1,5) or da=substring('$number',1,6) or da=substring('$number',1,7) or da=substring('$number',1,8) or da=substring('$number',1,9)) order by da desc limit 1 ";
          val ps = session.prepareStatement(sql)
          val rs = ps.executeQuery();
          if (rs.next()) {
            Success(Some(rs.getString(1)))
          } else {
            Success(None)
          }
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def retrieveBestMatchDestinationAll(number: String): Try[List[Int]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val sql = s"select destinationid from dialingcode where da=substring('$number',1,4) or da=substring('$number',1,5) or da=substring('$number',1,6) or da=substring('$number',1,7) or da=substring('$number',1,8) or da=substring('$number',1,9) order by destinationid ";
          val l = Q.queryNA[Int](sql).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def deleteDestination(contract: DeleteDestinationContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("delete from destination where id=?")
          ps.setInt(1, contract.destinationId)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def createDialingCode(contract: CreateDialingCodeContract) = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("INSERT INTO dialingcode (da,destinationId) VALUES (?,?)")
          ps.setString(1, contract.da)
          ps.setInt(2, contract.destinationId)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def deleteDialingCode(contract: DeleteDialingCodeContract) = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("delete from dialingcode where da=?")
          ps.setString(1, contract.da)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def retrieveDialingCodeByDestination(contract: RetrieveDialingCodeByDestinationContract): Try[List[DialingCodeData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val l = Q.queryNA[DialingCodeData]("select da,destinationId from dialingcode where destinationid=" + contract.destinationId + " order by da")(GetResult(r => DialingCodeData(r.<<, r.<<))).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def getDID(countryId:String): Try[Option[String]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val queryString = s"select did from gateway where gateway.active=true and gateway.id=(select id from gateway where gateway.countryId='$countryId' and gateway.active=true order by hitcount asc limit 1)"
          val l = Q.queryNA[String](queryString).firstOption
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  
  def retrieveLocationsFromZip(zipCode:String): Option[List[Int]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val queryString=
            s"""
              select postalcode.id postalcodeid,
              postalcode.parentid CityId,
              state.parentId state,
              country.parentId countryId
               from
               location postalcode,
               location state,
               location country
               where
               state.id=postalcode.parentid and
               country.id=state.parentid and
               postalcode.postalcode = '$zipCode'
            """
          val ps=session.prepareStatement(queryString);
          val rs=ps.executeQuery()
          val arrayBuffer=new ArrayBuffer[Int]()
          while(rs.next()) {
            arrayBuffer.append(rs.getInt(1))
            arrayBuffer.append(rs.getInt(2))
            arrayBuffer.append(rs.getInt(3))
            arrayBuffer.append(rs.getInt(4))
          }
          Some(arrayBuffer.toSet.toList)
        } catch {
          case t: Throwable => None
        }
    }
  }
  
  def createGateway(contract: CreateGatewayContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("INSERT INTO gateway (ip,did,name,providerid,active) VALUES (?,?,?,?,false)")
          ps.setString(1, contract.ip)
          ps.setString(2, contract.did)
          ps.setString(3, contract.name)
          ps.setString(4, contract.providerId)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def updateGateway(contract: UpdateGatewayContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update gateway set ip=?, name=?,did=?,providerid=?,active=? where id=?")
          ps.setString(1, contract.ip)
          ps.setString(2, contract.name)
          ps.setString(3, contract.did)
          ps.setString(4, contract.providerId)
          ps.setBoolean(5, contract.active)
          ps.setInt(6, contract.id)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def resetGatewayHitCount(gatewayId: Int) {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update gateway set hitcount=0 where id=?")
          ps.setInt(1, gatewayId)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def incrementDIDHitCount(did: String) {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update gateway set hitcount=hitcount+1 where did=?")
          ps.setString(1, did)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def decrementDIDHitCount(did: String) {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update gateway set hitcount=hitcount-1 where did=?")
          ps.setString(1, did)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def deleteGateway(id: Int): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("delete from gateway where id=?")
          ps.setInt(1, id)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def toggleGateway(id: Int): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update gateway set active=not active where id=?")
          ps.setInt(1, id)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def listGateways(): Try[List[GatewayData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val l = Q.queryNA[GatewayData]("select id,ip,did,providerId,name,hitcount,active from gateway order by id desc")(GetResult(r => GatewayData(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def createProviderGateway(contract: CreateProviderGatewayContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("INSERT INTO ProviderGateway (endpointip,providerid,prefix,username,password) VALUES (?,?,?,?,?)")
          ps.setString(1, contract.endpointip)
          ps.setString(2, contract.providerid)
          ps.setString(3, contract.prefix)
          ps.setString(4, contract.username)
          ps.setString(5, contract.password)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def updateProviderGateway(contract: UpdateProviderGatewayContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("Update ProviderGateway set endpointip=?,providerid=?,prefix=?,username=?,password=?,active=? where id=?")
          ps.setString(1, contract.endpointip)
          ps.setString(2, contract.providerid)
          ps.setString(3, contract.prefix)
          ps.setString(4, contract.username)
          ps.setString(5, contract.password)
          ps.setBoolean(6, contract.active)
          ps.setInt(7, contract.id)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def deleteProviderGateway(id: Int): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("delete from ProviderGateway where id=?")
          ps.setInt(1, id)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def toggleProviderGateway(id: Int): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update ProviderGateway set active=not active where id=?")
          ps.setInt(1, id)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def listProviderGateways(): Try[List[ProviderGatewayData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val l = Q.queryNA[ProviderGatewayData]("select id,endpointip,providerid,prefix,username,password,active from providergateway order by id desc")(GetResult(r => ProviderGatewayData(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def retrieveRoute(destinationId: Int): Try[Option[ProviderGatewayData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val sqlString = s"select providergateway.id,endpointip,providerid,prefix,username,password,providergateway.active from providergateway,routetable where destinationid=$destinationId and providergateway.id=routetable.providergatewayid and providergateway.active and routetable.active limit 1";
          Success(Q.queryNA[ProviderGatewayData](sqlString)(GetResult(r => ProviderGatewayData(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))).firstOption)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def addDID(contract: AddDIDContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("INSERT INTO DIDTable (did,gatewayid,providerId) VALUES (?,?,?)")
          ps.setString(1, contract.DID)
          ps.setInt(2, contract.gatewayId)
          ps.setString(3, contract.providerId)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def deleteDID(DID: String): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("delete from didtable where did=?")
          ps.setString(1, DID)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def listRoutes(destinationId: Int): Try[List[RouteData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val l = Q.queryNA[RouteData](s"select providergatewayid,destinationid,active from routeTable where destinationId=$destinationId ")(GetResult(r => RouteData(r.<<, r.<<, r.<<))).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def addRoute(contract: RouteContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("INSERT INTO routetable (providergatewayid,destinationid,active) VALUES (?,?,false)")
          ps.setInt(1, contract.providerGatewayId)
          ps.setInt(2, contract.destinationId)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def toggleRoute(contract: RouteContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update routetable set active=not active where providergatewayid=? and destinationid=?")
          ps.setInt(1, contract.providerGatewayId)
          ps.setInt(2, contract.destinationId)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def deleteRoute(contract: RouteContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("delete from routetable where providergatewayid=? and destinationid=?")
          ps.setInt(1, contract.providerGatewayId)
          ps.setInt(2, contract.destinationId)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def listDIDs(): Try[List[DIDData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val l = Q.queryNA[DIDData]("select DID,gatewayid,providerId from didtable order by gatewayId desc")(GetResult(r => DIDData(r.<<, r.<<, r.<<))).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def listDIDsByProvider(providerId: String): Try[List[DIDData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val l = Q.queryNA[DIDData](s"select DID,gatewayId,providerId from didtable where providerId=providerId order by DID desc")(GetResult(r => DIDData(r<<, r.<<, r.<<))).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def listDIDs(gatewayId: Int): Try[List[DIDData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val l = Q.queryNA[DIDData](s"select DID,gatewayId,providerId from didtable where gatewayid=$gatewayId order by DID desc")(GetResult(r => DIDData(r<<, r.<<, r.<<))).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def retrieveCDRSummaryByHour(contract: CDRSummaryContract): Try[List[CDRSummaryByHourData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val select = buildSelectString(contract)
          val l = Q.queryNA[CDRSummaryByHourData](select)(GetResult(r => CDRSummaryByHourData(r.<<,r.<<,r.<<, r.<<, r.<<, r.<<, r.<<,r.<<))).list
          Success(l)
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def retrieveCDRSummary(contract: CDRSummaryContract): Try[CDRSummaryData] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val select = buildSelectString(contract)
          val statement = session.prepareStatement(select)
          val rs = statement.executeQuery()

          val colCount = rs.getMetaData().getColumnCount();
          val metaList = new ListBuffer[String]
          val valuesList = new ListBuffer[List[String]]

          for (i <- 1 to colCount) {
            metaList += rs.getMetaData().getColumnName(i).capitalize
          }
          while (rs.next) {
            val tempList = new ListBuffer[String]()
            for (i <- 1 to colCount) {
              tempList += rs.getString(i)
            }
            valuesList += tempList.toList
          }
          Success(CDRSummaryData(metaList.toList, valuesList.toList))
        } catch {
          case t: Throwable => Failure(t)
        }
    }
  }

  def buildSelectString(c: CDRSummaryContract): String = {
    val reportType = c.reportType
    val filter = buildFilter(c.filter)
    val sumValues = " sum(calls) TotalCalls,sum(ccalls) CompletedCalls,sum(actualDuration) ActualDuration,sum(SellDuration) SellDuration,sum(sellTotal) SellTotal,sum(buyTotal) BuyTotal,sum(hit) HitTotal"
    val from = " cdrsummary inner join destination on cdrsummary.outdestinationid=destination.id inner join country on country.id=destination.countryid "

    reportType match {
      case "date" => s"select to_char(to_timestamp(calldate*60*60),'YYYY-MM-DD HH24') as CallDate,$sumValues from $from where $filter group by to_char(to_timestamp(calldate*60*60),'YYYY-MM-DD HH24'),(EXTRACT(hour from (TIMESTAMP WITH TIME ZONE 'epoch' + calldate*INTERVAL '1 hour'))) order by to_char(to_timestamp(calldate*60*60),'YYYY-MM-DD HH24')"
      case "destination" => s"select destination.name as DestinationName, $sumValues from $from where $filter group by destination.name"
      case "country" => s"select country.name as CountryName, $sumValues from $from where $filter group by country.name"
      case "indestination" => s"select indestination.name as InDestinationName, $sumValues from $from,destination as indestination where indestination.id=cdrsummary.indestinationid and $filter group by indestination.name"
    }
    
  }

  def buildFilter(contract: CDRFilter): String = {
    val date = contract match {
      case CDRFilter(None, _, _, _, _, _, _, _) => {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        "calldate between " + calendar.getTimeInMillis() / 1000 / 60 / 60 + " and " + System.currentTimeMillis() / 1000 / 60 / 60
      }
      case CDRFilter(Some(startDate), None, _, _, _, _, _, _) => "calldate between " + startDate + " and " + System.currentTimeMillis() / 1000 / 60 / 60
      case CDRFilter(Some(startDate), Some(endDate), _, _, _, _, _, _) => "calldate between " + startDate + " and " + endDate
    }

    val ad = contract match {
      case CDRFilter(_, _, Some(adId), _, _, _, _, _) => " and adid=" + adId
      case _ => ""
    }

    val provider = contract match {
      case CDRFilter(_, _, _, Some(providerId), _, _, _, _) => " and providerId='" + providerId + "'"
      case _ => ""
    }

    val country = contract match {
      case CDRFilter(_, _, _, _, Some(countryId), _, _, _) => " and country.id=" + countryId
      case _ => ""
    }

    val inDestination = contract match {
      case CDRFilter(_, _, _, _, _, Some(inDestinationId), _, _) => " and inDestinationId=" + inDestinationId
      case _ => ""
    }

    val outDestination = contract match {
      case CDRFilter(_, _, _, _, _, _, Some(outDestinationId), _) => " and outDestinationId=" + outDestinationId
      case _ => ""
    }

    val gateway = contract match {
      case CDRFilter(_, _, _, _, _, _, _, Some(gatewayId)) => " and gatewayId=" + gatewayId
      case _ => ""
    }

    date + ad + provider + country + inDestination + outDestination
  }

  def createCDR(contract: CreateCDRContract) {
    def getAttributeValue(value: String): String = if (value == null || value.size == 0) "NA" else value
    val putItemRequest = new PutItemRequest().withTableName("AccountCDRTable")
    putItemRequest.addItemEntry("accountId", new AttributeValue(contract.accountId))
    putItemRequest.addItemEntry("cdrId", new AttributeValue(contract.cdrId))
    putItemRequest.addItemEntry("adId", new AttributeValue(contract.data.adId))
    putItemRequest.addItemEntry("calldate", new AttributeValue().withN(System.currentTimeMillis().toString))
    putItemRequest.addItemEntry("from", new AttributeValue(getAttributeValue(contract.data.inNumber)))
    putItemRequest.addItemEntry("to", new AttributeValue(getAttributeValue(contract.data.outNumber)))
    putItemRequest.addItemEntry("description", new AttributeValue(getAttributeValue(contract.data.description)))
    putItemRequest.addItemEntry("ivrmedialocation", new AttributeValue(getAttributeValue(contract.data.ivrmedialocation)))
    putItemRequest.addItemEntry("provider", new AttributeValue(getAttributeValue(contract.data.providerId)))
    putItemRequest.addItemEntry("maxDuration", new AttributeValue(getAttributeValue(contract.data.maxduration.toString)))
    putItemRequest.addItemEntry("rate", new AttributeValue(getAttributeValue(contract.data.rate.toString)))
    putItemRequest.addItemEntry("buyRate", new AttributeValue(contract.data.rateBuy.setScale(4, scala.math.BigDecimal.RoundingMode.HALF_UP).toString))
    putItemRequest.addItemEntry("did", new AttributeValue(getAttributeValue(contract.data.did.toString)))
    putItemRequest.addItemEntry("inDestination", new AttributeValue(getAttributeValue(contract.data.inDestination)))
    putItemRequest.addItemEntry("inDestinationId", new AttributeValue().withN(contract.data.inDestinationId.toString))
    putItemRequest.addItemEntry("outDestination", new AttributeValue(getAttributeValue(contract.data.outDestination)))
    putItemRequest.addItemEntry("outDestinationId", new AttributeValue().withN(contract.data.outDestinationId.toString))
    putItemRequest.addItemEntry("billduration", new AttributeValue().withN("0"))
    putItemRequest.addItemEntry("callduration", new AttributeValue().withN("0"))
    DynamoDBEngine.client.putItem(putItemRequest)
  }
  
  
  def updateCDR(contract: UpdateCDRContract) {
    val updateItemRequest = new UpdateItemRequest().withTableName("AccountCDRTable")
    val map = new java.util.HashMap[String, AttributeValue]();
    map.put("accountId", new AttributeValue(contract.accountId))
    map.put("cdrId", new AttributeValue(contract.cdrId))
    updateItemRequest.setKey(map)
    updateItemRequest.addAttributeUpdatesEntry("billduration", new AttributeValueUpdate().withValue(new AttributeValue().withN(contract.billDuration.toString)))
    updateItemRequest.addAttributeUpdatesEntry("callduration", new AttributeValueUpdate().withValue(new AttributeValue().withN(contract.callDuration.toString)))
    updateItemRequest.addAttributeUpdatesEntry("reason", new AttributeValueUpdate().withValue(new AttributeValue(contract.reason)))
    DynamoDBEngine.client.updateItem(updateItemRequest)

  }

  override def getAccountCalls(accountId: String): Try[List[Option[CDRAccountData]]] = {
    try {
      val queryRequest = new QueryRequest()
        .withTableName("CDR")
        .withAttributesToGet("addata", "callDuration", "ext")
        .withScanIndexForward(false)
        .addKeyConditionsEntry("accountid", new Condition()
          .withAttributeValueList(new AttributeValue(accountId))
          .withComparisonOperator(ComparisonOperator.EQ))
        .withLimit(10)

      val items = DynamoDBEngine.client.query(queryRequest).getItems()
      val buffer: ArrayBuffer[CDRAccountData] = new ArrayBuffer(10)
      val l: Seq[Option[CDRAccountData]] = for (i <- 0 until items.size) yield {
        PickAdData.parse(Json.parse(items.get(i).get("addata").getS())) match {
          case Some(data) => Some(CDRAccountData(data.accountId, data.id.toLong, data))
          case None => None
        }
      }
      Success(l.toList)
    } catch {
      case t: Throwable => Failure(t)
    }
  }

  def updateCDRSummaryTable(contract: UpdateCDRSummaryContractTable): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        var call = 0
        var ccall = 0
        var hit = contract.hit
        var sellDuration = contract.data.maxduration
        var actualDuration = {
          if(contract.duration<60)
            1
            else
              Math.ceil(contract.duration.toDouble/60).toInt
        }
        var buyTotal = contract.data.rateBuy.setScale(4, BigDecimal.RoundingMode.HALF_UP)
        var sellTotal = contract.data.rate
        if (contract.duration == 0) {
          call = 1
          ccall = 0
          sellTotal = 0
        } else if (contract.duration > 0) {
          ccall = 1
          call = 1
        }

        if (contract.hit == 1) {
          call = 0
          ccall = 0
          sellDuration = 0
          actualDuration = 0
          buyTotal = 0
          sellTotal = 0
        }

        try {
          val ps = session.prepareStatement(
            "update cdrsummary set calls=calls+?,ccalls=ccalls+?,hit =hit+?,buyTotal=buyTotal+?,sellTotal=sellTotal+?,actualduration=actualduration+?,sellduration=sellduration+? " +
              " where calldate=? and indestinationid=? and outdestinationid=? and did=? and providerid=? and companyname=?")
          ps.setInt(1, call)
          ps.setInt(2, ccall)
          ps.setInt(3, hit)
          ps.setFloat(4, buyTotal.toFloat)
          ps.setFloat(5, sellTotal.toFloat)
          ps.setInt(6, actualDuration)
          ps.setInt(7, sellDuration)
          ps.setInt(8, contract.calldatehour)
          ps.setInt(9, contract.data.inDestinationId)
          ps.setInt(10, contract.data.outDestinationId)
          ps.setString(11, contract.data.did)
          ps.setString(12, contract.data.providerGatewayAddress)
          ps.setString(13, contract.data.name)
          val rowCount = ps.executeUpdate()
          if (rowCount == 0) {
            val ps = session.prepareStatement("insert into cdrsummary values(?,?,?,?,?,?,?,?,?,?,?,?,?)")
            ps.setInt(1, contract.calldatehour)
            ps.setInt(2, contract.data.inDestinationId)
            ps.setInt(3, contract.data.outDestinationId)
            ps.setString(4, contract.data.did)
            ps.setString(5, contract.data.providerGatewayAddress)
            ps.setString(6, contract.data.name)
            ps.setInt(7, call)
            ps.setInt(8, ccall)
            ps.setInt(9, hit)
            ps.setFloat(10, buyTotal.toFloat)
            ps.setFloat(11, sellTotal.toFloat)
            ps.setInt(12, actualDuration)
            ps.setInt(13, sellDuration)

            if (ps.executeUpdate() < 1) {
              Failure(new Exception("Weird.. Both statements returned no success!"))
            } else {
              Success(Unit)
            }

          } else {
            Success(Unit)
          }

        } catch {
          case t: Throwable => { t.printStackTrace; Failure(t) }
        }
    }
  }


}