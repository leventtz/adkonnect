package com.sponsoracall.cdr.contract

object RouteContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[RouteContract]
  def parse(value: JsValue): Option[RouteContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}
case class RouteContract(providerGatewayId:Int,destinationId:Int)