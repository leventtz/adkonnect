package com.sponsoracall.cdr.contract

object RetrieveDestinationsByCountryContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[RetrieveDestinationsByCountryContract]
  def parse(value: JsValue): Option[RetrieveDestinationsByCountryContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class RetrieveDestinationsByCountryContract(countryId:Int) {
  
}