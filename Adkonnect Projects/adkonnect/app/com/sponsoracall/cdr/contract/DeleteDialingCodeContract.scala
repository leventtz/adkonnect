package com.sponsoracall.cdr.contract

object DeleteDialingCodeContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[DeleteDialingCodeContract]
  def parse(value: JsValue): Option[DeleteDialingCodeContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class DeleteDialingCodeContract(da:String) {
  require(da!=null && da.length>0,"da Required")
}