package com.sponsoracall.cdr.service

import scala.util.Try
import com.sponsoracall.ad.data.PickAdData
import com.sponsoracall.cdr.contract.CDRSummaryContract
import com.sponsoracall.cdr.data.CDRSummaryByHourData
import com.sponsoracall.cdr.contract.DeleteDialingCodeContract
import com.sponsoracall.cdr.integration.CDRIntegrationComponent
import com.sponsoracall.cdr.data.DestinationData
import com.sponsoracall.cdr.contract.DeleteDestinationContract
import com.sponsoracall.cdr.contract.CreateGatewayContract
import com.sponsoracall.cdr.contract.CreateCountryContract
import com.sponsoracall.cdr.contract.CreateProviderGatewayContract
import com.sponsoracall.cdr.contract.UpdateDestinationContract
import com.sponsoracall.cdr.integration.CDRIntegrationImpl
import com.sponsoracall.cdr.contract.CreateCDRContract
import com.sponsoracall.cdr.contract.UpdateCDRContract
import com.sponsoracall.cdr.data.DIDData
import com.sponsoracall.cdr.contract.RetrieveDialingCodeByDestinationContract
import com.sponsoracall.cdr.contract.CreateDestinationContract
import com.sponsoracall.cdr.contract.UpdateGatewayContract
import com.sponsoracall.cdr.contract.UpdateProviderGatewayContract
import com.sponsoracall.cdr.data.ProviderGatewayData
import com.sponsoracall.cdr.contract.AddDIDContract
import com.sponsoracall.cdr.data.CDRSummaryData
import com.sponsoracall.cdr.data.GatewayData
import com.sponsoracall.cdr.contract.UpdateCountryContract
import com.sponsoracall.cdr.contract.CreateDialingCodeContract
import com.sponsoracall.cdr.data.BestMatchTemplateData
import com.sponsoracall.cdr.data.CountryData
import com.sponsoracall.cdr.data.CDRAccountData
import com.sponsoracall.cdr.data.DialingCodeData
import com.sponsoracall.cdr.contract.RetrieveDestinationsByCountryContract
import com.sponsoracall.cdr.data.RouteData
import com.sponsoracall.cdr.contract.RouteContract
import com.sponsoracall.cdr.contract.UpdateCDRSummaryContractTable
import com.sponsoracall.cdr.data.LocationData


object CDRServiceRegistry extends CDRServiceComponent with CDRIntegrationComponent {
  val integration = new CDRIntegrationImpl
  val service = new CDRService() {
    
    
    def createCountry(contract:CreateCountryContract):Try[Option[CountryData]] = integration.createCountry(contract)
    def retrieveCountries:Try[List[CountryData]]=integration.retrieveCountries:Try[List[CountryData]]
    def updateCountry(contract:UpdateCountryContract):Try[Unit] = integration.updateCountry(contract)

    def createDestination(contract:CreateDestinationContract):Try[Option[DestinationData]] = integration.createDestination(contract)
    def updateDestination(contract:UpdateDestinationContract):Try[Unit] = integration.updateDestination(contract)
	def retriveDestinationsByCountry(contract:RetrieveDestinationsByCountryContract):Try[List[DestinationData]] = integration.retriveDestinationsByCountry(contract);
	def retriveDestinations:Try[List[DestinationData]] = integration.retriveDestinations;
    def retriveIncomingDestinations:Try[List[DestinationData]] = integration.retriveIncomingDestinations;
    def deleteDestination(contract:DeleteDestinationContract):Try[Unit] = integration.deleteDestination(contract);
    
    
    def createDialingCode(contract:CreateDialingCodeContract):Try[Unit] = integration.createDialingCode(contract)
	def retrieveDialingCodeByDestination(contract:RetrieveDialingCodeByDestinationContract):Try[List[DialingCodeData]] = integration.retrieveDialingCodeByDestination(contract)    
	def deleteDialingCode(contract:DeleteDialingCodeContract):Try[Unit] = integration.deleteDialingCode(contract);
    def retrieveBestMatchTemplate(number: String): Try[Option[BestMatchTemplateData]]=integration.retrieveBestMatchTemplate(number)
    def createGateway(contract:CreateGatewayContract):Try[Unit] = integration.createGateway(contract)
    def updateGateway(contract:UpdateGatewayContract):Try[Unit] = integration.updateGateway(contract)
    def deleteGateway(id:Int):Try[Unit] = integration.deleteGateway(id)
    def toggleGateway(id:Int):Try[Unit] = integration.toggleGateway(id)
    def listGateways():Try[List[GatewayData]] = integration.listGateways   
    
    def addDID(contract:AddDIDContract) = integration.addDID(contract)
    def deleteDID(DID:String) = integration.deleteDID(DID)    
    def listDIDs(): Try[List[DIDData]] = integration.listDIDs(): Try[List[DIDData]] 
    def listDIDs(gatewayId:Int): Try[List[DIDData]] = integration.listDIDs(gatewayId)
    def listDIDsByProvider(providerId: String): Try[List[DIDData]] = integration.listDIDsByProvider(providerId)   
    
    def updateCDRSummaryTable(contract: UpdateCDRSummaryContractTable): Try[Unit] =integration.updateCDRSummaryTable(contract)

    def retrieveCDRSummary(contract:CDRSummaryContract):Try[CDRSummaryData]=integration.retrieveCDRSummary(contract)
    def retrieveCDRSummaryByHour(contract:CDRSummaryContract):Try[List[CDRSummaryByHourData]] =integration.retrieveCDRSummaryByHour(contract)
    
    def retrieveBestMatchDestination(number:String):Try[Option[(Int,String)]]=integration.retrieveBestMatchDestination(number)
    def retrieveBestMatchDestinationCountryCode(number:String):Try[Option[String]]=integration.retrieveBestMatchDestinationCountryCode(number)
    def retrieveBestMatchDestinationAll(number:String):Try[List[Int]] = integration.retrieveBestMatchDestinationAll(number)
    def getDID(countryId:String) : Try[Option[String]]=integration.getDID(countryId)
    
    def listProviderGateways(): Try[List[ProviderGatewayData]] = integration.listProviderGateways
    def deleteProviderGateway(id:Int): Try[Unit]  = integration.deleteProviderGateway(id)
    def updateProviderGateway(contract: UpdateProviderGatewayContract): Try[Unit]  = integration.updateProviderGateway(contract)
    def createProviderGateway(contract: CreateProviderGatewayContract): Try[Unit]  = integration.createProviderGateway(contract)   
    def toggleProviderGateway(id: Int): Try[Unit] = integration.toggleProviderGateway(id)   
    
    def retrieveRoute(destinationId:Int):Try[Option[ProviderGatewayData]] = integration.retrieveRoute(destinationId)
    
    def updateCDR(contract:UpdateCDRContract) = integration.updateCDR(contract)
    def createCDR(contract:CreateCDRContract) = integration.createCDR(contract)


    def incrementDIDHitCount(did:String) = integration.incrementDIDHitCount(did)
    def decrementDIDHitCount(did:String) = integration.decrementDIDHitCount(did)

    
    def resetGatewayHitCount(gatewayId:Int) = integration.resetGatewayHitCount(gatewayId)
    def getAccountCalls(accountId:String):Try[List[Option[CDRAccountData]]] = integration.getAccountCalls(accountId)    
    
    def listRoutes(destinationId:Int):Try[List[RouteData]]=integration.listRoutes(destinationId)
    def addRoute(contract: RouteContract): Try[Unit]= integration.addRoute(contract)
    def deleteRoute(contract: RouteContract): Try[Unit]= integration.deleteRoute(contract)
    def toggleRoute(contract: RouteContract): Try[Unit]= integration.toggleRoute(contract)
    
    def retrieveLocations(searchString:String): Try[List[LocationData]] = integration.retrieveLocations(searchString)
    def retrieveLocationsFromZip(zipCode:String): Option[List[Int]]=integration.retrieveLocationsFromZip(zipCode:String): Option[List[Int]]
  }
  
}