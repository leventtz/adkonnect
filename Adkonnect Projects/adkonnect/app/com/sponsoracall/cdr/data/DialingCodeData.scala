package com.sponsoracall.cdr.data

case class DialingCodeData(da:String,destinationId:Int) {
  require(destinationId>0,"Missing destinationId")
  require(da!=null && da.length>0,"Missing DA");

  import play.api.libs.json._
  val jsonWriter=Json.writes[DialingCodeData]
  override def toString={
    jsonWriter.writes(this).toString
  }    
  
}