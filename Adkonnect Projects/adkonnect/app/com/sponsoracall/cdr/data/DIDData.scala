package com.sponsoracall.cdr.data

case class DIDData(DID:String,gatewayId:Int,providerId:String) {
  require(DID!=null && DID.length()!=0,"DID required")
  
  import play.api.libs.json._
  val jsonWriter=Json.writes[DIDData]
  override def toString={
    jsonWriter.writes(this).toString
  }   
}