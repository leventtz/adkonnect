package com.sponsoracall.gatewayaccount.data

case class AccountLocationData(accountId:String,destinationId:Int,destinationName:String,countryName:String) {
	require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
	require(destinationId>0,"Missing DestinationId")
	require(destinationName!=null && !destinationName.isEmpty(),"Missing Destination Name")
	require(countryName!=null && !countryName.isEmpty(),"Missing Country Name")
}