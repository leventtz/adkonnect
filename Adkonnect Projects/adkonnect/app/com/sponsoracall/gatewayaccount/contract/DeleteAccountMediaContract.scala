package com.sponsoracall.gatewayaccount.contract

case class DeleteAccountMediaContract(id:Int) {
  require(id>0,"Missing Id")
}