package com.sponsoracall.gatewayaccount.contract

case class UpdateUserContract(
  id:String,
  email: String,
  password: String,
  firstName: String="",
  lastName: String="",
  active: Boolean = true) {
  require(email != null && email.length() > 0, "Missing email") 
  require(email.matches(AccountContract.validEmailRegex), "Invalid Email")
}