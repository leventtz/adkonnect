package com.sponsoracall.gatewayaccount.contract

import java.sql.Date

object RateContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[RateContract]
  def parse(value: JsValue): Option[RateContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}


case class RateContract(accountId:String,destinationId:Int,rate:Float,rateTypeId:String){
  require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
  require(destinationId>0,"Missing Account Id")
  require(rate>=0,"Missing rate")
}

