package com.sponsoracall.gatewayaccount.contract

import java.sql.Date

object RegisterContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[RegisterContract]
  def parse(value: JsValue): Option[RegisterContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}


case class RegisterContract(firstName:String,lastName:String,phoneNumber:String,deviceId:String,email:String,password:String,country:String,birthDay:String,gender:String){
  require(firstName!=null && firstName.length!=0, "Required firstName");
  require(lastName!=null && lastName.length!=0, "Required lastName");
  require(phoneNumber!=null && phoneNumber.length!=0, "Required phoneNumber");
  require(email!=null && email.length!=0, "Required email");
  require(country!=null && country.length!=0, "Required country");
  require(password!=null && password.length!=0, "Required password");
  require(birthDay!=null, "Required birthDay");
  require(gender!=null && gender.length!=0, "Required gender");
}