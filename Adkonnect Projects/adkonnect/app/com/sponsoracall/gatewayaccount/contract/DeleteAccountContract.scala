package com.sponsoracall.gatewayaccount.contract

object DeleteAccountContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[DeleteAccountContract]
  def parse(jsonString: String): DeleteAccountContract = {
    jsonReader.reads(Json.parse(jsonString)).get
  }
}

case class DeleteAccountContract(accountId:String) {
  require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
}