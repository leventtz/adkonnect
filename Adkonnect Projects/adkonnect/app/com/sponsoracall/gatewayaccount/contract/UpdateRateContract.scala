package com.sponsoracall.gatewayaccount.contract

import java.sql.Date

object UpdateRateContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[UpdateRateContract]
  def parse(value: JsValue): Option[UpdateRateContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}


case class UpdateRateContract(id:Int,accountId:String,destinationId:Int,rate:Float,rateTypeId:String){
  require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
  require(destinationId>0,"Missing Account Id")
  require(rate>=0,"Missing rate")
}

