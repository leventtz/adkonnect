package com.sponsoracall.gatewayaccount.contract

object UpdateTemplateContract {
  import play.api.libs.json._
  val jsonReader = Json.reads[UpdateTemplateContract]
  def parse(value: JsValue): Option[UpdateTemplateContract] = {
    try {
      Some(jsonReader.reads(value).get)
    } catch {
      case t: Throwable => None
    }
  }
}

case class UpdateTemplateContract(id:Int,name:String,description:String,mediaId:Int,maxDuration:Int,templateDefaultQty:Int,templateUnitPrice:BigDecimal,templatePromptCost:BigDecimal,byPassSwitch:Boolean) {
  require(name!=null && !name.isEmpty(),"name required")
  require(description!=null && !description.isEmpty(),"description required")
}