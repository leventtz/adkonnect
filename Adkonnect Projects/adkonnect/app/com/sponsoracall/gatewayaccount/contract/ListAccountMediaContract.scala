package com.sponsoracall.gatewayaccount.contract

case class ListAccountMediaContract(accountId:String) {
	require(accountId!=null && accountId.length()!=0,"Invalid AccountId")
}