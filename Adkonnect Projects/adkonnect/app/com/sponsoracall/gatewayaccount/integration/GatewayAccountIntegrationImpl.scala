package com.sponsoracall.gatewayaccount.integration

import java.util.HashMap
import scala.slick.jdbc.GetResult
import scala.slick.jdbc.{StaticQuery => Q}
import scala.util.Failure
import scala.util.Success
import scala.util.Try
import org.slf4j.LoggerFactory
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue
import com.amazonaws.services.dynamodbv2.model.GetItemRequest
import com.amazonaws.services.dynamodbv2.model.GetItemResult
import com.amazonaws.services.dynamodbv2.model.PutItemRequest
import com.sponsoracall.db.DynamoDBEngine
import com.sponsoracall.db.PostgresDBEngine
import com.sponsoracall.exception.EmailExistException
import com.sponsoracall.exception.PhoneExistException
import com.sponsoracall.exception.WrongPasswordException
import com.sponsoracall.gatewayaccount.contract._
import com.sponsoracall.gatewayaccount.data._
import com.sponsoracall.util.MD5Digest
import com.sponsoracall.gatewayaccount.contract.UpdateTemplateContract
import com.sponsoracall.gatewayaccount.contract.CreateTemplateContract


class GatewayAccountIntegrationImpl extends GatewayAccountIntegration {
  val log = LoggerFactory.getLogger(getClass())
  val SELECT_ACCOUNT = "select id,accountType,companyName,firstName,middleName,lastName,address1,address2,city,state,postalcode,country,email,phone1,phone2,birthday,gender,active from account "
  val SELECT_RATE = "select id,accountId,(select companyname from account where account.id=accountId) as companyName, destinationId,(select name from destination where destination.id=destinationId) destinationName,rate,rateTypeid from rate"
  implicit val accountDataResult = GetResult(r => AccountData(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))
  implicit val accountMediaDataResult = GetResult(r => AccountMediaData(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))
  implicit val rateDataResult = GetResult(r => RateData(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))


  override def deleteAccount(contract: DeleteAccountContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("delete from account where id=?")
          ps.setString(1, contract.accountId)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }

  def addMobileUserAccount(contract:AddAccountContract): Try[Option[String]] = {
    try {
    val putItemRequest=new PutItemRequest().withTableName("MobileUser")
    val expectedPhone=new HashMap[String,ExpectedAttributeValue]();
    expectedPhone.put("phonenumber", new ExpectedAttributeValue().withExists(false))
    putItemRequest.setExpected(expectedPhone)
    putItemRequest.addItemEntry("phonenumber", new AttributeValue(contract.phone1))
    putItemRequest.addItemEntry("accountData", new AttributeValue(contract.toString))
    DynamoDBEngine.client.putItem(putItemRequest)
    Success(Some(contract.id))
    }catch {
      case t:Throwable => Failure(new PhoneExistException(contract.phone1))
    }
  }  
  
  
  override def addAccount(account: AddAccountContract): Try[Option[String]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("insert into Account(id,accounttype,companyName,firstName,middleName,lastname,address1,address2,city,state,postalcode,country,email,phone1,phone2,password,birthDay,gender,active) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
          ps.setString(1, account.id)
          ps.setString(2, account.accountType)
          ps.setString(3, account.companyName)
          ps.setString(4, account.firstName)
          ps.setString(5, account.middleName)
          ps.setString(6, account.lastName)
          ps.setString(7, account.address1)
          ps.setString(8, account.address2)
          ps.setString(9, account.city)
          ps.setString(10, account.state)
          ps.setString(11, account.postalCode)
          ps.setString(12, account.country)
          ps.setString(13, account.email.toLowerCase())
          ps.setString(14, account.phone1)
          ps.setString(15, account.phone2)
          ps.setString(16, MD5Digest.digest(account.password))
          ps.setString(17, account.birthday)
          ps.setString(18, account.gender)
          ps.setBoolean(19, false)
          ps.execute()
          Success(Some(account.id))
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            if (t.getMessage().contains(account.email)) {
              Failure(new EmailExistException(account.email))
            } else if (t.getMessage().contains(account.phone1)) {
              Failure(new PhoneExistException(account.phone1))
            } else {
              Failure(t)
            }

          }
        }
    }
  }

  override def updateAccount(account: UpdateAccountContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update Account set accounttype=?,companyName=?,firstName=?,middleName=?,lastname=?,address1=?,address2=?,city=?,state=?,postalcode=?,country=?,email=?,phone1=?,phone2=?,active=?,birthDay=?,gender=? where id=?")
          ps.setString(1, account.accountType)
          ps.setString(2, account.companyName)
          ps.setString(3, account.firstName)
          ps.setString(4, account.middleName)
          ps.setString(5, account.lastName)
          ps.setString(6, account.address1)
          ps.setString(7, account.address2)
          ps.setString(8, account.city)
          ps.setString(9, account.state)
          ps.setString(10, account.postalCode)
          ps.setString(11, account.country)
          ps.setString(12, account.email.toLowerCase())
          ps.setString(13, account.phone1)
          ps.setString(14, account.phone2)
          ps.setBoolean(15, account.active)
          ps.setString(16, account.birthday)
          ps.setString(17, account.gender)
          ps.setString(18, account.id)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            if (t.getMessage().contains(account.email)) {
              Failure(new EmailExistException(account.email))
            } else if (t.getMessage().contains(account.phone1)) {
              Failure(new PhoneExistException(account.phone1))
            } else {
              Failure(t)
            }

          }
        }
    }
  }


  def toogleAccount(id:String):Try[Unit] = {
        PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update account set active=not active where id=?")
          ps.setString(1, id)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }
  
  override def activateAccount(contract: ActivateAccountContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update Account set active=true where id=?")
          ps.setString(1, contract.accountId)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }
  
  def loginMobileUser(contract: LoginContract): Try[Option[AccountData]] = {
	  try {
	    val key=new java.util.HashMap[String,com.amazonaws.services.dynamodbv2.model.AttributeValue]();
	    key.put("phonenumber", new AttributeValue(contract.emailPhone))
	    val getItemRequest:GetItemRequest=new GetItemRequest().withTableName("MobileUser").withKey(key)
	    
	    val getItemResult:GetItemResult=DynamoDBEngine.client.getItem(getItemRequest)
	    val item=getItemResult.getItem
	    if(item!=null) {
	      Success(AccountData.parse(item.get("accountData").getS))
	    } else {	
	      Success(None)
	    }
	  } catch {
	    case t:Throwable => Failure(t)
	  }
  }

  override def login(contract: LoginContract): Try[Option[AccountData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val emailPhone = contract.emailPhone
          val password = MD5Digest.digest(contract.password)
          val selectString = s"$SELECT_ACCOUNT where (email = '$emailPhone' or phone1 = '$emailPhone') and password='$password' "
          val accountDataOption = Q.queryNA[AccountData](selectString).firstOption
          accountDataOption match {
            case Some(accountData) => Success(accountDataOption)
            case None => Success(None)
          }
        } catch {
          case t: Throwable => {
            Failure(t)
          }
        }
    }
  }

  override def addAccountMedia(contract: AddAccountMediaContract): Try[Option[AccountMediaData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("insert into AccountMedia(accountId,description,mediatypeid,approved) values(?,?,?,?) returning id")
          ps.setString(1, contract.accountId)
          ps.setString(2, contract.description)
          ps.setString(3, contract.mediaTypeId)
          ps.setBoolean(4, contract.approved)
          val resultSet = ps.executeQuery();
          resultSet.next
          val id = resultSet.getInt(1);
          retrieveAccountMedia(RetrieveAccountMediaContract(id))
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }
  override def updateAccountMedia(contract: UpdateAccountMediaContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update AccountMedia set description=?,mediatypeid=?,approved=? where id=?")
          ps.setString(1, contract.description)
          ps.setString(2, contract.mediaTypeId)
          ps.setBoolean(3, contract.approved)
          ps.setInt(4, contract.id)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }

  override def updateAccountMediaLocation(contract: UpdateMediaLocationContract) {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update AccountMedia set mediaLocation=? where id=?")
          ps.setString(1, contract.mediaLocation)
          ps.setInt(2, contract.mediaId)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }

  override def deleteAccountMedia(contract: DeleteAccountMediaContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("delete from accountMedia  where id=?")
          ps.setInt(1, contract.id)
          ps.execute()
          Success(Unit)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }

  override def retrieveAccountMedia(contract: RetrieveAccountMediaContract): Try[Option[AccountMediaData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val id = contract.id.toString
          val selectString = s"select * from AccountMedia where id=$id"
          Success(Q.queryNA[AccountMediaData](selectString).firstOption)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }

  override def listAccountMedia(contract: ListAccountMediaContract): Try[List[AccountMediaData]] = {
    val accountId = contract.accountId
    PostgresDBEngine.listData[AccountMediaData](s"select * from AccountMedia where accountId='$accountId'",
      accountMediaDataResult)
  }

  override def listUnApprovedAccountMedia(): Try[List[AccountMediaData]] = {
    PostgresDBEngine.listData[AccountMediaData](s"select * from AccountMedia where approved=false",
      accountMediaDataResult)
  }


  override def addRate(contract: RateContract): Try[Option[RateData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("insert into rate(accountId,destinationId,rate,rateTypeid) values(?,?,?,?) returning id")
          ps.setString(1, contract.accountId)
          ps.setInt(2, contract.destinationId)
          ps.setDouble(3, contract.rate)
          ps.setString(4, contract.rateTypeId)
          val rs = ps.executeQuery();
          rs.next()
          val id = rs.getInt(1)
          retrieveRate(id)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }

  override def updateRate(contract: UpdateRateContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update rate set accountId=?,destinationId=?,rate=?,rateTypeid=? where id=?")
          ps.setString(1, contract.accountId)
          ps.setInt(2, contract.destinationId)
          ps.setFloat(3, contract.rate)
          ps.setString(4, contract.rateTypeId)
          ps.setInt(5, contract.id)
          Success(())
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }

  override def deleteRate(contract: DeleteRateContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("delete from rate where id=?")
          ps.setInt(1, contract.id)
          ps.execute();
          Success(Unit)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }

  override def retrieveRate(id: Int): Try[Option[RateData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          Success(Q.queryNA[RateData](s"$SELECT_RATE where id=" + id).firstOption)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }

  override def listRateByDestination(destinationId: Int): Try[List[RateData]] = {
    PostgresDBEngine.listData[RateData](s"$SELECT_RATE where destinationId=" + destinationId,
      rateDataResult)
  }

  override def retriveBuyRateByDestinationProvider(destinationId: Int, providerId: String): Try[Option[RateData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          Success(Q.queryNA[RateData](s"$SELECT_RATE where ratetypeid='B' and accountId='$providerId' and destinationId=" + destinationId).firstOption)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }

  }

  override def retrieveSellRateByDestination(destinationId: Int): Try[Option[RateData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          Success(Q.queryNA[RateData](s"$SELECT_RATE where ratetypeid='S' and accountId='1' and destinationId=" + destinationId).firstOption)
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }

  }
  
  

  override def listRateByAccount(contract: ListRateByAccountContract): Try[List[RateData]] = {
    val accountId = contract.accountId
    PostgresDBEngine.listData[RateData](s"$SELECT_RATE where accountId='$accountId'",
      rateDataResult)
  }

  override def listActiveProviders(): Try[List[AccountData]] = {
    PostgresDBEngine.listData[AccountData](s"$SELECT_ACCOUNT where active=true and accounttype='P' ", accountDataResult)
  }

  def listAllProviders(): Try[List[AccountData]] = {
    PostgresDBEngine.listData[AccountData](s"$SELECT_ACCOUNT where accounttype='P' ", accountDataResult)
  }
  
  
  def addUser(contract:AddUserContract):Try[Option[String]]  = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("insert into ToolUser(id,firstName,lastname,email,password,active) values(?,?,?,?,?,?)")
          ps.setString(1, contract.id)
          ps.setString(2, contract.firstName)
          ps.setString(3, contract.lastName)
          ps.setString(4, contract.email)
          ps.setString(5, MD5Digest.digest(contract.password))
          ps.setBoolean(6, true)
          ps.execute()
          Success(Some(contract.id))
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            if (t.getMessage().contains(contract.email)) {
              Failure(new EmailExistException(contract.email))
            } else {
              Failure(t)
            }
          }
        }
    }
  }

  def updateUser(contract:UpdateUserContract): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update ToolUser set firstName=?,lastname=?,email=?,password=?,active=? where id=?")
          ps.setString(1, contract.firstName)
          ps.setString(2, contract.lastName)
          ps.setString(3, contract.email)
          ps.setString(4, MD5Digest.digest(contract.password))
          ps.setBoolean(5, true)
          ps.setString(6, contract.id)
          ps.execute()
          Success(())
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            if (t.getMessage().contains(contract.email)) {
              Failure(new EmailExistException(contract.email))
            } else {
              Failure(t)
            }
          }
        }
    }
  }
  
  def toggleUser(id:String): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update ToolUser set active=not active where id=?")
          ps.setString(1, id)
          ps.execute()
          Success(())
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }
  
  def deleteUser(id:String): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("delete from ToolUser where id=?")
          ps.setString(1, id)
          ps.execute()
          Success(())
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }   
  
  def loginUser(contract:LoginContract):Try[Option[String]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val emailPhone = contract.emailPhone
          val password = MD5Digest.digest(contract.password)
          val selectString = s"Select id from tooluser where (email = '$emailPhone') and password='$password' and active=true"
          val dataOption = Q.queryNA[String](selectString).firstOption
          dataOption match {
            case Some(data) => {
              Success(dataOption)
            }
            case None => {
              Failure(new IllegalArgumentException("Invalid Email"))
            }
          }
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }

  def listTemplates(searchString: Option[String]): Try[List[AdTemplateData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          implicit val templateData = GetResult(r => AdTemplateData(r<<, r<<, r<<, r<<, r<<, r<<, r<<, r<<, r<<, r<<))
          val selectFrom = "select ad.id,name,ad.description,medialocation,maxDuration,templateUnitPrice,templateDefaultQty,templatepromptcost,bypassswitch,ad.active from ad,accountmedia ";
          def firstTen = Success(Q.queryNA[AdTemplateData](s"$selectFrom where ad.mediaid=accountmedia.id and ad.accountId='1' and ad.active=true order by name").list)

          searchString match {
            case None => firstTen
            case Some(searchString) =>
              Q.queryNA[AdTemplateData](s"$selectFrom where ad.mediaid=accountmedia.id and ad.accountId='1' and UPPER(name) like UPPER('$searchString%') and ad.active=true order by name").list match {
                case Nil => firstTen
                case l @ x :: xss => Success(l)
              }
          }

        } catch {
          case t: Throwable => {
            Failure(t)
          }
        }
    }
  }  
  
  def listAllTemplates(searchString: Option[String]): Try[List[AdTemplateData]] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          implicit val templateData = GetResult(r => AdTemplateData(r<<, r<<, r<<, r<<, r<<, r<<, r<<, r<<, r<<, r<<))
          val selectFrom = "select ad.id,name,ad.description,medialocation,maxDuration,templateUnitPrice,templateDefaultQty,templatepromptcost,bypassswitch,ad.active from ad,accountmedia ";
          def firstTen = Success(Q.queryNA[AdTemplateData](s"$selectFrom where ad.mediaid=accountmedia.id and ad.accountId='1' order by name").list)

          searchString match {
            case None => firstTen
            case Some(searchString) =>
              Q.queryNA[AdTemplateData](s"$selectFrom where ad.mediaid=accountmedia.id and ad.accountId='1' and UPPER(name) like UPPER('$searchString%') order by name").list match {
                case Nil => firstTen
                case l @ x :: xss => Success(l)
              }
          }

        } catch {
          case t: Throwable => {
            Failure(t)
          }
        }
    }
  }  
  
  
  
  def toggleTemplate(id:Int): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update ad set active=not active where id=?")
          ps.setInt(1, id)
          ps.execute()
          Success(())
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }

  def toggleBypassSwitch(id:Int): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update ad set bypassswitch=not bypassswitch where id=?")
          ps.setInt(1, id)
          ps.execute()
          Success(())
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }  
  
  def deleteTemplate(id:Int): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("delete from ad where id=?")
          ps.setInt(1, id)
          ps.execute()
          Success(())
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }
  
  def listTemplateDA(adId:Int):Try[List[TemplateDAData]] = {
      implicit val adDADataResult = GetResult(r => TemplateDAData(r.<<, r.<<, r.<<, r.<<))
	  PostgresDBEngine.listData[TemplateDAData]("select adId,destinationId,rate,exclude from adDa where adId="+adId, adDADataResult)
  }
  
  def listTemplateDAWithDestination(adId:Int):Try[List[ListTemplateDAData]] = {
      implicit val adDADataResult = GetResult(r => ListTemplateDAData(r.<<, r.<<, r.<<, r.<<, r.<<))
	  PostgresDBEngine.listData[ListTemplateDAData]("select adId,destinationId,destination.name,rate,exclude from adDa,destination where adDa.destinationid=destination.id and adId="+adId, adDADataResult)
  }  
  
  def addTemplateDa(contract:TemplateDAData): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("insert into adda values(?,?,?,?)")
          ps.setInt(1, contract.adId);
          ps.setInt(2, contract.destinationId);
          ps.setDouble(3, contract.rate.toDouble);
          ps.setBoolean(4, contract.exclude);
          ps.execute()
          Success(())
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }    
  }
  
  def removeTemplateDa(adId:Int,destinationId:Int): Try[Unit] = {
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("delete from adda where adid=? and destinationId=?")
          ps.setInt(1, adId)
          ps.setInt(2, destinationId)
          ps.execute()
          Success(())
        } catch {
          case t: Throwable => {
            log.error(t.getMessage(), t)
            Failure(t)
          }
        }
    }
  }
  
  
  def createTemplate(contract:CreateTemplateContract):Try[Option[Int]]={
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("insert into ad(accountId,name,description,mediaId,maxDuration,templateDefaultQty,templateUnitPrice,templatePromptCost,bypassSwitch,active) values('1',?,?,?,?,?,?,?,?,false) RETURNING id ")
          ps.setString(1, contract.name)
          ps.setString(2, contract.description)
          ps.setInt(3, contract.mediaId)
          ps.setInt(4, contract.maxDuration)
          ps.setInt(5, contract.templateDefaultQty)
          ps.setDouble(6, contract.templateUnitPrice.toDouble)
          ps.setDouble(7, contract.templatePromptCost.toDouble)
          ps.setBoolean(8,contract.byPassSwitch)
          val rs=ps.executeQuery();
          rs.next()
          Success(Some(rs.getInt(1)))
        } catch {
          case t: Throwable => Failure(t)
        }
    }    
  }
  
  def updateTemplate(contract:UpdateTemplateContract):Try[Unit]={
    PostgresDBEngine.database.withSession {
      implicit session =>
        try {
          val ps = session.prepareStatement("update ad set name=?,description=?,mediaId=?,maxDuration=?,templateDefaultQty=?,templateUnitPrice=?,templatePromptCost=?,bypassswitch=? where id=?")
          ps.setString(1, contract.name)
          ps.setString(2, contract.description)
          ps.setInt(3, contract.mediaId)
          ps.setInt(4, contract.maxDuration)
          ps.setInt(5, contract.templateDefaultQty)
          ps.setDouble(6, contract.templateUnitPrice.toDouble)
          ps.setDouble(7, contract.templatePromptCost.toDouble)
          ps.setBoolean(8, contract.byPassSwitch)
          ps.setInt(9,contract.id)
          ps.execute()
          Success(())
        } catch {
          case t: Throwable => Failure(t)
          
        }
    }    
  }
  
  
}