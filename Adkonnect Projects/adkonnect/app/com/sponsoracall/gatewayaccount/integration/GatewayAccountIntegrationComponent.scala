package com.sponsoracall.gatewayaccount.integration


trait GatewayAccountIntegrationComponent {
  val integration: GatewayAccountIntegration
}