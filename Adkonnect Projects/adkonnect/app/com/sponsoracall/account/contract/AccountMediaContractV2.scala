package com.sponsoracall.account.contract

case class AccountMediaContractV2(accountId: String, mediaId: String) {
  require(accountId != null && accountId.length() > 0, "Invalid AcccountId");
  require(mediaId != null && mediaId.length() > 0, "Invalid mediaId");
}