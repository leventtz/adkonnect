package com.sponsoracall.account.contract

import play.api.libs.functional.syntax.functionalCanBuildApplicative
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.JsValue

object ChangePasswordContractV2 {
  import play.api.libs.json._
  val jsonReader = Json.reads[ChangePasswordContractV2]
  def parse(value: JsValue): ChangePasswordContractV2 = {
    jsonReader.reads(value).get
  }
}
case class ChangePasswordContractV2(id:String,oldPassword:String,newPassword:String) {
  require(id != null && id.length() > 0, "Missing Account Id")
  require(newPassword.matches(AccountContractV2.validPasswordRegex), "Invalid Password")
}