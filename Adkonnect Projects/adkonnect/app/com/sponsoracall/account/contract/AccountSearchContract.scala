package com.sponsoracall.account.contract


class AccountSearchContract(searchString:String)
case class EmailSearchContract(searchString:String) extends AccountSearchContract(searchString)
case class PhoneSearchContract(searchString:String) extends AccountSearchContract(searchString)
case class AccountIDSearchContract(searchString:String) extends AccountSearchContract(searchString)
case class CompanySearchContract(searchString:String) extends AccountSearchContract(searchString)
