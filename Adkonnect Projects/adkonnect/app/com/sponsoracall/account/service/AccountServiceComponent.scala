package com.sponsoracall.account.service

import com.sponsoracall.account.integration.AccountIntegrationComponent



trait AccountServiceComponent {
	self: AccountIntegrationComponent =>
	val service:AccountService
}