package com.sponsoracall.account.integration

import com.sponsoracall.account.data.AccountDataV2
import com.sponsoracall.account.contract.AccountMediaContractV2
import com.sponsoracall.account.contract.CompanySearchContract
import com.sponsoracall.account.contract.EmailSearchContract
import com.sponsoracall.account.data.AccountMediaDataV2
import com.sponsoracall.account.contract.AccountIDSearchContract
import com.sponsoracall.account.contract.PhoneSearchContract
import com.sponsoracall.account.contract.ChangePasswordContractV2
import com.sponsoracall.account.contract.AccountSearchContract
import scala.collection.mutable.ListBuffer
import com.sponsoracall.account.contract.AccountContractV2
import com.sponsoracall.db.DynamoDBEngine
import java.util.HashMap
import com.amazonaws.services.dynamodbv2.model.Condition
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest
import com.amazonaws.services.dynamodbv2.model.QueryRequest
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest
import scala.util.Failure
import scala.util.Try
import com.amazonaws.services.dynamodbv2.model.GetItemRequest
import java.util.ArrayList
import com.amazonaws.services.dynamodbv2.model.PutItemRequest
import scala.util.Success
import com.sponsoracall.exception.PhoneExistException
import com.sponsoracall.exception.UnknownAccount
import com.sponsoracall.util.MD5DigestV2
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.sponsoracall.exception.EmailExistException
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue
import com.amazonaws.services.dynamodbv2.model.AttributeAction
import java.util.Date

class AccountIntegrationImpl extends AccountIntegration{

  def addMobileUserAccount(contract:AccountContractV2): Try[Option[String]] = {
    try {
	    val putItemRequest=new PutItemRequest().withTableName("MobileUser")
	    val expectedPhone=new HashMap[String,ExpectedAttributeValue]();
	    expectedPhone.put("phonenumber", new ExpectedAttributeValue().withExists(false))
	    putItemRequest.setExpected(expectedPhone)
	    putItemRequest.addItemEntry("phonenumber", new AttributeValue(contract.phone1))
	    putItemRequest.addItemEntry("accountData", new AttributeValue(contract.toString))
	    putItemRequest.addItemEntry("email", new AttributeValue(contract.email))
	    putItemRequest.addItemEntry("registerdate", new AttributeValue(new Date().toString))
	    putItemRequest.addItemEntry("phonetype", new AttributeValue(contract.phone2))
	    DynamoDBEngine.client.putItem(putItemRequest)
    Success(Some(contract.id))
    }catch {
      case t:Throwable => Failure(new PhoneExistException(contract.phone1))
    }
  }  
  
  
  def deleteAccountAll(email: String) {
    require(email != null && email.length() > 0, "Email Required");
    val accountData = this.retrieveAccountByEmail(email) match {
      case Success(Some(accountData)) => {
        deleteEmail(email);
        deleteAccount(accountData.id);
      }
      case _ => Unit
    }

  }

  def deleteAccount(accountId: String) {
    val key = new HashMap[String, AttributeValue]();
    key.put("accountId", new AttributeValue(accountId));
    val deleteItemRequest = new DeleteItemRequest();
    deleteItemRequest.setKey(key);
    deleteItemRequest.setTableName("AccountTable");
    DynamoDBEngine.client.deleteItem(deleteItemRequest);
  }

  def addEmail(email: String, accountId: String): Try[Unit] = {
    require(email != null && email.length() > 0, "Email Required");
    require(accountId != null && accountId.length() > 0, "accountId Required");
    try {
      val expected = new HashMap[String, ExpectedAttributeValue];
      expected.put("email", new ExpectedAttributeValue().withExists(false))

      val putItemRequest = new PutItemRequest().withTableName("EmailTable").withExpected(expected);
      val item = new HashMap[String, AttributeValue];
      item.put("email", new AttributeValue(email));
      item.put("accountId", new AttributeValue(accountId));
      putItemRequest.setItem(item);
      DynamoDBEngine.client.putItem(putItemRequest);
      Success(())
    } catch {
      case t: Throwable => t.printStackTrace();Failure(t)
    }
  }

  def deleteEmail(email: String): Try[Unit] = {
    require(email != null && email.length() > 0, "Email Required");

    val key = new HashMap[String, AttributeValue]();
    key.put("email", new AttributeValue(email));
    val deleteItemRequest = new DeleteItemRequest();
    deleteItemRequest.setKey(key);
    deleteItemRequest.setTableName("EmailTable");
    DynamoDBEngine.client.deleteItem(deleteItemRequest);
    Success(())
  }

  def updateEmail(oldEmail: String, newEmail: String, accountId: String): Try[Unit] = {
    require(oldEmail != null && oldEmail.length() > 0, "Old Email Required");
    require(newEmail != null && newEmail.length() > 0, "New Email Required");
    require(accountId != null && accountId.length() > 0, "accountId Required");
    retrieveAccountByEmail(oldEmail) match {
      case Success(Some(accountData)) if accountData.id == accountId => {
        addEmail(newEmail, accountId) match {
          case Success(()) => {
            accountData.email = newEmail;
            updateAccountInternal(AccountContractV2(accountData.id,accountData.accountType,accountData.companyName,accountData.firstName,accountData.middleName,accountData.lastName,accountData.address1,accountData.address2,accountData.city,accountData.state,accountData.postalCode,accountData.country,accountData.email,accountData.phone1,accountData.phone2,accountData.password,accountData.birthday,accountData.gender));
            deleteEmail(oldEmail);
          }
          case Failure(t) => Failure(new EmailExistException(newEmail))
        }
      }
      case Success(None) => Failure(new Error("Unknown Email"))
      case Failure(t) => Failure(t)
    }
  }

  def getAccountId(email: String): Try[Option[String]] = {
    require(email != null && email.length() > 0, "Email Required");
    val key = new HashMap[String, AttributeValue]();
    key.put("email", new AttributeValue(email));
    val getItemRequest = new GetItemRequest().withTableName("EmailTable").withKey(key)
    val result = DynamoDBEngine.client.getItem(getItemRequest);
    if (result.getItem() == null) {
      Success(None);
    } else {
      val accountId = result.getItem().get("accountId").getS()
      Success(Some(accountId));
    }
  }

  private def addAccount(account: AccountContractV2): Try[String] = {
    try {
      val putItemRequest = new PutItemRequest().withTableName("AccountTable");
      val item = new HashMap[String, AttributeValue];
      item.put("accountId", new AttributeValue(account.id));
      item.put("active", new AttributeValue("0"));
      item.put("phone", new AttributeValue(account.phone1));
      item.put("companyName", new AttributeValue(account.companyName));
      item.put("accountData", new AttributeValue(account.toString));
      putItemRequest.setItem(item);
      DynamoDBEngine.client.putItem(putItemRequest);
      Success(account.id);
    } catch {
      case t: Throwable => Failure(t);
    }
  }

  def createAccount(account: AccountContractV2): Try[String] = {
    addEmail(account.email, account.id) match {
      case Success(()) => addAccount(account)
      case _ => Failure(new EmailExistException(account.email))
    }
  }

  def toogleAccount(accountId: String, active: String) = Try[Unit] {
    require(active != null && active.length() > 0, "status Required");
    require(accountId != null && accountId.length() > 0, "accountId Required");

    retrieveAccount(accountId) match {
      case Success(Some(accountData)) => {
        val key = new HashMap[String, AttributeValue]();
        key.put("accountId", new AttributeValue(accountData.id));
        val updateItems = new HashMap[String, AttributeValueUpdate]();
        updateItems.put("active", new AttributeValueUpdate().withValue(new AttributeValue().withN(active)).withAction(AttributeAction.PUT));
        val updateItemRequest = new UpdateItemRequest()
          .withTableName("AccountTable")
          .withKey(key)
          .withAttributeUpdates(updateItems);

        DynamoDBEngine.client.updateItem(updateItemRequest);
        Success(Unit);
      }
      case _ => Failure(new UnknownAccount);
    }
  }

  def retrieveAccountByEmail(email: String): Try[Option[AccountDataV2]] = {
    require(email != null && email.length() > 0, "email Required");
    getAccountId(email) match {
      case Success(Some(accountId)) => retrieveAccount(accountId)
      case s @ Success(None) => Success(None)
      case f @ Failure(t) => Failure(t)
    }
  }

  def retrieveAccount(accountId: String): Try[Option[AccountDataV2]] = { 
    require(accountId != null && accountId.length() > 0, "accountId Required");
    val key = new HashMap[String, AttributeValue]();
    key.put("accountId", new AttributeValue(accountId));
    val getItemRequest = new GetItemRequest().withTableName("AccountTable").withKey(key)
    val result = DynamoDBEngine.client.getItem(getItemRequest);
    if (result.getItem() == null) {
      Success(None);
    } else {
      val accountData = AccountDataV2.parse(result.getItem().get("accountData").getS());
      val numberActive = result.getItem().get("active").getN();
      accountData.get.active = if (numberActive == "1") true else false;
      Success(accountData);
    }
  }
 
  private def updateAccountInternal(accountData: AccountContractV2): Try[Unit] = {

    val key = new HashMap[String, AttributeValue]();
    key.put("accountId", new AttributeValue(accountData.id));
    val updateItemRequest = new UpdateItemRequest()
      .withTableName("AccountTable")
      .withKey(key)

    val attributeUpdates = new HashMap[String, AttributeValueUpdate]();
    attributeUpdates.put("accountData", new AttributeValueUpdate().withValue(new AttributeValue(accountData.toString)))
    attributeUpdates.put("phone", new AttributeValueUpdate().withValue(new AttributeValue(accountData.phone1)))
    attributeUpdates.put("companyName", new AttributeValueUpdate().withValue(new AttributeValue(accountData.companyName)))

    updateItemRequest.setAttributeUpdates(attributeUpdates)
    val result = DynamoDBEngine.client.updateItem(updateItemRequest);
    Success(Unit)
  }

  def updateAccount(contract: AccountContractV2): Try[Unit] = {
    retrieveAccount(contract.id) match{
      case Success(Some(account)) if account.email!=contract.email=> {
    	   updateEmail(account.email, contract.email, contract.id) match {
    	      case s @ Success(()) =>  updateAccountInternal(contract)
              case f @ Failure(t) => f
    	   }
      }
      case Success(Some(account)) if account.email==contract.email=> updateAccountInternal(contract)
      case Success(None) => Failure(new UnknownAccount)
      case f@Failure(t) => Failure(t)  
    }
  }

  def resetPassword(email: String, password: String): Try[Unit] = {
    require(email != null && email.length() > 0, "email Required");
    require(password != null && password.length() > 0, "password Required");
    retrieveAccountByEmail(email) match {
      case Success(Some(accountData)) => {
        accountData.password = password
        updateAccountInternal(AccountContractV2(accountData.id,accountData.accountType,accountData.companyName,accountData.firstName,accountData.middleName,accountData.lastName,accountData.address1,accountData.address2,accountData.city,accountData.state,accountData.postalCode,accountData.country,accountData.email,accountData.phone1,accountData.phone2,accountData.password,accountData.birthday,accountData.gender));
      }
      case _ => Failure(new Error("Unknown Email"))

    }
  }

  def changePassword(contract: ChangePasswordContractV2): Try[Unit] = {
    retrieveAccount(contract.id) match {
      case Success(Some(accountData)) if (accountData.password == MD5DigestV2.digest(contract.oldPassword)) => {
        accountData.password = MD5DigestV2.digest(contract.newPassword);
        val key = new HashMap[String, AttributeValue]();
        key.put("accountId", new AttributeValue(contract.id));
        val attributeUpdates = new HashMap[String, AttributeValueUpdate];
        attributeUpdates.put("accountData", new AttributeValueUpdate().withValue(new AttributeValue(accountData.toString)))
        val updateItemRequest = new UpdateItemRequest().withTableName("AccountTable").withKey(key).withAttributeUpdates(attributeUpdates);
        DynamoDBEngine.client.updateItem(updateItemRequest);
        Success(Unit)
      }
      case Success(Some(accountData)) if (accountData.password != MD5DigestV2.digest(contract.oldPassword)) => Failure(new Error("Unknown Password"))
      case Success(None) => Failure(new UnknownAccount)
      case Failure(t) => Failure(t)
    }
  }

  def search(contract: AccountSearchContract) {
    contract match {
      case EmailSearchContract(searchString: String) => searchByEmail(searchString)
      case PhoneSearchContract(searchString: String) => searchBy(searchString, "phoneNumber-index")
      case AccountIDSearchContract(searchString: String) => searchByAccountId(searchString)
      case CompanySearchContract(searchString: String) => searchBy(searchString, "companyName-index")
      case _ => Nil
    }
  }

  def searchBy(searchString: String, indexName: String): Try[List[AccountDataV2]] = {
    try {

      val keyConditions = new HashMap[String, Condition]();
      val condition = new Condition().withComparisonOperator(ComparisonOperator.EQ);
      val attributeValueList = new ArrayList[AttributeValue]();
      attributeValueList.add(new AttributeValue(searchString));
      condition.setAttributeValueList(attributeValueList)
      keyConditions.put("phoneNumber", condition)

      val queryRequest = new QueryRequest().withTableName("AccountTable").withIndexName(indexName)
      queryRequest.setKeyConditions(keyConditions)
      val result = DynamoDBEngine.client.query(queryRequest);
      val items = result.getItems()
      val list = new ListBuffer[AccountDataV2]
      for (i <- 0 until items.size()) {
        AccountDataV2.parse(items.get(i).get("accountData").getS()) match {
          case Some(data) => list.append(data)
          case None => {}
        }
      }
      Success(list.toList)
    } catch {
      case t: Throwable => Failure(t)
    }
  }

  def searchByEmail(searchString: String): Try[List[AccountDataV2]] = {
    retrieveAccountByEmail(searchString) match {
      case Success(Some(data)) => Success(List(data))
      case Success(None) => Success(Nil)
      case Failure(t) => Failure(t)
    }
  }

  def searchByAccountId(searchString: String): Try[List[AccountDataV2]] = {
    retrieveAccount(searchString) match {
      case Success(Some(data)) => Success(List(data))
      case Success(None) => Success(Nil)
      case Failure(t) => Failure(t)
    }
    //val queryRequest=new QueryRequest().withTableName("AccountTable").withIndexName(indexName)
  }

  def retrieveAccountMedia(contract: AccountMediaContractV2): Option[AccountMediaDataV2] = {
    val key = new HashMap[String, AttributeValue]();
    key.put("accountId", new AttributeValue(contract.accountId));
    key.put("mediaId", new AttributeValue(contract.mediaId));
    val getItemRequest = new GetItemRequest().withTableName("AccountMedia").withKey(key);
    val result = DynamoDBEngine.client.getItem(getItemRequest);
    val item = result.getItem()
    if (item != null) {
      val accountId = item.get("accountId").getS();
      val mediaId = item.get("mediaId").getS();
      val approved = item.get("approved").getS();
      val description = item.get("description").getS();
      val mediaLocation = item.get("mediaLocation").getS();
      val mediaTypeId = item.get("mediaTypeId").getS();
      Some(AccountMediaDataV2(accountId = accountId, mediaId = mediaId, description = description, mediaTypeId = mediaTypeId, mediaLocation = mediaLocation, approved = approved))
    } else {
      None
    }
  }

  def addAccountMedia(contract: AccountMediaDataV2) {
    val putItemRequest = new PutItemRequest().withTableName("AccountMedia");
    putItemRequest.addItemEntry("accountId", new AttributeValue(contract.accountId));
    putItemRequest.addItemEntry("mediaId", new AttributeValue(contract.mediaId));
    putItemRequest.addItemEntry("description", new AttributeValue(contract.description));
    putItemRequest.addItemEntry("mediaTypeId", new AttributeValue(contract.mediaTypeId));
    putItemRequest.addItemEntry("mediaLocation", new AttributeValue("N/A"));
    putItemRequest.addItemEntry("approved", new AttributeValue(contract.approved));
    DynamoDBEngine.client.putItem(putItemRequest);
  }

  def deleteAccountMedia(contract: AccountMediaContractV2) {
    val key = new HashMap[String, AttributeValue]();
    key.put("accountId", new AttributeValue(contract.accountId));
    key.put("mediaId", new AttributeValue(contract.mediaId));
    val deleteItemRequest = new DeleteItemRequest().withTableName("AccountMedia").withKey(key);
    DynamoDBEngine.client.deleteItem(deleteItemRequest);
  }

  def updateAccountMedia(contract: AccountMediaDataV2) {
    val key = new HashMap[String, AttributeValue]();
    key.put("accountId", new AttributeValue(contract.accountId));
    key.put("mediaId", new AttributeValue(contract.mediaId));

    val attributeUpdates = new HashMap[String, AttributeValueUpdate]();
    attributeUpdates.put("description", new AttributeValueUpdate().withValue(new AttributeValue(contract.description)))
    attributeUpdates.put("mediaTypeId", new AttributeValueUpdate().withValue(new AttributeValue(contract.mediaTypeId)))
    attributeUpdates.put("approved", new AttributeValueUpdate().withValue(new AttributeValue(contract.approved)))
    attributeUpdates.put("mediaLocation", new AttributeValueUpdate().withValue(new AttributeValue(contract.mediaLocation)))
    val updateItemRequest = new UpdateItemRequest().withTableName("AccountMedia").withKey(key).withAttributeUpdates(attributeUpdates);
    DynamoDBEngine.client.updateItem(updateItemRequest)
  }

  def listAccountMedia(accountId: String): List[AccountMediaDataV2] = {
    val queryRequest = new QueryRequest().withTableName("AccountMedia");
    val keyConditions = new HashMap[String, Condition]();
    val condition = new Condition().withComparisonOperator(ComparisonOperator.EQ);
    val attributeValueList = new ArrayList[AttributeValue]();
    attributeValueList.add(new AttributeValue(accountId));
    condition.setAttributeValueList(attributeValueList)
    keyConditions.put("accountId", condition)
    queryRequest.setKeyConditions(keyConditions)
    val result = DynamoDBEngine.client.query(queryRequest);
    val items = result.getItems()
    val list = new ListBuffer[AccountMediaDataV2]
    for (i <- 0 until items.size()) {
      val accountId = items.get(i).get("accountId").getS();
      val mediaId = items.get(i).get("mediaId").getS();
      val approved = items.get(i).get("approved").getS();
      val description = items.get(i).get("description").getS();

      val mediaLocation = items.get(i).get("mediaLocation").getS();
      val mediaTypeId = items.get(i).get("mediaTypeId").getS();
      list.append(AccountMediaDataV2(accountId = accountId, mediaId = mediaId, description = description, mediaTypeId = mediaTypeId, mediaLocation = mediaLocation, approved = approved))
    }

    list.toList
  }

}