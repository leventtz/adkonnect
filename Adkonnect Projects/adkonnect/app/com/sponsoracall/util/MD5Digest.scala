package com.sponsoracall.util

import java.security.MessageDigest
import org.apache.commons.codec.binary.Hex
import java.nio.charset.Charset

object MD5Digest {
	def digest(value:String):String={
		val messageDigest = MessageDigest.getInstance("MD5");
		messageDigest.reset();
		messageDigest.update(value.getBytes(Charset.forName("UTF8")));
		val resultByte = messageDigest.digest();
		new String(Hex.encodeHex(resultByte));	  
	}
}