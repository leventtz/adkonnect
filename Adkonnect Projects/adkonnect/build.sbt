name := """adkonnect"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  "org.scala-lang.modules" %% "scala-async"       % "0.9.2",
  "com.typesafe.play"   %% "play"               % "2.3.4",
  "com.typesafe.akka"   %% "akka-actor"         % "2.3.4",
  // ### Misc ###
  "org.scalaz"          %% "scalaz-core"        % "7.0.6",
  "com.typesafe"         % "config"             % "1.2.1",
  "org.typelevel"       %% "scalaz-contrib-210" % "0.2",
  // ### Database ###
  "com.typesafe.slick"  %% "slick"              % "2.1.0",
  "com.typesafe.slick"  %% "slick-codegen"      % "2.1.0",
  // "org.slf4j" % "slf4j-nop" % "1.6.4",
  "com.github.tminglei" %% "slick-pg"           % "0.6.3",
  "com.github.tminglei" %% "slick-pg_play-json" % "0.6.3",
  "org.liquibase"        % "liquibase-core"     % "3.2.2",
  "commons-pool"         % "commons-pool"       % "1.6",
  "commons-dbcp"         % "commons-dbcp"       % "1.4",
  // ### Testing ###
  "junit"                % "junit"              % "4.11"     % "test",
  "org.scalatest"       %% "scalatest"          % "2.2.1"    % "test",
  "org.scalatestplus"   %% "play"               % "1.2.0"    % "test",
  "org.scalacheck"      %% "scalacheck"         % "1.11.5"   % "test"
)

libraryDependencies += "com.amazonaws" % "aws-java-sdk" % "1.9.16"

libraryDependencies += "redis.clients" % "jedis" % "2.6.2"

libraryDependencies += "net.tanesha.recaptcha4j" % "recaptcha4j" % "0.0.7"

libraryDependencies += "net.authorize" % "anet-java-sdk" % "1.8.3"



