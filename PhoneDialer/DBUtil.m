//
//  DBUtil.m
//  Dialer
//
//  Created by Levent Tinaz on 5/6/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "DBUtil.h"

@implementation DBUtil

-(NSString *) filePath {
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [[paths objectAtIndex:0] stringByAppendingPathComponent:@"spc5.db"];
}

-(DBUtil *)init {
    self=[super init];
    if(self) {
        [self openDB];
        [self createDBObjects];
    }
    return self;
}

-(void)openDB {
    if(sqlite3_open([[self filePath] UTF8String], &db) != SQLITE_OK) {
        NSAssert(0,@"Database failed to open");
    } else {
        NSLog(@"database opened");
    }
    
}
-(void)closeDB {
    sqlite3_close(db);
}


-(void)createDBObjects {
    char *err;
    NSString *sqlUser=@"CREATE TABLE IF NOT EXISTS 'user' ('phone' TEXT PRIMARY KEY,'countryCode' TEXT,'device' TEXT,'email' TEXT,'postalCode' TEXT)";
    
    NSString *sqlCalls=@"CREATE TABLE IF NOT EXISTS 'calls' ('callDateTime' TEXT PRIMARY KEY,'dialNumber' TEXT,'adId' TEXT)";
    NSString *sqlAd=@"CREATE TABLE IF NOT EXISTS ad(adId TEXT PRIMARY KEY,companyName TEXT,mediaDescription TEXT,mediaLocation TEXT)";
    NSString *sqlCompanyIndex=@"CREATE INDEX IF NOT EXISTS ADIDINDEX on Ad(adid)";
    
    if(sqlite3_exec(db, [sqlUser UTF8String], NULL,NULL,&err) != SQLITE_OK) {
        sqlite3_close(db);
        NSAssert(0,@"Could not create table");
    } else {
        NSLog(@"user table created");
    }
    if(sqlite3_exec(db, [sqlCalls UTF8String], NULL,NULL,&err) != SQLITE_OK) {
        sqlite3_close(db);
        NSAssert(0,@"Could not create table");
    } else {
        NSLog(@"calls table created");
    }
    if(sqlite3_exec(db, [sqlAd UTF8String], NULL,NULL,&err) != SQLITE_OK) {
        sqlite3_close(db);
        NSAssert(0,@"Could not create calls table");
    } else {
        NSLog(@"ad table created");
    }
    if(sqlite3_exec(db, [sqlCompanyIndex UTF8String], NULL,NULL,&err) != SQLITE_OK) {
        sqlite3_close(db);
        NSAssert(0,@"Could not create company name index");
    } else {
        NSLog(@"index created");
    }
}

-(void)insertCallDateTime:(NSString *)callDateTime withDialNumber:(NSString *)dialNumber withAdId:(NSString *)adId {
    char *errMsg;
    NSString *sqlString=[NSString stringWithFormat:@"INSERT INTO CALLS(callDateTime,dialNumber,adId) values('%@','%@','%@')",callDateTime,dialNumber,adId];
    if(sqlite3_exec(db, [sqlString UTF8String], NULL, NULL, &errMsg) !=SQLITE_OK) {
        NSLog(@"Could not update table");
    } else {
        NSLog(@"Table Updated");
    }
}
-(void)insertAd:(NSString *)adId withCompanyName:(NSString *) companyName withMediaDescription:(NSString *)mediaDescription withMediaLocation:(NSString *)mediaLocation {
    if([adId isEqualToString:@"0"]) {
        return;
    }
    const char *sqlstatement = "INSERT INTO ad (adId, companyName,mediaDescription,mediaLocation) VALUES (?,?,?,?)";

    sqlite3_stmt *compiledstatement;
  
    if(sqlite3_prepare_v2(db,sqlstatement , -1, &compiledstatement, NULL)==SQLITE_OK)
    {
        sqlite3_bind_text(compiledstatement, 1, [adId UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledstatement, 2, [companyName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledstatement, 3, [mediaDescription UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledstatement, 4, [mediaLocation UTF8String], -1, SQLITE_TRANSIENT);
        
        NSInteger resultCode=sqlite3_step(compiledstatement);
        
        if(resultCode==SQLITE_DONE)
        {
            NSLog(@"done");
        }
        else
            NSLog(@"ERROR");
        
        sqlite3_reset(compiledstatement);
    }
    
}
-(NSArray *)getAllCalls {
    NSMutableArray *entries=[[NSMutableArray alloc] init];
    NSString *sql=@"Select callDateTime,dialNumber from calls order by callDateTime DESC";
    sqlite3_stmt *statement;
    if(sqlite3_prepare(db, [sql UTF8String],-1, &statement,nil)==SQLITE_OK){
        while(sqlite3_step(statement)==SQLITE_ROW) {
            char *callDateTime=(char *)sqlite3_column_text(statement,0);
            char *dialNumber=(char *)sqlite3_column_text(statement,1);
            NSArray *array=[NSArray arrayWithObjects:[NSString stringWithUTF8String:callDateTime],[NSString stringWithUTF8String:dialNumber], nil];
            [entries addObject:array];
        }
    }
    return entries;
}

-(NSArray *)getAllCompanies {
    NSMutableArray *entries=[[NSMutableArray alloc] init];
    NSString *sql=@"Select companyName from ad group by companyName order by companyName";
    sqlite3_stmt *statement;
    if(sqlite3_prepare(db, [sql UTF8String],-1, &statement,nil)==SQLITE_OK){
        while(sqlite3_step(statement)==SQLITE_ROW) {
            char *companyName=(char *)sqlite3_column_text(statement,0);
            if(companyName!=NULL)
               [entries addObject:[NSString stringWithUTF8String:companyName]];
        }
    }
    return entries;
}


-(NSArray *)getAd:(NSString *)name {
    NSMutableArray *entries=[[NSMutableArray alloc] init];
    NSString *sql=[NSString stringWithFormat:@"Select adId,companyName,mediaDescription,mediaLocation from ad where companyName='%@'",name];
    sqlite3_stmt *statement;
    if(sqlite3_prepare(db, [sql UTF8String],-1, &statement,nil)==SQLITE_OK){
        while(sqlite3_step(statement)==SQLITE_ROW) {
            char *adId=(char *)sqlite3_column_text(statement,0);
            char *companyName=(char *)sqlite3_column_text(statement,1);
            char *mediaDescription=(char *)sqlite3_column_text(statement,2);
            char *mediaLocation=(char *)sqlite3_column_text(statement,3);
            NSArray *array=[NSArray arrayWithObjects:[NSString stringWithUTF8String:adId],[NSString stringWithUTF8String:companyName],[NSString stringWithUTF8String:mediaDescription],[NSString stringWithUTF8String:mediaLocation],nil];
            [entries addObject:array];
        }
    }
    return entries;
    
}

-(void)deleteCall:(NSString *)callDateTime {
    NSString *sql=[NSString stringWithFormat:@"delete from calls where callDateTime='%@'",callDateTime];
    char *errMsg;
    if(sqlite3_exec(db, [sql UTF8String], NULL,NULL,&errMsg)==SQLITE_OK){
        NSLog(@"deleted call");
    } else {
        NSLog(@"Couldn't delete");
    }
}

-(void)deleteCompany:(NSString *)companyName {
    NSString *sql=[NSString stringWithFormat:@"delete from ad where companyName='%@'",companyName];
    char *errMsg;
    if(sqlite3_exec(db, [sql UTF8String], NULL,NULL,&errMsg)==SQLITE_OK){
        NSLog(@"deleted company");
    } else {
        NSLog(@"Couldn't delete");
    }
}
-(void)truncateAdTable {
    NSString *sql=[NSString stringWithFormat:@"delete from ad"];
    char *errMsg;
    if(sqlite3_exec(db, [sql UTF8String], NULL,NULL,&errMsg)==SQLITE_OK){
        NSLog(@"truncated table ad");
    } else {
        NSLog(@"Couldn't truncate table ad");
    }
    
}

-(void)truncateCallsTable {
    NSString *sql=[NSString stringWithFormat:@"delete from calls"];
    char *errMsg;
    if(sqlite3_exec(db, [sql UTF8String], NULL,NULL,&errMsg)==SQLITE_OK){
        NSLog(@"truncated table calls");
    } else {
        NSLog(@"Couldn't truncate table calls");
    }
    
}
+ (void)registerPhone:(NSString *)phone withCountryCode:(NSString *)countryCode withDeviceId:(NSString *)device withEmail:(NSString *)email withPostalCode:(NSString *)postalCode{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:phone forKey:@"phone"];
    [standardUserDefaults setObject:countryCode forKey:@"countryCode"];
    [standardUserDefaults setObject:device forKey:@"device"];
    [standardUserDefaults setObject:email forKey:@"email"];
    [standardUserDefaults setObject:postalCode forKey:@"postalCode"];
    [standardUserDefaults synchronize];
}

+ (NSDictionary *)getRegisterInfo {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    return (NSDictionary *)standardUserDefaults;
}



@end
