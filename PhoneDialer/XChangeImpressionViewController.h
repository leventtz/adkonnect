//
//  XChangeImpressionViewController.h
//  Dialer
//
//  Created by Levent Tinaz on 1/24/15.
//  Copyright (c) 2015 Levent Tinaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DialerUtil.h"
#import <MobFox/MobFox.h>
#import "AdData.h"
#import "DialerAdTask.h"
#import "DialerDialTask.h"

@interface XChangeImpressionViewController : UIViewController
@property (strong,nonatomic) AdData  *adData;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadIndicator;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *buttonContinue;
- (IBAction)continueCall:(id)sender;

@property(strong,nonatomic) NSString *strippedString;
@property(strong,nonatomic) NSThread *workerThread;
@property(strong,nonatomic) NSTimer *timeoutTimer;

@end
