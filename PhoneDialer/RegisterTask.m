//
//  RegisterTask.m
//  Dialer
//
//  Created by Levent Tinaz on 5/8/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "RegisterTask.h"
#import "DialerUtil.h"

@implementation RegisterTask



-(void)registerUserPhone:(NSString *)phoneNumber withCountryCode:(NSString *)countryCode withDeviceId:(NSString *)deviceId withEmail:(NSString *)email withPostalCode:(NSString *)postalCode {
    
    
    
    //[[NSUUID UUID] UUIDString];
    
    
    nsData=[[NSMutableData alloc] init];
    
    NSString *urlString=[DialerUtil urlString:@"deviceregister"];

    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];
    
    [request setHTTPMethod:@"POST"];
    
    id uniqueDeviceId=[DialerUtil getUniqueDeviceId:deviceId];
    
    NSString *body=[NSString stringWithFormat:@"country=%@&email=%@&phone=%@&device=%@&uniquedeviceid=%@&postalCode=%@",countryCode,email,phoneNumber,deviceId,uniqueDeviceId,postalCode];
    
    [request setHTTPBody:[NSData dataWithBytes:[body UTF8String] length:[body length]]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    
}

-(void)connection:(NSConnection *)connection didFailWithError:(NSError *)error {
    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Connection Failied" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];

    [alertView show];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSHTTPURLResponse* httpResponse=(NSHTTPURLResponse *)response;
    
    self.statusCode=[httpResponse statusCode];
    nsData=[[NSMutableData alloc] init];

}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [nsData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *) connection {
    
   //self.statusString = [NSString stringWithUTF8String:[nsData bytes]];
}
@end

