//
//  XChangeImpressionViewController.m
//  Dialer
//
//  Created by Levent Tinaz on 1/24/15.
//  Copyright (c) 2015 Levent Tinaz. All rights reserved.
//

#import "XChangeImpressionViewController.h"


@interface XChangeImpressionViewController ()


@end


@implementation XChangeImpressionViewController
int webViewLoadedCount=0;
@synthesize adData;
@synthesize webView;
@synthesize buttonContinue;
@synthesize workerThread;
@synthesize timeoutTimer;

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    webViewLoadedCount=0;
    buttonContinue.hidden=YES;
    
}

- (void)viewDidUnload {
    [super viewDidUnload];
    [self cancelAdTask];
    
}


-(void)loadImpressionsAndDID {
    [self stopIndicator];
    self.loadIndicator.hidden=YES;
    
    [self.loadIndicator startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    NSDictionary *dict=[DBUtil getRegisterInfo];
    NSString *countryCode=[dict valueForKey:@"countryCode"];
    NSString *email=[dict valueForKey:@"email"];
    NSString *phoneNumber=[dict valueForKey:@"phone"];
    NSString *deviceId=[dict valueForKey:@"device"];
    NSString *postalCode=[dict valueForKey:@"postalCode"];
    
    
    
    NSString *urlString=[DialerUtil urlString:@"imp"];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];
    
    [request setHTTPMethod:@"POST"];
    
    id uniqueDeviceId=[DialerUtil getUniqueDeviceId:deviceId];
    
    NSString *body=[NSString stringWithFormat:@"dial=%@&country=%@&email=%@&phone=%@&device=%@&uniquedeviceid=%@&postalCode=%@",self.strippedString,countryCode,email,phoneNumber,deviceId,uniqueDeviceId,postalCode];
    
    [request setHTTPBody:[NSData dataWithBytes:[body UTF8String] length:[body length]]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [self.webView loadRequest:request];
    
    
    workerThread = [[NSThread alloc] initWithTarget:self selector:@selector(callAdTask) object:nil];
    [workerThread start];
    timeoutTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:workerThread selector:@selector(cancelAdTask) userInfo:nil repeats:NO];
    
}

-(void)callAdTask {
    NSString *accountId=@"1";
    NSString *adId=@"1";
    
    DialerAdTask *adTask=[[DialerAdTask alloc] init];
    [adTask retrieveAd:accountId withAdId:adId withDialNumber:self.strippedString];
    while (adTask.adData==nil) {
        NSLog(@"CAll Service");
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        
    }
    
    [timeoutTimer invalidate];
    adData=adTask.adData;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if([adData.did isEqualToString:@""]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                            message:@"Destination,Country not allowed!"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }];
    
    
    
    
    
    
}

-(void)cancelAdTask {
    [timeoutTimer invalidate];
    if(workerThread!=NULL && !workerThread.isCancelled){
        NSLog(@"Cancelled");
        [workerThread cancel];
        
    }
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self loadImpressionsAndDID];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    buttonContinue.hidden=YES;
    
    if(webViewLoadedCount>0) {
        buttonContinue.hidden=NO;
    }
    
    webViewLoadedCount++;
    [self stopIndicator];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [self stopIndicator];
}


- (void)stopIndicator {
    self.loadIndicator.hidden=YES;
    [self.loadIndicator stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
}



- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if([[alertView title] isEqualToString:@"Sorry"]){
        [self.navigationController popViewControllerAnimated:YES];
    }
    if (buttonIndex == 1) {
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [standardUserDefaults setBool:YES forKey:@"dontShowDurationDialog"];
        [standardUserDefaults synchronize];
    }
    
}



- (IBAction)continueCall:(id)sender {
    
    if([[self.adData adCrypto] length]!=0){
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        BOOL dontShowDurationDialog=[standardUserDefaults valueForKey:@"dontShowDurationDialog"];
        if(!dontShowDurationDialog && self.adData.maxDuration>0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Call Duration"
                                                            message:[NSString stringWithFormat:@"You have %d minutes.",self.adData.maxDuration]
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:@"Don't Show Again",nil];
            [alert show];
            
        }
        
        NSString *serviceName=@"devicecall";
        if(self.adData.maxDuration==0){
            serviceName=@"devicecalbypass";
        }
        
        [[DialerDialTask alloc] initWithDialNumber:self.adData.dialNumber withCryptoAd:self.adData.adCrypto withServiceName:serviceName];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeStyle:NSDateFormatterMediumStyle];
        [dateFormat setDateStyle:NSDateFormatterMediumStyle];
        
        NSDate *now = [[NSDate alloc] init];
        DBUtil *db=[[DBUtil alloc] init];
        [db insertCallDateTime:[NSString stringWithFormat:@"%@",[dateFormat stringFromDate:now]] withDialNumber:self.adData.dialNumber withAdId:self.adData.adId];
        
        
        
        NSString *urlString=[NSString stringWithFormat:@"tel://%@",self.adData.did];
        if(self.adData.maxDuration==0) {
            urlString=[NSString stringWithFormat:@"tel://%@",self.adData.dialNumber];
        }
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        
    }
}
@end
