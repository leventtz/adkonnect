//
//  SPCViewController.h
//  Dialer
//
//  Created by Levent Tinaz on 5/5/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "XChangeImpressionViewController.h"
#import "DialerUtil.h"


@interface SPCViewController : UIViewController

@property (weak, nonatomic) IBOutlet  UILabel *dialLabel;
@property (strong,nonatomic) NSString *dialNumber;

-(IBAction)digit:(UIButton *)button;
-(IBAction)callNumber:(UIButton *)button;

@property (weak, nonatomic) IBOutlet UIButton *backspacebutton;
@property (weak, nonatomic) IBOutlet UIButton *zeroButton;


@end
