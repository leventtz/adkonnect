//
//  RecentCallsTableViewController.h
//  Dialer
//
//  Created by Levent Tinaz on 5/6/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBUtil.h"
#import "SPCViewController.h"
#import "ImpressionTask.h"
#import "ImpressionViewController.h"

@interface RecentCallsTableViewController : UITableViewController

@property(nonatomic,strong) NSArray *entries;

@end