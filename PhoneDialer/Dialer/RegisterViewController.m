//
//  RegisterViewController.m
//  Dialer
//
//  Created by Levent Tinaz on 5/5/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
    }
    return self;
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.loading.hidden=NO;
    [self.loading startAnimating];
    
    self.textCountryCode.enabled=NO;
    
    
    NSString *urlString=[DialerUtil urlString:@"devicecountries"];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];
    [request setHTTPMethod:@"GET"];
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    self.textPostalCode.delegate=self;
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)countryCodeEditingDone:(UITextField *)sender {
    if([self.textCountryCode.text hasPrefix:@"+"]){
        self.textCountryCode.text=[self.textCountryCode.text substringFromIndex:1];
    }
    
    
    for(int i=[countries count]-1;i>0;i--){
        NSArray *country=[countries objectAtIndex:i];
        NSString *countryCode=[country valueForKey:@"countryCode"];
        if([countryCode isEqualToString:self.textCountryCode.text]){
            [self.countryPicker selectRow:i inComponent:0 animated:YES];
            self.textCountryCode.text=[NSString stringWithFormat:@"+%@",self.textCountryCode.text];
            return;
        }
        
    }
    
    self.textCountryCode.text=@"1";
    
    
    [self countryCodeEditingDone:self.textCountryCode];
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



-(void)connection:(NSConnection *)connection didFailWithError:(NSError *)error {

    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Connection Failied" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alertView show];
    
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {

    nsdata=[[NSMutableData alloc] init];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [nsdata appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *) connection {
    NSLog(@"connectionDidFinishLoading");
    NSLog(@"Succeeded! Received %d bytes of data",[nsdata length]);
    
    // convert to JSON
    NSError *myError = nil;
    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:nsdata options:NSJSONReadingMutableContainers error:&myError];
    NSArray *result=[res objectForKey:@"countryList"];
    countries=[[NSMutableArray alloc] init];
    for(unsigned int i=0;i<[result count];i++) {
        NSDictionary *JSON =
        [NSJSONSerialization JSONObjectWithData: [[result objectAtIndex:i]
                                                  dataUsingEncoding: NSUTF8StringEncoding]
                                        options: NSJSONReadingMutableContainers
                                          error: &myError];
        [countries addObject:JSON];
        self.textCountryCode.text=[NSString stringWithFormat:@"+%@",[[countries objectAtIndex:0] valueForKey:@"countryCode"]];
        
    }
    [self.countryPicker reloadAllComponents];
    
    
    self.loading.hidden=YES;
    [self.loading stopAnimating];
    
    self.textCountryCode.enabled=NO;
    self.textCountryCode.text=@"1";
    
    
    [self countryCodeEditingDone:self.textCountryCode];
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [countries count];
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [[countries objectAtIndex:row] valueForKey:@"name"];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.textCountryCode.text=[NSString stringWithFormat:@"+%@",[[countries objectAtIndex:row] valueForKey:@"countryCode"]];
}


- (IBAction)SubmitAction:(UIButton *)sender {
    
    
    
    [self.loading startAnimating];
}



- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if([DialerUtil isNumber:self.textPhoneNumber.text]!=YES) {
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter valid phone number." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        return NO;
    }
    if([self.textPhoneNumber.text length]<10) {
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter your phone number." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        return NO;
    }
    if([DialerUtil IsValidEmail:self.textEmail.text] !=YES) {
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter valid email." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        return NO;
    }
    if([self.textPostalCode.text length]<5) {
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter your postal code." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        return NO;
    }
    [self.loading startAnimating];
    
    RegisterTask *registerTask=[[RegisterTask alloc] init];
    NSString *phone=self.textPhoneNumber.text;
    NSString *countryCode=[self.textCountryCode.text substringFromIndex:1];
    NSString *email=self.textEmail.text;
    NSString *deviceId=[[NSUUID UUID] UUIDString];
    NSString *postalCode=self.textPostalCode.text;
    
    
    [registerTask registerUserPhone:phone withCountryCode:countryCode withDeviceId:deviceId withEmail:email withPostalCode:postalCode];
    
    registerTask.statusCode=-1;
    while (registerTask.statusCode==-1) {
        // If A job is finished, a flag should be set. and the flag can be a exit condition of this while loop
        
        // This executes another run loop.
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        
    }
    
    [self.loading stopAnimating];
    self.loading.hidden=YES;
    if(registerTask.statusCode==409) {
        UIAlertView *view=[[UIAlertView alloc] initWithTitle:@"Error" message:@" The phone number or the postal code is not a valid number for the country you selected." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [view show];
    } else {
        
        [DBUtil registerPhone:phone withCountryCode:countryCode withDeviceId:deviceId withEmail:email withPostalCode:postalCode];
  
        return YES;
    }
    

    return NO;
    
    
}


@end
