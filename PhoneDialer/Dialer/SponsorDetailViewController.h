//
//  SponsorDetailViewController.h
//  Dialer
//
//  Created by Levent Tinaz on 5/7/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "DBUtil.h"
#import "AdData.h"
#import "DialerDialTask.h"
#import <AudioToolbox/AudioToolbox.h>

@interface SponsorDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;
@property (weak, nonatomic) IBOutlet UIButton *buttonThanks;

@property (strong, nonatomic)  AdData *adData;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *buttonOk;
- (IBAction)buttonOkAction:(UIButton *)sender;
@end
