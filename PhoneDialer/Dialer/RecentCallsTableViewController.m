//
//  RecentCallsTableViewController.m
//  Dialer
//
//  Created by Levent Tinaz on 5/6/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import "RecentCallsTableViewController.h"

@interface RecentCallsTableViewController ()

@end

@implementation RecentCallsTableViewController {
    NSString *strippedString;
    ImpressionTask *impressionTask;
}



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setContentInset:UIEdgeInsetsMake(20,
                                                     self.tableView.contentInset.left,
                                                     self.tableView.contentInset.bottom,
                                                     self.tableView.contentInset.right)];
    
    UILongPressGestureRecognizer *rec=[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    
    
    [self.tableView addGestureRecognizer:rec];
    
    
    
}
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    NSArray *array=[self.entries objectAtIndex:alertView.tag];
    NSString *value=[array objectAtIndex:0];
    DBUtil *db=[[DBUtil alloc] init];
    
    
    switch(buttonIndex) {
        case 1:
            [db deleteCall:value];
            break;
        case 2:
            [db truncateCallsTable];
            break;
    }
    
    self.entries=[db getAllCalls];
    [self.tableView reloadData];
    [db closeDB];
}


- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    if(gestureRecognizer.state==UIGestureRecognizerStateBegan){
        CGPoint p=[gestureRecognizer locationInView:self.tableView];
        NSIndexPath *indexPath=[self.tableView indexPathForRowAtPoint:p];
        if(indexPath!=nil){
            //DBUtil *db=[[DBUtil alloc] init];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Actions" message:nil
                                                           delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", @"Delete All",nil];
            [alert setTag:indexPath.row];
            [alert show];
        }
        
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    DBUtil *db=[[DBUtil alloc] init];
    
    
    
    self.entries=[db getAllCalls];
    
    
    [db closeDB];
    
    
    [self.tableView reloadData];
    self.tableView.hidden=NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.entries count];
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Recent Calls";
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *originalString=[[self.entries objectAtIndex:indexPath.row] objectAtIndex:1];
    strippedString=[DialerUtil stripString:originalString];
    [self performSegueWithIdentifier:@"ImpressionRecentCalls" sender:self];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier=@"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    cell.textLabel.text=[[self.entries objectAtIndex:indexPath.row] objectAtIndex:1];
    cell.detailTextLabel.text=[[self.entries objectAtIndex:indexPath.row] objectAtIndex:0];
    return cell;
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
     self.tableView.hidden=YES;
     XChangeImpressionViewController *controller=segue.destinationViewController;
     controller.strippedString=strippedString;
}



-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    return sender==self;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation

@end
