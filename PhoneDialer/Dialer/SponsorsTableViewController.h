//
//  SponsorsTableViewController.h
//  Dialer
//
//  Created by Levent Tinaz on 5/7/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBUtil.h"
#import "AdData.h"

@interface SponsorsTableViewController : UITableViewController
@property(nonatomic,strong) NSArray *entries;
@end
