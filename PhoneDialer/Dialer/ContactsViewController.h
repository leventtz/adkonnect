//
//  ContactsViewController.h
//  Dialer
//
//  Created by Levent Tinaz on 5/6/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "XChangeImpressionViewController.h"

@interface ContactsViewController : UIViewController <ABPeoplePickerNavigationControllerDelegate>
{
    NSString *dialNumber;
    ABPeoplePickerNavigationController *picker;
}

@end
