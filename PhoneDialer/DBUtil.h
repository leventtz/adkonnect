//
//  DBUtil.h
//  Dialer
//
//  Created by Levent Tinaz on 5/6/14.
//  Copyright (c) 2014 Levent Tinaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
@interface DBUtil : NSObject
{
    sqlite3 *db;
}


-(void)closeDB;


-(void)insertCallDateTime:(NSString *)callDateTime withDialNumber:(NSString *)dialNumber withAdId:(NSString *)adId;
-(void)insertAd:(NSString *)adId withCompanyName:(NSString *) companyName withMediaDescription:(NSString *)mediaDescription withMediaLocation:(NSString *)mediaLocation;
-(NSArray *)getAllCalls;
-(NSArray *)getAllCompanies;
-(NSArray *)getAd:(NSString *)companyName;
-(void)deleteCall:(NSString *)callDateTime;
-(void)truncateCallsTable;
-(void)deleteCompany:(NSString *)companyName;
-(void)truncateAdTable;

+(void)registerPhone:(NSString *)phone withCountryCode:(NSString *)countryCode withDeviceId:(NSString *)device withEmail:(NSString *)email withPostalCode:(NSString *)postalCode;

+(NSDictionary *)getRegisterInfo;
@end
