iptables -N SIPDOS

iptables -A INPUT -i eth0 -p udp -m udp --dport 5060 -m string --string "sundayddr" --algo bm --to 65535 -m comment --comment "deny sundayddr" -j SIPDOS
iptables -A INPUT -i eth0 -p udp -m udp --dport 5060 -m string --string "sipsak" --algo bm --to 65535 -m comment --comment "deny sipsak" -j SIPDOS
iptables -A INPUT -i eth0 -p udp -m udp --dport 5060 -m string --string "sipvicious" --algo bm --to 65535 -m comment --comment "deny sipvicious" -j SIPDOS
iptables -A INPUT -i eth0 -p udp -m udp --dport 5060 -m string --string "friendly-scanner" --algo bm --to 65535 -m comment --comment "deny friendly-scanner" -j SIPDOS
iptables -A INPUT -i eth0 -p udp -m udp --dport 5060 -m string --string "iWar" --algo bm --to 65535 -m comment --comment "deny iWar" -j SIPDOS
iptables -A INPUT -i eth0 -p udp -m udp --dport 5060 -m string --string "sip-scan" --algo bm --to 65535 -m comment --comment "deny sip-scan" -j SIPDOS

iptables -A INPUT -i eth0 -p tcp -m tcp --dport 5060 -m string --string "sundayddr" --algo bm --to 65535 -m comment --comment "deny sundayddr" -j SIPDOS
iptables -A INPUT -i eth0 -p tcp -m tcp --dport 5060 -m string --string "sipsak" --algo bm --to 65535 -m comment --comment "deny sipsak" -j SIPDOS
iptables -A INPUT -i eth0 -p tcp -m tcp --dport 5060 -m string --string "sipvicious" --algo bm --to 65535 -m comment --comment "deny sipvicious" -j SIPDOS
iptables -A INPUT -i eth0 -p tcp -m tcp --dport 5060 -m string --string "friendly-scanner" --algo bm --to 65535 -m comment --comment "deny friendly-scanner" -j SIPDOS
iptables -A INPUT -i eth0 -p tcp -m tcp --dport 5060 -m string --string "iWar" --algo bm --to 65535 -m comment --comment "deny iWar" -j SIPDOS
iptables -A INPUT -i eth0 -p udp -m udp --dport 5060 -m string --string "User-Agent: sipcli" --algo bm --to 65535 -m comment --comment "deny sipcli" -j SIPDOS
iptables -A INPUT -i eth0 -p tcp -m tcp --dport 5060 -m string --string "sip-scan" --algo bm --to 65535 -m comment --comment "deny sip-scan" -j SIPDOS
