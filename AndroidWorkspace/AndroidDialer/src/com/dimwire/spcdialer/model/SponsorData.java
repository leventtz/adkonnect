package com.dimwire.spcdialer.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SponsorData implements Serializable{
	public final String adPicture;
	public final String adwords;
	public SponsorData(String adPicture,String adwords){
		this.adPicture=adPicture;
		this.adwords=adwords;
	}
}
