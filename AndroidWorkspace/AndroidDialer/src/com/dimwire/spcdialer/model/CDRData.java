package com.dimwire.spcdialer.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CDRData implements Serializable{
	public final SponsorData sponsorData;
	public final String phoneNumber;
	public CDRData(SponsorData sponsorData,String phoneNumber) {
		this.sponsorData=sponsorData;
		this.phoneNumber=phoneNumber;
	}
}
