package com.dimwire.spcdialer.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import android.app.Activity;

@SuppressWarnings("serial")
public class DialerData implements Serializable {
	private static DialerData dialerData;
	
	public static final int NETWORK_ACTIVITY_NONE = 0;
	public static final int NETWORK_ACTIVITY = 1;
	public static final int NETWORK_ACTIVITY_DONE = 2;
	
	public static void clearDialerData() {
		dialerData = null;
	}
	public static DialerData getInstance(Activity activity) {
		if (dialerData == null) {
			try {
				ObjectInputStream inStream = new ObjectInputStream(
						new FileInputStream(new File(activity.getFilesDir(),
								"spdata.obj")));
				dialerData = (DialerData) inStream.readObject();
				inStream.close();
			} catch (Exception e) {
				dialerData = new DialerData();
			}
		}
		return dialerData;
	}

	public static void login(String userName, String password) {

	}

	public static boolean writeSPCData(Activity activity) {
		if (dialerData != null) {
			try {
				ObjectOutputStream outStream = new ObjectOutputStream(
						new FileOutputStream(new File(activity.getFilesDir(),
								"spdata.obj")));
				outStream.writeObject(dialerData);
				outStream.close();
				return true;
			} catch (Exception e) {
				return false;
			}
		} else {
			return false;
		}
	}
}
