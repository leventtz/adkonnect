package com.dimwire.spcdialer;

import static com.dimwire.spcdialer.util.DialerUtil.DIALER_PREF_NAME;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.dimwire.spcdialer.db.DBUtils;

public class CallsActivity extends ListActivity {
	private DBUtils dbUtils = new DBUtils(this);
	private List<Map<String, String>> callList=new ArrayList<Map<String, String>>();
	private SimpleAdapter adapter; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calls);
		findViewById(R.id.tableRowEmptyAd).setVisibility(View.INVISIBLE);
		
		callList = dbUtils.getAllCalls();
		
		if (callList.size() != 0) {
			((ViewManager)findViewById(R.id.tableRowEmptyAd).getParent()).removeView(findViewById(R.id.tableRowEmptyAd));
			ListView listView = getListView();
			listView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					Editor editor = getSharedPreferences(DIALER_PREF_NAME,
							Context.MODE_PRIVATE).edit();
					editor.putString("dial",
							callList.get(position).get("dialNumber"));
					editor.commit();
					
					Intent dialIntent = new Intent(getApplication(),
							ImpressionActivity.class);
					startActivity(dialIntent);				
				}
			});
		} else {
			
			findViewById(R.id.tableRowEmptyAd).setVisibility(View.VISIBLE);
		}

		setAdapter();

		this.getListView().setLongClickable(true);
		this.getListView().setOnItemLongClickListener(
				new OnItemLongClickListener() {
					public boolean onItemLongClick(AdapterView<?> parent,View v, final int position, long id) {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								CallsActivity.this);
						builder.setTitle(R.string.action);
						OnClickListener listener = new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								switch (which) {
								case 0:
									dbUtils.deleteCall(callList.get(position)
											.get("callDateTimeSystem"));
									callList.remove(position);
									break;
								case 1:
									dbUtils.deleteAllCalls();
									callList.clear();
									break;
								case 2:
									return;
								}
								setAdapter();
								if(callList.size()==0) {
									Intent dialIntent = new Intent(getApplication(),
											DialActivity.class);
									startActivity(dialIntent);
								}								
								
							}
						};

						CharSequence[] items = { getText(R.string.delete),
								getText(R.string.delete_all),
								getText(R.string.cancel) };
						builder.setItems(items, listener);
						builder.show();
						return true;

					}
				});

	}


	private void setAdapter() {
		adapter = new SimpleAdapter(CallsActivity.this,
				callList, R.layout.call_entry, new String[] { "callDateTime",
						"dialNumber" }, new int[] { R.id.textCallDateTime,
						R.id.textDialNumber });
		setListAdapter(adapter);
		
	}
}
