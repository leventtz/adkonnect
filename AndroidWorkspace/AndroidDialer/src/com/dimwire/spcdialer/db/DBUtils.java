package com.dimwire.spcdialer.db;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBUtils extends SQLiteOpenHelper{

	public DBUtils(Context context) {
		super(context,"spc5.db",null,1);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		String createTable= "Create Table calls (callDateTime TEXT PRIMARY KEY,dialNumber TEXT,adId TEXT)";
		db.execSQL(createTable);
		
		createTable= "Create Table Ad (adId TEXT PRIMARY KEY,companyName TEXT,mediaDescription TEXT,mediaLocation TEXT)";
		db.execSQL(createTable);		
		
		String createIndex="Create Index adIdIndex on Ad(adId)";
		
		db.execSQL(createIndex);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String dropTable="Drop TABLE IF EXISTS calls";
		db.execSQL(dropTable);	
		onCreate(db);
	}

	public void insertCall(Map<String,String> insertValues) {
		SQLiteDatabase database=this.getWritableDatabase();
		ContentValues values=new ContentValues();
		values.put("callDateTime", Long.parseLong(insertValues.get("callDateTime")));
		values.put("dialNumber", insertValues.get("dialNumber"));
		
		values.put("adId", insertValues.get("adId"));
		database.insert("calls", null, values);
		database.close();
	}

	public void insertAd(String adId,String companyName,String mediaDescription,String mediaLocation) {
		Map<String,String> map=new HashMap<String,String>();
		map.put("adId", adId);
		map.put("companyName", companyName);
		map.put("mediaDescription", mediaDescription);
		map.put("mediaLocation", mediaLocation);
		insertAd(map);
	}
	
	public void insertAd(Map<String,String> insertValues) {
		SQLiteDatabase database=this.getWritableDatabase();
		String deleteQuery="Delete FROM ad where adId='"+insertValues.get("adId")+"'";
		database.execSQL(deleteQuery);		
		ContentValues values=new ContentValues();
		values.put("adId", insertValues.get("adId"));
		values.put("companyName", insertValues.get("companyName"));
		values.put("mediaDescription", insertValues.get("mediaDescription"));
		values.put("mediaLocation", insertValues.get("mediaLocation"));
		database.insert("Ad", null, values);
		database.close();
	}

	public void deleteAd(String adId) {
		SQLiteDatabase database=this.getWritableDatabase();
		String deleteQuery="Delete FROM ad where adId='"+adId+"'";
		database.execSQL(deleteQuery);
		database.close();
	}
	
	public void deleteCompany(String companyName) {
		SQLiteDatabase database=this.getWritableDatabase();
		String deleteQuery="Delete FROM ad where companyName='"+companyName+"'";
		database.execSQL(deleteQuery);
		database.close();
	}
	
	
	public void deleteCall(String callDateTimeSystem) {
		SQLiteDatabase database=this.getWritableDatabase();
		String deleteQuery="Delete FROM calls where callDateTime='"+callDateTimeSystem+"'";
		database.execSQL(deleteQuery);
		database.close();
	}
	
	public void deleteAllCalls() {
		SQLiteDatabase database=this.getWritableDatabase();
		String deleteQuery="Delete FROM calls";
		database.execSQL(deleteQuery);
		database.close();
	}
	
	
	public List<Map<String,String>> getAllCalls() {
		List<Map<String,String>> calls=new ArrayList<Map<String,String>>();
		String selectQuery="Select * from calls order by callDateTime DESC";
		SQLiteDatabase database=this.getWritableDatabase();
		
		Cursor cursor=database.rawQuery(selectQuery, null);
	
		if(cursor.moveToFirst()) {
			do {
				Map<String,String> call= new HashMap<String,String>();
				
				
				Calendar today=Calendar.getInstance();
				today.clear(Calendar.HOUR);
				today.clear(Calendar.MINUTE);
				today.clear(Calendar.SECOND);
				today.clear(Calendar.MILLISECOND);
				
				long longValue = Long.parseLong(cursor.getString(0));
				
				call.put("callDateTimeSystem", cursor.getString(0));

				if(longValue>=today.getTimeInMillis()) {
					call.put("callDateTime", "Today");
				} else {
					call.put("callDateTime", new Date(Long.parseLong(cursor.getString(0))).toString());	
				}
				call.put("dialNumber", cursor.getString(1).toString());					
				call.put("adId", cursor.getString(2));
				calls.add(call);
			} while(cursor.moveToNext());
		}
		database.close();
		return calls;
	}
	
	public List<Map<String,String>> getAllCompanies() {
		List<Map<String,String>> companies=new ArrayList<Map<String,String>>();
		String selectQuery="Select companyName from ad group by companyName order by companyName";
		SQLiteDatabase database=this.getWritableDatabase();
		
		Cursor cursor=database.rawQuery(selectQuery, null);
	
		if(cursor.moveToFirst()) {
			do {
				Map<String,String> company= new HashMap<String,String>();
				company.put("companyName", cursor.getString(0));
				companies.add(company);
			} while(cursor.moveToNext());
		}
		database.close();
		return companies;
	}	

	
	public List<Map<String,String>> getAds(String companyName) {
		List<Map<String,String>> ads=new ArrayList<Map<String,String>>();
		String selectQuery="Select * from ad where ad.companyName='"+companyName+"'";
		SQLiteDatabase database=this.getWritableDatabase();
		
		Cursor cursor=database.rawQuery(selectQuery, null);
	
		if(cursor.moveToFirst()) {
			do {
				Map<String,String> ad= new HashMap<String,String>();
				ad.put("adId",cursor.getString(0));
				ad.put("companyName", cursor.getString(1));
				ad.put("mediaDescription",cursor.getString(2));
				ad.put("mediaLocation",cursor.getString(3));
				ads.add(ad);
			} while(cursor.moveToNext());
		}
		database.close();
		Collections.reverse(ads);
		return ads;
	}	
	
}
