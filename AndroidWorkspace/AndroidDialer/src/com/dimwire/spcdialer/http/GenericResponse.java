package com.dimwire.spcdialer.http;

public class GenericResponse {
	private final String statusString;
	private final int statusCode;
	private final String responseString;

	public GenericResponse(String statusString,int statusCode,String responseString){
		this.statusString=statusString;
		this.statusCode=statusCode;
		this.responseString=responseString;
	}
	
	public String getStatusString() {
		return statusString;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public String getResponseString() {
		return responseString;
	}
}
