package com.dimwire.spcdialer.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;

import android.os.AsyncTask;

public abstract class GenericHttpTask extends AsyncTask<String, String, GenericResponse> {
	protected final String urlString;
	protected HttpClient client = HttpClientUtil.getNewHttpClient();
	public GenericHttpTask(String urlString) {
		if (urlString == null)
			throw new IllegalArgumentException("Null Url String");

		this.urlString = urlString;
	}
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
	}
	protected GenericResponse createGenericResponse(HttpResponse response) throws IOException {
		StringBuffer buffer = new StringBuffer();
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
			buffer.append(line);
		}
		GenericResponse result=new GenericResponse(response.getStatusLine()
				.getReasonPhrase(),response.getStatusLine()
						.getStatusCode(),buffer.toString());
		rd.close();
		return result;
	}
	
}