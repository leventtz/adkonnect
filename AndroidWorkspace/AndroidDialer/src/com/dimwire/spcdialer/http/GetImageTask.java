package com.dimwire.spcdialer.http;

import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

public class GetImageTask extends AsyncTask<String, String, Bitmap> {
	protected final String urlString;
	protected HttpClient client = HttpClientUtil.getNewHttpClient();

	public GetImageTask(String urlString) {
		if (urlString == null)
			throw new IllegalArgumentException("Null Url String");

		this.urlString = urlString;
	}

	@Override
	public Bitmap doInBackground(String... params) {
        Bitmap bitmap = null;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inSampleSize = 1;
        try {
        HttpResponse response = client.execute(new HttpGet(urlString));
        InputStream stream = response.getEntity().getContent();
            bitmap = BitmapFactory.
                    decodeStream(stream, null, bmOptions);
            stream.close();
        }catch(Exception e){
        	Log.e("GetImageTask",e.getMessage(),e);
        } 
        return bitmap;
		
	}
}