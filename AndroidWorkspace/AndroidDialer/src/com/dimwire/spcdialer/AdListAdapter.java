package com.dimwire.spcdialer;

import java.util.Map;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdListAdapter extends ArrayAdapter<Map<String,String>> {
	  private final Context context;
	  private final Map<String,String>[] values;

	  public AdListAdapter(Context context, Map<String,String>[] values) {
	    super(context, R.layout.activity_ads,values);
	    this.context = context;
	    this.values = values;
	  }
 
	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.ad_entry, parent, false);
	    final TextView textCompanyName = (TextView) rowView.findViewById(R.id.textCompanyName);
	    
	    
	    if(values.length!=0) {
		    Map<String,String> s = values[position];
		    textCompanyName.setText(s.get("companyName"));
		    if(s.get("deleted")!=null) {
		    	textCompanyName.setPaintFlags(textCompanyName.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);		    	
		    }
	    

	    } else {
	    	textCompanyName.setText(R.string.empty);
	    }
	    
	    
//		try {
//			URL newurl = new URL(s.get("mediaLocation"));
//			Bitmap logo = BitmapFactory.decodeStream(newurl.openConnection() .getInputStream());
//			webView.setImageBitmap(logo);
//		} catch (MalformedURLException e) {
//
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} 
	    
	    

	    return rowView;
	  }
	  

	  
	  
	} 