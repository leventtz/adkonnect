'use strict';

angular.module('spcAdminAccountControllers', [])
.controller('AccountsCtrl', ['$scope', 'SpcService', 'BuildServiceCallJson', '$modal','$routeParams',
    function ($scope, $spcService, $buildServiceCallJson, $modal,$routeParams) {
	    $spcService.save({serviceName: 'ListUnApprovedAd'}, $buildServiceCallJson('ListUnApprovedAd', {}), function (data) {
	        $scope.ads = new Array();
	        for (var i = 0; i < data.list.length; i++) {
	            $scope.ads.push(angular.fromJson(data.list[i]))
	        }
	    });

	
	$scope.toggleApprove=function(ad) {
		var contractValues={};
		contractValues.accountId=ad.accountId;
		contractValues.adId=ad.adId;
		contractValues.approve=!ad.approved;
	    $spcService.save({serviceName: 'TogggleApprove'}, $buildServiceCallJson('TogggleApprove', contractValues), function (data) {
	    	ad.approved=!ad.approved;
	    });
	}

	$scope.searchAccount=function(email) {
		var locationHost = "http://"+location.hostname+":9001";
		window.open(locationHost+"/acc/"+email,"_parent");
	}
	$scope.sendApprovedEmail=function(ad) {
		var modalInstance = $modal.open({
			templateUrl: 'partials/confirmationDialog',
			controller: 'OkCancelCtrl'
		});

		modalInstance.result.then(function () {
			var contractValues={};
			contractValues.accountId=ad.accountId;
			$spcService.save({serviceName: 'SendApprovedEmail'}, $buildServiceCallJson('SendApprovedEmail', contractValues), function (data) {
			});	
		}, function () {
			//cancelled
		});	
	}

	
	$scope.sendDeniedEmail=function(ad) {
		var modalInstance = $modal.open({
			templateUrl: 'partials/confirmationDialog',
			controller: 'OkCancelCtrl'
		});

		modalInstance.result.then(function () {
			var contractValues={};
			contractValues.accountId=ad.accountId;
			$spcService.save({serviceName: 'SendDeniedEmail'}, $buildServiceCallJson('SendDeniedEmail', contractValues), function (data) {
			});	
		}, function () {
			//cancelled
		});	
	}	
	
    $scope.editDesign = function (ad) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/editDesignDialog',
            controller: 'DesignCtrl',
            resolve: {
            	description: function () {
                  return ad.description;
                }
              }            
        });
        modalInstance.result.then(function (description) {
			var contractValues={};
			contractValues.accountId=ad.accountId;
			contractValues.adId=ad.adId;
			contractValues.description=description;
			console.log(contractValues)
			$spcService.save({serviceName: 'UpdateDesign'}, $buildServiceCallJson('UpdateDesign', contractValues), function (data) {
				ad.description=description;
			});				  
        }, function () {
            //cancelled
        });
    };   	
	
}
]);


var DesignCtrl = function ($modalInstance, $scope,$http,description) {

	$scope.data={};
	$scope.data.description=description;
	
    $scope.ok = function () {
        $modalInstance.close($scope.data.description);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };	
	
}
