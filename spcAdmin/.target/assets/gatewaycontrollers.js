'use strict';



angular.module('spcAdminGatewayControllers', [])
.controller('GatewayCtrl', ['$scope', 'SpcService', 'BuildServiceCallJson', '$modal','$routeParams',
    function ($scope, $spcService, $buildServiceCallJson, $modal,$routeParams) {
	
	
	$scope.providers = new Array();
	$spcService.save({	serviceName: 'ListActiveProviders'}, $buildServiceCallJson('ListActiveProviders', {}), function (data) {
		for (var i = 0; i < data.list.length; i++) {
			$scope.providers.push(angular.fromJson(data.list[i]))
		}
		listGateways();
	});	 
	
	
	function listGateways() {
	    $spcService.save({serviceName: 'ListGateways'}, $buildServiceCallJson('ListGateways', {}), function (data) {
	        $scope.gateways = new Array();
	        for (var i = 0; i < data.list.length; i++) {
	        	var gateway=angular.fromJson(data.list[i]);
	        	for(var j=0;j<$scope.providers.length;j++) {
	        		if(gateway.providerId==$scope.providers[j].id) {
	        			gateway.providerName=$scope.providers[j].companyName;
	        			break;
	        		}
	        	}
	            $scope.gateways.push(gateway)
	        }
	    });
	}
	
	
	
    $scope.toogleGateway=function(gateway) {
        var contractValues = {}
        contractValues.id = gateway.id;
        $spcService.save({serviceName: 'ToggleGateway'}, $buildServiceCallJson('ToggleGateway', contractValues), function (data) {
        	gateway.active=!gateway.active;
        });
    	
    }
    
    

    $scope.createGateway = function () {
        var modalInstance = $modal.open({
            templateUrl: 'partials/gatewayDialog',
            controller: 'GatewayDialogCtrl',
            resolve: {
            	data: function () {
                  return {gateway:{},providers:$scope.providers};
                }
              }            
        });

        modalInstance.result.then(function (data) {
			  var contractValues={}
			  contractValues.ip=data.gateway.ip;
        	  contractValues.name=data.gateway.name;
        	  contractValues.did=data.gateway.did;
        	  contractValues.providerId=data.gateway.provider.id;
				  $spcService.save({serviceName:'CreateGateway'},$buildServiceCallJson('CreateGateway',contractValues),function(data) {
					  listGateways();				  
		          });			  
        }, function () {
            //cancelled
        });
    };    

    
    $scope.addDID = function (thatGateway) {
    	var gateway=angular.copy(thatGateway)
    	gateway.did=""
        var modalInstance = $modal.open({
            templateUrl: 'partials/gatewayDialog',
            controller: 'GatewayDialogCtrl',
            resolve: {
            	data: function () {
                  return {gateway:gateway,providers:$scope.providers};
                }
              }            
        });

        modalInstance.result.then(function (data) {
			  var contractValues={}
			  contractValues.ip=data.gateway.ip;
        	  contractValues.name=data.gateway.name;
        	  contractValues.did=data.gateway.did;
        	  contractValues.providerId=data.gateway.provider.id;
				  $spcService.save({serviceName:'CreateGateway'},$buildServiceCallJson('CreateGateway',contractValues),function(data) {
					  listGateways();				  
		          });			  
        }, function () {
            //cancelled
        });
    };    
   
    
    
    $scope.updateGateway = function (gateway) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/gatewayDialog',
            controller: 'GatewayDialogCtrl',
            resolve: {
            	data: function () {
            	  return {gateway:angular.copy(gateway),providers:$scope.providers};
                }
              }            
        });

        modalInstance.result.then(function (data) {
			  var contractValues={}
			  contractValues.id=data.id;
			  contractValues.ip=data.gateway.ip;
        	  contractValues.name=data.gateway.name;
        	  contractValues.did=data.gateway.did;
        	  contractValues.providerId=data.gateway.providerId;
        	  contractValues.active=gateway.active;
				  $spcService.save({serviceName:'UpdateGateway'},$buildServiceCallJson('UpdateGateway',contractValues),function(data) {
					  listGateways();				  
		          });			  
        }, function () {
            //cancelled
        });
    };      

    
    
  
    
    
    $scope.deleteGateway = function (gateway) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/deleteConfirmationDialog',
            controller: 'OkCancelCtrl'
        });

        modalInstance.result.then(function () {
            var contractValues = {}
            contractValues.id = gateway.id;
            $spcService.save({
                serviceName: 'DeleteGateway'
            }, $buildServiceCallJson('DeleteGateway', contractValues), function (data) {
				  listGateways();
            });
        }, function () {
            //cancelled
        });
    };

    
    $scope.resetHitCount = function (gateway) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/confirmationDialog',
            controller: 'OkCancelCtrl'
        });

        modalInstance.result.then(function () {
            var contractValues = {}
            contractValues.id = gateway.id;
            $spcService.save({
                serviceName: 'ResetGateway'
            }, $buildServiceCallJson('ResetGateway', contractValues), function (data) {
				  listGateways();
            });
        }, function () {
            //cancelled
        });
    }; 
    
    
	function listProviderGateways() {
	    $spcService.save({serviceName: 'ListProviderGateways'}, $buildServiceCallJson('ListProviderGateways', {}), function (data) {
	        $scope.providergateways = new Array();
	        for (var i = 0; i < data.list.length; i++) {
	            $scope.providergateways.push(angular.fromJson(data.list[i]))
	        }
	    });
		
	}    

	function listAllProviders() {
	    $spcService.save({serviceName: 'ListAllProviders'}, $buildServiceCallJson('ListAllProviders', {}), function (data) {
	        $scope.allproviders = new Array();
	        for (var i = 0; i < data.list.length; i++) {
	        	var provider=angular.fromJson(data.list[i]);
	        	provider.contactName=provider.firstName+" "+provider.lastName;
	            $scope.allproviders.push(provider)
	        }
	    });
		
	}  	
	
	
    $scope.addProvider = function () {
        var modalInstance = $modal.open({
            templateUrl: 'partials/accountDialog',
            controller: 'AccountCtrl'
        });

        modalInstance.result.then(function (data) {
			  var contractValues={}
			  contractValues.companyName=data.companyName;
			  contractValues.firstName=data.firstName;
			  contractValues.middleName=data.middleName===undefined?'':data.middleName;
			  contractValues.lastName=data.lastName;
			  contractValues.address1=data.address1;
			  contractValues.address2=data.address2===undefined?'':data.address2;
			  contractValues.city=data.city;
			  contractValues.state=data.state;
			  contractValues.postalCode=data.postalCode;
			  contractValues.country=data.country;
			  contractValues.email=data.email;
			  contractValues.phone1=data.phone1;
			  contractValues.phone2=data.phone2===undefined?'':data.phone2;

        	  contractValues.name=data.name;
				  $spcService.save({serviceName:'AddProvider'},$buildServiceCallJson('AddProvider',contractValues),function(data) {
					  listAllProviders();				  
		          },function(data) {
		        	  alert(data.data);
		          });			  
        }, function () {
        	
        });
    }; 	
	
    
    
    $scope.updateProvider = function (provider) {
    	$routeParams.provider=provider;
        var modalInstance = $modal.open({
            templateUrl: 'partials/accountDialog',
            controller: 'AccountCtrl'
        });

        modalInstance.result.then(function (data) {
			  var contractValues={}
			  contractValues.id=provider.id;
			  contractValues.companyName=data.companyName;
			  contractValues.firstName=data.firstName;
			  contractValues.middleName=data.middleName===undefined?'':data.middleName;
			  contractValues.lastName=data.lastName;
			  contractValues.address1=data.address1;
			  contractValues.address2=data.address2===undefined?'':data.address2;
			  contractValues.city=data.city;
			  contractValues.state=data.state;
			  contractValues.postalCode=data.postalCode;
			  contractValues.country=data.country;
			  contractValues.email=data.email;
			  contractValues.phone1=data.phone1;
			  contractValues.phone2=data.phone2===undefined?'':data.phone2;
			  contractValues.active=provider.active;
        	  contractValues.name=data.name;
				  $spcService.save({serviceName:'UpdateProvider'},$buildServiceCallJson('UpdateProvider',contractValues),function(data) {
					  listAllProviders();				  
		          },function(data) {
		        	  alert(data.data);
		          });			  
        }, function () {
        	
        });
    };     
    
    
    $scope.deleteProvider = function (provider) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/deleteConfirmationDialog',
            controller: 'OkCancelCtrl'
        });

        modalInstance.result.then(function () {
            var contractValues = {}
            contractValues.id = provider.id;
            $spcService.save({
                serviceName: 'DeleteProvider'
            }, $buildServiceCallJson('DeleteProvider', contractValues), function (data) {
            	listAllProviders();
            },function(data) {
            	 alert(data.data);
            });
        }, function () {
        	
        });
    };     
    
	
    $scope.toogleProvider=function(provider) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/confirmationDialog',
            controller: 'OkCancelCtrl'
        });

        modalInstance.result.then(function () {
            var contractValues = {}
            contractValues.id = provider.id;
            $spcService.save({serviceName: 'ToggleProvider'}, $buildServiceCallJson('ToggleProvider', contractValues), function (data) {
            	listAllProviders();
            });
        }, function () {
            //cancelled
        });	    	
    }	
	
	
	$scope.providerGatewayClicked=function() {
		listProviderGateways();
	}

	$scope.providersClicked=function() {
		listAllProviders();	
	}	
	
	
	
	
	
    $scope.createProviderGateway = function () {
        var modalInstance = $modal.open({
            templateUrl: 'partials/providerGateway',
            controller: 'ProviderGatewayDialogCtrl'          
        });

        modalInstance.result.then(function (data) {
			  var contractValues={}
			  contractValues.endpointip=data.endpointip;
			  contractValues.providerid=data.provider.id;
			  contractValues.prefix=data.prefix==undefined?'':data.prefix;
			  contractValues.username=data.username==undefined?'':data.username;
			  contractValues.password=data.password==undefined?'':data.password; 
			  contractValues.active=false
				  $spcService.save({serviceName:'CreateProviderGateway'},$buildServiceCallJson('CreateProviderGateway',contractValues),function(data) {
					  listProviderGateways();				  
		          });			  
        }, function () {
            //cancelled
        });
    };    	
	
    
    $scope.updateProviderGateway = function (providerGateway) {
    	$routeParams.providerGateway=providerGateway;
        var modalInstance = $modal.open({
            templateUrl: 'partials/providerGateway',
            controller: 'UpdateProviderGatewayDialogCtrl'          
        });

        modalInstance.result.then(function (data) {
			  var contractValues={}
			  contractValues.id=providerGateway.id;
			  contractValues.endpointip=data.endpointip;
			  contractValues.providerid=data.provider.id;
			  contractValues.prefix=data.prefix==undefined?'':data.prefix;
			  contractValues.username=data.username==undefined?'':data.username;
			  contractValues.password=data.password==undefined?'':data.password; 
			  contractValues.active=false
				  $spcService.save({serviceName:'UpdateProviderGateway'},$buildServiceCallJson('UpdateProviderGateway',contractValues),function(data) {
					  listProviderGateways();				  
		          });			  
        }, function () {
            //cancelled
        });
    };      
    
    
    $scope.deleteProviderGateway = function (providergateway) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/deleteConfirmationDialog',
            controller: 'OkCancelCtrl'
        });

        modalInstance.result.then(function () {
            var contractValues = {}
            contractValues.id = providergateway.id;
            $spcService.save({
                serviceName: 'DeleteProviderGateway'
            }, $buildServiceCallJson('DeleteProviderGateway', contractValues), function (data) {
            	listProviderGateways();
            });
        }, function () {
            //cancelled
        });
    };    
    
    $scope.toogleProviderGateway=function(providergateway) {
    	
        var modalInstance = $modal.open({
            templateUrl: 'partials/confirmationDialog',
            controller: 'OkCancelCtrl'
        });

        modalInstance.result.then(function () {
            var contractValues = {}
            contractValues.id = providergateway.id;
            $spcService.save({serviceName: 'ToggleProviderGateway'}, $buildServiceCallJson('ToggleProviderGateway', contractValues), function (data) {
            	listProviderGateways();
            });
        }, function () {
            //cancelled
        });	    	
    	
    }
    
   
    
 }

]);

var GatewayDialogCtrl = function ($modalInstance, $scope,data) {
		$scope.data=data; 
        $scope.ok = function () {
            $modalInstance.close($scope.data);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }

var ProviderGatewayDialogCtrl = function ($modalInstance, $scope,$http) {
	$scope.providers = new Array();
	var postData = {serviceName:'ListActiveProviders',contractValues:angular.toJson({})}
	
	$http.post("service/ListActiveProviders",postData).success(function(data){
		for (var i = 0; i < data.list.length; i++) {
			$scope.providers.push(angular.fromJson(data.list[i]))
		}
		
	})
	
	$scope.data={}; 
    $scope.ok = function () {
        $modalInstance.close($scope.data);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

var UpdateProviderGatewayDialogCtrl = function ($modalInstance, $scope,$http,$routeParams) {
	$scope.data={};
	$scope.data.id=$routeParams.providerGateway.id;
	$scope.data.endpointip=$routeParams.providerGateway.endpointip;
	$scope.data.providerid=$routeParams.providerGateway.providerid;
	$scope.data.prefix=$routeParams.providerGateway.prefix;
	$scope.data.username=$routeParams.providerGateway.username;
	$scope.data.password=$routeParams.providerGateway.password;
	
	
	$scope.providers = new Array();
	var postData = {serviceName:'ListActiveProviders',contractValues:angular.toJson({})}
	$http.post("service/ListActiveProviders",postData).success(function(data){
		for (var i = 0; i < data.list.length; i++) {
			var provider=angular.fromJson(data.list[i]);
			$scope.providers.push(provider);
			if($scope.data.providerId=provider.id) {
				$scope.data.provider=provider;
			}
		}
		
	})

	
    $scope.ok = function () {
        $modalInstance.close($scope.data);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

var AccountCtrl = function ($modalInstance, $scope,$http,$routeParams) {
	if($routeParams.provider===undefined) {
		$scope.data={};
	} else {
		$scope.data=angular.copy($routeParams.provider);
	}
	
    $scope.ok = function () {
        $modalInstance.close($scope.data);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

