'use strict';

/* Services */

var spcServices = angular.module('spcAdminServices', [ 'ngResource' ]);

spcServices.factory('SpcService', [ '$resource', function($resource) {
	return $resource('/service/:serviceName', {serviceName : '@serviceName'});
}]);

spcServices.factory('BuildServiceCallJson', [ function() {
	return function(serviceName,contractValues) {
		  var serviceCall={};
		  serviceCall.serviceName=serviceName;
		  serviceCall.contractValues=angular.toJson(contractValues);	
		  return  angular.toJson(serviceCall)
	};
}]);

	
