'use strict';



angular.module('spcAdminTemplateControllers', [])
.controller('TemplateCtrl', ['$scope', 'SpcService', 'BuildServiceCallJson', '$modal','$routeParams',
    function ($scope, $spcService, $buildServiceCallJson, $modal,$routeParams) {
	function listTemplates() {
	    $spcService.save({serviceName: 'ListAllTemplates'}, $buildServiceCallJson('ListAllTemplates', {}), function (data) {
	        $scope.templates = new Array();
	        for (var i = 0; i < data.list.length; i++) {
	            $scope.templates.push(angular.fromJson(data.list[i]))
	        }
	    });
		
	}
    
	listTemplates();
	
	$scope.deleteMedia=function(media) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/confirmationDialog',
            controller: 'OkCancelCtrl'
        });
        
        modalInstance.result.then(function () {
    		var contractValues={}
    		contractValues.id=media.id
    	    $spcService.save({serviceName: 'DeleteMedia'}, $buildServiceCallJson('DeleteMedia', contractValues), function (data) {
    	    	var index=$scope.mediaList.indexOf(media)
    	    	if(index>-1) {
    	    		$scope.mediaList.splice(index,1);
    	    	}
    	    });
        }, function () {
            //cancelled
        });	        
    }

	
	$scope.createTemplate=function() {
        var modalInstance = $modal.open({
            templateUrl: 'partials/templateDialog',
            controller: 'EditTemplateCtrl'
        });
        $routeParams.template=undefined;
        modalInstance.result.then(function (data) {
    		var contractValues={}
    		contractValues.name=data.name;
    		contractValues.description=data.description;
    		contractValues.mediaId=data.media.id;
    		contractValues.maxDuration=data.maxDuration;
    		contractValues.templateDefaultQty=data.templateDefaultQty;
    		contractValues.templateUnitPrice=data.templateUnitPrice;
    		contractValues.templatePromptCost=data.templatePromptCost;
    		contractValues.byPassSwitch=data.byPassSwitch===undefined?false:data.byPassSwitch;
    		
    	    $spcService.save({serviceName: 'CreateTemplate'}, $buildServiceCallJson('CreateTemplate', contractValues), function (data) {
    	    	listTemplates();
    	    });
        }, function () {
            //cancelled
        });	        
    }	

	
	$scope.updateTemplate=function(template) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/templateDialog',
            controller: 'EditTemplateCtrl'
        });
        
        $routeParams.template=angular.copy(template);
        
        modalInstance.result.then(function (data) {
    		var contractValues={}
    		contractValues.id=template.id;
    		contractValues.name=data.name;
    		contractValues.description=data.description;
    		contractValues.mediaId=data.media.id;
    		contractValues.maxDuration=data.maxDuration;
    		contractValues.templateDefaultQty=data.templateDefaultQty;
    		contractValues.templateUnitPrice=data.templateUnitPrice;
    		contractValues.templatePromptCost=data.templatePromptCost;
    		contractValues.byPassSwitch=data.byPassSwitch===undefined?false:data.byPassSwitch;
    		
    	    $spcService.save({serviceName: 'UpdateTemplate'}, $buildServiceCallJson('UpdateTemplate', contractValues), function (data) {
    	    	listTemplates();
    	    });
        }, function () {
            //cancelled
        });	        
    }		
	

	$scope.accountMediaClicked=function() {
		$spcService.save({serviceName: 'ListAccountMedia'}, $buildServiceCallJson('ListAccountMedia', {}), function (data) {
	        $scope.mediaList = new Array();
	        for (var i = 0; i < data.list.length; i++) {
	            $scope.mediaList.push(angular.fromJson(data.list[i]))
	        }
	    });	
		}	
	
	$scope.toggleTemplate=function(template) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/confirmationDialog',
            controller: 'OkCancelCtrl'
        });

        modalInstance.result.then(function () {
    		var contractValues={}
    		contractValues.id=template.id
    	    $spcService.save({serviceName: 'ToogleTemplate'}, $buildServiceCallJson('ToogleTemplate', contractValues), function (data) {
    	    	template.active=!template.active;
    	    });
        }, function () {
            //cancelled
        });		
	}
	
	
	$scope.toggleBypassSwitch=function(template) {
		
        var modalInstance = $modal.open({
            templateUrl: 'partials/confirmationDialog',
            controller: 'OkCancelCtrl'
        });

        modalInstance.result.then(function () {
    		var contractValues={}
    		contractValues.id=template.id
    	    $spcService.save({serviceName: 'ToggleBypassSwitch'}, $buildServiceCallJson('ToggleBypassSwitch', contractValues), function (data) {
    	    	template.bypassSwitch=!template.bypassSwitch;
    	    });
        }, function () {
            //cancelled
        });		
		
		

	}
	
	$scope.deleteTemplate=function(template) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/deleteConfirmationDialog',
            controller: 'OkCancelCtrl'
        });

        modalInstance.result.then(function () {
    		var contractValues={}
    		contractValues.id=template.id
    	    $spcService.save({serviceName: 'DeleteTemplate'}, $buildServiceCallJson('DeleteTemplate', contractValues), function (data) {
    	    	var index=$scope.templates.indexOf(template)
    	    	if(index>-1) {
    	    		$scope.templates.splice(index,1);
    	    	}
    	    });        	
        }, function () {
        });		
	}
	
	
    $scope.updateTemplateDA = function (template) {
    	$routeParams.adId=template.id;
        var modalInstance = $modal.open({
            templateUrl: 'partials/templateDADialog',
            controller: 'TemplateDACtrl'
        });
    };    

    
    $scope.uploadMedia = function () {
        var modalInstance = $modal.open({
            templateUrl: 'partials/uploadMediaDialog',
            controller: 'UploadMediaCtrl'
        });
        
        modalInstance.result.then(function () {
    		var contractValues={}
    		contractValues.id=template.id
    	    $spcService.save({serviceName: 'DeleteTemplate'}, $buildServiceCallJson('DeleteTemplate', contractValues), function (data) {
    	    	var index=templates.indexOf(template)
    	    	if(index>-1) {
    	    		templates.splice(index,1);
    	    	}
    	    });
        }, function () {
            //cancelled
        });		        
        
    };    
    
    
    
}])

.controller('TemplateDACtrl', ['$scope', 'SpcService','BuildServiceCallJson','$modalInstance','$routeParams','$modal',
                                function ($scope, $spcService,$buildServiceCallJson,$modalInstance,$routeParams,$modal) {
	var contractValues={};
	contractValues.adId=$routeParams.adId;
	$scope.data={selectedCountry:{},selectedDestination:{}};
	$spcService.save({serviceName: 'ListTemplateDAWithDestination'}, $buildServiceCallJson('ListTemplateDAWithDestination', contractValues), function (data) {
        $scope.das = new Array();
        for (var i = 0; i < data.list.length; i++) {
            $scope.das.push(angular.fromJson(data.list[i]))
        }
    });

	

	
	$scope.data={}
	$scope.data.selectedDestination=null;
    $scope.countryChanged = function () {
        var contractValues = {}
        contractValues.countryId = $scope.data.selectedCountry.id;
        $spcService.save({serviceName: 'RetrieveDestinations'}, $buildServiceCallJson('RetrieveDestinations', contractValues), function (data) {
            $scope.destinations = new Array();
            for (var i = 0; i < data.list.length; i++) {
            	
                $scope.destinations.push(angular.fromJson(data.list[i]))
            }
        });

        
    }	
    
    
    $scope.addAdDa=function() {
    	var contractValues={}
    	contractValues.adId=$routeParams.adId;
    	contractValues.destinationId=$scope.data.selectedDestination.id;
    	contractValues.rate=0;
    	contractValues.exclude=false;
        $spcService.save({serviceName: 'AddTemplateDa'}, $buildServiceCallJson('AddTemplateDa', contractValues), function (data) {
        	var da={};
        	da.destinationId=$scope.data.selectedDestination.id;
        	da.destinationName=$scope.data.selectedDestination.name;
        	da.adId=$routeParams.adId;
        	da.rate=0;
        	da.exclude=false;
        	$scope.das.splice(0, 0, da);
        });
    };
    
    $spcService.save({serviceName: 'RetrieveCountries'}, $buildServiceCallJson('RetrieveCountries', {}), function (data) {
        $scope.countries = new Array();
        for (var i = 0; i < data.list.length; i++) {
            $scope.countries.push(angular.fromJson(data.list[i]))
        }
    });		
	
    
	$scope.removeAdDa=function(da) {
        var modalInstance = $modal.open({
            templateUrl: 'partials/deleteConfirmationDialog',
            controller: 'OkCancelCtrl'
        });

        modalInstance.result.then(function () {
    		var contractValues={}
    		contractValues.adId=$routeParams.adId;
    		contractValues.destinationId=da.destinationId;
    	    $spcService.save({serviceName: 'RemoveTemplateDa'}, $buildServiceCallJson('RemoveTemplateDa', contractValues), function (data) {
    	    	var index=$scope.das.indexOf(da)
    	    	if(index>-1) {
    	    		$scope.das.splice(index,1);
    	    	}
    	    });
        }, function () {
            //cancelled
        });		
	}    
    
    

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}
])

.controller('EditTemplateCtrl', ['$scope', 'SpcService','BuildServiceCallJson','$modalInstance','$routeParams','$modal',
                                function ($scope, $spcService,$buildServiceCallJson,$modalInstance,$routeParams,$modal) {
	$scope.data=$routeParams.template===undefined?{}:$routeParams.template;
	$scope.mediaList=new Array();
	$spcService.save({serviceName: 'ListAccountMedia'}, $buildServiceCallJson('ListAccountMedia', {}), function (data) {
		for (var i = 0; i < data.list.length; i++) {
			var media=angular.fromJson(data.list[i]);
			$scope.mediaList.push(media)
			if(media.mediaLocation==$scope.data.mediaLocation) {
				$scope.data.media=media;
			}
		}
		
	});
	
        $scope.save = function () {
            $modalInstance.close($scope.data);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
}
])

var UploadMediaCtrl=function($scope,$modalInstance) {
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}

