'use strict';



angular.module('spcAdminDashboardController', [])
.controller('DashboardCtrl', ['$scope', 'SpcService', 'BuildServiceCallJson', '$modal',
    function ($scope, $spcService, $buildServiceCallJson, $modal) {
	$( "#startDate" ).datepicker(); 
	$( "#endDate" ).datepicker();

    $scope.startDate = "";
    $scope.endDate = "";
	$scope.selectedCountry={id:0,name:''};
	$scope.selectedDestination={id:0,name:''};
	$scope.selectedProvider={id:"",companyName:''};
	
    $spcService.save({serviceName: 'RetrieveCountries'}, $buildServiceCallJson('RetrieveCountries', {}), function (data) {
        $scope.countries = new Array();
        $scope.countries.push($scope.selectedCountry);
        for (var i = 0; i < data.list.length; i++) {
            $scope.countries.push(angular.fromJson(data.list[i]))
        }
    });
    
    
    $scope.countryChanged = function () {
    	
    	$scope.destinations = new Array();
    	$scope.selectedDestination={id:0,name:''};
    	if($scope.selectedCountry.id==0) {
    		return;
    	}
        var contractValues = {}
        contractValues.countryId = $scope.selectedCountry.id;
        $spcService.save({serviceName: 'RetrieveDestinations'}, $buildServiceCallJson('RetrieveDestinations', contractValues), function (data) {
            for (var i = 0; i < data.list.length; i++) {
                $scope.destinations.push(angular.fromJson(data.list[i]))
            }
        });
        
    }    
	
	$spcService.save({	serviceName: 'ListActiveProviders'}, $buildServiceCallJson('ListActiveProviders', {}), function (data) {
		$scope.providers = new Array();
		$scope.providers.push($scope.selectedProvider);
		for (var i = 0; i < data.list.length; i++) {
			$scope.providers.push(angular.fromJson(data.list[i]))
		}
	});	

	



	
	$scope.retrieveCDRSummary=function(reportName) {
        var contractValues = {}
        contractValues.startDate=$scope.startDate;
        contractValues.endDate=$scope.endDate;
        contractValues.countryId=$scope.selectedCountry.id;
        contractValues.destinationId=$scope.selectedDestination.id;
        contractValues.providerId=$scope.selectedProvider.id;
        contractValues.countryId = $scope.selectedCountry.id;
        contractValues.reportName=reportName;
        $scope.total=0;
        $scope.providerTotal=0
		$spcService.save({	serviceName: 'CDRSummary'}, $buildServiceCallJson('CDRSummary', contractValues), function (data) {
			$scope.metaData=angular.copy(data.metadata);
			$scope.values=angular.copy(data.values);
			
		});	
	}
	
	$scope.retrieveCDRSummary("date");
	
    }
	
]);


