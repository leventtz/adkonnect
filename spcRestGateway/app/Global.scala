import java.util.UUID
import scala.util.Failure
import scala.util.Success
import controllers.Application
import play.api.Application
import play.api.GlobalSettings
import play.api.Logger
import play.api.Play
import play.api.libs.Crypto
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc.Cookie
import play.api.mvc.EssentialAction
import play.api.mvc.EssentialFilter
import play.api.mvc.Filters
import play.api.mvc.Handler
import play.api.mvc.RequestHeader
import com.sponsoracall.db.DynamoDBEngine
import com.sponsoracall.cdr.service.CDRServiceRegistry
import com.sponsoracall.db.PostgresDBEngine


object LoggingFilter extends EssentialFilter {
  def apply(nextFilter: EssentialAction) = new EssentialAction {
    def apply(requestHeader: RequestHeader) = {
      val startTime = System.currentTimeMillis
      nextFilter(requestHeader).map { result =>
        val endTime = System.currentTimeMillis
        val requestTime = endTime - startTime
       	Logger.info(s"Method:${requestHeader.method} URI:${requestHeader.uri} Time:${requestTime}ms Result:${result.header.status}")
        result
      }
    }
  }
}

object Global extends GlobalSettings {

  override def doFilter(next: EssentialAction): EssentialAction = {
    Filters(super.doFilter(next), LoggingFilter)
  }

  override def onStart(app: Application) {
    super.onStart(app)
    Logger.info("Starting...")
    init
    testDynamoDB
  }

  def init {
    System.setProperty("accessKey", Play.current.configuration.getString("accessKey").get)
    System.setProperty("secretKey", Play.current.configuration.getString("secretKey").get)
    System.setProperty("url", Play.current.configuration.getString("url").get)
    System.setProperty("maxActive", Play.current.configuration.getString("maxActive").get)
    System.setProperty("maxIdle", Play.current.configuration.getString("maxIdle").get)
    System.setProperty("minIdle", Play.current.configuration.getString("minIdle").get)
    System.setProperty("dynamoDBEndPoint", Play.current.configuration.getString("dynamoDBEndPoint").get)
    System.setProperty("redisEndPoint", Play.current.configuration.getString("redisEndPoint").get)  
  }

  def testDynamoDB {
    Logger.info("Test DynamoDB " + DynamoDBEngine.client.listTables())
  }

}