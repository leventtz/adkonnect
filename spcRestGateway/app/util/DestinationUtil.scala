package util


object DestinationUtil {
  def normalizeUserPhoneCode(phone: String, countryCode: String): String = {
    val stripPlus=phone.replace("+", "").replace("-", "").replace("(", "").replace(")", "")
    if (stripPlus.startsWith(countryCode)) {
      stripPlus
    } else {
      countryCode + stripPlus
    }
  }  
  
}