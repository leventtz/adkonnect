package util

import controllers.Gateway
import com.sponsoracall.ad.service.AdServiceRegistry
import scala.util.Success
import com.sponsoracall.db.DynamoDBEngine
import scala.util.Failure

object TestGateway extends App {
	val number="sipp"
	val countryCode="1"
	
	
    
    val normalizedNumber = DestinationUtil.normalizeUserPhoneCode(number, countryCode)
    val a=AdServiceRegistry.service.retrieveCachedCallRedis(normalizedNumber) match {
      case Success(data) => {
        //createCDR(data)
        //updateBalance(data)
        //incrementHitCount(data.did)

        val dialString = "1sipp";//data.providerPrefix + data.outNumber;

        val gatewayAddress = "10.214.154.2";//data.providerGatewayAddress

        val fileName = data.ivrmedialocation.substring(data.ivrmedialocation.lastIndexOf("/") + 1).split("\\.")(0);
        if (data.ivrmedialocation == "0") {
          (dialString + "-" + gatewayAddress + "-" + data.maxduration * 60 + "-" + data.accountId + "-" + data.id)
        } else {
          ("302");
        }
      }
      case Failure(t) => t.printStackTrace(); ("-1")
    }
  	
	println(a)
	
}