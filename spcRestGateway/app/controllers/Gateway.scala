package controllers

import java.util.UUID
import scala.concurrent.duration.DurationInt
import scala.util.Success
import com.sponsoracall.ad.data.PickAdData
import com.sponsoracall.cdr.contract.CreateCDRContract
import com.sponsoracall.cdr.contract.UpdateCDRContract
import com.sponsoracall.cdr.contract.UpdateCDRSummaryContractTable
import com.sponsoracall.cdr.service.CDRServiceRegistry
import play.api.Logger
import play.api.Play.current
import play.api.libs.concurrent.Akka
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc.Action
import play.api.mvc.Controller
import util.DestinationUtil
import com.sponsoracall.ad.service.AdServiceRegistry
import scala.util.Failure
import com.sponsoracall.ad.contract.RefundContractV2
import com.sponsoracall.ad.contract.ExcludeAccount
import com.sponsoracall.ad.contract.UpdateUserTallyContractV2
import com.sponsoracall.ad.service.AdServiceRegistry

object Gateway extends Controller {

  def updateCallData(adData: PickAdData) {
    Akka.system.scheduler.scheduleOnce(0.milliseconds) {
      try {
        AdServiceRegistry.service.returnFunds(RefundContractV2(adData.inNumber, None))
        AdServiceRegistry.service.updateBalance(adData.accountId, adData.adId, adData.rate)
        CDRServiceRegistry.service.incrementDIDHitCount(adData.did);
        CDRServiceRegistry.service.createCDR(CreateCDRContract(adData.accountId, adData.id, adData))
      } catch {
        case e: Throwable => Logger.error(e.toString())
      }
    }
  }




  def updateOpenSipsCDR(number: String, countryCode: String, duration: String, reason: String) = Action {
    Akka.system.scheduler.scheduleOnce(0.milliseconds) {
      val normalizedNumber = DestinationUtil.normalizeUserPhoneCode(number, countryCode)
      val durationFinal = {
        try {
          duration.toInt
        } catch {
          case t: Throwable => {
            Logger.error("Can't to Int:" + duration);
            0
          }
        }
      }

      val reasonFinal = if (reason == null) "N" else reason;

      AdServiceRegistry.service.retrieveCachedCallForCDR(normalizedNumber) match {
        case Success(data) => {
          try {
            AdServiceRegistry.service.updateUserTally(UpdateUserTallyContractV2(data.inNumber, data.outNumber, data.accountId, data.adId, duration.toInt));
            CDRServiceRegistry.service.updateCDR(UpdateCDRContract(data.accountId, data.id, durationFinal, durationFinal, reasonFinal))
            CDRServiceRegistry.service.updateCDRSummaryTable(new UpdateCDRSummaryContractTable(data, durationFinal, 0));
            CDRServiceRegistry.service.decrementDIDHitCount(data.did);        
          } catch {
            case e: Throwable => Logger.error(e.toString())
          }
        }
        case _ => //do nothing
      }
    }
    Ok("");
  }


  def callOpenSips(number: String, countryCode: String) = Action { request =>
    val normalizedNumber = DestinationUtil.normalizeUserPhoneCode(number, countryCode)
    AdServiceRegistry.service.retrieveCachedCallRedis(normalizedNumber) match {
      case Success(data) => {

        
        updateCallData(data)
        
        
        
        val dialString = data.providerPrefix + data.outNumber;

        val gatewayAddress = data.providerGatewayAddress

        val fileName = data.ivrmedialocation.substring(data.ivrmedialocation.lastIndexOf("/") + 1).split("\\.")(0);

        if (data.ivrmedialocation == "0") {
          Ok(dialString + "-" + gatewayAddress + "-" + data.maxduration * 60 + "-" + data.accountId + "-" + data.id)
        } else {
          Ok("302");
        }
      }
      case Failure(t) => Logger.error(t.toString()); Ok("-1")
    }
  }

}